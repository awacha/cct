import datetime
import logging
import multiprocessing
import multiprocessing.synchronize
import os
import traceback
from typing import Any, Callable, Dict, Final, Iterator, List, Optional, Tuple

import numpy as np
from taurus.external.qt import QtCore
import openpyxl
import dateutil.parser
import collections


from ...dataclasses import Header
from ...fastfindfile import fastfindfile
from ..settings import FileNameScheme, ProcessingSettings
from ..h5io.nexus_based import ProcessingH5FileNeXus
from ..h5io import utils as h5ioutils
from ..calculations.backgroundprocess import Message
from .task import ProcessingTask

logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)

def _parse_datetime(x):
    return dateutil.parser.parse(_parse_str(x))

def _parse_str(x):
    if isinstance(x, str):
        return x
    elif isinstance(x, bytes):
        return x.decode('utf-8')
    else:
        return str(x)


class HeaderStore(ProcessingTask):
    _data: List[Header]
    _data_being_loaded: Optional[List[Optional[Header]]] = None
    _singlejob = True
    columns: Final[List[str]] = [   # if you change this, update the return value of _ingestnexusfiles() method, too!
        "fsn",
        "title",
        "distance",
        "enddate",
        "thickness",
        "transmission",
        "temperature",
    ]

    HeaderNamedTuple = collections.namedtuple("HeaderNamedTuple", columns + ['entry', 'subentry', 'bad'])

    
    _nexuspath_for_columns: Final[Dict[str, Tuple[str, Callable[[str], Any]]]] = {
        "bad": ("frame_is_bad", bool),
        "fsn": ("run", int),
        "title": ("NXsample/name", _parse_str),
        "distance": ("NXinstrument/NXdetector/SDD", float),
        "enddate": ("end_time", _parse_datetime),
        "thickness": ("NXsample/thickness", float),
        "transmission": ("NXsample/transmission", float),
        "temperature": ("NXsample/temperature", float),
    }

    def __init__(self, processing: "Processing", settings: ProcessingSettings):
        self._data = []
        super().__init__(processing, settings)
        if self.is_canSAS_NeXus():
            self.beginResetModel()
            self._data = self._getMetaDataFromNeXus()
            self.endResetModel()

    def rowCount(self, parent: QtCore.QModelIndex = ...) -> int:
        return len(self._data)

    def columnCount(self, parent: QtCore.QModelIndex = ...) -> int:
        return len(self.columns)

    def setData(self, index: QtCore.QModelIndex, value: Any, role: int = ...) -> bool:
        if (index.column() == 0) and (role == QtCore.Qt.ItemDataRole.CheckStateRole):
            logger.debug(f"{type(value)=}, {value=}")
            if value == QtCore.Qt.CheckState.Checked:
                self.settings.markAsBad(self._data[index.row()].fsn)
            elif value == QtCore.Qt.CheckState.Unchecked:
                self.settings.markAsGood(self._data[index.row()].fsn)
            else:
                raise ValueError(f"Invalid check state: {value}")
            self.dataChanged.emit(index, index, [QtCore.Qt.ItemDataRole.CheckStateRole])
            return True
        return False

    def data(self, index: QtCore.QModelIndex, role: int = ...) -> Any:
        if role == QtCore.Qt.ItemDataRole.DisplayRole:
            value = getattr(self._data[index.row()], self.columns[index.column()])
            columnname = self.columns[index.column()]
            if isinstance(value, str):
                return value
            elif isinstance(value, int):
                return str(value)
            elif isinstance(value, datetime.datetime):
                return str(value)
            elif isinstance(value, float):
                if columnname == "distance":
                    return f'{value:.2f}'
                elif columnname == 'thickness':
                    return f'{value:.3f}'
                elif columnname == 'transmission':
                    return f'{value:.4f}'
                else:
                    return f'{value:g}'
            elif (
                isinstance(value, tuple)
                and (len(value) == 2)
                and isinstance(value[0], float)
                and isinstance(value[1], float)
            ):
                if columnname == "distance":
                    return f"{value[0]:.2f} \xb1 {value[1]:.2f}"
                elif columnname == "thickness":
                    return f"{value[0]:.3f} \xb1 {value[1]:.3f}"
                elif columnname == "transmission":
                    return f"{value[0]:.4f} \xb1 {value[1]:.4f}"
                else:
                    return f"{value[0]:g} \xb1 {value[1]:g}"
            else:
                return str(value)
        elif role == QtCore.Qt.ItemDataRole.UserRole:
            return self._data[index.row()]
        elif (role == QtCore.Qt.ItemDataRole.CheckStateRole) and (index.column() == 0):
            return (
                QtCore.Qt.CheckState.Checked
                if (self._data[index.row()].fsn in self.settings.badfsns)
                else QtCore.Qt.CheckState.Unchecked
            )
        else:
            return None

    def headerData(
        self, section: int, orientation: QtCore.Qt.Orientation, role: int = ...
    ) -> Any:
        if (orientation == QtCore.Qt.Orientation.Horizontal) and (
            role == QtCore.Qt.ItemDataRole.DisplayRole
        ):
            return self.columns[section]

    def parent(self, child: QtCore.QModelIndex) -> QtCore.QModelIndex:
        return QtCore.QModelIndex()

    def index(
        self, row: int, column: int, parent: QtCore.QModelIndex = ...
    ) -> QtCore.QModelIndex:
        return self.createIndex(row, column, None)

    def flags(self, index: QtCore.QModelIndex) -> QtCore.Qt.ItemFlag:
        if index.column() == 0:
            return (
                QtCore.Qt.ItemFlag.ItemNeverHasChildren
                | QtCore.Qt.ItemFlag.ItemIsSelectable
                | QtCore.Qt.ItemFlag.ItemIsEnabled
                | QtCore.Qt.ItemFlag.ItemIsUserCheckable
            )
        return (
            QtCore.Qt.ItemFlag.ItemNeverHasChildren
            | QtCore.Qt.ItemFlag.ItemIsSelectable
            | QtCore.Qt.ItemFlag.ItemIsEnabled
        )

    def _start(self, overwrite_if_present: bool=False):
        self.beginResetModel()
        self._data = []
        self.endResetModel()
        if self.is_canSAS_NeXus():
            self._submitTask(
                self._ingestnexusfiles, 0, 
                h5file=self.settings.h5io.filename,
                h5lock=self.settings.h5io.lock,
                rootdir=os.path.join(
                    self.settings.dataroot, self.settings.datayear, self.settings.nexussubpath,),
                filenamepattern = self.settings.filenamepattern if self.settings.filenamescheme == FileNameScheme.Pattern else f"{self.settings.prefix}_%0{self.settings.fsndigits}d",
                overwrite_if_present = overwrite_if_present,
                fsnranges = self.settings.fsnranges)
        else:
            self._data_being_loaded = []
            jobid = 0
            for start, end, description, onlysamples, suffix in self.settings.fsnranges:
                fsns = list(range(start, end+1))
                self._data_being_loaded.extend([None] * len(fsns))
                for fsn in fsns:
                    self._submitTask(
                        self._loadheader,
                        jobid,
                        rootdir=os.path.join(
                            self.settings.dataroot,
                            self.settings.datayear,
                            self.settings.eval2dsubpath,
                        ),
                        prefix=self.settings.prefix,
                        fsn=fsn,
                        fsndigits=self.settings.fsndigits,
                        filenamescheme=self.settings.filenamescheme,
                        filenamepattern=self.settings.filenamepattern,
                        onlysamples=onlysamples,
                        suffix=suffix,
                    )
                    jobid += 1

    @staticmethod
    def _ingestnexusfiles(jobid: int, messagequeue, stopEvent, h5file:str, h5lock: multiprocessing.synchronize.Lock, rootdir: str, fsnranges:List[Tuple[int, int, str, str, str]], filenamepattern: str, overwrite_if_present: bool):
        ingestor = ProcessingH5FileNeXus(h5file, h5lock)
        fsncount = sum([end-start+1 for start, end, description, onlysamples, suffix in fsnranges])
        iloaded = 0
        for start, end, description, onlysamples, suffix in fsnranges:
            if stopEvent.is_set():
                break
            for fsn in range(start, end+1):
                if stopEvent.is_set():
                    break
                try:
                    filename = fastfindfile(rootdir, filenamepattern % fsn, ['.nx5', '.nxs', '.h5'])
                except FileNotFoundError:
                    continue
                try:
                    # TODO: honor onlysamples and suffix!
                    ingestor.ingestNeXus(filename, overwrite_if_present)
                    tb = None
                except Exception as exc:
                    tb = traceback.format_exc()
                    raise
                iloaded += 1
                messagequeue.put_nowait(Message(sender=jobid, type_="progress", message=f"{filename} loaded.", totalcount=fsncount, currentcount=iloaded, traceback=tb))
        return jobid, None

    def _getMetaDataFromNeXus(self):
        if not self.is_canSAS_NeXus():
            raise ValueError('This is not a canSAS NeXus file')
        with self.settings.h5io.reader('/') as h5:
            result = []
            for entry in h5ioutils.h5_subgroups_of_type(h5, 'NXentry'):
                entryname = entry.name.split('/')[-1]
                for subentry in h5ioutils.h5_subgroups_of_type(entry, 'NXsubentry'):
                    subentryname = subentry.name.split('/')[-1]
                    arrays = {columnname:np.array(h5ioutils.h5_nxpath(subentry, nxpath)).tolist() 
                              for columnname, (nxpath, converter) in self._nexuspath_for_columns.items()}
                    count = len(arrays['bad'])

                    for key, array in arrays.items():
                        if not isinstance(array, list):
                            array = [array]*count
                        elif len(array) == 1:
                            array = [array[0]]*count
                        array = [(a if not isinstance(a, list) else a[0]) for a in array]
                        arrays[key] = array
                    result.extend([
                        {"entry": entryname, "subentry": subentryname} | 
                        {
                            columnname: self._nexuspath_for_columns[columnname][1](arrays[columnname][i]) for columnname in self._nexuspath_for_columns
                        } for i in range(len(arrays[list(arrays)[0]]))
                    ])
        return sorted([self.HeaderNamedTuple(**r) for r in result], key=lambda x:x.fsn)


    @staticmethod
    def _loadheader(
        h5file: str,
        h5lock,
        jobid,
        messagequeue,
        stopEvent,
        rootdir: str,
        prefix: str,
        fsn: int,
        fsndigits: int,
        filenamescheme: FileNameScheme,
        filenamepattern: str,
        onlysamples: Optional[List[str]],
        suffix: Optional[str],
    ) -> Tuple[int, Optional[Header]]:
        if filenamescheme == FileNameScheme.Parts:
            filename = f"{prefix}_{fsn:0{fsndigits}d}"
        else:
            filename = filenamepattern % fsn
        headerloaded = None
        try:
            headerloaded = Header(
                fastfindfile(
                    rootdir, filename, [".pickle", ".pickle.gz", ".param", ".param.gz"]
                )
            )
        except FileNotFoundError:
            print(f'Not found: {fsn} {rootdir}, {filename}')
            return jobid, None
        if (onlysamples is not None) and (headerloaded.title not in onlysamples):
            return jobid, None
        else:
            if suffix is not None:
                headerloaded.title = headerloaded.title + suffix
            return jobid, headerloaded

    def onAllBackgroundTasksFinished(self):
        self.beginResetModel()
        if self.is_canSAS_NeXus():
            self._data = self._getMetaDataFromNeXus()
        else:
            self._data = [h for h in self._data_being_loaded if h is not None]
        self._data_being_loaded = []
        self.endResetModel()

    def onBackgroundTaskFinished(self, result: Tuple[int, Optional[Header]]):
        if not self.is_canSAS_NeXus():
            jobid, header = result
            if header is None:
                return
            assert isinstance(header, Header)
            self._data_being_loaded[jobid] = header
        else:
            pass
        super().onBackgroundTaskFinished(result)

    def stop(self):
        super().stop()
        self._pool.terminate()

    def __iter__(self) -> Iterator[Header]:
        yield from self._data

    def __len__(self):
        return len(self._data)

    def badfsnschanged(self):
        self.dataChanged.emit(
            self.index(0, 0),
            self.index(self.rowCount(), 0),
            [QtCore.Qt.ItemDataRole.CheckStateRole],
        )

    def writeXLSX(self, filename: str):
        wb = openpyxl.Workbook()
        ws = wb.active
        for col in range(self.columnCount(QtCore.QModelIndex())):
            ws.cell(row=1, col=col+1, value=self.headerData(col, QtCore.Qt.Orientation.Horizontal, QtCore.Qt.ItemDataRole.DisplayRole))
        for row in range(self.rowCount(QtCore.QModelIndex())):
            for col in range(self.columnCount(QtCore.QModelIndex())):
                ws.cell(row=row+2, col=col+1, value=getattr(self._data[row], self.columns[col]))
        wb.save(filename)
