import logging
import os
from typing import Any, Iterator, List, Optional, Sequence, Set

from taurus.external.qt import QtCore, QtGui
from taurus.external.qt.QtCore import Signal, Slot

from ...algorithms.matrixaverager import ErrorPropagationMethod
from ...dataclasses.exposure import QRangeMethod
from ..calculations.outliertest import OutlierMethod
from ..calculations.summaryjob import SummaryJob
from ..calculations.summaryjob_nexus import SummaryJobNeXus
from .task import ProcessingSettings, ProcessingTask
from ..h5io import ProcessingH5File, ProcessingH5FileNeXus


logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)


class SummaryData:
    samplename: str
    distance: float
    fsns: List[int]
    h5io: ProcessingH5File
    statusmessage: str = '--'
    spinner: Optional[int] = None
    progresstotal: int = 0
    progresscurrent: int = 0
    errormessage: Optional[str] = None
    traceback: Optional[str] = None
    lastfoundbadfsns: List[int]

    def __init__(self, h5io, samplename: str, distance: float, fsns: Sequence[int]):
        self.samplename = samplename
        self.distance = distance
        self._fsns = list(fsns)
        self.lastfoundbadfsns = []
        self.h5io = h5io

    def isRunning(self) -> bool:
        return self.spinner is not None

    def goodfsns(self, badfsns: List[int]):
        if isinstance(self.h5io, ProcessingH5FileNeXus):
            with self.h5io.reader(self.h5io.getH5Path(self.samplename, self.distance)) as h5:
                isbad = h5['frame_is_bad'][()]
                run = h5['run'][()]
                return [int(run[i].decode('ascii')) for i in range(run.size) if not isbad[i]]
        return [f for f in self.fsns if f not in badfsns]
    
    def badfsns(self):
        if isinstance(self.h5io, ProcessingH5FileNeXus):
            with self.h5io.reader(self.h5io.getH5Path(self.samplename, self.distance)) as h5:
                isbad = h5['frame_is_bad'][()]
                run = h5['run'][()]
                return [int(run[i].decode('ascii')) for i in range(run.size) if isbad[i]]

    @property
    def fsns(self):
        if isinstance(self.h5io, ProcessingH5FileNeXus):
            with self.h5io.reader(self.h5io.getH5Path(self.samplename, self.distance)) as h5:
                return [int(x.decode('ascii')) for x in  h5['run'][()]]
        else:
            return self._fsns

class Summarization(ProcessingTask):
    _data: List[SummaryData] = None
    itemChanged = Signal(str, str)
    newbadfsns: Set[int]
    spinnerTimer: Optional[QtCore.QTimer] = None

    def __init__(self, processing: "Processing", settings: ProcessingSettings):
        self._data = []
        self.newbadfsns = set()
        super().__init__(processing, settings)
        if self.is_canSAS_NeXus():
            for sn, dist in self.settings.h5io.samples_distances():
                try:
                    floatdist = float(dist.replace('_', '.'))
                except ValueError:
                    continue
                self.addSample(sn, floatdist, [])


    def rowCount(self, parent: QtCore.QModelIndex = ...) -> int:
        return len(self._data)

    def columnCount(self, parent: QtCore.QModelIndex = ...) -> int:
        return 5

    def parent(self, child: QtCore.QModelIndex) -> QtCore.QModelIndex:
        return QtCore.QModelIndex()

    def index(self, row: int, column: int, parent: QtCore.QModelIndex = ...) -> QtCore.QModelIndex:
        return self.createIndex(row, column, None)

    def data(self, index: QtCore.QModelIndex, role: int = ...) -> Any:
        sd = self._data[index.row()]
        if role == QtCore.Qt.ItemDataRole.DisplayRole:
            if index.column() == 0:
                return sd.samplename
            elif index.column() == 1:
                return f'{sd.distance:.2f} mm'
            elif index.column() == 2:
                return str(len(sd.goodfsns(self.settings.badfsns)))
            elif index.column() == 3:
                return str(len(sd.fsns))
            elif index.column() == 4:
                return sd.statusmessage if sd.errormessage is None else sd.errormessage
        elif (role == QtCore.Qt.ItemDataRole.DecorationRole) and (sd.spinner is not None) and (index.column() == 0):
            return QtGui.QIcon(QtGui.QPixmap(f':/icons/spinner_{sd.spinner % 12:02d}.svg'))
        elif (role == QtCore.Qt.ItemDataRole.ToolTipRole) and (sd.errormessage is not None):
            return sd.traceback
        elif (role == QtCore.Qt.ItemDataRole.BackgroundRole) and (sd.errormessage is not None):
            return QtGui.QColor('red').lighter(150)
        elif (role == QtCore.Qt.ItemDataRole.ForegroundRole) and (sd.errormessage is not None):
            return QtGui.QColor('black')
        elif (role == QtCore.Qt.ItemDataRole.UserRole):
            return sd

    def flags(self, index: QtCore.QModelIndex) -> QtCore.Qt.ItemFlag:
        return QtCore.Qt.ItemFlag.ItemNeverHasChildren | QtCore.Qt.ItemFlag.ItemIsEnabled | \
            QtCore.Qt.ItemFlag.ItemIsSelectable | QtCore.Qt.ItemFlag.ItemIsDropEnabled | \
            QtCore.Qt.ItemFlag.ItemIsDropEnabled

    def headerData(self, section: int, orientation: QtCore.Qt.Orientation, role: int = ...) -> Any:
        if (orientation == QtCore.Qt.Orientation.Horizontal) and (role == QtCore.Qt.ItemDataRole.DisplayRole):
            return ['Sample', 'Distance', 'Good', 'All', 'Status'][section]

    def __iter__(self) -> Iterator[SummaryData]:
        yield from self._data

    def addSample(self, samplename: str, distance: float, fsns: Sequence[int]):
        logger.debug('Checking if this sample exists.')
        if [sd for sd in self._data if sd.samplename == samplename and sd.distance == distance]:
            raise ValueError('Already existing (samplename, distance) pair.')
        logger.debug('Invoking beginResetModel')
        self.beginResetModel()
        logger.debug(f'Adding sample {samplename=}, {distance=}')
        self._data.append(SummaryData(self.settings.h5io, samplename, distance, fsns))
        logger.debug('Created SummaryData instance.')
        self._data = sorted(self._data, key=lambda sd: (
            sd.samplename, sd.distance))
        logger.debug('Sorted model')
        self.endResetModel()
        logger.debug('Emitted endResetModel')

    def clear(self):
        if not self.isIdle():
            raise RuntimeError('Cannot clear summarization model: not idle.')
        self.beginResetModel()
        self._data = []
        self.endResetModel()

    def _start(self, startonly=None):
        self.newbadfsns = set()
        for i, sd in enumerate(self._data):
            if (not sd.goodfsns(self.settings.badfsns)) and not self.is_canSAS_NeXus():
                continue
            if (startonly is not None) and not [(sn, d) for (sn, d) in startonly if (sd.samplename == sn) and (abs(sd.distance - d)< 0.1)]:
                continue
            sd.errormessage = None
            sd.traceback = None
            sd.spinner = 0
            try:
                self.settings.h5io.addSample(sd.samplename)
            except ValueError:
                pass
            try:
                samplename, distkey = self.settings.h5io.addDistance(
                    sd.samplename, sd.distance)
            except ValueError:
                pass
            if self.is_canSAS_NeXus():
                h5path = self.settings.h5io.getH5Path(sd.samplename, sd.distance)
                with self.settings.h5io.writer(h5path) as grp:
                    dic = {}
                    for key in ['ierrorprop', 'qerrorprop', 'outliermethod', 'outlierthreshold', 'outlierlogcormat', 'qrangemethod', 'qcount', 'bigmemorymode']:
                        defaultvalue = getattr(self.settings, key)
                        if isinstance(defaultvalue, OutlierMethod):
                            defaultvalue = defaultvalue.value
                        elif isinstance(defaultvalue, (ErrorPropagationMethod, QRangeMethod)):
                            defaultvalue = defaultvalue.name    
                        dic[key] = grp.attrs.setdefault(key, defaultvalue)
                        if key in ['ierrorprop', 'qerrorprop']:
                            dic[key] = ErrorPropagationMethod[dic[key]]
                        elif key == 'outliermethod':
                            dic[key] = OutlierMethod(dic[key])
                        elif key == 'qrangemethod':
                            dic[key] = QRangeMethod[dic[key]]
                self._submitTask(
                    SummaryJobNeXus.run, 
                    (i, sd.samplename, sd.distance), 
                    **dic)
            else:
                with self.settings.h5io.writer(f'Samples/{samplename}/{distkey}') as grp:
                    grp.attrs.setdefault(
                        'ierrorprop', self.settings.ierrorprop.name)
                    grp.attrs.setdefault(
                        'qerrorprop', self.settings.qerrorprop.name)
                    grp.attrs.setdefault(
                        'outliermethod', self.settings.outliermethod.value)
                    grp.attrs.setdefault('outlierthreshold',
                                        self.settings.outlierthreshold)
                    grp.attrs.setdefault('outlierlogcormat',
                                        self.settings.outlierlogcormat)
                    grp.attrs.setdefault(
                        'qrangemethod', self.settings.qrangemethod.name)
                    grp.attrs.setdefault('qcount', self.settings.qcount)
                    grp.attrs.setdefault(
                        'bigmemorymode', self.settings.bigmemorymode)
                    attrs = dict(grp.attrs)
                self._submitTask(SummaryJob.run, (i, sd.samplename, sd.distance),
                                 rootpath=os.path.join(self.settings.dataroot, self.settings.datayear),
                                 eval2dsubpath=self.settings.eval2dsubpath,
                                 masksubpath=self.settings.masksubpath,
                                 fsndigits=self.settings.fsndigits,
                                 prefix=self.settings.prefix,
                                 filenamepattern=self.settings.filenamepattern,
                                 filenamescheme=self.settings.filenamescheme,
                                 fsnlist=list(sd.fsns),
                                 ierrorprop=ErrorPropagationMethod[attrs['ierrorprop']],
                                 qerrorprop=ErrorPropagationMethod[attrs['qerrorprop']],
                                 outliermethod=OutlierMethod(
                                     attrs['outliermethod']),
                                 outlierthreshold=float(attrs['outlierthreshold']),
                                 cormatLogarithmic=bool(attrs['outlierlogcormat']),
                                 qrangemethod=QRangeMethod[attrs['qrangemethod']],
                                 qcount=int(attrs['qcount']),
                                 bigmemorymode=bool(attrs['bigmemorymode']),
                                 badfsns=self.settings.badfsns,
                                 samplename_override=sd.samplename,
                                 )
                sd.statusmessage = 'Queued for processing...'
        self.dataChanged.emit(self.index(0, 0, QtCore.QModelIndex()),
                              self.index(self.rowCount(), self.columnCount(), QtCore.QModelIndex()))
        self.spinnerTimer = QtCore.QTimer()
        self.spinnerTimer.timeout.connect(self.updateSpinners)
        self.spinnerTimer.setTimerType(QtCore.Qt.TimerType.PreciseTimer)
        self.spinnerTimer.start(100)

    def onBackgroundTaskProgress(self, jobid: Any, total: int, current: int, message: str):
        j = jobid[0]
        self._data[j].progresstotal = total
        self._data[j].progresscurrent = current
        self._data[j].statusmessage = message
        self.dataChanged.emit(self.index(j, 4), self.index(j, 4))

    def onBackgroundTaskError(self, jobid: Any, errormessage: str, traceback: str):
        j = jobid[0]
        self._data[j].errormessage = errormessage
        self._data[j].traceback = traceback
        self._data[j].spinner = None
        self.dataChanged.emit(self.index(
            j, 0), self.index(j, self.columnCount()))
        super().onBackgroundTaskError(jobid, errormessage, traceback)

    def onBackgroundTaskFinished(self, result: Any):
        logger.debug(f'Summarization result: {result}')
        i, samplename, distance = result.jobid
        self._data[i].progresscurrent = 0
        self._data[i].progresstotal = 0
        self._data[i].spinner = None
        self._data[i].lastfoundbadfsns = result.newbadfsns
        self.newbadfsns = self.newbadfsns.union(result.newbadfsns)
        self._data[i].statusmessage = 'Processing done'
        # self.itemChanged.emit(self._data[i].samplename, f'{self._data[i].distance:.2f}')
        self.dataChanged.emit(self.index(
            i, 0), self.index(i, self.columnCount()))
        self.itemChanged.emit(
            self._data[i].samplename, f'{self._data[i].distance:.2f}')
        super().onBackgroundTaskFinished(result)

    def onAllBackgroundTasksFinished(self):
        if not self.is_canSAS_NeXus():
            # the NeXus version already updated them.
            self.settings.addBadFSNs(self.newbadfsns)
        else:
            self.settings.badfsnsChanged.emit()
        for i, d in enumerate(self._data):
            if d.spinner is not None:
                d.errormessage = 'User break'
                d.spinner = None
                self.dataChanged.emit(self.index(i, 0, QtCore.QModelIndex()),
                                      self.index(i, self.columnCount(QtCore.QModelIndex()), QtCore.QModelIndex()))

    @Slot()
    def updateSpinners(self):
        # update spinners
        for d in self._data:
            if d.spinner is not None:
                d.spinner += 1
        self.dataChanged.emit(
            self.index(0, 0, QtCore.QModelIndex()),
            self.index(self.rowCount(QtCore.QModelIndex()),
                       0, QtCore.QModelIndex()),
            [QtCore.Qt.ItemDataRole.DecorationRole])
        if not [d for d in self._data if d.spinner is not None]:
            self.spinnerTimer.stop()
            self.spinnerTimer.deleteLater()
            self.spinnerTimer = None
