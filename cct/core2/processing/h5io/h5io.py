import enum
import itertools
import logging
import multiprocessing.synchronize
import os
import re
from typing import List, Optional, Final, Self, Union, Tuple, Dict, Any

import dateutil.parser
import h5py
import numpy as np

from ...dataclasses import Exposure, Header, Curve
from ..calculations.outliertest import OutlierTest, OutlierMethod

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)


class ProcessingH5File:
    filename: str
    lock: multiprocessing.synchronize.Lock
    handle: h5py.File
    groupname: Optional[str] = None
    swmr: bool = False
    canSAS_format: bool = False

    _value_and_error_header_fields: Final[List[str]] = \
        ['distance', 'distancedecrease', 'dark_cps', 'wavelength', 'exposuretime', 'absintfactor',
         'beamposrow', 'beamposcol', 'flux', 'samplex', 'sampley', 'temperature', 'thickness',
         'transmission', 'vacuum']

    _as_is_header_fields: Final[List[str]] = [
        'title', 'sample_category', 'fsn', 'fsn_absintref', 'fsn_dark', 'fsn_emptybeam', 'maskname', 'username',
        'project', 'prefix', 'absintqmin', 'absintqmax', 'absintdof', 'absintchi2']

    _datetime_header_fields: Final[List[str]] = [
        'date', 'startdate', 'enddate']

    class MetaDataField(enum.Enum):
        Distance = "distance"
        DistanceDecrease = "distancedecrease"
        DarkCps = "dark_cps"
        Wavelength = "wavelength"
        ExposureTime = "exposuretime"
        AbsIntFactor = "absintfactor"
        BeamPosRow = "beamposrow"
        BeamPosCol = "beamposcol"
        Flux = "flux"
        SampleX = "samplex"
        SampleY = "sampley"
        Temperature = "temperature"
        Thickness = "thickness"
        Transmission = "transmission"
        Vacuum = "vacuum"
        Title = "title"
        SampleCategory = "sample_category"
        FSN = "fsn"
        FSNAbsIntRef = "fsn_absintref"
        FSNDark = "fsn_dark"
        FSNEmptyBeam = "fsn_emptybeam"
        MaskName = "maskname"
        UserName = "username"
        Project = "project"
        Prefix = "prefix"
        AbsIntQmin = "absintqmin"
        AbsIntQmax = "absintqmax"
        AbsIntDoF = "absintdof"
        AbsIntChi2 = "absintchi2"
        Date = "date"
        StartDate = "startdate"
        EndDate = "enddate"

    class Handler:
        readers = 0
        writers = 0

        def __init__(self, filename: str, lock: multiprocessing.synchronize.Lock | None, writable: bool = True,
                     group: Optional[str|Dict[str, List[str]]] = None, swmr: bool = False, canSAS_format: bool = False):
            self.swmr = swmr
            self.filename = filename
            self.lock = lock
            self.filehandle = None
            if group is None:
                group = '/'
            if isinstance(group, str):
                self._readgroups = [group] if not writable else []
                self._writegroups = [group] if writable else []
                self._flat = True
            elif isinstance(group, dict):
                self._readgroups = group.get("read", default=[])
                self._writegroups = group.get("write", default=[])
                self._flat = False
            else:
                raise TypeError(f"Invalid type for group argument: {type(group)}")
            self.canSAS_format = canSAS_format

        def __enter__(self) -> h5py.File:
            try:
                type(self).writers += len(self._writegroups)
                type(self).readers += len(self._readgroups)
                
                if self.lock is not None:
                    self.lock.acquire()
                if self._writegroups:
                    self.filehandle = h5py.File(self.filename, 'a', libver="latest")
                    if self.swmr:
                        self.filehandle.swmr_mode = True
                else:
                    self.filehandle = h5py.File(self.filename, 'r', swmr=self.swmr, libver="latest")
                for writegrp in self._writegroups:
                    if writegrp is not None:
                        self.filehandle.require_group(writegrp)
                grplist = [self.filehandle[grp] for grp in self._readgroups] + [self.filehandle[grp] for grp in self._writegroups]
                if self._flat:
                    assert len(grplist) == 1
                    return grplist[0]
                else:
                    return tuple(grplist[0])
            except Exception:
                type(self).writers -= len(self._writegroups)
                type(self).readers -= len(self._readgroups)
                try:
                    if self.filehandle is not None:
                        self.filehandle.close()
                finally:
                    self.filehandle = None
                    if self.lock is not None:
                        self.lock.release()
                raise

        def __exit__(self, exc_type, exc_val, exc_tb):
            try:
                if self.filehandle is not None:
                    self.filehandle.close()
            finally:
                self.filehandle = None
                if self.lock is not None:
                    self.lock.release()
                type(self).writers -= len(self._writegroups)
                type(self).readers -= len(self._readgroups)
    

    def __init__(self, filename: str, lock: Optional[multiprocessing.synchronize.Lock] = None):
        self.filename = filename
        self.lock = multiprocessing.Lock() if lock is None else lock
        # ensure that the file exists
        if not os.path.exists(filename):
            with self.lock:
                with h5py.File(filename, 'a', libver="latest") as h5:
                    pass

    def writer(self, group: Optional[str] = None):
        return self.Handler(self.filename, self.lock, writable=True, group=group, swmr=False)

    def reader(self, group: Optional[str] = None, locking: bool=True):
        return self.Handler(self.filename, self.lock if locking else None, writable=False, group=group, swmr=True)

    def readerswriters(self, readgroups: Optional[List[str]] = None, writegroups: Optional[List[str]] = None):
        return self.Handler(self.filename, self.lock, writable=None, 
                            group={
                                "read": readgroups if readgroups is not None else [],
                                "write": writegroups if writegroups is not None else [],
                                }, swmr=False)

    def samplenames(self) -> List[str]:
        raise NotImplementedError()

    def addSample(self, samplename: str, category: str = "sample"):
        raise NotImplementedError()

    def getH5Path(self, samplename: str, distance: str | float):
        raise NotImplementedError()
    
    def removeSample(self, samplename: str):
        raise NotImplementedError()

    def distances(self, samplename: str) -> List[float]:
        raise NotImplementedError()

    def distancekeys(self, samplename: str, onlynumeric: bool = True) -> List[str]:
        raise NotImplementedError()

    def addDistance(self, samplename: str, distance: Union[float, str]) -> Tuple[str, str]:
        raise NotImplementedError()

    def removeDistance(self, samplename: str, distance: Union[float, str]):
        raise NotImplementedError()

    def pruneSamplesWithNoDistances(self):
        raise NotImplementedError()

    def writeExposure(self, exposure: Exposure, group: h5py.Group):
        raise NotImplementedError()

    def writeHeader(self, header: Header, group: h5py.Group):
        raise NotImplementedError()

    def writeCurve(self, curve: Union[Curve, np.ndarray], group: h5py.Group, name: str):
        raise NotImplementedError()

    def readCurve(self, path: str) -> Curve:
        raise NotImplementedError()
    
    def readCurveFSN(self, fsn: int, samplename: str | None = None, distkey: str | None = None) -> Curve:
        if (samplename is not None) and (distkey is not None):
            raise NotImplementedError
        elif (samplename is not None) and (distkey is None):
            for distkey in self.distancekeys(samplename, onlynumeric=True):
                try:
                    return self.readCurveFSN(fsn, samplename, distkey)
                except IndexError:
                    continue
            raise IndexError(f'Could not find curve with FSN #{fsn} for sample {samplename}')
        else:
            assert samplename is None
            for samplename in self.samplenames():
                if distkey is not None:
                    distkeys = [distkey]
                else:
                    distkeys = self.distancekeys(samplename, onlynumeric=True)
                for dk in distkeys:
                    try:
                        return self.readCurveFSN(fsn, samplename, dk)
                    except IndexError as ie:
                        continue
            raise IndexError(f'Could not find curve with FSN #{fsn} for any sample, any distance')

    def readHeader(self, group: str) -> Header:
        raise NotImplementedError()

    def readExposure(self, group: str) -> Exposure:
        raise NotImplementedError()

    def readOutlierTest(self, group: str) -> OutlierTest:
        raise NotImplementedError()

    def writeOutlierTest(self, group: str, ot: OutlierTest):
        raise NotImplementedError()

    def items(self) -> List[Tuple[str, str]]:
        lis = []
        for sn in self.samplenames():
            for dist in self.distancekeys(sn, False):
                lis.append((sn, dist))
        return lis

    def readCurves(self, group: str, readall: bool = False) -> Dict[int, Curve]:
        raise NotImplementedError()

    def readHeaders(self, group: str, readall: bool = False) -> Dict[int, Header]:
        raise NotImplementedError()

    def readHeaderDict(self, group: str) -> Dict[str, Any]:
        raise NotImplementedError()

    def clear(self):
        for sample in self.samplenames():
            self.removeSample(sample)

    def __contains__(self, item: Tuple[str, str]) -> bool:
        return item in self.items()

    def getMetaDataField(self, samplename: str, distkey: str, field: MetaDataField | str, fsn: int | None = None):
        raise NotImplementedError()

    def getMetaDataSeries(self, samplename: str, distkey: str, field: MetaDataField | str):
        raise NotImplementedError()
