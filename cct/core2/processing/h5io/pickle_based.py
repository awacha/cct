import multiprocessing
import h5py
import logging
import numpy as np
import os
import re
import dateutil.parser
from typing import List, Final, Optional, Union, Tuple, Dict, Any

from ...dataclasses.exposure import Exposure
from ...dataclasses.header import Header
from ...dataclasses.curve import Curve
from ..calculations.outliertest import OutlierMethod, OutlierTest
from .h5io import ProcessingH5File

logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)


class ProcessingH5FilePickle(ProcessingH5File):
    groupname: Optional[str] = None
    swmr: bool = False
    canSAS_format: bool = False

    _value_and_error_header_fields: Final[List[str]] = \
        ['distance', 'distancedecrease', 'dark_cps', 'wavelength', 'exposuretime', 'absintfactor',
         'beamposrow', 'beamposcol', 'flux', 'samplex', 'sampley', 'temperature', 'thickness',
         'transmission', 'vacuum']

    _as_is_header_fields: Final[List[str]] = [
        'title', 'sample_category', 'fsn', 'fsn_absintref', 'fsn_dark', 'fsn_emptybeam', 'maskname', 'username',
        'project', 'prefix', 'absintqmin', 'absintqmax', 'absintdof', 'absintchi2']

    _datetime_header_fields: Final[List[str]] = [
        'date', 'startdate', 'enddate']

    def __init__(self, filename: str, lock: Optional[multiprocessing.synchronize.Lock] = None):
        super().__init__(filename, lock)
        with self.lock:
            with h5py.File(filename, 'a') as h5:
                # ensure that the file is present.
                h5.require_group('Samples')

    def samplenames(self) -> List[str]:
        with self.reader() as h5file:
            if 'Samples' not in h5file:
                return []
            return list(h5file['Samples'].keys())

    def addSample(self, samplename: str, category: str = "sample"):
        with self.writer() as h5file:
            h5file.require_group('Samples')
            h5file['Samples'].require_group(samplename).attrs['category'] = category

    def getH5Path(self, samplename: str, distance: str | float):
        if isinstance(distance, float):
            distance = f'{distance:.2f}'
        return f'Samples/{samplename}/{distance}'

    def removeSample(self, samplename: str):
        with self.writer() as h5file:
            h5file.require_group('Samples')
            del h5file['Samples'][samplename]

    def distances(self, samplename: str) -> List[float]:
        return [float(x) for x in self.distancekeys(samplename)]

    def distancekeys(self, samplename: str, onlynumeric: bool = True) -> List[str]:
        with self.reader() as h5file:
            try:
                lis = []
                for g in h5file[f'Samples/{samplename}']:
                    try:
                        if onlynumeric:
                            float(g)
                        lis.append(g)
                    except ValueError:
                        pass
                return lis
            except KeyError:
                return []

    def addDistance(self, samplename: str, distance: Union[float, str]) -> Tuple[str, str]:
        if isinstance(distance, float):
            distance = f'{distance:.2f}'
        with self.writer() as h5file:
            h5file.require_group(f'Samples/{samplename}/{distance}')
        return (samplename, distance)

    def removeDistance(self, samplename: str, distance: Union[float, str]):
        if isinstance(distance, float):
            distance = f'{distance:.2f}'
        with self.writer() as h5file:
            try:
                del h5file[f'Samples/{samplename}/{distance}']
            except KeyError:
                pass

    def pruneSamplesWithNoDistances(self):
        for samplename in self.samplenames():
            if not self.distancekeys(samplename):
                self.removeSample(samplename)

    def writeExposure(self, exposure: Exposure, group: h5py.Group):
        assert ((exposure.mask != 0).sum() != 0) and (
            (exposure.mask == 0).sum() != 0)
        for key in ['image', 'image_uncertainty']:
            try:
                del group[key]
            except KeyError:
                pass
        group.create_dataset(
            'image',
            exposure.intensity.shape,
            exposure.intensity.dtype,
            exposure.intensity, compression='lzf', fletcher32=True, shuffle=True)
        group.create_dataset(
            'image_uncertainty',
            exposure.uncertainty.shape,
            exposure.uncertainty.dtype,
            exposure.uncertainty, compression='lzf', fletcher32=True, shuffle=True
        )
        maskgroup = group.file.require_group('masks')
        try:
            del maskgroup[exposure.header.maskname]
        except KeyError:
            pass
        dsname = os.path.split(exposure.header.maskname)[-1]
        if dsname not in maskgroup:
            maskgroup.create_dataset(dsname,
                                     exposure.mask.shape, exposure.mask.dtype, exposure.mask,
                                     compression='lzf', fletcher32=True, shuffle=True)
        else:
            # the mask is already present
            maskpresent = np.array(
                maskgroup[dsname], dtype=exposure.mask.dtype)
            if ((exposure.mask - maskpresent)**2).sum() > 1e-9:
                del maskgroup[dsname]
                maskgroup.create_dataset(dsname, exposure.mask.shape, exposure.mask.dtype,
                                         exposure.mask, compression='lzf', fletcher32=True, shuffle=True)
                logger.info(f'Replaced mask {dsname} in the HDF5 file.')
        if 'mask' in group:
            del group['mask']
        group['mask'] = h5py.SoftLink(f'/masks/{dsname}')
        self.writeHeader(exposure.header, group)

    def writeHeader(self, header: Header, group: h5py.Group):
        group.attrs.update({
            'pixelsizex': header.pixelsize[0],
            'pixelsizey': header.pixelsize[0],
            'pixelsizex.err': header.pixelsize[1],
            'pixelsizey.err': header.pixelsize[1],
            'beamcenterx': header.beamposcol[0],
            'beamcentery': header.beamposrow[0],
            'beamcenterx.err': header.beamposcol[1],
            'beamcentery.err': header.beamposrow[1],
            'exposurecount': header.exposurecount,
        })
        for attribute in self._value_and_error_header_fields:
            try:
                group.attrs[attribute] = getattr(header, attribute)[0]
                group.attrs[f'{attribute}.err'] = getattr(header, attribute)[1]
            except KeyError as ke:
                logger.warning(
                    f'Cannot write attribute {attribute} because of KeyError {ke}')
        for attribute in self._datetime_header_fields:
            group.attrs[attribute] = str(getattr(header, attribute))
        for attribute in self._as_is_header_fields:
            try:
                group.attrs[attribute] = getattr(header, attribute)
            except KeyError as ke:
                logger.warning(
                    f'Cannot write attribute {attribute} because of KeyError {ke}')

    def writeCurve(self, curve: Union[Curve, np.ndarray], group: h5py.Group, name: str):
        for n in [name, name+'_nx']:
            try:
                del group[n]
            except KeyError:
                pass
        if isinstance(curve, Curve):
            array = curve.asArray()
        else:
            array = curve
        array = array[:, :6]
        nxgroup = group.create_group(name+'_nx')
        nxgroup.attrs['NX_class'] = 'NXdata'
        nxgroup.attrs['canSAS_class'] = 'SASdata'
        nxgroup.attrs['signal'] = 'I'
        nxgroup.attrs['I_axes'] = ['Q']
        nxgroup.attrs['Q_indices'] = [0]

        virtuallayout = h5py.VirtualLayout(
            shape=array.shape, dtype=array.dtype)

        for icol, colname, units, uncname in zip(
                range(array.shape[1]),
                ['Q', 'I', 'Idev', 'Qdev', 'area', 'pixel'],
                ['1/nm', '1/cm', '1/cm', '1/nm', None, None],
                ['Qdev', 'Idev', None, None, None, None]
                ):
            ds = nxgroup.create_dataset(
                f'{name}_{colname}', data=array[:, icol])
            if units is not None:
                ds.attrs['units'] = units
            if uncname is not None:
                ds.attrs['uncertainties'] = uncname
            virtuallayout[:, icol] = h5py.VirtualSource(ds)
        group.create_virtual_dataset(name, virtuallayout, fillvalue=np.nan)

    def readCurve(self, path: str, curvesubpath: str = "curve") -> Curve:
        if curvesubpath is not None:
            path = path+'/'+ curvesubpath
        with self.reader(path) as grp:
            assert isinstance(grp, h5py.Dataset)
            return Curve.fromArray(np.array(grp))

    def readCurveFSN(self, fsn: int, samplename: str | None = None, distkey: str | None = None) -> Curve:
        if (samplename is not None) and (distkey is not None):
            grpname = self.getH5Path(samplename, distkey) + '/allcurves'
            try:
                with self.reader(grpname) as grp:
                    fsngrp = [g for g in grp if (re.match(r'^\d+$', g) is not None) and int(g) == int(fsn)][0]
                    return self.readCurve(grp[fsngrp].name, None)
            except KeyError:
                raise IndexError()
        else:
            return super().readCurveFSN(fsn, samplename, distkey)

    def readHeader(self, group: str) -> Header:
        with self.reader(group) as grp:
            # assert isinstance(grp, h5py.Group)
            header = Header(datadict={})
            for attribute in self._as_is_header_fields:
                try:
                    setattr(header, attribute, grp.attrs[attribute])
                except KeyError:
                    pass
            for attribute in self._datetime_header_fields:
                try:
                    s = grp.attrs[attribute]
                    if isinstance(s, str):
                        setattr(header, attribute, dateutil.parser.parse(
                            grp.attrs[attribute]))
                    else:
                        raise ValueError(s, type(s))
                except (dateutil.parser.ParserError, KeyError):
                    pass
            for attribute in self._value_and_error_header_fields:
                try:
                    val = float(grp.attrs[attribute])
                except (KeyError, ValueError):
                    continue
                try:
                    unc = float(grp.attrs[attribute + '.err'])
                except (KeyError, ValueError):
                    unc = 0.0
                setattr(header, attribute, (val, unc))
            try:
                header.exposurecount = grp.attrs['exposurecount']
            except KeyError:
                try:
                    header.exposurecount = len(grp['curves'])
                except KeyError:
                    header.exposurecount = 1

            def getattr_failsafe(grp, name, default: Any):
                """grp.attrs.setdefault() does not work on read-only H5 files."""
                try:
                    return grp.attrs[name]
                except KeyError:
                    return default

            header.beamposrow = (
                getattr_failsafe(grp, 'beamcentery', np.nan),
                getattr_failsafe(grp, 'beamcentery.err', 0.0))
            header.beamposcol = (
                getattr_failsafe(grp, 'beamcenterx', np.nan),
                getattr_failsafe(grp, 'beamcenterx.err', 0.0))
            header.pixelsize = (
                getattr_failsafe(grp, 'pixelsizex', np.nan),
                getattr_failsafe(grp, 'pixelsizex.err', 0.0))
            return header

    def readExposure(self, group: str) -> Exposure:
        header = self.readHeader(group)
        with self.reader(group) as grp:
            intensity = np.array(grp['image'])
            unc = np.array(grp['image_uncertainty'])
            mask = np.array(grp['mask'], dtype=bool)
            assert (mask.sum() != 0) and ((~mask).sum() != 0)
            return Exposure(intensity, header, unc, mask)

    def readOutlierTest(self, group: str) -> OutlierTest:
        with self.reader(group) as grp:
            cmat = np.array(grp['correlmatrix'])
            try:
                method = OutlierMethod(grp['correlmatrix'].attrs['method'])
            except KeyError:
                method = OutlierMethod.IQR
            try:
                threshold = float(grp['correlmatrix'].attrs['threshold'])
            except KeyError:
                threshold = 1.5
            fsns = sorted([int(s)
                          for s in grp['curves'] if re.match(r'^\d+$', s)])
            return OutlierTest(method=method, threshold=threshold, correlmatrix=cmat, fsns=fsns)

    def writeOutlierTest(self, group: str, ot: OutlierTest):
        with self.writer(group) as grp:
            if 'correlmatrix' in grp:
                del grp['correlmatrix']
            ds = grp.create_dataset(
                'correlmatrix', data=ot.correlmatrix, compression='lzf', shuffle=True, fletcher32=True)
            ds.attrs['method'] = ot.method.value
            ds.attrs['threshold'] = ot.threshold

    def items(self) -> List[Tuple[str, str]]:
        lis = []
        with self.reader('Samples') as grp:
            for sn in grp:
                for dist in grp[sn]:
                    lis.append((sn, dist))
        return lis

    def readCurves(self, group: str, readall: bool = False) -> Dict[int, Curve]:
        dic = {}
        with self.reader(group) as grp:
            for fsn in grp['allcurves' if readall else 'curves']:
                if not re.match(r'^\d+$', fsn):
                    continue
                dic[int(fsn)] = Curve.fromArray(
                    np.array(grp[f'{"allcurves" if readall else "curves"}/{fsn}']))
        return dic

    def readHeaders(self, group: str, readall: bool = False) -> Dict[int, Header]:
        dic = {}
        with self.reader(group) as grp:
            for fsn in grp['allcurves' if readall else 'curves']:
                if not re.match(r'^\d+$', fsn):
                    continue
                dic[int(fsn)] = self.readHeader(
                    f'{grp.name}/{"allcurves" if readall else "curves"}/{fsn}')
        return dic

    def readHeaderDict(self, group: str) -> Dict[str, Any]:
        with self.reader(group) as grp:
            return dict(**grp.attrs)

    def clear(self):
        for sample in self.samplenames():
            self.removeSample(sample)

    def __contains__(self, item: Tuple[str, str]) -> bool:
        with self.reader('Samples') as grp:
            return (item[0] in grp) and (item[1] in grp[item[0]])

    def getMetaDataField(self, samplename: str, distkey: str, field: Any | str, fsn: int | None = None):
        if isinstance(field, self.MetaDataField):
            field = field.value
        with self.reader(self.getH5Path(samplename, distkey)) as distgrp:
            if fsn is not None:
                fsngroup = [f for f in distgrp['allcurves'] if (re.match(r'^\d+$', f) is not None) and (int(f) == fsn)][0]
                distgrp = distgrp[f'allcurves/{fsngroup}']
            value = distgrp.attrs[field]
            if field + '.err' in distgrp.attrs:
                return value, distgrp.attrs[field + '.err']
            else:
                return value

    def getMetaDataSeries(self, samplename: str, distkey: str, field: Any | str):
        if isinstance(field, self.MetaDataField):
            field = field.value
        with self.reader(self.getH5Path(samplename, distkey)) as distgrp:
            fsngroups = sorted([f for f in distgrp['allcurves'] if re.match(r'^\d+$', f) is not None], key=lambda x:int(x))
            values = []
            for fsngrpname in fsngroups:
                fsngrp = distgrp[f'allcurves/{fsngrpname}']
                value = fsngrp.attrs[field]
                if field + '.err' in fsngrp.attrs:
                    values.append((value, fsngrp.attrs[field + '.err']))
                else:
                    values.append(value)
        return values
