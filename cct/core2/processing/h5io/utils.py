import enum
from typing import Iterator, List, Tuple, Union
import h5py
import re
import numpy as np
import pint
import itertools


def h5_get_string(dataset: h5py.Dataset) -> str:
    """Load the HDF5 dataset as a string"""
    return dataset[()].decode("utf-8")


def h5_get_float(dataset: h5py.Dataset, check_units: str | None = None):
    """Load the HDF5 dataset as a floating point number,
    optionally assuring that it has the required units"""
    if not dataset.shape:
        value = float(dataset[()])
    elif dataset.shape == (1,):
        value = float(dataset[0])
    else:
        raise ValueError(f"Multidimensional dataset: {dataset.shape=}")
    if check_units is not None:
        units = dataset.attrs.get("units", "<units attribute nonexistent>")
        if units == check_units:
            # the units are the same, no need to do anything fancy
            pass
        else:
            # try to convert it
            value = pint.Quantity(value, units).to(check_units).magnitude
    return value


def h5_get_int(dataset: h5py.Dataset):
    """Load the HDF5 dataset as an integer number"""
    if not dataset.shape:
        return int(dataset[()])
    elif dataset.shape == (1,):
        return int(dataset[0])
    else:
        raise ValueError(f"Multidimensional dataset: {dataset.shape=}")


def h5_nxpath_iter(group: h5py.Group, path: str) -> h5py.Group | h5py.Dataset:
    """Recursively iterate the NeXus file hierarchy, yielding groups or datasets

    Paths can either be specified by their true name, e.g.:
        "entry123/raw/credo/pilatus300k",
    or with the Nexus classes:
        "NXentry/raw/NXinstrument/NXdetector".
    
    A special '*' element is a wildcard.

    In the latter case, all groups or datasets whose path matches the pattern are
    yielded.

    Paths are always relative, i.e., trailing slashes are removed from the beginning.
    """
    while path.startswith("/"):
        path = path[1:]
    if not path:
        yield group
        return
    while "//" in path:
        path = path.replace("//", "/")
    if "/" not in path:
        nextpathelem = path
        otherpathelems = ""
    else:
        nextpathelem, otherpathelems = path.split("/", 1)
    if nextpathelem.startswith("NX"):
        for gname in sorted(
            [g for g in group if group[g].attrs.get("NX_class", None) == nextpathelem]
        ):
            yield from h5_nxpath_iter(group[gname], otherpathelems)
    elif nextpathelem == '*':
        for gname in sorted(group):
            yield from h5_nxpath_iter(group[gname], otherpathelems)
    else:
        yield from h5_nxpath_iter(group[nextpathelem], otherpathelems)


def h5_nxpath(group: h5py.Group, path: str, take_first_if_ambiguous: bool = False):
    """Resolve a NeXus path where some of the path elements may be NX class names.

    If `take_first_if_ambiguous` is False, a ValueError is raised if the path is
    non-unique.
    """
    results = list(h5_nxpath_iter(group, path))
    if len(results) > 1 and not take_first_if_ambiguous:
        raise ValueError(f"Ambiguous path {path}")
    return results[0]


def h5_subgroups_of_type(
    group: h5py.Group, nxclass: str, nxdefinition: str | None = None
) -> Iterator[h5py.Group]:
    """Yield subgroups having the NeXus class `nxclass` and (optionally) conform to the
    NeXus application definition `nxdefinition`"""
    for g in group:
        if group[g].attrs.get("NX_class", None) == nxclass:
            if nxdefinition is not None:
                if "definition" not in group[g]:
                    continue
                if group[g]["definition"][()].decode("utf-8") != nxdefinition:
                    continue
            yield group[g]


def h5_copygroup(
    src: h5py.Group,
    dest: h5py.Group | None = None,
    destparent: h5py.Group | None = None,
):
    """
    Copy a HDF5 group into another place, probably into another file. Copying is
    recursive.

    If `dest` is not given, a new group of the same name as `src` is created under
    `destparent`.

    :param src: source group
    :type src: h5py.Group
    :param dest: destination group
    :type dest: h5py.Group | None, optional
    :param destparent: destination parent group, defaults to None
    :type destparent: h5py.Group | None, optional
    :raises ValueError: if destination parent group not given when dest is not given
    """
    if dest is None:
        if destparent is None:
            raise ValueError(
                "Destination parent group must be given if destination is None."
            )
        dest = destparent.require_group(src.name.split("/")[-1])
    dest.attrs.update(src.attrs)
    for key in src:
        if isinstance(src[key], h5py.Group):
            h5_copygroup(src[key], destparent=dest)
        else:
            assert isinstance(src[key], h5py.Dataset)
            dest.create_dataset(key, data=np.array(src[key]))


def h5_extendgroup(src: h5py.Group, dest: h5py.Group):
    """
    Extends an already existing HDF5 group with new elements

    A typical use case of this function is when several experiments need to be
    collected into one entry. Some datasets, which are expected to change
    between individual experiments, will have another dimension added, and new
    values appended along this dimension.

    Processing is done recursively.

    :param src: Source group
    :type src: h5py.Group
    :param dest: Destination group
    :type dest: h5py.Group
    :raises KeyError: If a key is found in `src` which is not present in `dest`.
    :raises ValueError: If no more space is left in a destination dataset or if source and
        destination have different types
    """
    for key in src:
        if key not in dest:
            raise KeyError(f"Unknown key in destination: {src.name}/{key}")
        elif type(src[key]) != type(dest[key]):
            raise ValueError(
                f"Source and destination have different types at {src[key].name}."
            )
        elif isinstance(src[key], h5py.Group):
            # recurse into subgroups
            assert isinstance(dest[key], h5py.Group)
            h5_extendgroup(src[key], dest[key])
        else:
            assert isinstance(src[key], h5py.Dataset)
            assert isinstance(dest[key], h5py.Dataset)
            if len(dest[key].shape) == len(src[key].shape) + 1:
                # the destination has one dimension more. This is the first dimension, which
                # should be either unlimited, or there should be space for at least one more element
                if (dest[key].maxshape[0] != None) and (
                    dest[key].maxshape[0] <= dest[key].shape[0]
                ):
                    raise ValueError(
                        f"No more space in {dest[key].name} to add another element"
                    )
                dest[key].resize(dest[key].shape[0] + 1, axis=0)
                dest[key][
                    (dest[key].shape[0] - 1,)
                    + (slice(None, None, None),) * (len(dest[key].shape) - 1)
                ] = src[key][()]
            else:
                # do nothing: other datasets keep their first value
                pass


class CompareProblem(enum.Enum):
    MISSING_FROM_1 = "Missing from grp1"
    MISSING_FROM_2 = "Missing from grp2"
    DIFFERENT_H5CLASS = "Different HDF5 class"
    DIFFERENT_DTYPE = "Different dtype"
    DIFFERENT_SHAPE = "Different shape"
    DIFFERENT_VALUE_SCALAR_INT = "Different value (scalar integer)"
    DIFFERENT_VALUE_ARRAY_INT = "Different value (integer array)"
    DIFFERENT_VALUE_SCALAR_FLOAT = "Different value (scalar float)"
    DIFFERENT_VALUE_ARRAY_FLOAT = "Different value (float array)"
    DIFFERENT_VALUE_SCALAR_BOOL = "Different value (scalar bool)"
    DIFFERENT_VALUE_ARRAY_BOOL = "Different value (bool array)"
    DIFFERENT_VALUE_STR = "Different value (string)"

def h5_comparedatasets(
        ds1: h5py.Dataset, ds2: h5py.Dataset, float_tolerance: float = 1e7
):
    if ds1.dtype.name != ds2.dtype.name:
        return CompareProblem.DIFFERENT_DTYPE
    elif ds1.shape != ds2.shape:
        return CompareProblem.DIFFERENT_SHAPE
    else:
        if re.match("u?int(8|16|32|64|128)", ds1.dtype.name):
            # integer
            if (ds1.shape == ()) and (ds1[()] - ds2[()]) != 0:
                return CompareProblem.DIFFERENT_VALUE_SCALAR_INT
            elif np.abs(np.array(ds1) - np.array(ds2)).sum() > 0:
                return CompareProblem.DIFFERENT_VALUE_ARRAY_INT
        elif re.match("float(8|16|32|64|128)", ds1.dtype.name):
            if (ds1.shape == ()) and np.abs(
                ds1[()] - ds2[()]
            ) > float_tolerance:
                return CompareProblem.DIFFERENT_VALUE_SCALAR_FLOAT
            elif (
                np.nanmax(np.abs(np.array(ds1) - np.array(ds2)))
                > float_tolerance
            ):
                return CompareProblem.DIFFERENT_VALUE_ARRAY_FLOAT
        elif ds1.dtype.name == "bool":
            if (ds1.shape == ()) and ds1[()] != ds2[()]:
                return CompareProblem.DIFFERENT_VALUE_SCALAR_BOOL
            elif np.sum(np.array(ds1) != np.array(ds2)):
                return CompareProblem.DIFFERENT_VALUE_ARRAY_BOOL
        elif ds1.dtype.name == "object":
            if ds1[()].decode("utf-8") != ds2[()].decode("utf-8"):
                return CompareProblem.DIFFERENT_VALUE_STR
    

def h5_comparegroups(
    grp1: h5py.Group, grp2: h5py.Group, float_tolerance: float = 1e-7
) -> List[Tuple[str, CompareProblem]]:
    """
    Recursively compare two HDF5 groups

    :param grp1: first HDF5 group
    :type grp1: h5py.Group
    :param grp2: second HDF5 group
    :type grp2: h5py.Group
    :param float_tolerance: float comparison tolerance, defaults to 1e-7
    :type float_tolerance: float, optional
    :return: list of (path, problem) tuples
    :rtype: List[Tuple[str, CompareProblem]]
    """
    problems = []
    for key in set(grp1.keys()) | set(grp2.keys()):
        if key not in grp1:
            problems.append((key, CompareProblem.MISSING_FROM_1))
        elif key not in grp2:
            problems.append((key, CompareProblem.MISSING_FROM_2))
        elif type(grp1[key]) is type(grp2[key]):
            problems.append((key, CompareProblem.DIFFERENT_H5CLASS))
        elif isinstance(grp1[key], h5py.Group):
            assert isinstance(grp2[key], h5py.Group)
            problems.extend(h5_comparegroups(grp1[key], grp2[key]))
        elif problem := h5_comparedatasets(grp1[key], grp2[key]):
            if problem is not None:
                problems.append((key, problem))
    return problems


def h5_finddatatypes(grp):
    dts = {}
    for g in grp:
        if isinstance(grp[g], h5py.Dataset):
            dtype = grp[g].dtype
            if dtype.name not in dts:
                dts[dtype.name] = []
            dts[dtype.name].append(grp[g].name)
        elif isinstance(grp[g], h5py.Group):
            for key, value in h5_finddatatypes(grp[g]).items():
                if key not in dts:
                    dts[key] = value
                else:
                    dts[key].extend(value)
        else:
            raise TypeError(grp)
    return dts

def h5_iter(group) -> Iterator[Union[h5py.Group, h5py.Dataset]]:
    """Iterate over a HDF5 subtree, recursively."""
    yield group
    if isinstance(group, h5py.Group):
        for key in group:
            yield from h5_iter(group[key])

def h5_name_format_match(h5element: Union[h5py.Group, h5py.Dataset], path: str, subtree_match: bool=False, verbose: bool = False):
    while '//' in path:
        path = path.replace('//', '/')
    name = h5element.name[1:]  # strip the leading '/'
    path = path[1:]
    currentgroup = h5element.file
    if verbose: print(f'Matching {h5element.name} with pattern {path}')
    if verbose: print(f'{name.split("/")=}')
    if verbose: print(f'{path.split("/")=}')
    for n, p in itertools.zip_longest(name.split('/'), path.split('/')):
        if (p is None) and subtree_match:
            return True
        if (n is None) or (p is None):
            # either is None
            if verbose: print(f'{n=}, {p=}, either is None, breaking')
            return False
        currentgroup = currentgroup[n]
        if n == p:
            # matches
            if verbose: print(f'{n=}, {p=}, exact match')
            continue 
        elif p.startswith('NX') and currentgroup.attrs.get('NX_class', 'no_nexus_class') == p:
            # matches the NX_class
            if verbose: print(f'{n=}, {p=}, NXclass match')
            continue
        elif p == '*':
            # wild-card match
            if verbose: print(f'{n=}, {p=}, wildcard match')
            continue
        elif p.startswith('^') and p.endswith('$') and ((m:=re.match(p, n)) is not None):
            if verbose: print(f'RegEx match: {p=}, {n=}, {m=}')
            continue
        else:
            if verbose: print(f'Mismatch {p=}, {n=}')
            return False
    else:
        if verbose: print('All matched.')
        return True
