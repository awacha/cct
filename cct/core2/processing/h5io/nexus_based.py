import datetime
from multiprocessing.synchronize import Lock
import re
import time
import traceback
from typing import List, Tuple, Iterator, Union, Dict, Any

import dateutil
import numpy as np

from ...dataclasses.exposure import Exposure, Header, Curve
from ..calculations.outliertest import OutlierMethod, OutlierTest

from .h5io import ProcessingH5File
from .utils import h5_copygroup, h5_get_float, h5_get_int, h5_get_string, h5_nxpath, h5_subgroups_of_type, h5_nxpath_iter, h5_comparegroups, h5_iter, h5_name_format_match, h5_comparedatasets
import h5py
import enum

import logging
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

class NexusIngestStrategy(enum.Enum):
    KeepFirst = 0
    KeepLast = 1
    Collect = 2
    EnsureUnique = 3
    CollectUnique = 4
    HandledSeparately = 5
    Relink = 6
    Skip = 7

class ProcessingH5FileNeXus(ProcessingH5File):
    distance_decimals = 2

    def __init__(self, filename: str, lock: Lock | None = None):
        super().__init__(filename, lock)

    def samplenames(self) -> List[str]:
        with self.reader() as h5:
            return [h5_get_string(sn) for sn in h5_nxpath_iter(h5, 'NXentry/NXsample/name')]

    def addSample(self, samplename: str, category: str) -> str:
        if samplename in self.samplenames():
            raise ValueError(
                f"Entry already exists for sample {samplename}"
            )
        with self.writer() as h5:
            entries = [int(m["idx"]) for m in [re.match(r"/entry(?P<idx>\d+)", egrp.name) for egrp in h5_nxpath_iter(h5, 'NXentry')] if m is not None]
            maxentryindex = max(entries, default=0)
            grp = h5.create_group(f"entry{maxentryindex+1}")
            grp.attrs.update({"NX_class": "NXentry"})
            grp.create_dataset("title", data=samplename)
            sgrp = grp.create_group("sample")
            sgrp.attrs.update({"NX_class": "NXsample", "canSAS_class": "SASsample"})
            sgrp.create_dataset("name", data=samplename)
            sgrp.create_dataset("type", data=category)
            return f'entry{maxentryindex+1}'

    def _entryname_for_sample(self, samplename: str) -> str:
        with self.reader() as h5:
            return [sn.name.split('/')[1] for sn in h5_nxpath_iter(h5, 'NXentry/NXsample/name') if h5_get_string(sn) == samplename][0]
    
    def _subentryname_for_distance(self, samplename: str, distance: str | float):
        distance = self._normalize_distance(distance)
        with self.reader() as h5:
            for subentry in h5_nxpath_iter(h5, 'NXentry/NXsubentry'):
                if h5_get_string(h5_nxpath(subentry.parent, 'NXsample/name')) == samplename and subentry.name.split('/')[-1] == distance:
                    return subentry.name
        raise KeyError(f'Subentry not found for sample {samplename}, distance {distance}.')

    def getH5Path(self, samplename: str, distance: str | float):
        if distance is None:
            return self._entryname_for_sample(samplename)
        else:
            return self._subentryname_for_distance(samplename, distance)

    def removeSample(self, samplename: str):
        entryname = self._entryname_for_sample(samplename)
        with self.writer() as h5:
            del h5[entryname]

    def distances(self, samplename: str) -> List[float]:
        distances = []
        for d in self.distancekeys(samplename):
            try:
                distances.append(float(d.replace('_', '.')))
            except ValueError:
                pass
        return distances

    def distancekeys(self, samplename: str, onlynumeric: bool = True) -> List[str]:
        entryname = self._entryname_for_sample(samplename)
        with self.reader() as h5:
            distancekeys = [subentry.name.split('/')[-1] for subentry in h5_nxpath_iter(h5, f'/{entryname}/NXsubentry')]
        if onlynumeric:
            return [
                dk
                for dk in distancekeys
                if re.match(r"\d+([._]\d+)?", dk)
            ]
        return distancekeys

    def _normalize_distance(self, distance: str | float):
        if not isinstance(distance, str):
            return f'{distance:.{self.distance_decimals}f}'.replace('.', '_')
        else:
            return distance.replace('.', '_')

    def addDistance(self, samplename: str, distance: float | str) -> Tuple[str, str]:
        entryname = self._entryname_for_sample(samplename)
        distance = self._normalize_distance(distance)
        with self.writer() as h5:
            egrp = h5[entryname]
            dgrp = egrp.require_group(distance)
            dgrp.attrs["NX_class"] = "NXsubentry"
            dgrp.attrs["canSAS_class"] = "SASentry"
            dgrp.create_dataset("definition", data="NXcanSAS")
            return dgrp.name

    def removeDistance(self, samplename: str, distance: float | str):
        entryname = self._entryname_for_sample(samplename)
        distance = self._normalize_distance(distance)
        with self.writer() as h5:
            del h5[entryname][distance]

    def pruneSamplesWithNoDistances(self):
        with self.writer() as h5:
            for egrp in self.subgroups_of_nxclass(h5, "NXentry"):
                if not list(self.subgroups_of_nxclass(egrp, "NXsubentry")):
                    del h5[egrp.name]

    def ingestNeXus(self, nexusfile: str, overwrite_if_present: bool = False, compression:str="gzip", compression_opts=5):
        """Import the contents of a NeXus file written by nxsbegin/nxsend macros and the data reduction pipeline.
        
        
        """
        defaultingeststrategy = NexusIngestStrategy.EnsureUnique
        # in the ingest strategy list, paths are relative to the "processed" NXsubentry
        ingeststrategies = {
            "NXinstrument/NXbeamstop/*": NexusIngestStrategy.EnsureUnique,
            "NXinstrument/NXpositioner/acceleration_time": NexusIngestStrategy.Collect,
            "NXinstrument/NXpositioner/target_value": NexusIngestStrategy.Collect,
            "NXinstrument/NXpositioner/value": NexusIngestStrategy.Collect,
            "NXinstrument/NXpositioner/velocity": NexusIngestStrategy.Collect,
            "NXinstrument/NXsensor/high_trip_value": NexusIngestStrategy.Collect,
            "NXinstrument/NXsensor/low_trip_value": NexusIngestStrategy.Collect,
            "NXinstrument/NXsensor/value": NexusIngestStrategy.Collect,
            "NXinstrument/NXsource/current": NexusIngestStrategy.Collect,
            "NXinstrument/NXsource/incident_wavelength": NexusIngestStrategy.EnsureUnique,
            "NXinstrument/NXsource/incident_wavelength_errors": NexusIngestStrategy.EnsureUnique,
            "NXinstrument/NXsource/power": NexusIngestStrategy.Collect,
            "NXinstrument/NXsource/voltage": NexusIngestStrategy.Collect,
            "NXinstrument/NXbeam/distance": NexusIngestStrategy.EnsureUnique,
            "NXinstrument/NXbeam/flux": NexusIngestStrategy.Collect,
            "NXinstrument/NXbeam/flux_errors": NexusIngestStrategy.Collect,
            "NXinstrument/NXmonochromator/*": NexusIngestStrategy.EnsureUnique,
            "NXinstrument/name": NexusIngestStrategy.EnsureUnique,
            "NXinstrument/NXcite/*": NexusIngestStrategy.EnsureUnique,
            "NXinstrument/NXdetector/beam_center_x": NexusIngestStrategy.EnsureUnique,
            "NXinstrument/NXdetector/beam_center_y": NexusIngestStrategy.EnsureUnique,
            "NXinstrument/NXdetector/beam_center_x_errors": NexusIngestStrategy.EnsureUnique,
            "NXinstrument/NXdetector/beam_center_y_errors": NexusIngestStrategy.EnsureUnique,
            "NXinstrument/NXdetector/count_time": NexusIngestStrategy.Collect,
            "NXinstrument/NXdetector/data": NexusIngestStrategy.Collect,
            "NXinstrument/NXdetector/pixel_mask": NexusIngestStrategy.Collect,
            "NXinstrument/NXtransformations/*": NexusIngestStrategy.EnsureUnique,
            "NXdata/I": NexusIngestStrategy.Collect,
            "NXdata/Idev": NexusIngestStrategy.Collect,
            "NXdata/mask": NexusIngestStrategy.Collect,
            "NXdata/Q": NexusIngestStrategy.Collect,
            "NXdata/Qdev": NexusIngestStrategy.Collect,
            "definition": NexusIngestStrategy.EnsureUnique,
            "processing_steps/*/date": NexusIngestStrategy.Collect,
            "processing_steps/*/description": NexusIngestStrategy.Collect,
            "processing_steps/absolute_calibration/chi2_red": NexusIngestStrategy.Collect,
            "processing_steps/absolute_calibration/dof": NexusIngestStrategy.Collect,
            "processing_steps/absolute_calibration/factor": NexusIngestStrategy.Collect,
            "processing_steps/absolute_calibration/factor_unc": NexusIngestStrategy.Collect,
            "processing_steps/absolute_calibration/filename": NexusIngestStrategy.Collect,
            "processing_steps/absolute_calibration/flux": NexusIngestStrategy.Collect,
            "processing_steps/absolute_calibration/flux_unc": NexusIngestStrategy.Collect,
            "processing_steps/absolute_calibration/nq": NexusIngestStrategy.Collect,
            "processing_steps/absolute_calibration/qmax": NexusIngestStrategy.Collect,
            "processing_steps/absolute_calibration/qmin": NexusIngestStrategy.Collect,
            "processing_steps/air_absorption_correction/vacuum_pressure": NexusIngestStrategy.Collect,
            "processing_steps/dark_subtraction/dark_cps": NexusIngestStrategy.Collect,
            "processing_steps/dark_subtraction/dark_cps_errors": NexusIngestStrategy.Collect,
            "processing_steps/dark_subtraction/filename": NexusIngestStrategy.Collect,
            "processing_steps/empty_beam_subtraction/filename": NexusIngestStrategy.Collect,
            "processing_steps/mask_update/new_masked_pixels": NexusIngestStrategy.Collect,
            "processing_steps/monitor_normalization/exposure_time": NexusIngestStrategy.Collect,
            "processing_steps/normalize_by_transmission/transmission": NexusIngestStrategy.Collect,
            "processing_steps/normalize_by_transmission/transmission_errors": NexusIngestStrategy.Collect,
            "run": NexusIngestStrategy.Collect,
            "title": NexusIngestStrategy.KeepFirst,
            "collection_description": NexusIngestStrategy.CollectUnique,
            "collection_identifier": NexusIngestStrategy.CollectUnique,
            "end_time": NexusIngestStrategy.Collect,
            "start_time": NexusIngestStrategy.Collect,
            "entry_identifier": NexusIngestStrategy.Collect,
            "experiment_identifier": NexusIngestStrategy.CollectUnique,
            "NXdata/Q": NexusIngestStrategy.EnsureUnique,
            "NXdata/Qdev": NexusIngestStrategy.EnsureUnique,
            "NXmonitor/integral": NexusIngestStrategy.Collect,
            "NXmonitor/preset": NexusIngestStrategy.Collect,
            "NXsample/description": NexusIngestStrategy.EnsureUnique,
            "NXsample/details": NexusIngestStrategy.EnsureUnique,
            "NXsample/name": NexusIngestStrategy.EnsureUnique,
            "NXsample/preparation_date": NexusIngestStrategy.CollectUnique,
            "NXsample/temperature": NexusIngestStrategy.Collect,
            "NXsample/thickness": NexusIngestStrategy.Collect,
            "NXsample/thickness_errors": NexusIngestStrategy.Collect,
            "NXsample/transmission": NexusIngestStrategy.Collect,
            "NXsample/transmission_errors": NexusIngestStrategy.Collect,
            "NXinstrument/NXdetector/start_time": NexusIngestStrategy.HandledSeparately,
            "NXinstrument/NXdetector/stop_time": NexusIngestStrategy.HandledSeparately,
            "NXinstrument/*/depends_on": NexusIngestStrategy.HandledSeparately,
            "NXuser": NexusIngestStrategy.HandledSeparately,
            "NXsample/x_motor": NexusIngestStrategy.Relink,
            "NXsample/y_motor": NexusIngestStrategy.Relink,
            "NXsample/x_position": NexusIngestStrategy.Relink,
            "NXsample/y_position": NexusIngestStrategy.Relink,
        }

        compression_kwargs = {"compression": compression, "compression_opts": compression_opts, "shuffle": True}

        # special handling: 
        #   "NXinstrument/NXdetector/start_time"
        #   "NXinstrument/NXdetector/stop_time"
        #   "NXinstrument/*/depends_on"
        #   "NXuser" groups
        #   adjust softlinks NXsample/x_motor, y_motor, x_position, y_position
        different_handling = {}
        with h5py.File(nexusfile, 'r') as h5in:
            # first check the processed data is already there
            try:
                if h5_nxpath(h5in, 'NXentry/processed/NXdata/I').size == 0:
                    # not yet processed
                    raise IndexError()
            except IndexError:
                # this file has not yet been processed
                print(f'{h5in.filename} has not yet been processed')
                return

            # first create groups for sample and sample-to-detector distance
            samplename = h5_get_string(h5_nxpath(h5in, 'NXentry/NXsample/name'))
            try:
                self.addSample(samplename)
            except ValueError:
                pass
            distance = h5_get_float(h5_nxpath(h5in, 'NXentry/NXinstrument/NXdetector/SDD'), 'mm')
            try:
                distgrpname = self.addDistance(samplename, distance)
            except ValueError:
                distgrpname = self._subentryname_for_distance(samplename, distance)
            # now we have the distance group as well. Write the data
            findstrattime = 0
            with self.writer(distgrpname) as distgrp:
                if ('all_patterns' in distgrp) and ('data' not in distgrp):
                    # the scattering patterns will eventually be written in "all_patterns", which is a NXdata group.
                    # However, in the input files patterns are in the "data" group. For the time of ingestion,
                    # create the 'data' group, as a hard link to the 'all_patterns'
                    distgrp['data'] = distgrp['all_patterns']
                if 'frame_is_bad' not in distgrp:
                    badnessds = distgrp.create_dataset('frame_is_bad', shape=(0,), dtype=bool, maxshape=(None,))
                else:
                    badnessds = distgrp['frame_is_bad']
                procgroup = h5_nxpath(h5in, 'NXentry/processed')
                # find how manyeth repeated measurement of the sample is this.
                if 'run' not in distgrp:
                    distgrp.create_dataset("run", shape=(0,), maxshape=(None,), dtype=procgroup["run"].dtype)
                try:
                    repeatedindex = [i for i in range(distgrp["run"].size) if int(distgrp["run"][i].decode('ascii')) == int(h5_get_string(procgroup["run"]))][0]
                    print(f"Run {procgroup['run'][()]} is already present, as index {repeatedindex}")
                    if not overwrite_if_present:
                        return
                except (ValueError, KeyError, IndexError):
                    print(f"Run {procgroup['run'][()]} is new, this is repeat #{distgrp['run'].size}")
                    repeatedindex = distgrp["run"].size
                # In the following, whenever the item has a "Collect" ingestion strategy, the first dimension of the
                # dataset is extended that there is enough space to accommodate the new data.
                if badnessds.size <= repeatedindex:
                    # the first dataset to extend is the badness flag
                    badnessds = distgrp['frame_is_bad']
                    badnessds.resize(repeatedindex + 1, axis=0)
                    badnessds[repeatedindex] = False
                # now iterate over all elements (groups and datasets) in the HDF5 hierarchy
                for h5element in h5_iter(procgroup):
                    # the name of the element:
                    elementname = h5element.name.split('/')[-1]
                    # the relative path (below the "processed" NXsubentry) of the element
                    relgroupname = '/'.join(h5element.name.split('/')[3:])
                    # find the ingestion strategy. 
                    try:
                        # First try if the ingestion strategy is in the attribute of this entry in the output file
                        t0 = time.monotonic()
                        strat = NexusIngestStrategy[distgrp[relgroupname].attrs['ingeststrategy']]
                        findstrattime += time.monotonic() - t0
                    except (ValueError, KeyError):
                        # if not (probably, because this is the first time this element is added), look it up. This is slower.
                        t0 = time.monotonic()
                        for path, strat in ingeststrategies.items():
                            if h5_name_format_match(h5element, '/NXentry/NXsubentry/'+path):
                                break
                        else:
                            strat = defaultingeststrategy
                        findstrattime += time.monotonic() - t0
                    # Now ingest the data
                    if strat == NexusIngestStrategy.Relink:
                        # If this entry is a soft link in the input file, make it a soft link in the output file, too
                        if isinstance(h5element.parent.get(h5element.name.split('/')[-1], getlink=True), h5py.SoftLink):
                            softlink_in_original = h5element.parent.get(h5element.name.rsplit('/')[-1], getlink=True)
                            if h5_name_format_match(h5in['/'.join(softlink_in_original.path.split('/')[:3])], '/NXentry/NXinstrument'):
                                # this is directly under the main NXentry
                                origrel = '/'.join(softlink_in_original.path.split('/')[2:])
                            elif h5_name_format_match(h5in['/'.join(softlink_in_original.path.split('/')[:3])], '/NXentry/processed'):
                                origrel = '/'.join(softlink_in_original.path.split('/')[3:])
                            else:
                                raise ValueError(f'Cannot categorize softlink path in HDF5: {softlink_in_original.path}')
                            if rel not in distgrp:
                                distgrp[relgroupname] = h5py.SoftLink(distgrp.name+'/'+origrel)
                            continue
                        else:
                            print(f'Warning: ingest strategy is Relink, but element {h5element.name} is not a SoftLink. Going with {defaultingeststrategy.name}')
                            strat = defaultingeststrategy
                    if isinstance(h5element, h5py.Group):
                        # require the presence of this group in the output file and copy all attributes.
                        if not relgroupname:
                            # this is the "processed" NXsubentry itself.
                            grp = distgrp
                        elif strat == NexusIngestStrategy.HandledSeparately:
                            continue
                        else:
                            grp = distgrp.require_group(relgroupname)
                        grp.attrs.update(h5element.attrs)
                        grp.attrs['ingeststrategy'] = strat.name
                    else:
                        # The current element is a dataset
                        assert isinstance(h5element, h5py.Dataset)
                        parentgroupname = '/'.join(h5element.name.split('/')[3:-1])

                        if not parentgroupname:
                            parentgroup = distgrp
                        else:
                            try:
                                parentgroup = distgrp[parentgroupname]
                            except KeyError:
                                # this should not happen
                                assert False
                        if elementname not in parentgroup:
                            # This element is not yet present in the output file. Create it. Either the same 
                            # shape as the original, or another dimension prepended
                            if strat in [
                                NexusIngestStrategy.EnsureUnique, 
                                NexusIngestStrategy.KeepFirst, 
                                NexusIngestStrategy.KeepLast
                                ]:
                                # in this case, data is not to be collected but a single value will be stored
                                # finally. The data is stored at this point, too.
                                ds = parentgroup.create_dataset(
                                    elementname, shape=h5element.shape, dtype=h5element.dtype, 
                                    data=h5element[()], **(compression_kwargs if h5element.shape else {}))
                            elif strat in [NexusIngestStrategy.Collect, NexusIngestStrategy.CollectUnique]:
                                # data from different exposures will be collected, so create an empty array with
                                # an additional dimension prepended to the shape.
                                # The current data is not added yet. This will be done later.
                                ds = parentgroup.create_dataset(
                                    elementname, shape=(0, ) + h5element.shape, dtype=h5element.dtype, 
                                    maxshape = (None, ) + h5element.shape,  **compression_kwargs)
                            elif strat == NexusIngestStrategy.HandledSeparately:
                                # some data require manual intervention
                                if h5_name_format_match(h5element, procgroup.name+'/NXinstrument/NXdetector/^(start|stop)_time$'):
                                    ds = parentgroup.create_dataset(
                                        elementname, shape=(1,), dtype=h5element.dtype, 
                                        maxshape=(None,),  **compression_kwargs)
                                    ds[0] = h5element[()]
                                elif h5_name_format_match(h5element, procgroup.name+'/NXinstrument/*/depends_on'):
                                    rel = distgrp.name[1:]+'/'+h5_get_string(h5element).split('/',1)[1]
                                    ds = parentgroup.create_dataset(elementname, data=rel)
                                else:
                                    raise ValueError(f'HDF5 element {h5element.name} requires manual handling but is not implemented (yet).')
                            else:
                                raise ValueError(f'Invalid strategy: {strat.name}')
                            ds.attrs.update(h5element.attrs)
                            ds.attrs['ingeststrategy'] = strat.name
                        else:
                            ds = parentgroup[elementname]
                        # data for this entry is already present
                        if (strat == NexusIngestStrategy.KeepFirst):
                            # discard the new data, except this is a reload of the very first data
                            if repeatedindex == 0:
                                ds[()] = h5element[()]
                                ds.attrs.update(h5element.attrs)
                        elif (strat == NexusIngestStrategy.KeepLast):
                            ds[()] = h5element[()]
                            ds.attrs.update(h5element.attrs)
                        elif (strat == NexusIngestStrategy.EnsureUnique) and (repeatedindex == 0):
                            ds[()] = h5element[()]
                            ds.attrs.update(h5element.attrs)
                        elif strat in [NexusIngestStrategy.Collect, NexusIngestStrategy.CollectUnique]:
                            newunits = h5element.attrs.get('units', default=None)
                            if ('units' in ds.attrs) and (ds.attrs['units'] != newunits):
                                raise ValueError(f'Measurement units mismatch: expected "{ds.attrs["units"]}", got "{newunits}"')
                            # now ensure that we have enough space
                            if strat == NexusIngestStrategy.Collect:
                                if repeatedindex >= ds.shape[0]:
                                    ds.resize(repeatedindex+1, axis=0)
                                ds[(repeatedindex,) + (slice(None, None, None),)*(len(ds.shape)-1)] = h5element[()]
                            elif strat == NexusIngestStrategy.CollectUnique:
                                for i in range(ds.shape[0]):
                                    if h5element[()] == ds[(i,)+ (slice(None, None, None),)*(len(ds.shape)-1)]:
                                        # this value is already present
                                        break
                                else:
                                    # the value is not present yet, append it
                                    ds.resize(ds.shape[0]+1, axis=0)
                                    ds[(ds.shape[0]-1, ) + (slice(None, None, None), )*(len(ds.shape)-1)] = h5element[()]
                            else:
                                assert False
                        elif strat == NexusIngestStrategy.EnsureUnique:
                            if (problem := h5_comparedatasets(ds, h5element)) is not None:
                                raise ValueError(f'Value {h5element.name} is different: {problem.value} (original value: {ds[()]}. New value: {h5element[()]})')
                        elif strat in [NexusIngestStrategy.HandledSeparately]:
                            if h5_name_format_match(h5element, procgroup.name+'/NXinstrument/NXdetector/^(start|stop)_time$'):
                                if ds.shape[0] <= repeatedindex:
                                    ds.resize(repeatedindex+1, axis=0)
                                t = (dateutil.parser.parse(h5element.attrs['start']) - dateutil.parser.parse(ds.attrs['start'])).total_seconds()
                                ds[repeatedindex] = t
                            elif h5_name_format_match(h5element, procgroup.name+'/NXinstrument/*/depends_on'):
                                rel = distgrp.name[1:]+'/'+h5_get_string(h5element).split('/',1)[1]
                                if h5_get_string(parentgroup[elementname]) != rel:
                                    raise ValueError(f'Different values for transformation reference entry: {h5element.name}: expected  {h5_get_string(parentgroup[elementname])}, got {rel}.')
                            else:
                                raise ValueError(f'HDF5 element {h5element.name} requires manual handling but is not implemented (yet).')
                        else:
                            raise ValueError(f'Invalid strategy: {strat.name}')
                if 'all_patterns' not in distgrp:
                    distgrp['all_patterns'] = distgrp['data']
                if 'data' in distgrp:
                    del distgrp['data']
                distgrp.attrs['default'] = 'all_patterns'
                if 'run' not in distgrp['all_patterns']:
                    distgrp['all_patterns']['run'] = h5py.SoftLink(distgrp['run'].name)
                distgrp['all_patterns'].attrs.update({'I_axes': ["run", "Q", "Q"], "Q_indices":[0, 1], "mask":"mask", "Mask_indices": [0, 1], "NX_class": "NXdata", "canSAS_class": "SASdata", "signal": "I", })
                n_runs = h5_nxpath(distgrp, 'run').size
                for h5object in h5_iter(distgrp):
                    if h5object.attrs.get('ingeststrategy') == 'Collect':
                        if isinstance(h5object, h5py.Dataset) and h5object.shape[0] != n_runs:
                            print(f'Error: {h5object.name}.shape[0] should be {n_runs}, instead {h5object.shape[0]}.')
            print(f'Time required for finding out ingest strategy: {findstrattime*1000:.3f} ms')
        return distgrpname
    
    def samples_distances(self) -> Iterator[Tuple[str, str]]:
        with self.reader('/') as h5:
            for grp in h5_nxpath_iter(h5, 'NXentry/NXsubentry'):
                entry, subentry = grp.name.split('/')[1:]
                samplename = h5_get_string(h5_nxpath(grp, 'NXsample/name'))
                yield samplename, subentry


    def writeExposure(self, exposure: Exposure, group: h5py.Group):
        raise NotImplementedError()

    def writeHeader(self, header: Header, group: h5py.Group):
        raise NotImplementedError()

    def writeCurve(self, curve: Union[Curve, np.ndarray], group: h5py.Group, name: str):
        raise NotImplementedError()

    def readCurve(self, path: str, curvesubpath="curve") -> Curve:
        if curvesubpath is not None:
            path = path + "/" + curvesubpath
        with self.reader(path) as h5:
            return Curve.fromVectors(
                h5['Q'][()],
                h5["I"][()],
                h5["Idev"][()],
                h5["Qdev"][()],
                h5["binarea"][()] if "binarea" in h5 else None,
                h5["pixels"][()] if "pixels" in h5 else None,
            )

    def readCurveFSN(self, fsn: int, samplename: str | None = None, distkey: str | None = None) -> Curve:
        fsn = int(fsn)  # sometimes it is a string
        if (samplename is not None) and (distkey is not None):
            grpname = self.getH5Path(samplename, distkey)
            with self.reader(grpname) as grp:
                for i, r in enumerate(grp['run'][()]):
                    if int(r.decode('utf-8')) == fsn:
                        return Curve.fromVectors(
                            grp['all_curves/Q'][i, :],
                            grp['all_curves/I'][i, :],
                            grp['all_curves/Idev'][i, :],
                            grp['all_curves/Qdev'][i, :],
                            grp['all_curves/binarea'][i, :] if "binarea" in grp["all_curves"] else None,
                            grp['all_curves/pixels'][i, :] if "pixels" in grp["all_curves"] else None,
                        )
                else:
                    raise IndexError(f'Curve with FSN {fsn} not present in data for sample {samplename}, distance {distkey}. Runs: {grp["run"][()]}')
        else:
            return super().readCurveFSN(fsn, samplename, distkey)

    def readHeader(self, group: str) -> Header:
        raise NotImplementedError()

    def readExposure(self, group: str) -> Exposure:
        raise NotImplementedError()

    def readOutlierTest(self, group: str) -> OutlierTest:
        with self.reader(group) as h5:
            outliertestgrp = h5_nxpath(h5, "processing_steps/outliertest")
            method = [otm for otm in OutlierMethod if otm.value == h5_get_string(outliertestgrp["method"])][0]
            threshold = h5_get_float(outliertestgrp["threshold"])
            fsns = outliertestgrp["fsns"][()]
            verdict = outliertestgrp["verdict"][()]
            correlmatrix = outliertestgrp["corrmat"][()]
            return OutlierTest(method, threshold, correlmatrix=correlmatrix, fsns=fsns[~verdict])

    def writeOutlierTest(self, group: str, ot: OutlierTest):
        raise NotImplementedError()

    def items(self) -> List[Tuple[str, str]]:
        lis = []
        for sn in self.samplenames():
            for dist in self.distancekeys(sn, False):
                lis.append((sn, dist))
        return lis

    def readCurves(self, group: str, readall: bool = False) -> Dict[int, Curve]:
        dic = {}
        with self.reader(group) as h5:
            is_bad = h5["frame_is_bad"][()]
            runs = h5["run"][()]
            for i in range(h5["all_curves/Q"].shape[0]):
                if is_bad[i] and not readall:
                    continue
                dic[int(runs[i].decode("ascii"))] = Curve.fromVectors(
                    h5["all_curves/Q"][i, :],
                    h5["all_curves/I"][i, :],
                    h5["all_curves/Idev"][i, :],
                    h5["all_curves/Qdev"][i, :],
                    h5["all_curves/binarea"][i, :] if "binarea" in h5["all_curves"] else None,
                    h5["all_curves/pixel"][i, :] if "pixel" in h5["all_curves"] else None,

                )
        return dic


    def readHeaders(self, group: str, readall: bool = False) -> Dict[int, Header]:
        raise NotImplementedError()

    def readHeaderDict(self, group: str) -> Dict[str, Any]:
        raise NotImplementedError()

    def clear(self):
        for sample in self.samplenames():
            self.removeSample(sample)

    def __contains__(self, item: Tuple[str, str]) -> bool:
        return item in self.items()

    def find_fsn(self, fsn: int) -> Tuple[str, int]:
        """Return the HDF5 path and a run index for a given FSN"""
        with self.reader(None) as reader:
            for runnumbers in h5_nxpath_iter(reader, 'NXentry/NXsubentry/run'):
                for i, ri in enumerate(runnumbers[()]):
                    if int(ri) == fsn:
                        return runnumbers.parent.name, i
            else:
                raise IndexError(f'Could not find exposure with fsn {fsn} in the NeXus HDF5 file.')

    @classmethod
    def _nxpath_for_metadata_field(cls, field: ProcessingH5File.MetaDataField) -> str | Tuple[str, str]:
        match field:
            case cls.MetaDataField.Distance:
                return 'NXinstrument/NXdetector/SDD', 'NXinstrument/NXdetector/SDD_errors'
            case cls.MetaDataField.DarkCps:
                return 'processing_steps/dark_subtraction/dark_cps', 'processing_steps/dark_subtraction/dark_cps_errors'
            case cls.MetaDataField.Wavelength:
                return 'NXinstrument/NXsource/incident_wavelength', 'NXinstrument/NXsource/incident_wavelength_errors'
            case cls.MetaDataField.ExposureTime:
                return 'NXmonitor/integral'
            case cls.MetaDataField.AbsIntFactor:
                return 'processing_steps/absolute_calibration/factor', \
                    'processing_steps/absolute_calibration/factor_unc'
            case cls.MetaDataField.BeamPosRow:
                return 'NXinstrument/NXdetector/beam_center_y', 'NXinstrument/NXdetector/beam_center_y_errors'
            case cls.MetaDataField.BeamPosCol:
                return 'NXinstrument/NXdetector/beam_center_x', 'NXinstrument/NXdetector/beam_center_x_errors'
            case cls.MetaDataField.Flux:
                return 'processing_steps/absolute_calibration/flux', 'processing_steps/absolute_calibration/flux_unc'
            case cls.MetaDataField.SampleX:
                return 'NXsample/x_position'
            case cls.MetaDataField.SampleY:
                return 'NXsample/y_position'
            case cls.MetaDataField.Temperature:
                return 'NXsample/temperature'
            case cls.MetaDataField.Thickness:
                return 'NXsample/thickness', 'NXsample/thickness_errors'
            case cls.MetaDataField.Transmission:
                return 'NXsample/transmission', 'NXsample/transmission_errors'
            case cls.MetaDataField.Vacuum:
                return 'NXinstrument/chamber_vacuum/value'
            case cls.MetaDataField.Title:
                return 'NXsample/name'
            case cls.MetaDataField.SampleCategory:
                return 'NXsample/type'
            case cls.MetaDataField.FSN:
                return 'run'
            case cls.MetaDataField.Project:
                return 'collection_identifier'
            case cls.MetaDataField.Prefix:
                return 'crd'
            case cls.MetaDataField.AbsIntQmin:
                return 'processing_steps/absolute_calibration/qmin'
            case cls.MetaDataField.AbsIntQmax:
                return 'processing_steps/absolute_calibration/qmax'
            case cls.MetaDataField.AbsIntDoF:
                return 'processing_steps/absolute_calibration/dof'
            case cls.MetaDataField.AbsIntChi2:
                return 'processing_steps/absolute_calibration/chi2_red'
            case cls.MetaDataField.Date:
                return 'end_time'
            case cls.MetaDataField.StartDate:
                return 'start_time'
            case cls.MetaDataField.EndDate:
                return 'end_time'
            case cls.MetaDataField.DistanceDecrease | cls.MetaDataField.FSNAbsIntRef | \
                cls.MetaDataField.FSNDark | cls.MetaDataField.FSNEmptyBeam | \
                    cls.MetaDataField.MaskName | cls.MetaDataField.UserName:
                raise ValueError('This field is not present in the NeXus file structure')
            case _:
                raise ValueError('This field is not present in the NeXus file structure')

    def getMetaDataField(self, samplename: str, distkey: str, field: Any | str, fsn: int | None = None):
        with self.reader(self.getH5Path(samplename, distkey)) as distgrp:
            paths = self._nxpath_for_metadata_field(field)
            if fsn is None:
                match field:
                    case self.MetaDataField.Distance | self.MetaDataField.Wavelength | self.MetaDataField.BeamPosRow | \
                            self.MetaDataField.BeamPosCol | self.MetaDataField.Title | self.MetaDataField.SampleCategory:
                        take = 'unique'
                    case self.MetaDataField.FSN | self.MetaDataField.Project | self.MetaDataField.Prefix | \
                            self.MetaDataField.StartDate | self.MetaDataField.Date:
                        take = 'first'
                    case self.MetaDataField.ExposureTime:
                        take = 'sum'
                    case self.MetaDataField.EndDate:
                        take = 'last'
                    case self.MetaDataField.AbsIntFactor | self.MetaDataField.Flux | self.MetaDataField.SampleX | \
                            self.MetaDataField.SampleY | self.MetaDataField.Temperature | \
                            self.MetaDataField.Transmission | \
                            self.MetaDataField.Thickness | self.MetaDataField.Vacuum | self.MetaDataField.DarkCps | \
                            self.MetaDataField.AbsIntQmin | self.MetaDataField.AbsIntQmax | self.MetaDataField.AbsIntDoF |\
                            self.MetaDataField.AbsIntChi2:
                        take = 'mean'
                    case self.MetaDataField.DistanceDecrease | self.MetaDataField.FSNAbsIntRef | \
                        self.MetaDataField.FSNDark | self.MetaDataField.FSNEmptyBeam | self.MetaDataField.MaskName | \
                            self.MetaDataField.UserName:
                        raise ValueError('This field is not present in the NeXus file structure')
                    case _:
                        raise ValueError('This field is not present in the NeXus file structure')
            else:
                take = 'fsn'
            if isinstance(paths, tuple):
                ds_values = h5_nxpath(distgrp, paths[0])
                ds_errors = h5_nxpath(distgrp, paths[1])
            else:
                ds_values = h5_nxpath(distgrp, paths)
                ds_errors = None
            if not ds_values.shape and (ds_errors is None):
                retval = ds_values[()]
            elif not ds_values.shape and not ds_errors.shape:
                retval = ds_values[()], ds_errors[()]
            elif take == 'fsn':
                runs = h5_nxpath(distgrp, 'run')[()]
                for i, r in enumerate(runs):
                    if int(r.decode("utf-8")) == fsn:
                        if ds_errors is not None:
                            retval = ds_values[i], ds_errors[i]
                        else:
                            retval = ds_values[i]
            elif take == 'first':
                if ds_errors is not None:
                    retval = ds_values[0], ds_errors[0]
                else:
                    retval = ds_values[0]
            elif take == 'last':
                if ds_errors is not None:
                    retval = ds_values[-1], ds_errors[-1]
                else:
                    retval = ds_values[-1]
            elif take == 'sum':
                if ds_errors is not None:
                    retval = np.sum(ds_values[()]), np.sum(ds_errors[()]**2)**0.5
                else:
                    retval = np.sum(ds_values[()])
            elif take == 'mean':
                if ds_errors is not None:
                    retval = np.mean(ds_values[()]), np.mean(ds_errors[()]**2)**0.5
                else:
                    retval = np.mean(ds_values[()])
            elif take == 'unique':
                if bool(ds_values.shape) and (len(set(ds_values[()])) > 1):
                    raise ValueError(f'Data for metadata field {field.name} is not unique: {ds_values[()]}')
                if (ds_errors is not None) and len(set(ds_errors[()])) > 1:
                    raise ValueError(f'Uncertainty data for metadata field {field.name} is not unique: {ds_errors[()]}')
                if ds_errors is not None:
                    retval = ds_values[0], ds_errors[0]
                else:
                    retval = ds_values[0]
        # now do some post-processing
        if isinstance(retval, bytes):
            # decode bytes to unicode string
            retval = retval.decode("utf-8")
        if isinstance(retval, str) and re.match(
                r'^\d{4}-\d{2}-\d{2}([ T]\d{2}:\d{2}:\d{2}(\.\d+)?)?([+-]\d{2}:\d{2})?$', retval):
            # parse a timestamp
            retval = dateutil.parser(retval)
        return retval

    def getMetaDataSeries(self, samplename: str, distkey: str, field: Any | str):
        with self.reader(self.getH5Path(samplename, distkey)) as distgrp:
            paths = self._nxpath_for_metadata_field(field)
            if isinstance(paths, tuple):
                values = h5_nxpath(distgrp, paths[0])[()]
                errors = h5_nxpath(distgrp, paths[1])[()]
                lis = list(zip(values, errors))
            else:
                lis = list(h5_nxpath(distgrp, paths))
        match field:
            case self.MetaDataField.FSN:
                lis = [int(f) for f in lis]
            case _:
                pass
        return lis
