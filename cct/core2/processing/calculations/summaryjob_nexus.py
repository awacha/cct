"""A class representing a processing job: for one sample and one sample-to-detector distance"""

import datetime
import multiprocessing
import time
import traceback
from multiprocessing.synchronize import Lock
from typing import List, Any, Set, Optional

import numpy as np

from .backgroundprocess import (
    BackgroundProcessNeXus,
    Results,
    BackgroundProcessError,
    UserStopException,
)
from .outliertest import OutlierMethod, OutlierTest
from ..h5io.nexus_based import ProcessingH5FileNeXus
from ..h5io import utils as h5ioutils
from ...algorithms.matrixaverager import ErrorPropagationMethod, MatrixAverager
from ...algorithms.radavg import autoq, radavg
from ...algorithms.correlmatrix import correlmatrix_cython
from ...dataclasses import Header, Exposure, Curve
from ...dataclasses.exposure import QRangeMethod


class SummaryError(BackgroundProcessError):
    pass


class SummaryJobResults(Results):
    time_loadheaders: float = 0
    time_loadexposures: float = 0
    time_outlierdetection: float = 0
    time_averaging: float = 0
    time_averaging_header: float = 0
    time_averaging_exposures: float = 0
    time_averaging_curves: float = 0
    time_output: float = 0
    time_output_waitforlock: float = 0
    time_output_write: float = 0
    badfsns: Set[int] = None
    newbadfsns: Set[int] = None
    jobid: Any

    def __init__(self, jobid: Any):
        super().__init__(jobid)
        self.badfsns = set()
        self.newbadfsns = set()


class SummaryJobNeXus(BackgroundProcessNeXus):
    """A separate process for processing (summarizing, azimuthally averaging, finding outliers) of a range
    of exposures belonging to the same sample, measured at the same sample-to-detector distance.

    This version of the class is developed for the canSAS NeXus format.
    """

    outliermethod: OutlierMethod
    outlierthreshold: float
    cormatLogarithmic: bool
    ierrorprop: ErrorPropagationMethod
    qerrorprop: ErrorPropagationMethod
    h5io: ProcessingH5FileNeXus

    curves: np.ndarray
    intensities2D: np.ndarray
    uncertainties2D: np.ndarray
    masks2D: np.ndarray
    goodindex: np.ndarray
    outliertest: OutlierTest
    averagedHeader: Header
    averagedExposure: Exposure
    averagedCurve: Curve
    reintegratedCurve: Curve
    qcount: int
    qrangemethod: QRangeMethod
    h5path: str

    result: SummaryJobResults

    def __init__(
        self,
        jobid: Any,
        h5file: str,
        h5lock: Lock,
        stopEvent: multiprocessing.synchronize.Event,
        messagequeue: multiprocessing.queues.Queue,
        ierrorprop: ErrorPropagationMethod,
        qerrorprop: ErrorPropagationMethod,
        outliermethod: OutlierMethod,
        outlierthreshold: float,
        outlierlogcormat: bool,
        qrangemethod: QRangeMethod,
        qcount: int,
        bigmemorymode: bool,
    ):
        super().__init__(jobid, h5file, h5lock, stopEvent, messagequeue)
        self.ierrorprop = ierrorprop
        self.qerrorprop = qerrorprop
        self.outliermethod = outliermethod
        self.outlierthreshold = outlierthreshold
        self.cormatLogarithmic = outlierlogcormat
        self.qrangemethod = qrangemethod
        self.qcount = qcount
        self.bigmemorymode = bigmemorymode
        self.result = SummaryJobResults(jobid)
        self.h5path = self.h5io.getH5Path(jobid[1], jobid[2])
        with self.h5io.reader(self.h5path) as h5:
            fsns = [int(r.decode('ascii')) for r in h5ioutils.h5_nxpath(h5, 'run')[()]]
            is_bad = h5ioutils.h5_nxpath(h5, 'frame_is_bad')[()]
            badfsns = [f for f, b in zip(fsns, is_bad) if b]

        self.result.badfsns = set(badfsns)



    def _azimuthalaverage(self):
        t0 = time.monotonic()
        aveconfig = {
            "intensity_error_propagation": self.ierrorprop.name,
            "q_error_propagation": self.qerrorprop.name,
            "qcount": self.qcount if self.qcount > 0 else -1,
            "qrangemethod": self.qrangemethod.name,
        }
        self.sendProgress("Waiting for HDF5 lock (before azimuthal averaging)...", total=0, current=0)
        with self.h5io.writer(self.h5path) as distgrp:
            self.sendProgress("Preparing for azimuthal averaging...", total=0, current=0)
            try:
                ac = distgrp["all_curves"]
                if any([x not in ac for x in ["Q", "Qdev", "I", "Idev", "pixels", "binarea", "run"]]):
                    print(f'Deleting incomplete all_curves group {ac.name}')
                    del distgrp["all_curves"]
                    raise KeyError("all_curves")
            except KeyError:
                print(f'Creating all_curves group in {distgrp.name}')
                ac = distgrp.create_group("all_curves")
            else:
                if any(
                    [
                        ac.attrs.get(key, None) != value
                        for key, value in aveconfig.items()
                    ]
                ):
                    del distgrp["all_curves"]
                    ac = distgrp.create_group("all_curves")

            # fetch some geometry parameters

            # get a common mask. Per the NXcanSAS application definition, True means masked, False means good pixels.
            # The masks for individual patterns are "logical-or-ed" together.
            mask = (
                ~(np.array(distgrp["all_patterns/mask"], dtype=bool).max(axis=0))
            ).astype(np.uint8)
            geopars = {}
            for name, path, units in [
                ("wavelength", "NXinstrument/NXsource/incident_wavelength", "nm"),
                ("distance", "NXinstrument/NXdetector/SDD", "mm"),
                ("pixelsizex", "NXinstrument/NXdetector/x_pixel_size", "mm"),
                ("pixelsizey", "NXinstrument/NXdetector/x_pixel_size", "mm"),
                ("beamposcol", "NXinstrument/NXdetector/beam_center_x", "pixels"),
                ("beamposrow", "NXinstrument/NXdetector/beam_center_y", "pixels"),
            ]:
                val = h5ioutils.h5_get_float(
                    h5ioutils.h5_nxpath(distgrp, path), check_units=units
                )
                unc = h5ioutils.h5_get_float(
                    h5ioutils.h5_nxpath(distgrp, path + "_errors"), check_units=units
                )
                geopars[name] = (val, unc)

            qrange = autoq(
                mask,
                geopars["wavelength"][0],
                geopars["distance"][0],
                geopars["pixelsizex"][0],
                geopars["beamposrow"][0],
                geopars["beamposcol"][0],
                linspacing=self.qrangemethod.value,
                N=self.qcount if self.qcount > 0 else -1,
            )
#            print(f'qrange length: {len(qrange)}')

            if "Q" not in ac:
                print(f'Creating datasets in {ac.name}')
                # ac is empty, create all data
                ac.attrs.update(
                    {
                        "I_axes": ["run", "Q"],
                        "Q_indices": [0],
                        "NX_class": "NXdata",
                        "canSAS_class": "SASdata",
                        "signal": "I",
                    }
                    | aveconfig
                )
                q = ac.create_dataset(
                    "Q",
                    shape=(0, qrange.size),
                    dtype=np.double,
                    maxshape=(None, qrange.size),
                )
                q.attrs.update(
                    {
                        "type": "NX_FLOAT",
                        "uncertainties": "Qdev",
                        "units": distgrp["all_patterns/Q"].attrs["units"],
                    }
                )
                dq = ac.create_dataset(
                    "Qdev",
                    shape=(0, qrange.size),
                    dtype=np.double,
                    maxshape=(None, qrange.size),
                )
                dq.attrs.update(
                    {
                        "type": "NX_FLOAT",
                        "units": distgrp["all_patterns/Qdev"].attrs["units"],
                    }
                )
                I = ac.create_dataset(
                    "I",
                    shape=(0, qrange.size),
                    dtype=np.double,
                    maxshape=(None, qrange.size),
                )
                I.attrs.update(
                    {
                        "type": "NX_FLOAT",
                        "uncertainties": "Idev",
                        "units": distgrp["all_patterns/I"].attrs["units"],
                    }
                )
                Idev = ac.create_dataset(
                    "Idev",
                    shape=(0, qrange.size),
                    dtype=np.double,
                    maxshape=(None, qrange.size),
                )
                Idev.attrs.update(
                    {
                        "type": "NX_FLOAT",
                        "units": distgrp["all_patterns/Idev"].attrs["units"],
                    }
                )
                pix = ac.create_dataset(
                    "pixels",
                    shape=(0, qrange.size),
                    dtype=np.double,
                    maxshape=(None, qrange.size),
                )
                pix.attrs.update({"type": "NX_FLOAT", "units": "pixel"})
                binarea = ac.create_dataset(
                    "binarea",
                    shape=(0, qrange.size),
                    dtype=np.double,
                    maxshape=(None, qrange.size),
                )
                binarea.attrs.update({"type": "NX_FLOAT", "units": "pixel"})
                run = ac.create_dataset(
                    name="run",
                    shape=(0,),
                    maxshape=(None,),
                    dtype=distgrp["all_patterns/run"].dtype,
                )
                print(f'Created datasets in {ac.name}. Datasets present: {list(ac.keys())}')
            nimages_values = [
                distgrp[f"all_patterns/{key}"].shape[0] for key in ["I", "Idev", "run"]
            ]
            if len(set(nimages_values)) > 1:
                raise ValueError(
                    f"Size mismatch between I, Idev and run datasets in {distgrp.name}"
                )
            nimages = nimages_values[0]
            print(f'Number of images: {nimages}')
            already_integrated_runs = np.array(distgrp["all_curves/run"])
            for i in range(nimages):
                self.sendProgress("Performing azimuthal averaging...", total=nimages, current=i)
                run = distgrp["all_patterns/run"][i]
                if run in already_integrated_runs:
                    #print(f'Run {run} has already been integrated')
                    continue
                intensity2d = np.array(
                    distgrp["all_patterns/I"][i, :, :], dtype=np.double
                )
                uncertainty2d = np.array(
                    distgrp["all_patterns/Idev"][i, :, :], dtype=np.double
                )
                q1d, i1d, di1d, dq1d, a1d, p1d = radavg(
                    intensity2d,
                    uncertainty2d,
                    mask,
                    geopars["wavelength"][0],
                    geopars["wavelength"][1],
                    geopars["distance"][0],
                    geopars["distance"][1],
                    geopars["pixelsizex"][0],
                    geopars["pixelsizex"][1],
                    geopars["beamposrow"][0],
                    geopars["beamposrow"][1],
                    geopars["beamposcol"][0],
                    geopars["beamposcol"][1],
                    qrange,
                    self.ierrorprop.value,
                    self.qerrorprop.value,
                )
                if ac["Q"].shape[0] > i:
                    # already there, should not happen but who knows
                    print('Already there!')
                    pass
                elif ac["Q"].shape[0] == i:
                    # arrays should be extended
                    for name in ["Q", "I", "Qdev", "Idev", "pixels", "binarea", "run"]:
                        assert ac[name].shape[0] == i
                        ac[name].resize(ac[name].shape[0] + 1, axis=0)
                    ac["Q"][i, :] = q1d
                    ac["I"][i, :] = i1d
                    ac["Idev"][i, :] = di1d
                    ac["Qdev"][i, :] = dq1d
                    ac["binarea"][i, :] = a1d
                    ac["pixels"][i, :] = p1d
                    ac["run"][i] = run
                else:
                    assert ac["Q"].shape[0] > i
                    raise ValueError("Curves can only be added sequentially.")

    def _checkforoutliers(self):
        t0 = time.monotonic()
        self.sendProgress("Waiting for HDF5 lock (before outlier test)...", total=0, current=0)
        with self.h5io.writer(self.h5path) as distgrp:
            self.sendProgress("Testing for outliers...", total=0, current=0)
            frame_is_bad = np.array(distgrp["frame_is_bad"], dtype=bool)
            goodu = np.array(
                distgrp["all_curves"]["Idev"][~frame_is_bad, :], dtype=np.double
            )
            goodi = np.array(
                distgrp["all_curves"]["I"][~frame_is_bad, :], dtype=np.double
            )
            cm = correlmatrix_cython(goodi.T, goodu.T)
            outliertest = OutlierTest(
                self.outliermethod, self.outlierthreshold, correlmatrix=cm, fsns=[int(x) for x in distgrp["run"][()][~frame_is_bad]]
            )
            original_bads = distgrp["frame_is_bad"][()].sum()
            frame_is_bad[~frame_is_bad] = outliertest.outlierverdict
            distgrp["frame_is_bad"][:] = frame_is_bad
            new_bads = distgrp["frame_is_bad"][()].sum()
            self.sendWarning(f"{self.h5path=}, {original_bads=}, {new_bads=}, {outliertest.outlierverdict.sum()=}")
            self.sendWarning(f"{distgrp['frame_is_bad'][()]=}, {distgrp['frame_is_bad'].name=}")
            assert new_bads - original_bads == outliertest.outlierverdict.sum()
            
            procsteps = h5ioutils.h5_nxpath(distgrp, "processing_steps")
            try:
                del procsteps["outliertest"]
            except KeyError:
                pass
            seqid_max = max([si[()] for si in h5ioutils.h5_nxpath_iter(procsteps, 'NXprocess/sequence_index')], default=0)
            outliertestgrp = procsteps.create_group('outliertest')
            outliertestgrp.attrs.update({"NX_class": "NXprocess", "canSAS_class": "SASprocess"})
            outliertestgrp.create_dataset("date", data=str(datetime.datetime.now()))
            outliertestgrp.create_dataset("description", data="Outlier test")
            outliertestgrp.create_dataset("name", data="outliertest")
            outliertestgrp.create_dataset("sequence_index", data=seqid_max+1)
            outliertestgrp.create_dataset("corrmat", data=outliertest.correlmatrix)
            outliertestgrp.create_dataset("score", data=outliertest.score)
            outliertestgrp.create_dataset("method", data=outliertest.method.value)
            outliertestgrp.create_dataset("threshold", data=outliertest.threshold)
            outliertestgrp.create_dataset("fsns", data=outliertest.fsns)
            outliertestgrp.create_dataset("verdict", data=outliertest.outlierverdict)

        self.result.time_outlierdetection = time.monotonic() - t0

    def _summarize(self):
        """Calculate average scattering pattern and curve"""
        self.sendProgress("Waiting for HDF5 lock (before averaging)...", total=0, current=0)
        with self.h5io.reader(self.h5path, locking=True) as distgrp:
            self.sendProgress("Averaging...", total=0, current=0)
            ma_intensity2d = MatrixAverager(self.ierrorprop)
            ma_intensity1d = MatrixAverager(self.ierrorprop)
            ma_q1d = MatrixAverager(self.qerrorprop)
            frame_is_bad = np.array(distgrp["frame_is_bad"], dtype=bool)
            for i in range(frame_is_bad.size):
                if frame_is_bad[i]:
                    continue
                ma_intensity2d.add(
                    distgrp["all_patterns/I"][i, :, :],
                    distgrp["all_patterns/Idev"][i, :, :],
                )
                ma_intensity1d.add(
                    distgrp["all_curves/I"][i, :],
                    distgrp["all_curves/Idev"][i, :],
                )
                ma_q1d.add(
                    distgrp["all_curves/Q"][i, :],
                    distgrp["all_curves/Qdev"][i, :],
                )

            for grpname in ["pattern", "curve"]:
                try:
                    del distgrp[grpname]
                except KeyError:
                    pass

        self.sendProgress("Waiting for HDF5 lock (before writing averaged data)...", total=0, current=0)
        with self.h5io.writer(self.h5path) as distgrp:
            self.sendProgress("Writing averaged data...", total=0, current=0)
            if ma_intensity1d.count <= 0:
                # no images to average
                pass
            else:
                for key, ma_q, ma_intensity in [
                    ("pattern", None, ma_intensity2d),
                    ("curve", ma_q1d, ma_intensity1d),
                ]:
                    nxdata = distgrp.require_group(key)
                    for k in nxdata.keys():
                        del nxdata[k]
                    nxdata.attrs.update(
                        {
                            "I_axes": ["run", "Q"],
                            "Q_indices": [0],
                            "NX_class": "NXdata",
                            "canSAS_class": "SASdata",
                            "signal": "I",
                        }
                    )
                    if key == 'pattern':
                        nxdata.attrs["Mask_indices"] = (0, 1)
                        nxdata.attrs["mask"] = "mask"
                        nxdata['Q'] = distgrp["all_patterns/Q"]
                        nxdata['Qdev'] = distgrp["all_patterns/Qdev"]
                    if ma_q is not None:
                        q, dq = ma_q.get()
                        qds = nxdata.create_dataset("Q", data=q)
                        qds.attrs.update(
                            {
                                "type": "NX_FLOAT",
                                "uncertainties": "Qdev",
                                "units": distgrp[f"all_{key}s/Q"].attrs["units"],
                            }
                        )
                        dqds = nxdata.create_dataset("Qdev", data=dq)
                        dqds.attrs.update(
                            {
                                "type": "NX_FLOAT",
                                "units": distgrp[f"all_{key}s/Qdev"].attrs["units"],
                            }
                        )
                    intensity, dintensity = ma_intensity.get()
                    intensityds = nxdata.create_dataset("I", data=intensity)
                    intensityds.attrs.update(
                        {
                            "type": "NX_FLOAT",
                            "uncertainties": "Idev",
                            "units": distgrp[f"all_{key}s/I"].attrs["units"],
                        }
                    )
                    dintensityds = nxdata.create_dataset("Idev", data=dintensity)
                    dintensityds.attrs.update(
                        {
                            "type": "NX_FLOAT",
                            "units": distgrp[f"all_{key}s/Idev"].attrs["units"],
                        }
                    )
                # now write a mask, too
                masks = np.sum(distgrp['all_patterns/mask'][()], axis=0) 
                maskds = distgrp["pattern"].create_dataset("mask", data=masks>0)

    def main(self):
        print('Summary job running')
        try:
            t0 = time.monotonic()
            self._azimuthalaverage()
            self._checkforoutliers()
            self._summarize()
            self.result.success = True
            self.result.time_total = time.monotonic() - t0
        except UserStopException:
            self.result.success = False
            self.result.status = "User break"
            self.sendError("User stop requested")
        except Exception as exc:
            self.result.success = False
            self.result.status = "Error"
            self.sendError(str(exc), traceback=traceback.format_exc())
        finally:
            pass

        return self.result
