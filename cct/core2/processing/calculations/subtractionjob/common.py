from ..backgroundprocess import BackgroundProcessPickle, Results, BackgroundProcessError
from typing import List
import enum

class SubtractionError(BackgroundProcessError):
    pass


class SubtractionResult(Results):
    distancekeys: List[str]
    subtractedname: str

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.distancekeys = []
        self.subtractedname = ""


class SubtractionScalingMode(enum.Enum):
    Unscaled = 'None'  # no scaling, no parameter
    Constant = 'Constant'  # multiply the background with a fixed constant. One parameter: the constant
    Interval = 'Interval'  # Scale the sample and the background together on the given q-range (qmin, qmax, Nq)
    PowerLaw = 'Power-law'  # Scale the sample and the background for best power-law fit on the q-range (qmin, qmax, Nq)
