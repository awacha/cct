import copy
import logging
import multiprocessing.synchronize
import os
from typing import Optional, Tuple

import numpy as np
import scipy.odr
import scipy.optimize

from ..backgroundprocess import BackgroundProcessNeXus
from ....dataclasses import Exposure, Curve
from ....dataclasses.exposure import QRangeMethod
from ....algorithms.matrixaverager import ErrorPropagationMethod
from ...h5io import utils as h5ioutils

logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)

from .common import SubtractionError, SubtractionScalingMode, SubtractionResult


class SubtractionJobNeXus(BackgroundProcessNeXus):
    samplename: Optional[str]
    backgroundname: Optional[str]
    subtractedname: str
    scalingmode: SubtractionScalingMode
    factor: Tuple[float, float]
    interval: Tuple[float, float, int]
    result: SubtractionResult
    qrangemethod: QRangeMethod
    qcount: int
    errorprop: ErrorPropagationMethod
    qerrorprop: ErrorPropagationMethod

    def __init__(self, jobid: int, 
                 h5file: str, h5lock: multiprocessing.synchronize.Lock, 
                 stopEvent: multiprocessing.synchronize.Event,
                 messagequeue: multiprocessing.Queue, 
                 
                 samplename: Optional[str], backgroundname: Optional[str],
                 scalingmode: SubtractionScalingMode, factor: Tuple[float, float], interval: Tuple[float, float, int],
                 subtractedname: Optional[str], qrangemethod: QRangeMethod, qcount: int,
                 errorprop: ErrorPropagationMethod, qerrorprop: ErrorPropagationMethod):
        super().__init__(jobid, h5file, h5lock, stopEvent, messagequeue)
        self.samplename = samplename
        self.backgroundname = backgroundname
        self.scalingmode = scalingmode
        self.factor = factor
        self.interval = interval
        self.qrangemethod = qrangemethod
        self.qcount = qcount
        self.errorprop = errorprop
        self.qerrorprop = qerrorprop
        self.result = SubtractionResult(jobid=self.jobid)
        if subtractedname is None:
            self.result.subtractedname = f'{samplename}-{backgroundname if backgroundname is not None else "constant"}'
        else:
            self.result.subtractedname = subtractedname

    def subtractconstant(self, distkey: str) -> Tuple[Exposure, Curve, Curve]:
        exposure = self.h5io.readExposure(f'Samples/{self.samplename}/{distkey}')
        if self.scalingmode == SubtractionScalingMode.PowerLaw:
            # fit a power-law
            curve = exposure.radial_average(
                np.linspace(self.interval[0], self.interval[1], self.interval[2])).sanitize()
            odrdata = scipy.odr.RealData(curve.q, curve.intensity, curve.quncertainty, curve.uncertainty)
            odrmodel = scipy.odr.Model(lambda params, x: params[0] * x ** params[1] + params[2])
            odrresult = scipy.odr.ODR(odrdata, odrmodel, [1, -4, curve.intensity[-10:].mean()])
            bglevel = odrresult.beta[2], odrresult.sd_beta[2]
        elif self.scalingmode == SubtractionScalingMode.Constant:
            bglevel = self.factor
        elif self.scalingmode == SubtractionScalingMode.Interval:
            # fit a constant
            curve = exposure.radial_average(
                np.linspace(self.interval[0], self.interval[1], self.interval[2])).sanitize()
            odrdata = scipy.odr.RealData(curve.q, curve.intensity, curve.quncertainty, curve.uncertainty)
            odrmodel = scipy.odr.Model(lambda params, x: params[0])
            odrresult = scipy.odr.ODR(odrdata, odrmodel, [curve.intensity.mean()])
            bglevel = odrresult.beta[0], odrresult.sd_beta[0]
        elif self.scalingmode == SubtractionScalingMode.Unscaled:
            bglevel = (0.0, 0.0)
        else:
            assert False
        ex = Exposure(exposure.intensity - bglevel[0], exposure.header,
                      (exposure.uncertainty ** 2 + bglevel[1] ** 2) ** 0.5, exposure.mask)
        assert (ex.mask==0).sum() != 0
        assert (ex.mask!=0).sum() != 0
        curve_avg = self.h5io.readCurve(self.h5io.getH5Path(self.samplename, distkey), "curve_averaged")
        return ex, ex.radial_average(
            qbincenters=(self.qrangemethod, self.qcount), errorprop=self.errorprop, qerrorprop=self.qerrorprop), \
               curve_avg - bglevel

    def subtractbackground(self, distkey: str) -> Tuple[Exposure, Curve, Curve]:
        exposure = self.h5io.readExposure(f'Samples/{self.samplename}/{distkey}')
        bg = self.h5io.readExposure(f'Samples/{self.backgroundname}/{distkey}')
        if self.scalingmode == SubtractionScalingMode.Unscaled:
            factor = (1.0, 0.0)
        elif self.scalingmode == SubtractionScalingMode.Constant:
            factor = self.factor
        elif self.scalingmode in [SubtractionScalingMode.Interval, SubtractionScalingMode.PowerLaw]:
            q = np.linspace(self.interval[0], self.interval[1], self.interval[2])
            rad = exposure.radial_average(q)
            bgrad = bg.radial_average(q)
            idx = np.logical_and(rad.isvalid(), bgrad.isvalid())
            if idx.sum() < 2:
                raise SubtractionError('Not enough valid points between sample and background')
            rad = rad[idx]
            bgrad = bgrad[idx]
            if self.scalingmode == SubtractionScalingMode.Interval:
                odrdata = scipy.odr.RealData(bgrad.intensity, rad.intensity, bgrad.uncertainty, rad.uncertainty)
                odrmodel = scipy.odr.Model(lambda params, x: x * params[0])
                odrresult = scipy.odr.ODR(odrdata, odrmodel, [1.0]).run()
                factor = odrresult.beta[0], odrresult.sd_beta[0]
            else:
                odrmodel = scipy.odr.Model(lambda params, x: params[0] * x ** params[1])

                def powerlaw_goodnessoffit(paramarray):
                    odrdata = scipy.odr.RealData(rad.q, rad.intensity - bgrad.intensity * paramarray[0],
                                                 rad.quncertainty, (rad.uncertainty ** 2 + paramarray[
                            0] ** 2 * bgrad.uncertainty ** 2) ** 0.5)
                    return scipy.odr.ODR(odrdata, odrmodel, [1.0, -4.0]).run().res_var

                result = scipy.optimize.minimize(
                    powerlaw_goodnessoffit,
                    np.array([1.0]),
                    method='L-BFGS-B',
                    options={'ftol': 1e7 * np.finfo(float).eps,
                             'eps': 0.0001,  # finite step size for approximating the jacobian
                             },
                )
                ftol = 1e7 * np.finfo(float).eps  # L-BFGS-B default factr value is 1e7
                covar = max(1, np.abs(result.fun)) * ftol * result.hess_inv.todense()
                factor = result.x[0], covar[0, 0] ** 0.5
        else:
            assert False
        ex = Exposure(exposure.intensity - factor[0] * bg.intensity, copy.deepcopy(exposure.header), (
                exposure.uncertainty ** 2 + factor[0] ** 2 * bg.uncertainty ** 2 + bg.intensity ** 2 * factor[
            1] ** 2) ** 0.5, exposure.mask)
        if exposure.header.maskname != bg.header.maskname:
            ex.header.maskname = f'{os.path.split(exposure.header.maskname)[-1]}-{os.path.split(bg.header.maskname)[-1]}'
            ex.mask = np.logical_and(exposure.mask, bg.mask)
        samplecurve_avg = self.h5io.readCurve(self.h5io.getH5Path(self.samplename, distkey), "curve_averaged")
        bgcurve_avg = self.h5io.readCurve(self.h5io.getH5Path(self.backgroundname, distkey), "curve_averaged")
        ret = ex, ex.radial_average(
            qbincenters=(self.qrangemethod, self.qcount), errorprop=self.errorprop, qerrorprop=self.qerrorprop), \
               samplecurve_avg - factor * bgcurve_avg
        return ret

    def main(self):
        if self.samplename is None:
            # do nothing
            return
        distkeys_sample = self.h5io.distancekeys(self.samplename)
        if self.backgroundname is not None:
            distkeys_background = self.h5io.distancekeys(self.backgroundname)
        else:
            distkeys_background = None
        for dk in distkeys_sample:
            if (dk not in distkeys_background) and (self.backgroundname is not None):
                self.sendWarning(f'No measurement exists for background {self.backgroundname} at {dk} mm.')
                continue
            try:
                self.h5io.addSample(self.subtractedname)
            except ValueError:
                pass
            try:
                self.h5io.addDistance(self.subtractedname, dk)
            except ValueError:
                pass
            h5path = self.h5io.getH5Path(self.subtractedname, dk)
            with self.h5io.writer(h5path) as h5:
                pass
                

            if self.backgroundname is None:
                # subtract a constant
                sub, curve_reint, curve_avg = self.subtractconstant(dk)
            else:
                # subtract the background
                sub, curve_reint, curve_avg = self.subtractbackground(dk)
            self.result.distancekeys.append(dk)
