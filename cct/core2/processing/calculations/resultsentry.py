import datetime
import enum
import gzip
import logging
from typing import Dict, Optional, Tuple
import warnings


import numpy as np
import openpyxl.cell
import openpyxl.worksheet.worksheet
import scipy.io

from .outliertest import OutlierTest
from ..h5io import ProcessingH5File, ProcessingH5FileNeXus, ProcessingH5FilePickle
from ..h5io import utils as h5ioutils
from ...dataclasses import Exposure, Curve, Header, Sample

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

class CurveFileType(enum.Enum):
    ASCII = ('ASCII text file', '.txt')
    ATSAS = ('ATSAS data file', '.dat')
    PDH = ('PCG data file', '.pdh')
    RSR = ('RSR data file', '.rsr')
    PNG = ('Portable Network Graphics', '.png')
    JPG = ('JPEG image', '.jpg')
    SVG = ('Scalable Vector Graphics', '.svg')
    PDF = ('Portable Document Format', '.pdf')


class PatternFileType(enum.Enum):
    ASCII = ('ASCII text file', '.txt')
    ASCIIGZIP = ('GZip-ped ASCII file', '.txt.gz')
    NUMPY = ('NumPy compressed', '.npz')
    MATLAB = ('MatLab(R) matrix', '.mat')
    PNG = ('Portable Network Graphics', '.png')
    JPG = ('JPEG image', '.jpg')
    SVG = ('Scalable Vector Graphics', '.svg')
    PDF = ('Portable Document Format', '.pdf')


CorMatFileType = PatternFileType


class SampleDistanceEntryType(enum.Enum):
    Primary = 'primary'
    Subtracted = 'subtracted'
    Merged = 'merged'


class SampleDistanceEntry:
    """An entry in the HDF5 file corresponding one sample measured at one sample-to-detector distance"""
    samplename: str
    distancekey: str
    _entrytype: Optional[SampleDistanceEntryType] = None
    h5io: ProcessingH5File
    _header: Optional[Header] = None
    _exposure: Optional[Exposure] = None
    _outliertest: Optional[OutlierTest] = None
    _curve: Optional[Curve] = None
    _h5path: Optional[str] = None

    MetaDataField = ProcessingH5File.MetaDataField

    def __init__(self, samplename: str, distancekey: str, h5io: ProcessingH5File):
        self.samplename = samplename
        self.distancekey = distancekey
        self.h5io = h5io

    def curves(self, all: bool = False) -> Dict[int, Curve]:
        return self.h5io.readCurves(self.h5path, readall=all)

    def headers(self, all: bool = False) -> Dict[int, Header]:
        warnings.warn("SampleDistaceEntry.headers() is deprecated", DeprecationWarning)
        return self.h5io.readHeaders(self.h5path, readall=all)

    def is_canSAS_NeXus(self) -> bool:
        return isinstance(self.h5io, ProcessingH5FileNeXus)
    
    @property
    def sample_category(self) -> str:
        val = self.getMetaDataField(self.MetaDataField.SampleCategory)
        return val
    
    @property
    def exposurecount(self) -> int:
        if self.is_canSAS_NeXus():
            with self.h5io.reader(self.h5path) as h5:
                return h5ioutils.h5_nxpath(h5, 'run').size - h5ioutils.h5_nxpath(h5, 'frame_is_bad')[()].sum()
        return self.header.exposurecount
    
    @property
    def exposuretime(self) -> float:
        return self.getMetaDataField(self.MetaDataField.ExposureTime)
    
    @property
    def exposuretime_hms(self) -> Tuple[int, int, float]:
        et = self.exposuretime
        if isinstance(et, tuple):
            et = et[0]
        h = int(et // 3600)
        m = int((et - h*3600) // 60)
        s = float(et - h*3600-m*60) 
        return h, m, s

    @property
    def h5path(self) -> str:
        if self._h5path is None:
            self._h5path = self.h5io.getH5Path(self.samplename, self.distancekey)
        return self._h5path

    @property
    def curve(self) -> Curve:
        if self._curve is None:
            self._curve = self.h5io.readCurve(self.h5path)
        return self._curve

    @property
    def exposure(self) -> Exposure:
        warnings.warn("SampleDistaceEntry.exposure() is deprecated", DeprecationWarning)
        if self._exposure is None:
            self._exposure = self.h5io.readExposure(self.h5path)
        return self._exposure

    @property
    def outliertest(self) -> OutlierTest:
        if self.entrytype in [SampleDistanceEntryType.Merged, SampleDistanceEntryType.Subtracted]:
            return None
        if self._outliertest is None:
            try:
                self._outliertest = self.h5io.readOutlierTest(self.h5path)
            except KeyError:
                return None
        return self._outliertest

    @property
    def transmission(self) -> Tuple[float, float]:
        return self.getMetaDataField(self.MetaDataField.Transmission)

    @property
    def thickness(self) -> Tuple[float, float]:
        return self.getMetaDataField(self.MetaDataField.Thickness)

    @property
    def header(self) -> Header:
        warnings.warn("SampleDistaceEntry.header() is deprecated", DeprecationWarning)
        if self._header is None:
            self._header = self.h5io.readHeader(self.h5path)
        return self._header

    @property
    def entrytype(self) -> SampleDistanceEntryType:
        if self._entrytype is None:
            cat = self.sample_category
            if cat == Sample.Categories.Subtracted.value:
                self._entrytype = SampleDistanceEntryType.Subtracted
            elif cat == Sample.Categories.Merged.value:
                self._entrytype = SampleDistanceEntryType.Merged
            else:
                self._entrytype = SampleDistanceEntryType.Primary
        return self._entrytype

    def writeCurve(self, filename: str, filetype: CurveFileType):
        with open(filename, 'wt') as f:
            if not self.is_canSAS_NeXus():
                dic = self.h5io.readHeaderDict(self.h5path)
            else:
                dic = {}
            curvearray = np.array(self.curve, copy=True)
            if filetype in [CurveFileType.RSR, CurveFileType.ATSAS, CurveFileType.PDH]:
                curvearray[:, 0] /= 10
                curvearray[:, 3] /= 10
            if filetype == CurveFileType.RSR:
                f.write(' TIME\n')
                f.write(' 1.0\n')
                f.write(f' {curvearray.shape[0]}\n')
                np.savetxt(f, curvearray[:, :3], delimiter=' ', fmt='%.9f')
            elif filetype in [CurveFileType.ATSAS, CurveFileType.ASCII]:
                for key in sorted(dic):
                    f.write(f'# {key} : {dic[key]}\n')
                f.write('# Columns:\n')
                f.write('#  q [1/nm],  Intensity [1/cm * 1/sr], '
                        'Propagated uncertainty of the intensity [1/cm * 1/sr], '
                        'Propagated uncertainty of q [1/nm], Bin area [# of pixels], Pixel coordinate [pixel]\n'
                        )
                np.savetxt(f, curvearray if filetype == CurveFileType.ASCII else curvearray[:, :3])
            elif filetype == CurveFileType.PDH:
                f.write(f'{self.samplename} @ {self.distancekey} mm\n')
                f.write('SAXS\n')
                f.write(
                    f'{curvearray.shape[0]:>9d}         0         0         0         0         0         0         0\n'
                    '  0.000000E+00   0.000000E+00   0.000000E+00   1.000000E+00   0.000000E+00\n'
                    '  0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00   0.000000E+00\n'
                )
                np.savetxt(f, curvearray[:, :3], delimiter=' ', fmt='%14.6E')
            else:
                raise ValueError(f'Unknown curve file type: {filetype}')

    def writeCurveToXLSX(self, worksheet: openpyxl.worksheet.worksheet.Worksheet, topleftcell: openpyxl.cell.Cell):
        row = topleftcell.row
        column = topleftcell.column
        worksheet.cell(row=row, column=column, value='Sample name:')
        worksheet.cell(row=row + 1, column=column, value='S-D distance:')
        worksheet.cell(row=row, column=column + 1, value=self.samplename)
        try:
            worksheet.cell(row=row + 1, column=column + 1, value=float(self.distancekey))
        except ValueError:
            # e.g. distancekey == 'merged'
            worksheet.cell(row=row + 1, column=column + 1, value=self.distancekey)
        worksheet.merge_cells(start_row=row, start_column=column + 1, end_row=row, end_column=column + 3)
        worksheet.merge_cells(start_row=row + 1, start_column=column + 1, end_row=row + 1, end_column=column + 3)
        row += 2
        #        worksheet.cell(row=row, column=column, value=f'{self.samplename} @ {self.distancekey} mm')
        #        worksheet.merge_cells(start_row=row, start_column=column, end_row=row, end_column=column+3)
        for i, (quantity, unit) in enumerate([
            ('q', '1/nm'),
            ('Intensity', '1/cm * 1/sr'),
            ('dIntensity', '1/cm * 1/sr'),
            ('dq', '1/nm'),
        ]):
            worksheet.cell(row=row + 1, column=column + i, value=quantity)
            worksheet.cell(row=row + 2, column=column + i, value=unit)
        curvearray = np.array(self.curve)
        for r in range(len(self.curve)):
            for c in range(4):
                worksheet.cell(row=row + 3 + r, column=column + c, value=curvearray[r, c])

    def writePattern(self, filename: str, filetype: PatternFileType):
        if filetype == PatternFileType.NUMPY:
            np.savez_compressed(
                filename, intensity=self.exposure.intensity, error=self.exposure.uncertainty, mask=self.exposure.mask)
        elif filetype == PatternFileType.MATLAB:
            scipy.io.savemat(filename, {
                'intensity': self.exposure.intensity,
                'error': self.exposure.uncertainty,
                'mask': self.exposure.mask
            }, do_compression=True)
        elif filetype in [PatternFileType.ASCII, PatternFileType.ASCIIGZIP]:
            with (open(filename, 'wb') if (
                    filetype == PatternFileType.ASCII) else gzip.GzipFile(filename, 'w')) as f:
                f.write('# Intensity\n'.encode('utf-8'))
                np.savetxt(f, self.exposure.intensity)
                f.write('\n# Uncertainty\n'.encode('utf-8'))
                np.savetxt(f, self.exposure.uncertainty)
                f.write('# Mask\n'.encode('utf-8'))
                np.savetxt(f, self.exposure.mask)

    def writeCorMat(self, filename: str, filetype: CorMatFileType):
        if self.entrytype != SampleDistanceEntryType.Primary:
            raise ValueError('Cannot export correlation matrix of derived data.')
        if filetype == CorMatFileType.NUMPY:
            np.savez_compressed(
                filename, correlmatrix=self.outliertest.correlmatrix)
        elif filetype == CorMatFileType.MATLAB:
            scipy.io.savemat(filename, {
                'correlmatrix': self.outliertest.correlmatrix,
            }, do_compression=True)
        elif filetype in [CorMatFileType.ASCII, CorMatFileType.ASCIIGZIP]:
            with (open(filename, 'wt') if (
                    filetype == CorMatFileType.ASCII) else gzip.GzipFile(filename, 'w')) as f:
                np.savetxt(f, self.outliertest.correlmatrix)

    def isDerived(self) -> bool:
        return self.entrytype != SampleDistanceEntryType.Primary

    def getMetaDataField(self, field):
        return self.h5io.getMetaDataField(self.samplename, self.distancekey, field)
    
    def getMetaDataSeries(self, field):
        return self.h5io.getMetaDataSeries(self.samplename, self.distancekey, field)