"""A faster way to find a file in a recursive directory hierarchy"""

import os
from typing import List


def walk_subdirs(directory):
    yield directory
    for x in os.scandir(directory):
        if x.is_dir():
            yield x.path


def fastfindfile(rootdir: str, filename: str, extensions: List[str]):
    for directory in walk_subdirs(rootdir):
        for ext in extensions:
            if os.path.exists(os.path.join(directory, filename+ext)):
                return os.path.join(directory, filename+ext)
    else:
        raise FileNotFoundError(filename)
