import pickle


class SardanaEnvironmentDecoder:
    name = "SardanaEnvironment"
    format = None
    dtype = None
    __value = None
    __data = None

    def __init__(self):
        pass

    def load(self, data):
        self.__data = data
        self.format = data[0]
        self.__value = None 

    def shape(self):
        return [1, 0]
    
    def decode(self):
        if not self.__value:
            self.__value = pickle.dumps(pickle.loads(self.__data[1])['new'])
        return self.__value