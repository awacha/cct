import datetime
import enum
from typing import List, Optional

import sqlalchemy
from sqlalchemy import (CHAR, ForeignKey, ForeignKeyConstraint,
                        PrimaryKeyConstraint)
from sqlalchemy.orm import (DeclarativeBase, Mapped, MappedAsDataclass,
                            mapped_column, relationship)


class Categories(enum.Enum):
    Buffer = 'Buffer'
    Calibrant = 'Calibrant'
    Can = 'Can'
    Dark = 'Dark'
    Empty_beam = 'Empty_beam'
    Merged = 'Merged'
    None_ = 'None_'
    NormalizationSample = 'NormalizationSample'
    Sample = 'Sample'
    Sample_and_buffer = 'Sample_and_buffer'
    Sample_and_can = 'Sample_and_can'
    Sample_environment = 'Sample_environment'
    Simulated_data = 'Simulated_data'
    Subtracted = 'Subtracted'


class Situations(enum.Enum):
    Air = 'air'
    Sealed_can = 'sealed can'
    Vacuum = 'vacuum'


class Base(MappedAsDataclass, DeclarativeBase):
    type_annotation_map = {
        int: sqlalchemy.INT(),
        datetime.datetime: sqlalchemy.TIMESTAMP(timezone=True),
        str: sqlalchemy.String(1024),
    }


class Sample(Base):
    __tablename__ = 'samples'
    __table_args__ = (
        ForeignKeyConstraint(['project_category', 'project_year', 'project_id'],
                             ['projects.category', 'projects.year', 'projects.id']),
        {'sqlite_autoincrement': True, 'extend_existing': True},
    )
    project: Mapped[Optional["Project"]] = relationship(
        back_populates="samples")
    title: Mapped[str] = mapped_column(
        CHAR(64), unique=True, index=True, primary_key=True)
    description: Mapped[str] = mapped_column(default='')
    positionx: Mapped[float] = mapped_column(default=0.0)
    positionx_error: Mapped[float] = mapped_column(default=0.0)
    positiony: Mapped[float] = mapped_column(default=0.0)
    positiony_error: Mapped[float] = mapped_column(default=0.0)
    thickness: Mapped[float] = mapped_column(default=1.0)
    thickness_error: Mapped[float] = mapped_column(default=0.0)
    preparedby: Mapped[str] = mapped_column(default='Anonymous')
    preparetime: Mapped[datetime.datetime] = mapped_column(
        sqlalchemy.DateTime(), default_factory=datetime.datetime.now)
    distminus: Mapped[float] = mapped_column(default=0.0)
    distminus_error: Mapped[float] = mapped_column(default=0.0)
    category: Mapped[Categories] = mapped_column(default=Categories.Sample)
    situation: Mapped[Situations] = mapped_column(default=Situations.Vacuum)
    project_id: Mapped[Optional[int]] = mapped_column(
        nullable=True, default=None)
    project_category: Mapped[Optional[str]] = mapped_column(
        CHAR(64), nullable=True, default=None)
    project_year: Mapped[Optional[int]] = mapped_column(
        nullable=True, default=None)
    maskoverride: Mapped[Optional[str]] = mapped_column(
        nullable=True, default=None)
    transmission: Mapped[float] = mapped_column(default=1.0)
    transmission_error: Mapped[float] = mapped_column(default=0.0)


class Project(Base):
    __tablename__ = 'projects'
    __table_args__ = (
        PrimaryKeyConstraint('category', 'year', 'id'),
        {'sqlite_autoincrement': True, 'extend_existing': True},
    )
    id: Mapped[int] = mapped_column(primary_key=True)
    samples: Mapped[List["Sample"]] = relationship(back_populates="project")
    category: Mapped[str] = mapped_column(
        CHAR(64), default='Uncategorized', primary_key=True)
    year: Mapped[int] = mapped_column(
        default_factory=lambda: datetime.datetime.now().year, primary_key=True)
    title: Mapped[str] = mapped_column(default='Untitled')
    proposer: Mapped[str] = mapped_column(default='Anonymous')


class Person(Base):
    __tablename__ = 'people'
    __table_args__ = (
        sqlalchemy.UniqueConstraint('email'),
        {'sqlite_autoincrement': True, 'extend_existing': True},
    )
    id: Mapped[int] = mapped_column(primary_key=True, autoincrement=True)
    name: Mapped[str] = mapped_column(default="")
    email: Mapped[str] = mapped_column(nullable=True, default=None)
    role: Mapped[str] = mapped_column(nullable=True, default=None)
    affiliation: Mapped[str] = mapped_column(nullable=True, default=None)
    address: Mapped[str] = mapped_column(nullable=True, default=None)
    phone: Mapped[str] = mapped_column(nullable=True, default=None)
    orcid: Mapped[str] = mapped_column(nullable=True, default=None)


class TangoSensor(Base):
    __tablename__ = 'tangosensors'
    __table_args__ = (
        {'sqlite_autoincrement': True, 'extend_existing': True},
    )
    name: Mapped[str] = mapped_column(CHAR(64), primary_key=True)
    device: Mapped[str] = mapped_column()
    attribute: Mapped[str] = mapped_column()
    sensortype: Mapped[str] = mapped_column()
    quantityname: Mapped[str] = mapped_column()


class MotorRole(enum.Enum):
    Sample = 'sample'
    BeamStop = 'beamstop'
    Pinhole = 'pinhole'
    Other = 'other'
    Any = 'any'


class MotorDirection(enum.Enum):
    X = 'x'
    Y = 'y'
    Z = 'z'
    Other = 'other'
    Any = 'any'


class SardanaMotor(Base):
    __tablename__ = 'sardanamotors'
    __table_args__ = (
        {'sqlite_autoincrement': True, 'extend_existing': True},
    )
    name: Mapped[str] = mapped_column(CHAR(64), primary_key=True)
    motor: Mapped[str] = mapped_column()
    role: Mapped[MotorRole] = mapped_column()
    direction: Mapped[MotorDirection] = mapped_column()


class WatchedTangoDevice(Base):
    __tablename__ = 'watchedtangodevices'
    __table_args__ = (
        {'sqlite_autoincrement': True, 'extend_existing': True},
    )
    device: Mapped[str] = mapped_column(CHAR(128), primary_key=True)
    attribute: Mapped[str] = mapped_column(CHAR(128), default='status')


class QCalibrant(Base):
    __tablename__ = 'qcalibrant'
    __table_args__ = (
        {'sqlite_autoincrement': True, 'extend_existing': True},
    )
    name: Mapped[str] = mapped_column(CHAR(128), primary_key=True)
    peaks: Mapped[List["QCalibrantPeak"]] = relationship("QCalibrantPeak", cascade='all, expunge, delete-orphan')
    calibrationdate: Mapped[datetime.datetime] = mapped_column(
        sqlalchemy.DateTime(), default_factory=datetime.datetime.now)
    description: Mapped[str] = mapped_column(nullable=False, default='')
    regex: Mapped[str] = mapped_column(nullable=False, default='^.*$')


class QCalibrantPeak(Base):
    __tablename__ = 'qcalibrantpeak'
    __table_args__ = (
        PrimaryKeyConstraint('qcalibrant_id', 'id'),
        {'sqlite_autoincrement': True, 'extend_existing': True},
    )
    id: Mapped[int] = mapped_column()
    name: Mapped[str] = mapped_column(nullable=False)
    value: Mapped[float] = mapped_column()
    uncertainty: Mapped[float] = mapped_column()
    qcalibrant_id: Mapped[str] = mapped_column(ForeignKey('qcalibrant.name'))


class IntensityCalibrant(Base):
    __tablename__ = 'intensitycalibrant'
    __table_args__ = (
        {'sqlite_autoincrement': True, 'extend_existing': True},
    )
    name: Mapped[str] = mapped_column(CHAR(128), primary_key=True)
    datafile: Mapped[str] = mapped_column(nullable=False)
    calibrationdate: Mapped[datetime.datetime] = mapped_column(
        sqlalchemy.DateTime(), default_factory=datetime.datetime.now)
    description: Mapped[str] = mapped_column(nullable=False, default='')
    regex: Mapped[str] = mapped_column(nullable=False, default='^.*$')
