import datetime
import itertools
import logging
import re
from typing import Sequence, BinaryIO, Union, Optional, Tuple, List

import dateutil.parser
import numpy as np
import h5py

logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)


class Scan:
    index: int = None
    command: str = ''
    date: datetime.datetime
    comment: str = ''
    countingtime: float
    _data: np.ndarray
    _nextpoint: int

    def __init__(self, motorname: str, counters: Sequence[str], maxcounts: int, index: int, date: datetime.datetime,
                 comment: str, command: str, countingtime: float):
        dtype = np.dtype(list(
            zip(itertools.chain([motorname], counters),
                itertools.repeat('f4'))))
        self._data = np.zeros(maxcounts, dtype=dtype)
        self._nextpoint = 0
        self.index = index
        self.date = date
        self.command = command
        self.comment = comment
        self.countingtime = countingtime

    @property
    def columnnames(self):
        return self._data.dtype.names

    @property
    def motorname(self):
        return [n for n in self._data.dtype.names if n != 'Pt_No'][0]

    def __getitem__(self, item):
        return self._data[:self._nextpoint][item]

    def __setitem__(self, item, value):
        self._data[item] = value

    @classmethod
    def scanindicesinfile(cls, scanfile) -> List[int]:
        try:
            with h5py.File(scanfile, 'r', swmr=True, libver='latest', track_order=True, locking=False) as h5:
                return list(sorted([int(h5[f'{grp}/entry_identifier'][()]) for grp in h5]))
        except OSError:
            with open(scanfile, 'rt') as f:
                indices = []
                for line in f:
                    if m := re.match(r'#S (?P<idx>\d+)\s+', line):
                        indices.append(int(m['idx']))
            return list(sorted(indices))

    @classmethod
    def fromspecfile(cls, specfile: Union[str, BinaryIO], index: Optional[int] = None):
        if isinstance(specfile, str):
            with open(specfile, 'rb') as f:
                return cls.fromspecfile(f, index)
        for line in specfile:
            if line.startswith(b'#S') and ((index is None) or (line.split()[1].decode('utf-8') == str(index))):
                break
        else:
            raise ValueError(
                f'Could not find scan index {index} in file {specfile.name}')
        command = line.split(None, 2)[-1].decode('utf-8')
        index = int(line.split(None, 2)[1])
        startpos = []
        scan = None
        comment = []
        date = None
        countingtime = None
        for full_line in specfile:
            line = full_line.strip().decode('utf-8')
            if line.startswith('#D'):  # this is the date
                date = dateutil.parser.parse(line.split(None, 1)[1])
            elif line.startswith('#T'):  # this is the counting time
                countingtime, units = line.split(None, 2)[1:]
                if units != '(Seconds)':
                    raise ValueError(f'Unsupported time unit: {units}')
                countingtime = float(countingtime)
            # ignore these
            elif line.startswith('#G') or line.startswith('#Q') or line.startswith('#U'):
                continue
            elif line.startswith('#P'):  # motor start positions
                startpos.extend([float(x) for x in line.split()[1:]])
            elif line.startswith('#N'):  # columns
                pass
            elif line.startswith('#C'):  # comment
                comment.append(line.split(None, 1)[1])
            elif line.startswith('#L'):  # column labels
                labels = line.split()[1:]
                scan = cls(labels[0], labels[1:], 1, index,
                           date, '\n'.join(comment), command, countingtime)
                scandata = []
            elif line.startswith('#S'):  # this is another scan: break
                specfile.seek(-len(full_line), 1)
                break
            elif (not line.startswith('#')) and bool(line):  # scan data
                assert isinstance(scan, cls)
                scandata.append(tuple([float(x) for x in line.split()]))
            else:
                continue
        scan._data = np.array(scandata, dtype=scan._data.dtype)
        scan.maxpoints = len(scan._data)
        scan._nextpoint = scan.maxpoints
        return scan

    @classmethod
    def fromh5file(cls, h5file: Union[str, h5py.File], index: Optional[int] = None, hdf5_locking: bool = True):
        if isinstance(h5file, str):
            with h5py.File(h5file, 'r', swmr=True, libver='latest', track_order=True, locking=hdf5_locking) as f:
                return cls.fromh5file(f, index)
        assert isinstance(h5file, h5py.File)
        if index is None:
            entries = sorted(h5file.keys())
            entry = entries[0]
        elif index == -1:
            entries = sorted(h5file.keys())[-1]
        else:
            entry = f'entry{index}'

        entrygrp = h5file[entry]
        date = dateutil.parser.parse(
            entrygrp['start_time'][()].decode('utf-8'))
        command = entrygrp['title'][()].decode('utf-8')
        index = int(entrygrp['entry_identifier'][()])
        motors = set()
        for key in entrygrp['measurement']:
            try:
                motors.add(entrygrp[f'measurement/{key}'].attrs['axes'])
            except KeyError:
                pass
        if len(motors) != 1:
            raise RuntimeError(
                f'Ambiguity while determining which signal was the motor: {motors=}')
        motorname = motors.pop()
        columns = [motorname] + \
            [c for c in entrygrp['measurement']
             if (c != motorname) and (isinstance(entrygrp[f'measurement/{c}'], h5py.Dataset))
             and (len(entrygrp[f'measurement/{c}'].shape) == 1)]
        dtype = [(c, entrygrp[f'measurement/{c}'].dtype) for c in columns]
        datalen = entrygrp[f'measurement/{motorname}'].size

        scan = Scan(motorname, [c for c in columns if c != motorname], maxcounts=datalen,
                    index=index, date=date, comment=command, command=command, countingtime=np.nan)
        scan._data = np.zeros(datalen, dtype=dtype)
        for c in columns:
            scan._data[c] = np.array(entrygrp[f'measurement/{c}'])
        scan._nextpoint = len(scan._data)
        return scan

    def append(self, readings: Tuple[float, ...]):
        if self._nextpoint < len(self._data):
            self._data[self._nextpoint] = readings
            self._nextpoint += 1
        else:
            raise ValueError('Cannot append to scan: already done.')

    def finished(self) -> bool:
        return self._nextpoint >= self._data.size

    def __len__(self) -> int:
        return self._nextpoint

    def maxpoints(self) -> int:
        return self._data.size

    def truncate(self):
        self._nextpoint = self._data.size
