import copy
import datetime
import gzip
import logging
import os
import pickle
import re
import typing
from typing import Any, Dict, Final, List, Optional, Sequence, TextIO, Tuple

import dateutil.parser
import h5py
import numpy as np


from .headerparameter import (
    DateTimeHeaderParameter,
    FloatHeaderParameter,
    IntHeaderParameter,
    StringHeaderParameter,
    ValueAndUncertaintyHeaderParameter,
)
from .sample import Sample

ValueAndUncertaintyType = Tuple[float, float]

logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)


def float_None(x):
    if x == "None":
        return float("nan")
    else:
        return float(x)


class Header:
    motorpositions: Dict[str, float]

    distance = ValueAndUncertaintyHeaderParameter(
        ("geometry", "truedistance"),
        ("geometry", "truedistance.err"),
        default=(np.nan, 0.0),
    )
    beamposrow = ValueAndUncertaintyHeaderParameter(
        ("geometry", "beamposx"), ("geometry", "beamposx.err"), default=(np.nan, 0.0)
    )
    beamposcol = ValueAndUncertaintyHeaderParameter(
        ("geometry", "beamposy"), ("geometry", "beamposy.err"), default=(np.nan, 0.0)
    )
    pixelsize = ValueAndUncertaintyHeaderParameter(
        ("geometry", "pixelsize"), ("geometry", "pixelsize.err"), default=(1.0, 0.0)
    )
    wavelength = ValueAndUncertaintyHeaderParameter(
        ("geometry", "wavelength"),
        ("geometry", "wavelength.err"),
        default=(np.nan, 0.0),
    )
    flux = ValueAndUncertaintyHeaderParameter(
        ("datareduction", "flux"),
        ("datareduction", "flux.err"),
        default=(np.nan, np.nan),
    )
    samplex = ValueAndUncertaintyHeaderParameter(
        ("sample", "positionx.val"),
        ("sample", "positionx.err"),
        default=(np.nan, np.nan),
    )
    sampley = ValueAndUncertaintyHeaderParameter(
        ("sample", "positiony.val"),
        ("sample", "positiony.err"),
        default=(np.nan, np.nan),
    )
    temperature = ValueAndUncertaintyHeaderParameter(
        ("environment", "temperature"),
        ("environment", "temperature.err"),
        default=(np.nan, np.nan),
    )
    vacuum = ValueAndUncertaintyHeaderParameter(
        ("environment", "vacuum_pressure"),
        ("environment", "vacuum_pressure.err"),
        default=(np.nan, np.nan),
    )
    fsn_absintref = IntHeaderParameter(("datareduction", "absintrefFSN"), default=-1)
    fsn_emptybeam = IntHeaderParameter(("datareduction", "emptybeamFSN"), default=-1)
    fsn_dark = IntHeaderParameter(("datareduction", "darkFSN"), default=-1)
    dark_cps = ValueAndUncertaintyHeaderParameter(
        ("datareduction", "dark_cps.val"),
        ("datareduction", "dark_cps.err"),
        default=(np.nan, 0),
    )
    project = StringHeaderParameter(
        ("accounting", "projectid"), default="--no-project--"
    )
    projectname = StringHeaderParameter(
        ("accounting", "projectname"), default="Untitled project"
    )
    proposer = StringHeaderParameter(("accounting", "proposer"), default="Anonymous")
    username = StringHeaderParameter(("accounting", "operator"), default="Anonymous")
    distancedecrease = ValueAndUncertaintyHeaderParameter(
        ("sample", "distminus.val"), ("sample", "distminus.err"), default=(0.0, 0.0)
    )
    sample_category = StringHeaderParameter(("sample", "category"), default="sample")
    startdate = DateTimeHeaderParameter(
        ("exposure", "startdate"), default=datetime.datetime.fromtimestamp(0)
    )
    enddate = DateTimeHeaderParameter(
        ("exposure", "enddate"), default=datetime.datetime.fromtimestamp(0)
    )
    exposurecount = IntHeaderParameter(("exposure", "count"), default=1)
    date = enddate
    exposuretime = ValueAndUncertaintyHeaderParameter(
        ("exposure", "exptime"), ("exposure", "exptime.err"), default=(np.nan, 0)
    )
    absintfactor = ValueAndUncertaintyHeaderParameter(
        ("datareduction", "absintfactor"),
        ("datareduction", "absintfactor.err"),
        default=(np.nan, 0),
    )
    absintdof = IntHeaderParameter(("datareduction", "absintdof"), default=-1)
    absintchi2 = FloatHeaderParameter(
        ("datareduction", "absintchi2_red"), default=np.nan
    )
    absintqmin = FloatHeaderParameter(("datareduction", "absintqmin"), default=np.nan)
    absintqmax = FloatHeaderParameter(("datareduction", "absintqmax"), default=np.nan)

    prefix = StringHeaderParameter(("exposure", "prefix"), default="crd")
    title = StringHeaderParameter(("sample", "title"), default="Untitled")
    description = StringHeaderParameter(("sample", "description"), default="")
    preparetime = DateTimeHeaderParameter(
        ("sample", "preparetime"), default=datetime.datetime.fromtimestamp(0)
    )
    preparedby = StringHeaderParameter(("sample", "preparedby"), default="Anonymous")
    fsn = IntHeaderParameter(("exposure", "fsn"), default=-1)
    maskname = StringHeaderParameter(("geometry", "mask"), default="")
    thickness = ValueAndUncertaintyHeaderParameter(
        ("sample", "thickness.val"), ("sample", "thickness.err"), default=(np.nan, 0.0)
    )
    transmission = ValueAndUncertaintyHeaderParameter(
        ("sample", "transmission.val"),
        ("sample", "transmission.err"),
        default=(np.nan, 0.0),
    )
    _data: Dict[str, Any]

    _headerattributes_ensureunique: Final[List[str]] = [
        "title",
        "distance",
        "beamposrow",
        "beamposcol",
        "wavelength",
        "pixelsize",
        "distancedecrease",
        "prefix",
    ]

    _headerattributes_average: Final[List[str]] = [
        "transmission",
        "thickness",
        "flux",
        "samplex",
        "sampley",
        "temperature",
        "vacuum",
        "dark_cps",
        "absintfactor",
    ]

    _headerattributes_collectfirst: Final[List[str]] = [
        "startdate",
        "date",
        "fsn",
        "fsn_absintref",
        "fsn_emptybeam",
        "maskname",
        "project",
        "username",
        "description",
    ]

    _headerattributes_collectlast: Final[List[str]] = ["enddate"]

    _headerattributes_drop: Final[List[str]] = [
        "absintdof",
        "absintchi2",
        "absintqmin",
        "absintqmax",
    ]

    _headerattributes_sum: Final[List[str]] = ["exposuretime", "exposurecount"]

    def __init__(
        self, filename: Optional[str] = None, datadict: Optional[Dict[str, Any]] = None
    ):
        if filename is None and datadict is None:
            raise ValueError("Either filename or datadict must be supplied.")
        if filename is not None and datadict is not None:
            raise ValueError("Filename and datadict must not be supplied together.")
        if filename is not None:
            self._data = None
            valueerrors = []
            for mode, loader in [("rb", pickle.load), ("rt", self._readfromB1header)]:
                if filename.endswith(".gz"):
                    f = gzip.open(filename, mode)
                else:
                    f = open(filename, mode)
                try:
                    self._data = loader(f)
                except FileNotFoundError:
                    raise
                except ValueError as ve:
                    valueerrors.append(ve)
                    continue
                finally:
                    f.close()
                break
            if self._data is None:
                raise ValueError(
                    f"Could not open file {filename}. Errors occurred: {valueerrors}"
                )
        else:
            assert datadict is not None
            self._data = datadict
        if "motors" not in self._data:
            self._data["motors"] = {}
        if "exposure" not in self._data:
            self._data["exposure"] = {}
        if (
            ("prefix" not in self._data["exposure"])
            and (filename is not None)
            and (
                (
                    m := re.match(
                        r"^(?P<prefix>\w+)_(?P<fsn>\d+)\.(?P<extn>\S+)$",
                        os.path.split(filename)[-1],
                    )
                )
                is not None
            )
        ):
            self._data["exposure"]["prefix"] = m["prefix"]
        elif "prefix" not in self._data["exposure"]:
            self._data["exposure"]["prefix"] = "crd"
        else:
            logger.debug(
                f'prefix in self._data["exposure"]: {self._data["exposure"]["prefix"]}'
            )
        self.motorpositions = self._data["motors"]

    def sample(self) -> Optional[Sample]:
        return Sample.fromdict(self._data["sample"]) if "sample" in self._data else None

    @classmethod
    def average(cls, *headers: "Header") -> "Header":
        collatedvalues = {}

        def collect(f: str, hs: Sequence["Header"]) -> List[Any]:
            lis = []
            for h in hs:
                try:
                    lis.append(getattr(h, f))
                except (KeyError, TypeError):
                    continue
            return lis

        for field in cls._headerattributes_ensureunique:
            values = set(collect(field, headers))
            if len(values) < 1:
                raise ValueError(
                    f'Field {field} is not unique. Found values: {", ".join([str(x) for x in values])}'
                )
            collatedvalues[field] = values.pop()
        for field in cls._headerattributes_sum:
            values = collect(field, headers)
            if not values:
                continue
            if isinstance(cls.__dict__[field], IntHeaderParameter):
                collatedvalues[field] = int(np.sum(values))
            elif isinstance(cls.__dict__[field], FloatHeaderParameter):
                collatedvalues[field] = float(np.sum(values))
            elif isinstance(cls.__dict__[field], ValueAndUncertaintyHeaderParameter):
                val = np.array([v[0] for v in values])
                err = np.array([v[1] for v in values])
                collatedvalues[field] = (val.sum(), (err**2).sum() ** 0.5)
            else:
                raise TypeError(
                    f"Cannot sum header parameter of type {type(cls.__dict__[field])}."
                )
        for field in cls._headerattributes_average:
            values = collect(field, headers)
            if not values:
                continue
            if isinstance(cls.__dict__[field], FloatHeaderParameter):
                collatedvalues[field] = float(np.mean(values))
            elif isinstance(cls.__dict__[field], ValueAndUncertaintyHeaderParameter):
                val = np.array([v[0] for v in values])
                err = np.array([v[1] for v in values])
                if np.isfinite(err).sum() == 0:
                    # no error bars anywhere
                    err = np.ones_like(val)
                elif (err > 0).sum() == 0:
                    # no positive error bars
                    err = np.ones_like(val)
                else:
                    # some non-finite error bars may exist: replace them with the lowest positive error bar data
                    minposerr = np.nanmin(err[err > 0])
                    err[err <= 0] = minposerr
                    err[~np.isfinite(err)] = minposerr
                collatedvalues[field] = (
                    (val / err**2).sum() / (1 / err**2).sum(),
                    1 / (1 / err**2).sum() ** 0.5,
                )
            else:
                raise TypeError(
                    f"Cannot average header parameter of type {type(cls.__dict__[field])}"
                )
        for field in cls._headerattributes_collectfirst:
            try:
                collatedvalues[field] = collect(field, headers)[0]
            except IndexError:
                continue
        for field in cls._headerattributes_collectlast:
            try:
                collatedvalues[field] = collect(field, headers)[-1]
            except IndexError:
                continue
        h = cls(datadict={})
        for field in collatedvalues:
            setattr(h, field, collatedvalues[field])
        return h

    def __deepcopy__(self, *args):
        return Header(datadict=copy.deepcopy(self._data))

    _B1formatting: Tuple[List[str], re.Pattern, typing.Callable, Any] = [
        (["exposure", "fsn"], re.compile(r"FSN:\s+(\d+)"), int, -1),
        (["sample", "title"], re.compile(r"Sample name:\s+(.*)"), str, "Untitled"),
        (
            ["geometry", "truedistance"],
            re.compile(
                r"Sample-to-detector distance \(mm\):\s+([+-]?\d+(?:\.\d+)?|nan|inf|None)"
            ),
            float_None,
            np.nan,
        ),
        (
            ["sample", "thickness.val"],
            re.compile(r"Sample thickness \(cm\):\s+([+-]?\d+(?:\.\d+)?|nan|inf|None)"),
            float_None,
            np.nan,
        ),
        (
            ["sample", "positionx.val"],
            re.compile(r"Sample position \(cm\):\s+([+-]?\d+(?:\.\d+)?|nan|inf|None)"),
            float_None,
            np.nan,
        ),
        (
            ["sample", "positionx.val"],
            re.compile(r"Sample position \(mm\):\s+([+-]?\d+(?:\.\d+)?|nan|inf|None)"),
            float_None,
            np.nan,
        ),
        (
            ["exposure", "exptime"],
            re.compile(
                r"Measurement time \(sec\):\s+([+-]?\d+(?:\.\d+)?|nan|inf|None)"
            ),
            float_None,
            np.nan,
        ),
        (
            ["exposure", "exptime"],
            re.compile(r"Exposure_time:\s+([+-]?\d+(?:\.\d+)?|nan|inf|None)"),
            float_None,
            np.nan,
        ),
        (
            ["geometry", "beamposx"],
            re.compile(
                r"Beam x y for integration:\s+([+-]?\d+(?:\.\d+)?|nan|inf|None)\s+(?:[+-]?\d+(?:\.\d+)?|nan|inf|None)"
            ),
            lambda x: float(x) - 1,
            np.nan,
        ),
        (
            ["geometry", "beamposy"],
            re.compile(
                r"Beam x y for integration:\s+(?:[+-]?\d+(?:\.\d+)?|nan|inf|None)\s+([+-]?\d+(?:\.\d+)?|nan|inf|None)"
            ),
            lambda x: float(x) - 1,
            np.nan,
        ),
        (
            ["geometry", "pixelsize"],
            re.compile(
                r"Pixel size of 2D detector \(mm\):\s+([+-]?\d+(?:\.\d+)?|nan|inf|None)"
            ),
            float_None,
            np.nan,
        ),
        (
            ["exposure", "date"],
            re.compile(
                r"Date:\s+(\d{4}-\d{1,2}-\d{1,2}\s+\d{1,2}:\d{1,2}:\d{1,2}(?:\.\d{1,6})?)"
            ),
            dateutil.parser.parse,
            datetime.datetime.fromtimestamp(0),
        ),
        (
            ["datareduction", "absintrefFSN"],
            re.compile(r"Glassy carbon FSN:\s+(\d+)"),
            int,
            -1,
        ),
        (
            ["datareduction", "absintfactor"],
            re.compile(
                r"Normalisation factor \(to absolute units\):\s+([+-]?\d+(?:\.\d+)?|nan|inf|None)"
            ),
            float_None,
            np.nan,
        ),
        (
            ["datareduction", "absintfactor.err"],
            re.compile(r"NormFactorError:\s+([+-]?\d+(?:\.\d+)?|nan|inf|None)"),
            float_None,
            np.nan,
        ),
        (["exposure", "emptybeamFSN"], re.compile(r"Empty beam FSN:\s+(\d+)"), int, -1),
        (["sample", "preparedby"], re.compile(r"Preparedby:\s+(.*)"), str, ""),
        (
            ["sample", "preparetime"],
            re.compile(
                r"Preparetime:\s+(\d{4}-\d{1,2}-\d{1,2}\s+\d{1,2}:\d{1,2}:\d{1,2}(?:\.\d{1,6})?)"
            ),
            dateutil.parser.parse,
            datetime.datetime.fromtimestamp(0),
        ),
        (
            ["sample", "transmission.val"],
            re.compile(r"Sample transmission:\s+([+-]?\d+(?:\.\d+)?|nan|inf|None)"),
            float_None,
            np.nan,
        ),
        (
            ["sample", "transmission.val"],
            re.compile(r"Transmission:\s+([+-]?\d+(?:\.\d+)?|nan|inf|None)"),
            float_None,
            np.nan,
        ),
        (
            ["sample", "thickness.err"],
            re.compile(r"ThicknessError:\s+([+-]?\d+(?:\.\d+)?|nan|inf|None)"),
            float_None,
            np.nan,
        ),
        (
            ["sample", "positionx.val"],
            re.compile(r"PosSampleX:\s+([+-]?\d+(?:\.\d+)?|nan|inf|None)"),
            float_None,
            np.nan,
        ),
        (
            ["sample", "positionx.err"],
            re.compile(r"PosSampleXError:\s+([+-]?\d+(?:\.\d+)?|nan|inf|None)"),
            float_None,
            np.nan,
        ),
        (
            ["sample", "distminus"],
            re.compile(r"DistMinus:\s+([+-]?\d+(?:\.\d+)?|nan|inf|None)"),
            float_None,
            np.nan,
        ),
        (
            ["sample", "distminus.err"],
            re.compile(r"DistMinusErr:\s+([+-]?\d+(?:\.\d+)?|nan|inf|None)"),
            float_None,
            np.nan,
        ),
        (
            ["sample", "transmission.err"],
            re.compile(r"TransmError:\s+([+-]?\d+(?:\.\d+)?|nan|inf|None)"),
            float_None,
            np.nan,
        ),
        (
            ["sample", "positiony.err"],
            re.compile(r"PosSampleError:\s+([+-]?\d+(?:\.\d+)?|nan|inf|None)"),
            float_None,
            np.nan,
        ),
        (["sample", "description"], re.compile(r"SampleDescription:\s+(.*)"), str, ""),
        (
            ["sample", "situation"],
            re.compile(r"SampleSituation:\s+(.*)"),
            str,
            "vacuum",
        ),
        (["sample", "category"], re.compile(r"SampleCategory:\s+(.*)"), str, "sample"),
        (
            ["environment", "temperature_setpoint"],
            re.compile(r"TemperatureSetpoint:\s+([+-]?\d+(?:\.\d+)?|nan|inf|None)"),
            float_None,
            np.nan,
        ),
        (
            ["environment", "temperature"],
            re.compile(r"Temperature:\s+([+-]?\d+(?:\.\d+)?|nan|inf|None)"),
            float_None,
            np.nan,
        ),
        (
            ["environment", "vacuum_pressure"],
            re.compile(r"Vacuum:\s+([+-]?\d+(?:\.\d+)?|nan|inf|None)"),
            float_None,
            np.nan,
        ),
        (
            ["exposure", "enddate"],
            re.compile(
                r"EndDate:\s+(\d{4}-\d{1,2}-\d{1,2}\s+\d{1,2}:\d{1,2}:\d{1,2}(?:\.\d{1,6}))"
            ),
            dateutil.parser.parse,
            datetime.datetime.fromtimestamp(0),
        ),
        (
            ["geometry", "description"],
            re.compile(r"SetupDescription:\s+(.*)"),
            str,
            "Undescribed geometry",
        ),
        (
            ["geometry", "truedistance.err"],
            re.compile(r"DistError:\s+([+-]?\d+(?:\.\d+)?|nan|inf|None)"),
            float_None,
            np.nan,
        ),
        (
            ["geometry", "truedistance"],
            re.compile(
                r"Calibrated sample-to-detector distance \(mm\):\s+([+-]?\d+(?:\.\d+)?|nan|inf|None)"
            ),
            float_None,
            np.nan,
        ),
        (
            ["geometry", "truedistance.err"],
            re.compile(r"DistCalibratedError:\s+([+-]?\d+(?:\.\d+)?|nan|inf|None)"),
            float_None,
            np.nan,
        ),
        (
            ["geometry", "pixelsize"],
            re.compile(r"XPixel:\s+([+-]?\d+(?:\.\d+)?|nan|inf|None)"),
            float_None,
            1.0,
        ),
        (
            ["geometry", "pixelsize"],
            re.compile(r"YPixel:\s+([+-]?\d+(?:\.\d+)?|nan|inf|None)"),
            float_None,
            1.0,
        ),
        (["accounting", "operator"], re.compile(r"Owner:\s+(.*)"), str, "Anonymous"),
        (
            ["geometry", "wavelength"],
            re.compile(r"Wavelength:\s+([+-]?\d+(?:\.\d+)?|nan|inf|None)"),
            float_None,
            np.nan,
        ),
        (
            ["geometry", "wavelength.err"],
            re.compile(r"WavelengthError:\s+([+-]?\d+(?:\.\d+)?|nan|inf|None)"),
            float_None,
            np.nan,
        ),
        (
            ["accounting", "projectid"],
            re.compile(r"Project:\s+(.*)"),
            str,
            "--no-project--",
        ),
        (["geometry", "mask"], re.compile(r"maskid:\s+(.*)"), str, ""),
        (
            ["exposure", "startdate"],
            re.compile(
                r"StartDate:\s+(\d{4}-\d{1,2}-\d{1,2}\s+\d{1,2}:\d{1,2}:\d{1,2}(?:\.\d{1,6}))"
            ),
            dateutil.parser.parse,
            datetime.datetime.fromtimestamp(0),
        ),
        (
            ["datareduction", "flux"],
            re.compile(r"datareduction.flux:\s+([+-]?\d+(?:\.\d+)?|nan|inf|None)"),
            float_None,
            np.nan,
        ),
        (
            ["datareduction", "flux.err"],
            re.compile(r"datareduction.flux.err:\s+([+-]?\d+(?:\.\d+)?|nan|inf|None)"),
            float_None,
            np.nan,
        ),
        (
            ["motors", "PH1_X"],
            re.compile(r"PH1_X:\s+([+-]?\d+(?:\.\d+)?|nan|inf|None)"),
            float_None,
            np.nan,
        ),
        (
            ["motors", "PH1_Y"],
            re.compile(r"PH1_Y:\s+([+-]?\d+(?:\.\d+)?|nan|inf|None)"),
            float_None,
            np.nan,
        ),
        (
            ["motors", "PH2_X"],
            re.compile(r"PH2_X:\s+([+-]?\d+(?:\.\d+)?|nan|inf|None)"),
            float_None,
            np.nan,
        ),
        (
            ["motors", "PH2_Y"],
            re.compile(r"PH2_Y:\s+([+-]?\d+(?:\.\d+)?|nan|inf|None)"),
            float_None,
            np.nan,
        ),
        (
            ["motors", "PH3_X"],
            re.compile(r"PH3_X:\s+([+-]?\d+(?:\.\d+)?|nan|inf|None)"),
            float_None,
            np.nan,
        ),
        (
            ["motors", "PH4_Y"],
            re.compile(r"PH3_Y:\s+([+-]?\d+(?:\.\d+)?|nan|inf|None)"),
            float_None,
            np.nan,
        ),
        (
            ["motors", "Sample_X"],
            re.compile(r"Sample_X:\s+([+-]?\d+(?:\.\d+)?|nan|inf|None)"),
            float_None,
            np.nan,
        ),
        (
            ["motors", "Sample_Y"],
            re.compile(r"Sample_Y:\s+([+-]?\d+(?:\.\d+)?|nan|inf|None)"),
            float_None,
            np.nan,
        ),
        (
            ["motors", "BeamStop_X"],
            re.compile(r"BeamStop_X:\s+([+-]?\d+(?:\.\d+)?|nan|inf|None)"),
            float_None,
            np.nan,
        ),
        (
            ["motors", "BeamStop_Y"],
            re.compile(r"BeamStop_Y:\s+([+-]?\d+(?:\.\d+)?|nan|inf|None)"),
            float_None,
            np.nan,
        ),
        (
            ["accounting", "projectname"],
            re.compile(r"accounting.projectname:\s+(.*)"),
            str,
            "",
        ),
        (
            ["accounting", "proposer"],
            re.compile(r"accounting.proposer:\s+(.*)"),
            str,
            "",
        ),
    ]

    def _readfromB1header(self, f: TextIO):
        self._data = {"motors": {}}
        notmatchedlines = []
        for iline, line in enumerate(f, start=1):
            matched = False
            for dictpath, regex, typeconv, default in self._B1formatting:
                if m := regex.match(line):
                    matched = True
                    try:
                        value = typeconv(m[1])
                    except Exception as ve:
                        logger.error(f"Conversion error of {dictpath}: {ve}")
                        raise
                    dic = self._data
                    for p in dictpath[:-1]:
                        if p not in dic:
                            dic[p] = {}
                        dic = dic[p]
                    dic[dictpath[-1]] = value
            else:
                if m := re.match(
                    r"motor.(?P<motorname>.*):\s+(?P<pos>[+-]?\d+(?:\.\d+)?)", line
                ):
                    self._data["motors"][m["motorname"]] = float(m["pos"])
                    matched = True
            if not matched:
                left = line.split(":")[0].strip()
                if left not in [
                    "Energy (eV)",
                    "Primary intensity at monitor (counts/sec)",
                    "CBF_Date",
                    "CBF_X-Binary-ID",
                    "CBF_X-Binary-Element-Byte-Order",
                    "Detector",
                    "N_excluded_pixels",
                    "Gain_setting",
                    "GeniX_HT_end",
                    "GeniX_Current",
                    "CBF_X-Binary-Size-Fastest-Dimension",
                    "CBF_Content-MD5",
                    "GeniX_HT",
                    "GeniX_Current_end",
                    "CBF_conversions",
                    "CBF_Content-Type",
                    "__particle__",
                    "Excluded_pixels",
                    "Count_cutoff",
                    "Flat_field",
                    "Threshold_setting",
                    "Exposure_period",
                    "Tau",
                    "CBF_X-Binary-Size-Second-Dimension",
                    "CBF_X-Binary-Size-Padding",
                    "Silicon sensor, thickness",
                    "Trim_file",
                    "CBF_X-Binary-Number-of-Elements",
                    "Filter",
                    "CBF_Content-Transfer-Encoding",
                    "Image_path",
                    "CBF_X-Binary-Element-Type",
                    "__Origin__",
                    "CBF_X-Binary-Size",
                    "CBF_header_convention",
                    "History",
                    "VirtDet_beam_x",
                    "PID",
                    "sel_bank",
                    "camstate",
                    "imgpath",
                    "shutterstate",
                    "temperature",
                    "masterPID",
                    "exptime",
                    "VirtDet_max",
                    "sel_chip",
                    "Hpix",
                    "cameraname",
                    "VirtDet_Current",
                    "VirtDet_sum",
                    "humidity",
                    "VirtDet_Status",
                    "targetfile",
                    "lastcompletedimage",
                    "Shutter",
                    "HT",
                    "Current",
                    "VirtDet_EpochTime",
                    "diskfree",
                    "VirtDet_HT",
                    "channel",
                    "imgmode",
                    "controllingPID",
                    "Status",
                    "VirtDet_beam_y",
                    "cameradef",
                    "date",
                    "Wpix",
                    "sel_module",
                    "cameraSN",
                    "timeleft",
                    "lastimage",
                    "TubeTime",
                    "command",
                    "scanfsn",
                    "TemperatureController",
                    "VacuumGauge",
                    "PumpPower",
                    "cutoff",
                    "threshold",
                    "hpix",
                    "temperature2",
                    "temperature1",
                    "imagesremaining",
                    "camerasn",
                    "trimfile",
                    "temperature0",
                    "humidity2",
                    "wpix",
                    "tau",
                    "nimages",
                    "humidity0",
                    "humidity1",
                    "expperiod",
                    "gain",
                    "vcmp",
                    "Calibrated energy (eV)",
                    "FileName",
                    "WavelengthCalibrated",
                    "ErrorFlags",
                    "Unused_1",
                    "Unused_2",
                    "MonitorError",
                    "DistMinusError",
                    "TemperatureSet",
                    "WavelengthCalibratedError",
                    "MeasTimeError",
                ] and (
                    ("." in left)
                    and (
                        left.split(".")[0]
                        not in [
                            "devices",
                            "geometry",
                            "accounting",
                            "datareduction",
                            "exposure",
                        ]
                    )
                ):
                    notmatchedlines.append(left)
        if notmatchedlines:
            raise RuntimeError(f"Unknown lines found: {notmatchedlines!r}")
        if np.isnan(self.flux[0]):
            self.flux = (
                1 / self.absintfactor[0],
                self.absintfactor[1] / self.absintfactor[0],
            )
        return self._data

    @classmethod
    def fromH5(cls, h5file: str, samplename: str, distkey: str) -> "Header":
        with h5py.File(h5file, "r", swmr=True) as h5:
            grp = h5["Samples"][samplename][distkey]
            assert isinstance(grp, h5py.Group)
            header = Header(datadict={})
            header.beamposrow = (grp.attrs["beamcentery"], grp.attrs["beamcentery.err"])
            header.beamposcol = (grp.attrs["beamcenterx"], grp.attrs["beamcenterx.err"])
            try:
                header.flux = (grp.attrs["flux"], grp.attrs["flux.err"])
            except KeyError:
                header.flux = (0.0, 0.0)
            header.samplex = (grp.attrs["samplex"], grp.attrs["samplex.err"])
            header.sampley = (grp.attrs["sampley"], grp.attrs["sampley.err"])
            header.temperature = (
                (
                    grp.attrs["temperature"]
                    if "temperature" in grp.attrs and grp.attrs["temperature"] != "None"
                    else np.nan
                ),
                grp.attrs["temperature.err"] if "temperature.err" in grp.attrs else 0.0,
            )
            header.thickness = (grp.attrs["thickness"], grp.attrs["thickness.err"])
            header.transmission = (
                grp.attrs["transmission"],
                grp.attrs["transmission.err"],
            )
            header.vacuum = (grp.attrs["vacuum"], grp.attrs["vacuum.err"])
            header.fsn = grp.attrs["fsn"]
            header.fsn_absintref = grp.attrs["fsn_absintref"]
            header.fsn_emptybeam = grp.attrs["fsn_emptybeam"]
            header.maskname = grp.attrs["maskname"]
            header.project = grp.attrs["project"]
            header.username = grp.attrs["username"]
            header.title = grp.attrs["title"]
            header.distance = (
                grp.attrs["distance"],
                list(grp["curves"].values())[0].attrs["distance.err"],
            )
            header.distancedecreaase = (
                grp.attrs["distancedecrease"],
                list(grp["curves"].values())[0].attrs["distancedecrease.err"],
            )
            header.pixelsize = (
                grp.attrs["pixelsizex"],
                list(grp["curves"].values())[0].attrs["pixelsizex.err"],
            )
            header.wavelength = (
                grp.attrs["wavelength"],
                list(grp["curves"].values())[0].attrs["wavelength.err"],
            )
            header.sample_category = (
                grp.attrs["sample_category"]
                if "sample_category" in grp.attrs
                else "sample"
            )
            header.startdate = grp.attrs["startdate"]
            header.enddate = grp.attrs["enddate"]
            header.exposuretime = (
                grp.attrs["exposuretime"],
                grp.attrs["exposuretime.err"],
            )
            header.absintfactor = (
                grp.attrs["absintfactor"],
                grp.attrs["absintfactor.err"],
            )
            return header

    @classmethod
    def fromNeXus(cls, entry: h5py.Group, read_processed: bool = True):
        header = Header(datadict={})
        for attr, path_value, path_error, units in [
            (
                "distance",
                "NXinstrument/NXdetector/SDD",
                "NXinstrument/NXdetector/SDD_errors",
                "mm",
            ),
            (
                "beamposrow",
                "NXinstrument/NXdetector/beam_center_y",
                "NXinstrument/NXdetector/beam_center_y_errors",
                "pixels",
            ),
            (
                "beamposcol",
                "NXinstrument/NXdetector/beam_center_x",
                "NXinstrument/NXdetector/beam_center_x_errors",
                "pixels",
            ),
            (
                "pixelsize",
                "NXinstrument/NXdetector/x_pixel_size",
                "NXinstrument/NXdetector/x_pixel_size_errors",
                "mm",
            ),
            (
                "wavelength",
                "NXinstrument/NXmonochromator/wavelength",
                "NXinstrument/NXmonochromator/wavelength_errors",
                "nm",
            ),
            (
                "flux",
                "NXinstrument/intensity_at_sample/flux",
                "NXinstrument/intensity_at_sample/flux_errors",
                "sec^-1",
            ),
            ("samplex", "NXsample/x_position", "NXsample/x_position_errors", "mm"),
            ("sampley", "NXsample/y_position", "NXsample/y_position_errors", "mm"),
            ("temperature", "NXsample/temperature", "NXsample/temperature_errors", "C"),
            (
                "vacuum",
                "NXinstrument/chamber_vacuum/value",
                "NXinstrument/chamber_vacuum/value_errors",
                "mbar",
            ),
            ("exposuretime", "NXmonitor/integral", "NXmonitor/integral_errors", "s"),
            ("thickness", "NXsample/thickness", "NXsample/thickness_errors", "cm"),
            (
                "transmission",
                "NXsample/transmission",
                "NXsample/transmission_errors",
                None,
            ),
            (
                "dark_cps",
                "processed/processing_steps/dark_subtraction/dark_cps",
                "processed/processing_steps/dark_subtraction/dark_cps_errors",
                None,
            ),
            (
                "absintfactor",
                "processed/processing_steps/absolute_calibration/factor",
                "processed/processing_steps/absolute_calibration/factor_unc",
                None,
            ),
            (
                "absintchi2",
                "processed/processing_steps/absolute_calibration/chi2_red",
                None,
                None,
            ),
            (
                "absintqmin",
                "processed/processing_steps/absolute_calibration/qmin",
                None,
                None,
            ),
            (
                "absintqmax",
                "processed/processing_steps/absolute_calibration/qmax",
                None,
                None,
            ),
        ]:
            try:
                value = _h5_get_float(_h5_path_by_nxclass(entry, path_value), units)
            except KeyError:
                value = np.nan
            if path_error is None:
                setattr(header, attr, value)
            else:
                try:
                    unc = _h5_get_float(_h5_path_by_nxclass(entry, path_error), units)
                except KeyError:
                    unc = np.nan
                setattr(header, attr, (value, unc))
        for label, proclabel in [
            ("absintref", "absolute_calibration"),
            ("dark", "dark_subtraction"),
            ("empty", "empty_beam_subtraction"),
        ]:
            try:
                filename = _h5_get_string(
                    entry[f"processed/processing_steps/{proclabel}/filename"]
                )
            except KeyError:
                setattr(header, f"fsn_{label}", -1)
                continue
            m = re.match(
                r"(?P<prefix>[a-zA-Z0-9]+)_(?P<fsn>\d+)\.(?P<extn>.*)",
                os.path.split(filename)[-1],
            )
            if m is None:
                setattr(header, f"fsn_{label}", -1)
                continue
            setattr(header, f"fsn_{label}", int(m["fsn"]))

        header.project = _h5_get_string(entry["collection_identifier"])
        header.projectname = _h5_get_string(entry["collection_description"])
        header.proposer = _h5_get_string(entry["proposer/name"])
        header.username = "labuser"
        header.distancedecrease = (
            _h5_get_float(entry["credo/transformations/sample"])
            - _h5_get_float(entry["credo/transformations/calibratedsampleposition"]),
            0,
        )
        sampletype = _h5_get_string(entry["sample/type"])
        samplecategory = [
            c
            for c in Sample.Categories
            if c.value.lower() == sampletype.lower()
            or c.name.lower() == sampletype.lower()
        ][0]
        header.sample_category = samplecategory.name
        header.startdate = dateutil.parser.parse(_h5_get_string(entry["start_time"]))
        header.enddate = dateutil.parser.parse(_h5_get_string(entry["end_time"]))
        try:
            header.absintdof = _h5_get_float(
                entry["processed/processing_steps/absolute_calibration/dof"]
            )
        except KeyError:
            header.absintdof = -1
        else:
            header.absintdof = int(header.absintdof)

        header.exposurecount = 1
        header.date = header.enddate
        expid = _h5_get_string(entry["entry_identifier"])
        m = re.match(r"(?P<year>\d{4})/(?P<prefix>[a-zA-Z0-9]+)_(?P<fsn>\d{5,})", expid)
        if m is None:
            raise ValueError(f"Invalid entry_identifier ({expid}), cannot parse.")
        header.prefix = m["prefix"]
        header.fsn = int(m["fsn"])
        header.year = int(m["year"])
        header.title = _h5_get_string(entry["sample/name"])
        header.description = _h5_get_string(entry["sample/description"])
        try:
            header.preparetime = dateutil.parser.parse(
                _h5_get_string(entry["sample/preparation_date"])
            )
        except dateutil.parser.ParserError:
            header.preparetime = datetime.datetime.fromtimestamp(0.0)
        header.preparedby = _h5_get_string(entry["sample_prepared_by/name"])
        header.maskname = ""
        return header


def _h5_get_string(dataset: h5py.Dataset):
    return dataset[()].decode("utf-8")


def _h5_get_float(dataset: h5py.Dataset, units: Optional[str] = None):
    if not isinstance(dataset, h5py.Dataset):
        raise TypeError(
            f"{dataset} has type {type(dataset)}, expected h5py.Dataset. {units=}, {type(units)=}"
        )
    if units is not None:
        if "units" not in dataset.attrs:
            raise ValueError(f"units attribute not defined for dataset {dataset.name}.")
        elif dataset.attrs["units"] != units:
            raise ValueError(
                f'Units mismatch for dataset {dataset.name}: expected {units}, got {dataset.attrs["units"]}.'
            )
    if not dataset.shape:
        return float(dataset[()])
    elif dataset.shape == (1,):
        return float(dataset[0])
    else:
        raise ValueError("Multidimensional dataset")


def _h5_path_by_nxclass(
    group: h5py.Group, path: str, take_first_on_ambiguity: bool = False
):
    for pathelem in path.split("/"):
        if pathelem.startswith("NX"):
            grps = sorted(
                [
                    g
                    for g in group
                    if ("NX_class" in group[g].attrs)
                    and (group[g].attrs["NX_class"] == pathelem)
                ]
            )
            if (len(grps) > 1) and not take_first_on_ambiguity:
                raise ValueError(
                    f"Ambiguous path: group {group.name} has more than one subgroups of NeXus type {pathelem}"
                )
            elif not grps:
                raise ValueError(
                    f"Group {group.name} does not have any subgroups of NeXus type {pathelem}"
                )
            group = group[grps[0]]
        else:
            group = group[pathelem]
    if not isinstance(group, (h5py.File, h5py.Group, h5py.Dataset)):
        raise TypeError(f"Invalid group type found: {type(group)}, {group=}")
    return group
