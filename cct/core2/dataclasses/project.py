import re

import sqlalchemy.orm

from .. import orm_schema


class Project:
    projectid: str
    proposer: str
    title: str
    year: int
    category: str
    id: int

    def __init__(self, year: int, category: str, id: str, proposer: str = 'Anonymous', title: str = 'Untitled'):
        self.year = year
        self.category = category
        self.id = id
        self.proposer = proposer
        self.title = title

    def __getstate__(self):
        return {'projectid': self.projectid, 'title': self.title,
                'proposer': self.proposer, 'id': self.id,
                'category': self.category, 'year': self.year}

    def __setstate__(self, state):

        self.title = state['title']
        self.proposer = state['proposer']

    def __str__(self) -> str:
        return f'{self.projectid}: {self.title} (by {self.proposer})'

    def __eq__(self, otherproject: object) -> bool:
        if isinstance(otherproject, type(self)):
            return (otherproject.category == self.category) and \
                (otherproject.id == self.id) and \
                (otherproject.year % 100 == self.year % 100)

    def __copy__(self, ) -> "Project":
        return type(self)(self.year, self.category, self.id, self.proposer, self.title)

    @property
    def projectid(self):
        return f'{self.category} {self.year%100:02d}/{self.id:02d}'

    @projectid.setter
    def projectid(self, value):
        if (m := re.match(r'^(?P<category>.+)\s+(?P<year>\d{2,4})/(?P<id>\d+)$', value)) is None:
            raise ValueError(f'Invalid project id format: {value}')
        self.id = int(m['id'])
        self.year = int(m['year']) % 100
        self.category = m['category']

    @classmethod
    def fromORM(cls, ormprj: orm_schema.Project):
        return cls(ormprj.year, ormprj.category, ormprj.id, ormprj.proposer, ormprj.title)

    def toORM(self, session: sqlalchemy.orm.Session) -> orm_schema.Project:
        prj = session.scalar(sqlalchemy.select(orm_schema.Project).where(sqlalchemy.and_(
            orm_schema.Project.id == self.id,
            orm_schema.Project.category == self.category,
            sqlalchemy.or_(orm_schema.Project.year == (2000+self.year % 100),
                           orm_schema.Project.year == (self.year % 100)))))
        if prj is None:
            prj = orm_schema.Project(
                category=self.category, year=self.year % 100, id=self.id,
                title=self.title, proposer=self.proposer, samples=[])
            session.add(prj)
        else:
            assert isinstance(prj, orm_schema.Project)
            prj.title = self.title
            prj.proposer = self.proposer
        return prj
