import datetime
import re
from typing import List, Any
import logging

import dateutil.parser
import sqlalchemy.orm
import tango
from taurus.external.qt import QtCore
from taurus.external.qt.QtCore import Signal

from .calibrant import Calibrant
from .intensity import IntensityCalibrant
from .q import QCalibrant
from ..component import Component
from .... import orm_schema

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)


class CalibrantStore(Component, QtCore.QAbstractItemModel):
    _calibrants: List[Calibrant]
    calibrantListChanged = Signal()

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self._calibrants = []
        self.loadFromConfig()

    def rowCount(self, parent: QtCore.QModelIndex = ...) -> int:
        if not parent.isValid():
            return 2  # Q calibrants, intensity calibrants
        elif isinstance(parent.internalPointer(), Calibrant):
            return 0
        elif parent.row() == 0:
            return len(self.qcalibrants())
        elif parent.row() == 1:
            return len(self.intensitycalibrants())
        else:
            assert False

    def columnCount(self, parent: QtCore.QModelIndex = ...) -> int:
        return 4  # name, regex, date, data

    def headerData(self, section: int, orientation: QtCore.Qt.Orientation, role: int = ...) -> Any:
        if (orientation == QtCore.Qt.Orientation.Horizontal) and (role == QtCore.Qt.ItemDataRole.DisplayRole):
            return ['Name', 'Regular expression', 'Date', 'Data'][section]

    def index(self, row: int, column: int, parent: QtCore.QModelIndex = ...) -> QtCore.QModelIndex:
        if not parent.isValid():
            assert row in [0, 1]
            return self.createIndex(row, column, None)
        elif parent.internalPointer() is None:
            return self.createIndex(row, column,
                                    self.qcalibrants()[row] if parent.row() == 0 else self.intensitycalibrants()[row])
        else:
            assert False

    def parent(self, child: QtCore.QModelIndex) -> QtCore.QModelIndex:
        if not child.isValid():
            return QtCore.QModelIndex()
        elif child.internalPointer() is None:
            return QtCore.QModelIndex()
        elif isinstance(child.internalPointer(), QCalibrant):
            return self.index(0, 0, QtCore.QModelIndex())
        elif isinstance(child.internalPointer(), IntensityCalibrant):
            return self.index(1, 0, QtCore.QModelIndex())
        else:
            assert False

    def data(self, index: QtCore.QModelIndex, role: int = ...) -> Any:
        if not index.isValid():
            return None
        elif index.internalPointer() is None:
            if index.column() == 0 and role == QtCore.Qt.ItemDataRole.DisplayRole:
                return ['Q calibrants', 'Intensity calibrants'][index.row()]
        else:
            calibrant = index.internalPointer()
            assert isinstance(calibrant, Calibrant)
            if (index.column() == 0) and \
                    (role in [QtCore.Qt.ItemDataRole.DisplayRole, QtCore.Qt.ItemDataRole.EditRole]):
                return calibrant.name
            elif (index.column() == 1) and \
                    (role in [QtCore.Qt.ItemDataRole.DisplayRole, QtCore.Qt.ItemDataRole.EditRole]):
                return calibrant.regex
            elif (index.column() == 2) and \
                    (role in [QtCore.Qt.ItemDataRole.DisplayRole, QtCore.Qt.ItemDataRole.EditRole]):
                return str(calibrant.calibrationdate) \
                    if role == QtCore.Qt.ItemDataRole.DisplayRole else calibrant.calibrationdate
            elif (index.column() == 3) and \
                    (role == QtCore.Qt.ItemDataRole.DisplayRole):
                if isinstance(calibrant, QCalibrant):
                    return f'{len(calibrant)} peaks'
                elif isinstance(calibrant, IntensityCalibrant):
                    return calibrant.datafile
            elif (index.column() == 0) and (role == QtCore.Qt.ItemDataRole.ToolTipRole):
                return calibrant.description

    def setData(self, index: QtCore.QModelIndex, value: Any, role: int = ...) -> bool:
        if (not index.isValid()) or (index.internalPointer() is None):
            return False
        if role != QtCore.Qt.ItemDataRole.EditRole:
            return False
        calibrant = index.internalPointer()
        assert isinstance(calibrant, Calibrant)
        if index.column() == 0:
            if value in [c.name for c in self._calibrants]:
                return False
            calibrant.name = value
        elif index.column() == 1:
            try:
                re.compile(value)
            except re.error:
                return False
            calibrant.regex = value
        elif index.column() == 2:
            if isinstance(value, str):
                try:
                    calibrant.calibrationdate = dateutil.parser.parse(value)
                except dateutil.parser.ParserError:
                    return False
            elif isinstance(value, QtCore.QDateTime):
                calibrant.calibrationdate = datetime.datetime(value.date().year(), value.date().month(
                ), value.date().day(), value.time().hour(), value.time().minute(), value.time().second())
            elif isinstance(value, datetime.datetime):
                calibrant.calibrationdate = value
            else:
                return False
        self.dataChanged.emit(index, index)
        self.saveToConfig()
        self.calibrantListChanged.emit()
        return True

    def flags(self, index: QtCore.QModelIndex) -> QtCore.Qt.ItemFlag:
        if index.isValid() and isinstance(index.internalPointer(), Calibrant) and index.column() in [0, 1, 2]:
            return QtCore.Qt.ItemFlag.ItemNeverHasChildren | \
                QtCore.Qt.ItemFlag.ItemIsEditable | \
                QtCore.Qt.ItemFlag.ItemIsSelectable | \
                QtCore.Qt.ItemFlag.ItemIsEnabled
        elif index.isValid() and isinstance(index.internalPointer(), Calibrant):
            return QtCore.Qt.ItemFlag.ItemNeverHasChildren | \
                QtCore.Qt.ItemFlag.ItemIsEnabled | \
                QtCore.Qt.ItemFlag.ItemIsSelectable
        else:
            return QtCore.Qt.ItemFlag.ItemIsEnabled

    def qcalibrants(self) -> List[QCalibrant]:
        return [c for c in self._calibrants if isinstance(c, QCalibrant)]

    def intensitycalibrants(self) -> List[IntensityCalibrant]:
        return [c for c in self._calibrants if isinstance(c, IntensityCalibrant)]

    def loadFromConfig(self):
        self._calibrants = []
        self.cfg.setdefault(('calibrants', ), [])
        if ('calibrants' in self.cfg) and (self.cfg['calibrants']):
            logger.warning('Reading calibrants from config file')
            self.beginResetModel()
            try:
                for calibrantname in self.cfg.keysAt('calibrants'):
                    conf = self.cfg.toDict(('calibrants',  calibrantname))
                    if all([(isinstance(v, dict) and ('val' in v) and ('err' in v)) for k, v in conf.items()]):
                        # this is an old-style q-calibrant
                        calibrant = QCalibrant(calibrantname)
                        calibrant.__setstate__({'name': calibrantname,
                                                'description': '',
                                                'regex': '^' + calibrantname + '$',
                                                'calibrationdate': str(datetime.datetime.now()),
                                                'peaks': [
                                                    (peakname, conf[peakname]
                                                     ['val'], conf[peakname]['err'])
                                                    for peakname in sorted(conf)
                                                ]
                                                })
                    elif 'peaks' in conf:
                        calibrant = QCalibrant(calibrantname)
                        calibrant.__setstate__(conf)
                    elif 'datafile' in conf:
                        calibrant = IntensityCalibrant(calibrantname)
                        calibrant.__setstate__(conf)
                    else:
                        assert False
                    self._calibrants.append(calibrant)
                self._calibrants.sort(key=lambda c: c.name)
            finally:
                self.endResetModel()
            self.writeToDB()
            self.cfg['calibrants'] = {}
        else:
            logger.info('Reading calibrants from SQL DB')
            self.readFromDB()
        self.calibrantListChanged.emit()

    def saveToConfig(self):
        try:
            self.writeToDB()
        except Exception:
            for c in self._calibrants:
                self.cfg.updateAt(('calibrants',  c.name), c.__getstate__())
            missing = [k for k in self.cfg.keysAt('calibrants') if k not in [
                c.name for c in self._calibrants]]
            for k in missing:
                del self.cfg['calibrants',  k]

    @property
    def dburl(self) -> str:
        try:
            return tango.Database().get_property('CREDO', 'dburl')['dburl'][0]
        except Exception:
            raise ValueError(
                'Tango property CREDO->dburl must be a SQLAlchemy-compatible database URL')

    def writeToDB(self) -> None:
        with sqlalchemy.orm.Session(sqlalchemy.create_engine(self.dburl)) as session, session.begin():
            for calibrant in self._calibrants:
                if isinstance(calibrant, QCalibrant):
                    ormpeaks = [
                        orm_schema.QCalibrantPeak(
                            qcalibrant_id=calibrant.name,
                            id=i,
                            name=name, value=value, uncertainty=uncertainty)
                        for i, (name, value, uncertainty) in enumerate(calibrant.peaks)
                        ]
                    ormcalibrant = orm_schema.QCalibrant(
                        name=calibrant.name,
                        calibrationdate=calibrant.calibrationdate,
                        description=calibrant.description, regex=calibrant.regex, peaks=ormpeaks,)
                    session.merge(ormcalibrant)
                    logger.debug(f'Written qcalibrant {calibrant.name} to SQL DB' )
                elif isinstance(calibrant, IntensityCalibrant):
                    ormcalibrant = orm_schema.IntensityCalibrant(
                        name=calibrant.name,
                        datafile=calibrant.datafile,
                        calibrationdate=calibrant.calibrationdate,
                        description=calibrant.description,
                        regex=calibrant.regex)
                    session.merge(ormcalibrant)
                    logger.debug(f'Written intensitycalibrant {calibrant.name} to SQL DB' )
            session.commit()
        logger.debug(f'{[c.name for c in self._calibrants]}')
        with sqlalchemy.orm.Session(sqlalchemy.create_engine(self.dburl)) as session, session.begin():
            for qc in session.scalars(sqlalchemy.select(orm_schema.QCalibrant)):
                if str(qc.name) not in [c.name for c in self._calibrants]:
                    logger.debug(f'Deleting qcalibrant {qc.name} from SQL DB')
                    session.delete(qc)
            for ic in session.scalars(sqlalchemy.select(orm_schema.IntensityCalibrant)):
                if str(ic.name) not in [c.name for c in self._calibrants]:
                    logger.debug(f'Deleting intensitycalibrant {ic.name} from SQL DB')
                    session.delete(ic)

    def readFromDB(self):
        self.beginResetModel()
        self._calibrants = []
        try:
            with sqlalchemy.orm.Session(sqlalchemy.create_engine(self.dburl)) as session, session.begin():
                for qc in session.scalars(sqlalchemy.select(orm_schema.QCalibrant)):
                    assert isinstance(qc, orm_schema.QCalibrant)
                    logger.debug(f'Read qcalibrant {qc.name} from SQL DB')
                    qcal = QCalibrant(qc.name)
                    qcal.calibrationdate = qc.calibrationdate
                    qcal.description = qc.description
                    qcal.regex = qc.regex
                    qcal.peaks = [(qcp.name, qcp.value, qcp.uncertainty)
                                  for qcp in qc.peaks]
                    self._calibrants.append(qcal)
                for ic in session.scalars(sqlalchemy.select(orm_schema.IntensityCalibrant)):
                    assert isinstance(ic, orm_schema.IntensityCalibrant)
                    logger.debug(f'Read intensitycalibrant {ic.name} from SQL DB')
                    ical = IntensityCalibrant(ic.name)
                    ical.calibrationdate = ic.calibrationdate
                    ical.description = ic.description
                    ical.regex = ic.regex
                    ical.datafile = ic.datafile
                    self._calibrants.append(ical)
            self._calibrants = list(
                sorted(self._calibrants, key=lambda x: x.name))
        finally:
            logger.debug(str(self._calibrants))
            self.endResetModel()

    def addQCalibrant(self):
        i = 0
        while f'Untitled{i}' in [c.name for c in self._calibrants]:
            i += 1
        self.beginResetModel()
        self._calibrants.append(QCalibrant(f'Untitled{i}'))
        self._calibrants.sort(key=lambda c: c.name)
        self.endResetModel()
        self.saveToConfig()
        self.calibrantListChanged.emit()

    def addIntensityCalibrant(self):
        i = 0
        while f'Untitled{i}' in [c.name for c in self._calibrants]:
            i += 1
        self.beginResetModel()
        self._calibrants.append(IntensityCalibrant(f'Untitled{i}'))
        self._calibrants.sort(key=lambda c: c.name)
        self.endResetModel()
        self.saveToConfig()
        self.calibrantListChanged.emit()

    def removeCalibrant(self, name: str):
        calibrant = [c for c in self._calibrants if c.name == name][0]
        if isinstance(calibrant, QCalibrant):
            self.beginRemoveRows(self.index(0, 0, QtCore.QModelIndex()), self.qcalibrants(
            ).index(calibrant), self.qcalibrants().index(calibrant))
        elif isinstance(calibrant, IntensityCalibrant):
            self.beginRemoveRows(self.index(1, 0, QtCore.QModelIndex()), self.intensitycalibrants(
            ).index(calibrant), self.intensitycalibrants().index(calibrant))
        else:
            assert False
        self._calibrants.remove(calibrant)
        self.endRemoveRows()
        self.saveToConfig()
        self.calibrantListChanged.emit()
