import logging
from typing import Dict, Optional, Tuple
import pint

import sqlalchemy.orm
import tango
import taurus
from taurus.external.qt import QtCore
from taurus.qt.qtcore.taurusqlistener import QTaurusBaseListener

from ... import orm_schema
from .component import Component

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)


class TangoDeviceManager(Component, QtCore.QAbstractItemModel, QTaurusBaseListener):
    _devices: Dict[str, str]
    _timer: Optional[int] = None
    devicesChanged = QtCore.Signal()

    def __init__(self, **kwargs):
        self._devices = {}
        super().__init__(**kwargs)

    def onFlagChanged(self, flag: str, state: bool):
        if (flag == 'watchedtangodeviceschanged') and state:
            self.loadFromConfig()
            self.instrument.tangoflags.clearFlag(flag)

    def rowCount(self, parent=None, *args, **kwargs):
        return len(self._devices)

    def columnCount(self, parent=None, *args, **kwargs):
        return 4

    def data(self, index: QtCore.QModelIndex, role=None):
        devname = sorted(self._devices)[index.row()]
        attrname = self._devices[devname]
        if (index.column() == 0) and (role == QtCore.Qt.ItemDataRole.DisplayRole):
            return devname
        elif (index.column() == 1) and (role == QtCore.Qt.ItemDataRole.DisplayRole):
            try:
                return taurus.Device(devname).getDeviceProxy().State().name
            except Exception as exc:
                return f'Tango/Taurus error: {exc}'
        elif (index.column() == 2) and (role == QtCore.Qt.ItemDataRole.DisplayRole):
            try:
                return taurus.Device(devname).getDeviceProxy().Status()
            except Exception as exc:
                return f'Tango/Taurus error: {exc}'
        elif (index.column() == 3) and (role == QtCore.Qt.ItemDataRole.DisplayRole):
            try:
                val = taurus.Device(devname)[attrname].rvalue
                if isinstance(val, pint.Quantity):
                    return val.magnitude
                else:
                    return str(taurus.Device(devname)[attrname].rvalue)
            except Exception as exc:
                return f'Tango/Taurus error: {exc}'

    def index(self, row, column, parent=None, *args, **kwargs) -> QtCore.QModelIndex:
        return self.createIndex(row, column, None)

    def flags(self, index: QtCore.QModelIndex) -> QtCore.Qt.ItemFlags:
        return QtCore.Qt.ItemFlag.ItemIsEnabled | QtCore.Qt.ItemFlag.ItemIsSelectable | \
            QtCore.Qt.ItemFlag.ItemNeverHasChildren

    def parent(self, index: Optional[QtCore.QModelIndex] = None):
        if index is None:
            return QtCore.QObject.parent(self)
        else:
            return QtCore.QModelIndex()

    def headerData(self, section, orientation, role=None):
        if orientation == QtCore.Qt.Horizontal:
            return ['Device', 'State', 'Status', 'Watched attribute value']

    def handleEvent(self, evt_src, evt_type, evt_value):
        if evt_type == taurus.core.taurusbasetypes.TaurusEventType.Change:
            devname = evt_src.getParentObj().name
            attrname = evt_src.name
            try:
                idev = sorted(self._devices).index(devname)
            except ValueError:
                logger.error(f'Device {devname} not in the watch list')
                return
            if attrname == self._devices[devname]:
                self.dataChanged.emit(
                    self.index(idev, 3, QtCore.QModelIndex()),
                    self.index(idev, 3, QtCore.QModelIndex()))
            elif attrname == 'state':
                self.dataChanged.emit(
                    self.index(idev, 1, QtCore.QModelIndex()),
                    self.index(idev, 1, QtCore.QModelIndex()))
            elif attrname == 'status':
                self.dataChanged.emit(
                    self.index(idev, 2, QtCore.QModelIndex()),
                    self.index(idev, 2, QtCore.QModelIndex()))
            else:
                logger.error(
                    f'Attribute {attrname} is not the watched one for device {devname}')

    def startComponent(self):
        logger.debug('Starting TangoDeviceManager')
        self.instrument.tangoflags.flagchanged.connect(self.onFlagChanged)
        logger.debug('Started TangoDeviceManager')

    def stopComponent(self):
        self.instrument.tangoflags.flagchanged.disconnect(self.onFlagChanged)
        for devname in self._devices:
            dev = taurus.Device(devname)
            for attr in ['state', 'status', self._devices[devname]]:
                dev.getAttribute(attr).removeListener(self)
        self.beginResetModel()
        self._devices = {}
        self.endResetModel()

        super().stopComponent()

    @property
    def dburl(self) -> str:
        return tango.Database().get_property('CREDO', 'dburl')['dburl'][0]

    def loadFromConfig(self):
        logger.debug('Loading watched tango device list')
        try:
            engine = sqlalchemy.create_engine(
                self.dburl, poolclass=sqlalchemy.pool.NullPool)
        except ValueError:
            logger.error('Cannot contact database')
            return

        orm_schema.Base.metadata.create_all(engine)
        with sqlalchemy.orm.Session(engine) as session, session.begin():
            tangodevices = list(session.scalars(
                sqlalchemy.select(orm_schema.WatchedTangoDevice)))
            for tangodevice in tangodevices:
                logger.debug(f'{tangodevice=}')
                assert isinstance(
                    tangodevice, orm_schema.WatchedTangoDevice)
                if tangodevice.device in self._devices:
                    logger.debug(
                        f'Tango device {tangodevice.device} already present')
                    continue
                self._devices[tangodevice.device] = tangodevice.attribute
                newrow = sorted(self._devices).index(tangodevice.device)
                self.beginInsertRows(QtCore.QModelIndex(), newrow, newrow)
                logger.debug(
                    f'Added tango device {tangodevice.device} as index {newrow}')
                self.endInsertRows()
                logger.debug('Subscribing to tango events')
                dev = taurus.Device(tangodevice.device)
                for attr in ['state', 'status', tangodevice.attribute]:
                    logger.debug(f'{attr=}, adding listener')
                    dev.getAttribute(attr).addListener(self)
                    logger.debug(f'{attr=}, listener added')
                logger.debug('Subscribed to tango events')
            for idevice, devname in reversed(list(enumerate(sorted(self._devices)))):
                if devname not in [td.device for td in tangodevices]:
                    logger.debug(
                        f'Removing device {devname} because not in the watch list')
                    for attr in ['state', 'status', self._devices[devname]]:
                        dev.getAttribute(attr).removeListener(self)
                    logger.debug(
                        f'Deleted {devname} from event subscriptions')
                    self.beginRemoveRows(
                        QtCore.QModelIndex(), idevice, idevice)
                    del self._devices[devname]
                    self.endRemoveRows()
        self.devicesChanged.emit()

    def __contains__(self, item: str):
        return item in self._devices

    def devices(self) -> Dict[str, taurus.core.tango.TangoDevice]:
        return {devname: taurus.Device(devname) for devname in self._devices}

    def devicesAndAttributes(self) -> Dict[str, Tuple[taurus.core.tango.TangoDevice, str]]:
        return {devname: (taurus.Device(devname), self._devices[devname]) for devname in self._devices}
