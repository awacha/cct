import enum
import logging
import weakref
from typing import Optional

import h5py
from taurus.external.qt import QtCore
from taurus.external.qt.QtCore import Signal

from ...config2 import Config

logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)


class Component:
    """Logical component of an instrument"""
    cfg: Config
    instrument: "Instrument"
    started = Signal()
    stopped = Signal()
    stopping: bool = False
    __running: bool = False
    _panictimer: Optional[QtCore.QTimer] = None
    panicAcknowledged = Signal()

    class PanicState(enum.Enum):
        NoPanic = enum.auto()
        Panicking = enum.auto()
        Panicked = enum.auto()

    _panicking: PanicState = PanicState.NoPanic

    def __init__(self, **kwargs):  # see https://www.riverbankcomputing.com/static/Docs/PySide2/multiinheritance.html
        self.cfg = kwargs.pop('cfg')
        if isinstance(self.cfg, Config):
            logger.debug('Connecting configChanged signal')
            self.cfg.changed.connect(self.onConfigChanged)
        if (instrument := kwargs.pop('instrument')) is not None:
            self.instrument = weakref.proxy(instrument)
        else:
            self.instrument = None
        super().__init__(**kwargs)
        self.loadFromConfig()

#    @Slot('PyQt_PyObject', 'PyQt_PyObject')
    def onConfigChanged(self, path, value):
        pass

    def saveToConfig(self):
        pass

    def loadFromConfig(self):
        pass

    def startComponent(self):
        self.__running = True
        self.started.emit()

    def stopComponent(self):
        self.stopping = True
        self.__running = False
        self.stopped.emit()

    def running(self) -> bool:
        return self.__running

    def panichandler(self):
        """Default panic handler: schedules the emission of the panicAcknowledged signal soon afterwards."""
        self._panicking = self.PanicState.Panicked
        self._panictimer = QtCore.QTimer()
        self._panictimer.setTimerType(QtCore.Qt.TimerType.VeryCoarseTimer)
        self._panictimer.setSingleShot(True)
        self._panictimer.timeout.connect(self.panicAcknowledged)
        self._panictimer.start(0)

    # noinspection PyMethodMayBeStatic
    def toNeXus(self, instrumentgroup: h5py.Group) -> h5py.Group:
        """Write NeXus-formatted information

        :param instrumentgroup: HDF5 group with NX_class == 'NXinstrument'
        :type instrumentgroup: h5py.Group instance
        """
        return instrumentgroup
