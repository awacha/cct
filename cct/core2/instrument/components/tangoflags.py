import logging
from typing import List

from taurus.external.qt import QtCore
from taurus.core.tango import TangoAttribute, TangoDevice
from taurus.qt.qtcore.taurusqlistener import QObjectTaurusListener
import taurus

from .component import Component

logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)


class TangoFlags(Component, QObjectTaurusListener):
    flagdevice: TangoDevice
    flags: List[TangoAttribute]
    flagchanged = QtCore.Signal(str, bool, name='flagchanged')

    def __init__(self, *args, **kwargs):
        self.flags = []
        super().__init__(*args, **kwargs)

    def startComponent(self):
        logger.debug('Starting flags component')
        try:
            flagdevname = [
                x for x in sorted(
                    taurus.Authority().getServerNameInstances('CredoFlags'),
                    key=lambda tsi: tsi.name())[0].devices().values()
                if x.klass().name() == 'CredoFlags'][0].name()
            self.flagdevice = taurus.Device(flagdevname)
            logger.debug(f'Flag device is {flagdevname}')
        except IndexError:
            logger.warning(
                'No tango device from class "CredoFlags" found, disabling tango flag notifications')
            return
        self.flags = [
            self.flagdevice.getAttribute(tai.name())
            for tai in taurus.Authority().getDevice(flagdevname).attributes()
            if tai.name() not in ['state', 'status']
        ]
        logger.debug(f'Flags found {[f.name for f in self.flags]}')
        for attr in self.flags:
            logger.debug(f'Adding listener to flag {attr.name}')
            attr.addListener(self)
        return super().startComponent()

    def handleEvent(self, evt_src, evt_type, evt_value):
        logger.debug(
            f'handleEvent({evt_src.name}, {evt_type=}, {evt_value=})')
        assert isinstance(evt_src, TangoAttribute)
        if evt_type == taurus.core.taurusbasetypes.TaurusEventType.Change:
            self.flagchanged.emit(evt_src.name, bool(evt_value.rvalue))

    def availableFlags(self) -> List[str]:
        logger.debug('Refreshing flags')
        taurus.Authority().cache().refresh()
        flagsnow = [tai.name() for tai in taurus.Authority().getDevice(
            self.flagdevice.name).attributes() if tai.name() not in ['state', 'status']]
        for flag in self.flags[:]:
            if flag.name not in flagsnow:
                try:
                    logger.debug(
                        f'Removing listener from deleted flag {flag.name}')
                    flag.removeListener(self)
                except Exception:
                    logger.debug(
                        f'Could not remove listener from deleted flag {flag.name}')
                    pass
                self.flags.remove(flag)
        for flagname in flagsnow:
            if not [f for f in self.flags if f.name == flagname]:
                flag = taurus.Attribute(self.flagdevice.name, flagname)
                logger.debug(f'Adding listener to new flag {flagname}')
                flag.addListener(self)
                self.flags.append(flag)
        logger.debug(f'Available flags: {[a.name for a in self.flags]}')
        return [a.name for a in self.flags]

    def stopComponent(self):
        for flag in self.flags:
            try:
                flag.removeListener(self)
            except Exception:
                continue
        self.flags = []
        del self.flagdevice
        super().stopComponent()

    def createFlag(self, flagname: str):
        self.flagdevice.CreateFlag(flagname)
        self.availableFlags()

    def removeFlag(self, flagname: str):
        self.flagdevice.RemoveFlag(flagname)
        self.availableFlags()

    def setFlag(self, flagname: str, state: bool = True):
        logger.debug(f'Setting flag {flagname} to {state}')
        [f for f in self.flags if f.name == flagname][0].write(state)

    def clearFlag(self, flagname: str):
        return self.setFlag(flagname, False)
