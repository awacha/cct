import logging
from typing import Dict, Optional, List

import h5py
from taurus.external.qt import QtCore
from taurus.external.qt.QtCore import Signal, Slot

from .components.auth import UserManager
from .components.component import Component
from .components.tangodevicemanager import TangoDeviceManager
from .components.tangoflags import TangoFlags
from ..config2 import Config

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)


class Instrument(QtCore.QObject):
    _singleton_instance: "Instrument" = None
    cfg: Config
    auth: UserManager
    tangodevicemanager: TangoDeviceManager
    tangoflags: TangoFlags
    stopping: bool = False
    running: bool = False
    shutdown = Signal()
    panicAcknowledged = Signal()
    online: bool = False
    components: Dict[str, Component] = None
    _panic_components: Optional[List[List[str]]] = None
    panicreason: Optional[str] = None
    doorname: Optional[str] = None

    def __init__(self, configfile: str, doorname: Optional[str] = None):
        if type(self)._singleton_instance is not None:
            raise RuntimeError('Only one instance can exist from Instrument.')
        type(self)._singleton_instance = self
        super().__init__()
        self.online = False
        self.doorname = doorname

        self.cfg = Config(parent=self)
        logger.debug(f'Using config file {configfile}')
        try:
            self.cfg.load(configfile)
        except FileNotFoundError:
            logger.warning(f'Config file {configfile} does not exist.')
            self.cfg.filename = configfile
        except EOFError:
            logger.warning(f'Empty or invalid config file {configfile}.')
            pass
        logger.debug('Initializing components')
        # initializing components
        self.components = {}
        for componentname, componentclass in [
            ('tangoflags', TangoFlags),
            ('auth', UserManager),
            ('tangodevicemanager', TangoDeviceManager),
        ]:
            # NOTE! When you add a new component, add a corresponding entry in the panic() method as well!
            comp = componentclass(cfg=self.cfg, instrument=self, parent=self)
            setattr(self, componentname, comp)
            self.components[componentname] = comp
            comp.started.connect(self.onComponentStarted)
            comp.stopped.connect(self.onComponentStopped)

    @Slot()
    def onComponentStarted(self):
        if all([c.running() for n, c in self.components.items()]):
            logger.info('All components are up and running.')

    @Slot()
    def onComponentStopped(self):
        logger.debug(
            f'Currently running components: {", ".join(c for c in self.components if self.components[c].running())}')
        if all([not c.running() for n, c in self.components.items()]):
            self.running = False
            self.stopping = False
            logger.debug('Emitting instrument shutdown signal.')
            self.shutdown.emit()
        else:
            logger.debug(
                'Not emitting the shutdown signal yet, there are running instances: '
                f'{[c for c in self.components if self.components[c].running()]}')

    @Slot()
    def onComponentPanicAcknowledged(self):
        component: Component = self.sender()
        try:
            componentname = [
                cn for cn in self.components if self.components[cn] is component][0]
        except IndexError:
            pass  # happens at the first round.
        else:
            component.panicAcknowledged.disconnect(
                self.onComponentPanicAcknowledged)
            logger.info(
                f'Component {componentname} acknowledged the panic situation.')
            try:
                self._panic_components[0].remove(componentname)
            except ValueError:
                # should not happen, but who knows
                pass
        if self._panic_components[0]:
            # still waiting for some components from this round
            return
        # all components of this round have acted, notify the new ones
        del self._panic_components[0]
        if not self._panic_components:
            # all components done
            logger.info('Panic sequence finished.')
            self.panicAcknowledged.emit()
            # stop the instrument.
            self.stop()
            return
        for cname in self._panic_components[0]:
            self.components[cname].panicAcknowledged.connect(
                self.onComponentPanicAcknowledged)
            logger.info(f'Notifying component {cname} on the panic situation.')
            self.components[cname].panichandler()

    def setOnline(self, online: bool):
        self.online = online
        logger.info(f'Running {"on-line" if online else "off-line"}')

    def start(self):
        logger.info('Starting Instrument')
        self.running = True
        for component in self.components:
            logger.info(f'Starting component {component}')
            self.components[component].startComponent()

    def stop(self):
        logger.info('Stopping Instrument')
        self.stopping = True
        for component in reversed(self.components):
            logger.info(f'Stopping component {component}')
            self.components[component].stopComponent()
#        self.devicemanager.disconnectDevices()

    @classmethod
    def instance(cls) -> "Instrument":
        return cls._singleton_instance

    def saveConfig(self):
        for name, component in self.components.items():
            component.saveToConfig()
        self.cfg.save(self.cfg.filename)

    def panic(self, reason: str = 'Unspecified reason'):
        """Start the panic sequence

        Whenever something really bad happens, the panic sequence ensures that everything is put in a clean state and
        powered off.

        Some events which might evoke the panic sequence:
        - the user pressing the PANIC button
        - serious trouble with the X-ray source
        - grid power outage reported by the UPS
        """
        if self._panic_components is not None:
            logger.error(
                f'Another panic occurred while handling a panic situation. Reason: {reason}')
            return
        self.panicreason = reason
        self._panic_components = [
            [],
            ['tangodevicemanager'],
            ['auth', 'tangoflags'],
        ]
        self.onComponentPanicAcknowledged()

    def toNeXus(self, entrygrp: h5py.Group) -> h5py.Group:
        """Write NeXus-formatted data to a HDF5 group about the instrument

        Parameter `entrygrp` is the NXentry group into which the NXinstrument and its subgroups will be placed

        :param entrygrp: group of the NXentry
        :type entrygrp: h5py.Group instance
        """
        raise NotImplementedError()

    def __del__(self):
        self.deleteLater()

    def deleteLater(self) -> None:
        self.cfg.deleteLater()
        del self.cfg
        super().deleteLater()
