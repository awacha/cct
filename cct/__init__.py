import multiprocessing

try:
    multiprocessing.set_start_method('spawn')
except RuntimeError:
    if multiprocessing.get_start_method() != 'spawn':
        pass

# from . import cmdline, core2, dbutils2, qtgui2, tangologger, utils

from .ipython_magics import load_ipython_extension

__all__ = ['load_ipython_extension']
