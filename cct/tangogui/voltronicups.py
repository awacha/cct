from taurus.external.qt import QtWidgets
from taurus.qt.qtgui.base.taurusbase import _DEFAULT
from taurus.qt.qtgui.container import TaurusWidget
from taurus.qt.qtgui.util import UILoadable

from .tangodeviceui import TangoDeviceUI


@UILoadable()
class VoltronicWinnerWindow(TangoDeviceUI):
    deviceserverclass = 'VoltronicWinnerUPS'
    iconresource = ":icons/ups.svg"

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.loadUi()

    def setModel(self, model, *, key=_DEFAULT):
        super().setModel(model)
        for widgetname, attr in [
            ('stateTaurusLed', 'state'),
            ('statusTaurusLabel', 'status'),
            ('stateTaurusLabel', 'state'),
            ('inputVoltageTaurusLabel', 'inputvoltage'),
            ('inputFrequencyTaurusLabel', 'inputfrequency'),
            ('batteryVoltageTaurusLabel', 'batteryvoltage'),
            ('batteryCapacityTaurusLabel', 'batterycapacity'),
            ('batteryRemainingTimeTaurusLabel', 'batteryremaintime'),
            ('outputVoltageTaurusLabel', 'outputvoltage'),
            ('outputFrequencyTaurusLabel', 'outputfrequency'),
            ('outputCurrentTaurusLabel', 'outputcurrent'),
            ('loadLevelTaurusLabel', 'loadlevel_wattpercent'),
            ('pfcTemperatureTaurusLabel', 'temperature_pfc'),
            ('ambientTemperatureTaurusLabel', 'temperature_ambient'),
            ('chargerTemperatureTaurusLabel', 'temperature_charger'),
            ('modelTaurusLabel', 'modelname'),
            ('batteryCountTaurusLabel', 'batterycount'),
            ('firmwareVersionTaurusLabel', 'firmwareversion'),
            ('hardwareVersionTaurusLabel', 'hardwareversion'),
            ('batteryCapacityAHTaurusLabel', 'batterycapacity_ah'),
            ('ratedVoltageTaurusLabel', 'ratedvoltage'),
            ('ratedCurrentTaurusLabel', 'ratedcurrent'),
            ('ratedFrequencyTaurusLabel', 'ratedfrequency'),
            ('batteryVoltage2TaurusLabel', 'ratedbatteryvoltage'),
            ('batteryOKTaurusLed', 'batterylow'),
            ('bypassActiveTaurusLed', 'bypassactive'),
            ('shutdownTaurusLed', 'shutdownactive'),
            ('testingTaurusLed', 'testinprogress'),
            ('upsOKTaurusLed', 'upsfailed'),
            ('utilityPowerTaurusLed', 'utilityfail'),
            ('warningBatteryLowTaurusLed', 'warning_batterylow'),
            ('warningBatteryOpenTaurusLed', 'warning_batteryopen'),
            ('warningBatteryOverchargeTaurusLed', 'warning_batteryovercharge'),
            ('warningChargerFailTaurusLed', 'warning_chargerfail'),
            ('warningEPOTaurusLed', 'warning_epo'),
            ('warningOverloadTaurusLed', 'warning_overload'),
            ('warningOvertemperatureTaurusLed', 'warning_overtemperature'),
        ]:
            widget: TaurusWidget = self.findChild(QtWidgets.QWidget, widgetname)
            if attr:
                widget.setModel(model + '/' + attr)
            else:
                widget.setModel(model)
