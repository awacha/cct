import logging

from taurus.external.qt import QtCore, QtWidgets
from taurus.qt.qtgui.base.taurusbase import _DEFAULT
from taurus.qt.qtgui.container import TaurusWidget
from taurus.qt.qtgui.util import UILoadable
from .tangodeviceui import TangoDeviceUI


@UILoadable()
class HaakePhoenixWindow(TangoDeviceUI):
    deviceserverclass = 'HaakePhoenixCirculator'
    iconresource = ':/icons/circulator.svg'

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.setLogLevel(logging.INFO)
        self.loadUi()

    def setModel(self, model, *, key=_DEFAULT):
        super().setModel(model)
        for widgetname, attr in [
            ('stateTaurusLed', 'state'),
            ('statusTaurusLabel', 'status'),
            ('setpointTaurusLCD', 'setpoint'),
            ('temperatureTaurusLCD', 'temperature_internal'),
            ('pumpspeedTaurusLCD', 'pump_power'),
            ('lowlimitTaurusLCD', 'lowlimit'),
            ('highlimitTaurusLCD', 'highlimit'),
            ('firmwareversionTaurusLabel', 'firmwareversion'),
            ('internalcontrolTaurusLed', 'control_external'),
            ('coolingonTaurusLed', 'cooling_on'),
            ('lowlimitTaurusWheelEdit', 'lowlimit'),
            ('highlimitTaurusWheelEdit', 'highlimit'),
            ('setpointTaurusWheelEdit', 'setpoint'),
            ('startTaurusCommandButton', ''),
            ('stopTaurusCommandButton', ''),
        ]:
            widget: TaurusWidget = self.findChild(QtWidgets.QWidget, widgetname)
            if attr:
                widget.setModel(model+'/'+attr)
            else:
                widget.setModel(model)
            self.startTaurusCommandButton.setCommand('Start')
            self.stopTaurusCommandButton.setCommand('Stop')
