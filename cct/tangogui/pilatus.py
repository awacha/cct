from taurus.external.qt import QtWidgets
from taurus.external.qt.QtCore import Slot
from taurus.qt.qtgui.base.taurusbase import _DEFAULT
from taurus.qt.qtgui.container import TaurusWidget
from taurus.qt.qtgui.display import TaurusLabel
from taurus.qt.qtgui.input import TaurusValueLineEdit, TaurusValueSpinBox, TaurusValueComboBox
from taurus.qt.qtgui.util import UILoadable

from .tangodeviceui import TangoDeviceUI


@UILoadable()
class PilatusWindow(TangoDeviceUI):
    deviceserverclass = 'Pilatus'
    iconresource = ":icons/detector.svg"

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.loadUi()

    def setModel(self, model, *, key=_DEFAULT):
        super().setModel(model)
        for switcher, writewidgetclass in [
            (self.exposureTimeTaurusReadWriteSwitcher, TaurusValueSpinBox),
            (self.exposurePeriodTaurusReadWriteSwitcher, TaurusValueSpinBox),
            (self.nimagesTaurusReadWriteSwitcher, TaurusValueSpinBox),
        ]:
            switcher.setReadWidget(TaurusLabel(switcher))
            switcher.setWriteWidget(writewidgetclass(switcher))
        for widgetname, attr in [
            ('stateTaurusLed', 'state'),
            ('statusTaurusLabel', 'status'),
            ('vcmpTaurusLabel', 'vcmp'),
            ('setAgTaurusCommandButton', None),
            ('setCrTaurusCommandButton', None),
            ('setCuTaurusCommandButton', None),
            ('setFeTaurusCommandButton', None),
            ('setMoTaurusCommandButton', None),
            ('setAgTaurusCommandButton', None),
            ('imgpathTaurusLabelEditRW', 'imgpath'),
            ('exposureTimeTaurusReadWriteSwitcher', 'exptime'),
            ('nimagesTaurusReadWriteSwitcher', 'nimages'),
            ('exposurePeriodTaurusReadWriteSwitcher', 'expperiod'),
            ('stopExposureTaurusCommandButton', None),
            ('cameranameTaurusLabel', 'cameraname'),
            ('camera_snTaurusLabel', 'camera_sn'),
            ('cutoffTaurusLabel', 'cutoff'),
            ('tauTaurusLabel', 'tau'),
            ('versionTaurusLabel', 'version'),
            ('diskfreeTaurusLabel', 'diskfree'),
            ('hpixTaurusLabel', 'hpix'),
            ('wpixTaurusLabel', 'wpix'),
            ('imgmodeTaurusLabel', 'imgmode'),
            ('timeleftTaurusLabel', 'timeleft'),
            ('readonlyTaurusLed', 'readonly'),
            ('lastimageTaurusLabel', 'lastimage'),
            ('dcuclockskewTaurusLabel', 'dcuclockskew'),
            ('thresholdTaurusLabel', 'threshold'),
            ('gainTaurusLabel', 'gain'),
        ]:
            widget: TaurusWidget = self.findChild(
                QtWidgets.QWidget, widgetname)
            if attr:
                widget.setModel(model + '/' + attr)
            else:
                widget.setModel(model)
        self.on_gainComboBox_currentIndexChanged(0)

    @Slot()
    def on_trimPushButton_clicked(self):
        self.modelObj.getDeviceProxy().SetThreshold(
            self.thresholdSpinBox.value(),
            self.gainComboBox.currentText()+'G')

    @Slot()
    def on_exposePushButton_clicked(self):
        self.modelObj.getDeviceProxy().Expose(self.firstfilenameLineEdit.text())

    @Slot(int)
    def on_gainComboBox_currentIndexChanged(self, index: int) -> None:
        thresholdranges = {'low': (6685, 20202),
                           'mid': (4425, 14328),
                           'high': (3814, 11614),}
        try:
            minthreshold, maxthreshold = thresholdranges[self.gainComboBox.currentText()]
            self.thresholdSpinBox.setRange(minthreshold, maxthreshold)
        except KeyError:
            pass