import tango
import taurus.core.taurusbasetypes
from taurus.external.qt import QtWidgets
from taurus.qt.qtgui.base.taurusbase import _DEFAULT
from taurus.qt.qtgui.container import TaurusWidget
from taurus.qt.qtgui.util import UILoadable

from .tangodeviceui import TangoDeviceUI


@UILoadable()
class GenixWindow(TangoDeviceUI):
    deviceserverclass = 'Genix'
    iconresource = ':/icons/xraysource.svg'

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.loadUi()
        # self.shutterPushButton.toggled.connect(self.on_shutterPushButton_toggled)

    def setModel(self, model, *, key=_DEFAULT):
        listened_attributes = ['shutter', 'warmingup', 'xrays', 'state']
        if self.getModelObj() is not None:
            for attr in listened_attributes:
                self.modelObj.getAttribute(attr).removeListener(self)
                self.modelObj.getAttribute(attr).deactivatePolling()

        super().setModel(model)
        for attr in listened_attributes:
            self.modelObj.getAttribute(attr).addListener(self)
            self.modelObj.getAttribute(attr).activatePolling(1000)

        for widgetname, attr in [
            ('stateTaurusLed', 'state'),
            ('statusTaurusLabel', 'status'),
            ('htTaurusLCD', 'ht'),
            ('currentTaurusLCD', 'current'),
            ('powerTaurusLCD', 'power'),
            ('tubetimeTaurusLCD', 'tubetime'),
            ('tubetempTaurusLCD', 'tube_temperature'),
            ('remote_modeTaurusLed', 'remote_mode'),
            ('conditions_autoTaurusLed', 'conditions_auto'),
            ('door_faultTaurusLed', 'door_fault'),
            ('faultsTaurusLed', 'faults'),
            ('filament_faultTaurusLed', 'filament_fault'),
            ('interlockTaurusLed', 'interlock'),
            ('overriddenTaurusLed', 'overridden'),
            ('relay_interlock_faultTaurusLed', 'relay_interlock_fault'),
            ('safety_shutter_faultTaurusLed', 'safety_shutter_fault'),
            ('sensor1_faultTaurusLed', 'sensor1_fault'),
            ('sensor2_faultTaurusLed', 'sensor2_fault'),
            ('shutterTaurusLed', 'shutter'),
            ('shutter_light_faultTaurusLed', 'shutter_light_fault'),
            ('temperature_faultTaurusLed', 'temperature_fault'),
            ('tube_position_faultTaurusLed', 'tube_position_fault'),
            ('tube_warmup_neededTaurusLed', 'tube_warmup_needed'),
            ('vacuum_faultTaurusLed', 'vacuum_fault'),
            ('waterflow_faultTaurusLed', 'waterflow_fault'),
            ('xray_light_faultTaurusLed', 'xray_light_fault'),
            ('xraysTaurusLed', 'xrays'),
            ('fullpowerTaurusCommandButton', ''),
            ('poweroffTaurusCommandButton', ''),
            ('resetFaultsTaurusCommandButton', ''),
            ('standbyTaurusCommandButton', ''),
        ]:
            widget: TaurusWidget = self.findChild(
                QtWidgets.QWidget, widgetname)
            if attr:
                widget.setModel(model+'/'+attr)
            else:
                widget.setModel(model)

    def handleEvent(self, evt_src, evt_type, evt_value):
        if evt_type == taurus.core.taurusbasetypes.TaurusEventType.Change:
            for attr, pb in [
                ('shutter', self.shutterPushButton),
                ('xrays', self.xraysPushButton),
                    ('warmingup', self.warmupPushButton)]:
                if evt_src.name.lower() == attr:
                    pb.blockSignals(True)
                    pb.setChecked(bool(evt_value.rvalue))
                    pb.blockSignals(False)

    def on_shutterPushButton_toggled(self, state: bool):
        try:
            self.modelObj.getAttribute('shutter').write(state)
        except tango.DevFailed as devf:
            QtWidgets.QMessageBox.critical(
                self, f'Error while {"opening" if state else "closing"} the X-ray shutter', devf.args[0].desc)
            self.shutterPushButton.blockSignals(True)
            self.shutterPushButton.setChecked(
                self.modelObj.getAttribute('shutter').read().rvalue)
            self.shutterPushButton.blockSignals(False)

    def on_xraysPushButton_toggled(self, state: bool):
        try:
            self.modelObj.getAttribute('xrays').write(state)
        except tango.DevFailed as devf:
            QtWidgets.QMessageBox.critical(
                self, f'Error while turning the X-ray generator {"on" if state else "off"}', devf.args[0].desc)
            self.xraysPushButton.blockSignals(True)
            self.xraysPushButton.setChecked(
                self.modelObj.getAttribute('xrays').read().rvalue)
            self.xraysPushButton.blockSignals(False)

    def on_warmupPushButton_toggled(self, state: bool):
        try:
            if state:
                self.modelObj.getDeviceProxy().StartWarmUp()
            else:
                self.modelObj.getDeviceProxy().StopWarmUp()
        except tango.DevFailed as devf:
            QtWidgets.QMessageBox.critical(
                self, f'Error while {"starting" if state else "stopping"} the warm-up cycle', devf.args[0].desc)
            self.warmupPushButton.blockSignals(True)
            self.warmupPushButton.setChecked(bool(
                self.modelObj.getAttribute('warmingup').read().rvalue))
            self.warmupPushButton.blockSignals(False)
