from taurus.external.qt import QtWidgets
from taurus.qt.qtgui.base.taurusbase import _DEFAULT
from taurus.qt.qtgui.container import TaurusWidget
from taurus.qt.qtgui.util import UILoadable

from .tangodeviceui import TangoDeviceUI


@UILoadable()
class SE521Window(TangoDeviceUI):
    deviceserverclass = 'SE521Thermometer'
    iconresource = ":icons/thermometer.svg"

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.loadUi()

    def setModel(self, model, *, key=_DEFAULT):
        super().setModel(model)
        for widgetname, attr in [
            ('stateTaurusLed', 'state'),
            ('statusTaurusLabel', 'status'),
            ('t1TaurusLCD', 't1'),
            ('t2TaurusLCD', 't2'),
            ('t3TaurusLCD', 't3'),
            ('t4TaurusLCD', 't4'),
            ('t1_t2TaurusLCD', 't1_t2'),
            ('t1TaurusLabel', 't1'),
            ('t2TaurusLabel', 't2'),
            ('t3TaurusLabel', 't3'),
            ('t4TaurusLabel', 't4'),
            ('t1_t2TaurusLabel', 't1_t2'),
            ('firmwareversionTaurusLabel', 'firmwareversion'),
            ('thermistortypeTaurusLabel', 'thermistortype'),
            ('alarmTaurusLed', 'isalarm'),
            ('batteryTaurusLed', 'battery_level'),
        ]:
            widget: TaurusWidget = self.findChild(QtWidgets.QWidget, widgetname)
            if attr:
                widget.setModel(model + '/' + attr)
            else:
                widget.setModel(model)
