from ..resource import icons_rc
from taurus.qt.qtgui.container import TaurusWidget
import taurus.core.tango.tangodatabase
import logging
logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)


icons_rc.qInitResources()


class TangoDeviceUI(TaurusWidget):
    deviceserverclass: str
    iconresource: str

    @classmethod
    def findAvailableDevices(cls):
        authority: taurus.core.tango.tangodatabase.TangoAuthority = taurus.Authority()
        devices = []
        for tsi in authority.getServerNameInstances(cls.deviceserverclass):
            for devname, devinfo in tsi.devices().items():
                if devinfo.klass().name() == cls.deviceserverclass:
                    devices.append(devname)
                else:
                    logger.debug(
                        f'Device {devname} is of class {devinfo.klass().name()}, not {cls.deviceserverclass}')
        return devices
