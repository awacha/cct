"""Data reduction pipeline for SAXS exposures"""

import logging
import multiprocessing
import multiprocessing.synchronize
import os
import re
import h5py
import datetime
import traceback
from typing import List, Optional, Dict, Tuple, Any
import queue

import numpy as np
import scipy.odr
import scipy.io
import sqlalchemy

from ..core2.orm_schema import IntensityCalibrant

from .datareductionconfig import DataReductionConfig

from ..core2.algorithms.geometrycorrections import (
    angledependentabsorption,
    angledependentairtransmission,
    solidangle,
)
from ..core2.algorithms.radavg import radavg
from ..core2.dataclasses.header import Header
from ..dbutils2 import exposuredb

logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)


class ProcessingError(Exception):
    pass


class DataReductionPipeLineNeXus:
    darkfilename: Optional[str] = None
    dark_cps: Optional[Tuple[float, float]] = None
    emptyfilename: Optional[str] = None
    emptyimage: Optional[Tuple[np.ndarray, np.ndarray]] = None
    absintreffilename: Optional[str] = None
    absintdata: Optional[Dict[str, Any]] = None

    commandqueue: Optional[multiprocessing.Queue] = None
    resultqueue: Optional[multiprocessing.Queue] = None
    config: DataReductionConfig
    currentfile: str
    dbengine: sqlalchemy.Engine
    dbengine_exposures: sqlalchemy.Engine

    def __init__(
        self,
        config: DataReductionConfig,
        commandqueue: multiprocessing.Queue,
        resultqueue: multiprocessing.Queue,
    ):
        self.config = config
        self.commandqueue = commandqueue
        self.resultqueue = resultqueue
        self.currentfile = None
        self.dbengine = sqlalchemy.create_engine(
            self.config.dburl, pool_pre_ping=True, pool_recycle=300
        )
        self.dbengine_exposures = sqlalchemy.create_engine(
            self.config.dburl_expdb, pool_pre_ping=True, pool_recycle=300
        )

    @classmethod
    def run_in_background(
        cls,
        config: DataReductionConfig,
        commandqueue: multiprocessing.Queue,
        resultqueue: multiprocessing.Queue,
        stopevent: Optional[multiprocessing.synchronize.Event] = None,
    ):
        self = cls(config, commandqueue, resultqueue)
        while True:
            if (stopevent is not None) and (stopevent.is_set()):
                break
            msg = commandqueue.get()
            self.debug(f"Got msg: {msg}")
            if msg[0] == "process":
                nexusfilename = msg[1]
                self.currentfile = nexusfilename
                try:
                    processed_okay = False
                    self.debug(f"Processing NeXus file {self.currentfile}")
                    if not os.path.exists(self.currentfile):
                        raise FileNotFoundError(self.currentfile)
                    with h5py.File(self.currentfile, "a") as h5:
                        entries = [e for e in h5 if h5[e].attrs["NX_class"] == "NXentry"]
                        self.debug(f'Entries in NeXus file {self.currentfile}: {entries}. All groups: {[list(h5)]}.')
                        for entryname in sorted(
                            [e for e in h5 if h5[e].attrs["NX_class"] == "NXentry"]
                        ):
                            try:
                                self.currententry = entryname
                                self.debug(f'Processing entry "{self.currententry}"')
                                self.process(h5[entryname])
                                self.debug(f'Processed entry "{self.currententry} from file "{self.currentfile}"')
                                processed_okay = True
                            except ProcessingError as pe:
                                self.error(pe.args[0])
                            except Exception as exc:
                                self.error(repr(exc) + "\n" + traceback.format_exc())
                            finally:
                                self.resultqueue.put_nowait(
                                    (
                                        nexusfilename,
                                        entryname,
                                        (
                                            "entrydone"
                                            if processed_okay
                                            else "entryfailed"
                                        ),
                                        "OK" if processed_okay else "ERROR",
                                    )
                                )
                except FileNotFoundError:
                    self.error(f'Could not find file {self.currentfile}')
                    self.resultqueue.put_nowait(
                        (nexusfilename, None, "notfound", "ERROR")
                    )
                except KeyError as ke:
                    self.error(f'Keyerror in file "{self.currentfile}", entry "{self.currententry}": {ke}')
                    self.resultqueue.put_nowait(
                        (nexusfilename, None, f"keyerror: {ke.args[0]}", "ERROR")
                    )
                except Exception as exc:
                    self.error(f'Error while processing file "{self.currentfile}", entry "{self.currententry}": {exc}')
                    self.resultqueue.put_nowait(
                        (
                            nexusfilename,
                            None,
                            f"exception: {exc}, {traceback.format_exc()}",
                            "ERROR",
                        )
                    )
                finally:
                    self.resultqueue.put_nowait(
                        (
                            nexusfilename,
                            None,
                            "done" if processed_okay else "failed",
                            "OK" if processed_okay else "ERROR",
                        )
                    )
            elif msg[0] == "exit":
                break
            else:
                self.error(f"Unknown command: {msg[0]}")
        self.debug("Emptying command queue")
        while True:
            try:
                commandqueue.get_nowait()
            except queue.Empty:
                break
        self.dbengine.dispose()
        self.dbengine_exposures.dispose()
        self.dbengine_exposures = None
        self.dbengine = None
        self.info("Finishing background thread.")

    @staticmethod
    def path_by_nxclass(
        group: h5py.Group, path: str, take_first_on_ambiguity: bool = False
    ):
        for pathelem in path.split("/"):
            if pathelem.startswith("NX"):
                grps = sorted(
                    [
                        g
                        for g in group
                        if ("NX_class" in group[g].attrs)
                        and (group[g].attrs["NX_class"] == pathelem)
                    ]
                )
                if (len(grps) > 1) and not take_first_on_ambiguity:
                    raise ValueError(
                        f"Ambiguous path: group {group.name} has more than one subgroups of NeXus type {pathelem}"
                    )
                elif not grps:
                    raise ValueError(
                        f"Group {group.name} does not have any subgroups of NeXus type {pathelem}"
                    )
                group = group[grps[0]]
            else:
                group = group[pathelem]
        return group

    def add_process_entry(
        self, entry: h5py.Group, name: str, description: str, **kwargs
    ):
        entry['processed'].require_group('processing_steps')
        proc = entry["processed/processing_steps"]
        proc.attrs['NX_class'] = 'NXcollection'
        try:
            del proc[name]
        except KeyError:
            pass
        pg = proc.create_group(name)
        last_process_sequence_index = max(
            [
                proc[f"{gname}/sequence_index"][()]
                for gname in proc
                if "NX_class" in proc[gname].attrs
                and proc[gname].attrs["NX_class"] == "NXprocess"
            ],
            default=0,
        )
        pg.attrs.update({"NX_class": "NXprocess", "canSAS_class": "SASprocess"})
        pg.create_dataset("name", data=name)
        pg.create_dataset("description", data=description)
        pg.create_dataset("date", data=str(datetime.datetime.now()))
        pg.create_dataset("sequence_index", data=last_process_sequence_index + 1)
        for key, value in kwargs.items():
            pg.create_dataset(key, data=value)
        self.info(
            f"{name}: {description} ("
            + ", ".join([f"{key}={value}" for key, value in kwargs.items()])
            + ")"
        )

    def get_processed_intensity(self, entry: h5py.Group):
        return np.array(self.path_by_nxclass(entry, "processed/NXdata/I")), np.array(
            self.path_by_nxclass(entry, "processed/NXdata/Idev")
        )

    def get_processed_mask(self, entry: h5py.Group):
        """Mask=1 invalid, Mask=0 valid"""
        return np.array(self.path_by_nxclass(entry, "processed/NXdata/mask"))

    def get_float(self, dataset: h5py.Dataset, units: Optional[str] = None):
        if units is not None:
            if "units" not in dataset.attrs:
                raise ValueError(
                    f"units attribute not defined for dataset {dataset.name}."
                )
            elif dataset.attrs["units"] != units:
                raise ValueError(
                    f'Units mismatch for dataset {dataset.name}: expected {units}, got {dataset.attrs["units"]}.'
                )
        if not dataset.shape:
            return float(dataset[()])
        elif dataset.shape == (1,):
            return float(dataset[0])
        else:
            raise ValueError(f"Multidimensional dataset: {dataset.shape=}")

    def do_radial_average(self, entry: h5py.Group, q: np.ndarray):
        det = self.path_by_nxclass(entry, "NXinstrument/NXdetector")
        beamposx = self.get_float(det["beam_center_x"], "pixels")
        dbeamposx = self.get_float(det["beam_center_x_errors"], "pixels")
        beamposy = self.get_float(det["beam_center_y"], "pixels")
        dbeamposy = self.get_float(det["beam_center_y_errors"], "pixels")
        pixelsizex = self.get_float(det["x_pixel_size"], "mm")
        pixelsizey = self.get_float(det["y_pixel_size"], "mm")
        dpixelsizex = self.get_float(det["x_pixel_size_errors"], "mm")
        dpixelsizey = self.get_float(det["y_pixel_size_errors"], "mm")
        if (pixelsizex != pixelsizey) or (dpixelsizex != dpixelsizey):
            raise NotImplementedError("Only rectangular pixels are supported.")
        wavelength = self.get_float(
            self.path_by_nxclass(entry, "NXinstrument/NXmonochromator/wavelength"), "nm"
        )
        dwavelength = self.get_float(
            self.path_by_nxclass(
                entry, "NXinstrument/NXmonochromator/wavelength_errors"
            ),
            "nm",
        )
        distance = self.get_float(det["SDD"], "mm")
        ddistance = self.get_float(det["SDD_errors"], "mm")
        intensity, unc = self.get_processed_intensity(entry)
        mask = (self.get_processed_mask(entry) == 0).astype(np.uint8)
        q1d, i1d, u1d, dq1d, a1d, p1d = radavg(
            intensity,
            unc,
            mask,
            wavelength,
            dwavelength,
            distance,
            ddistance,
            pixelsizex,
            dpixelsizex,
            beamposy,
            dbeamposy,
            beamposx,
            dbeamposx,
            q,
            2,
            2,
        )
        return q1d, i1d, u1d, dq1d, a1d, p1d

    def process(self, entry: h5py.Group):
        self.info(f'Starting data reduction on {entry["title"][()].decode("utf-8")}')
        for operation in [
            self.prepareprocessedsubentry,
            self.add_qmatrix,
            self.sanitize_data,
            self.normalize_by_monitor,
            self.subtract_dark_background,
            self.normalize_by_transmission,
            self.subtract_empty_background,
            self.correct_geometry,
            self.divide_by_thickness,
            self.absolute_intensity_scaling,
        ]:
            try:
                operation(entry)
            except StopIteration:
                break
        entry['processed'].attrs["processing_finished"] = str(datetime.datetime.now())
        try:
            self.write_sql(entry)
        except Exception as exc:
            self.error(
                f"Error while writing exposure to SQL DB: {exc}. {traceback.format_exc()}"
            )

    def prepareprocessedsubentry(self, entry: h5py.Group):
        proc = entry.require_group("processed")
        proc.attrs["NX_class"] = "NXsubentry"
        proc.attrs["canSAS_class"] = "SASentry"
        proc.attrs["default"] = "data"
        try:
            del proc['processing_steps']
        except KeyError:
            pass
        try:
            del proc.attrs['processing_finished']
        except KeyError:
            pass
        if "definition" not in proc:
            proc.create_dataset("definition", data="NXcanSAS")
        if "run" not in proc:
            proc.create_dataset("run", data=entry.name[6:])
        for attr in ['title', "collection_description", "collection_identifier", "end_time", "entry_identifier", "experiment_identifier", "start_time", "monitor"]:
            self.debug(f'Linking {entry.name}/{attr} to {proc.name}/{attr}')
            if attr in proc:
                del proc[attr]
            proc[attr] = h5py.SoftLink(f"{entry.name}/{attr}")
        data = proc.require_group("data")
        data.attrs.update(
            {
                "axes": ["Q"],
                "mask_indices": [0, 1],
                "NX_class": "NXdata",
                "Q_indices": [0, 1],
                "canSAS_class": "SASdata",
                "mask": "mask",
                "signal": "I",
            }
        )
        for name in ["Q", "I", "Idev", "Qdev"]:
            try:
                del data[name]
            except KeyError:
                pass
        intensity = np.array(
            self.path_by_nxclass(entry, "raw/NXdata/counts"), dtype=np.double
        )
        unc = intensity**0.5
        ds_i = data.create_dataset("I", data=intensity)
        ds_i.attrs.update(
            {"type": "NX_FLOAT", "uncertainties": "Idev", "units": "counts"}
        )
        ds_u = data.create_dataset("Idev", data=unc)
        ds_u.attrs.update({"type": "NX_FLOAT", "units": "counts"})
        # remove all process notes
        for g in [
            g
            for g in proc
            if "NX_class" in proc[g].attrs and proc[g].attrs["NX_class"] == "NXprocess"
        ]:
            del proc[g]
        self.add_process_entry(entry, "import", "Imported data from raw dataset")

    def sanitize_data(self, entry: h5py.Group):
        mask = np.array(
            self.path_by_nxclass(entry, "NXinstrument/NXdetector")["pixel_mask"],
            dtype=np.uint8,
        )
        # in the NeXus file, the mask is non-zero for invalid pixels
        mask[mask != 0] = 1  # set it to only 1 or 0
        validbefore = (mask == 0).sum()
        intensity, unc = self.get_processed_intensity(entry)
        mask[intensity < 0] = 1
        mask[~np.isfinite(intensity)] = 1
        mask[unc < 0] = 1
        mask[~np.isfinite(unc)] = 1
        validafter = (mask == 0).sum()
        # Save new mask in the processed part
        data = self.path_by_nxclass(entry, "processed/NXdata")
        try:
            del data["mask"]
        except KeyError:
            pass
        data.create_dataset("mask", data=mask)
        self.add_process_entry(
            entry,
            "mask_update",
            f"Updated mask, {validbefore-validafter} more points are masked.",
            new_masked_pixels=validbefore - validafter,
        )

    def normalize_by_monitor(self, entry: h5py.Group):
        exptime = self.get_float(self.path_by_nxclass(entry, "NXmonitor/integral"), "s")
        dexptime = 0
        intensity, unc = self.get_processed_intensity(entry)

        unc = (dexptime**2 * intensity**2 / exptime**4 + unc**2 / exptime**2) ** 0.5
        intensity = intensity / exptime
        ds_i = self.path_by_nxclass(entry, "processed/NXdata/I")
        ds_i[:, :] = intensity
        ds_u = self.path_by_nxclass(entry, "processed/NXdata/Idev")
        ds_u[:, :] = unc
        ds_i.attrs["units"] = "counts/s"
        ds_u.attrs["units"] = "counts/s"
        self.add_process_entry(
            entry,
            "monitor_normalization",
            f"Normalized by exposure time {exptime} s.",
            exposure_time=exptime,
        )

    def subtract_dark_background(self, entry: h5py.Group):
        if (entry["sample/type"][()].decode("utf-8").lower() == "dark") or (
            entry["sample/name"][()].decode("utf-8").lower() == "dark"
        ):
            # this is a dark current measurement
            self.darkfilename = self.currentfile
            intensity, unc = self.get_processed_intensity(entry)
            mask = self.get_processed_mask(entry)
            dark_cps = np.nanmean(intensity[mask == 0])
            d_dark_cps = np.nanmean(unc[mask == 0] ** 2) ** 0.5
            self.add_process_entry(
                entry,
                "dark_subtraction",
                f"Level is {dark_cps:.3g} \xb1 {d_dark_cps:.3g} cps per pixel ({d_dark_cps/dark_cps*100:.2f} %).\n"
                f"This corresponds to {dark_cps*intensity.size:.3g} \xb1 {d_dark_cps*intensity.size:.3g} cps "
                "on the whole detector surface.\n"
                f"Max counts: {np.nanmax(intensity[mask==0])}, min counts: {np.nanmin(intensity[mask==0])}.",
                dark_cps=dark_cps,
                dark_cps_errors=d_dark_cps,
                filename=self.currentfile,
            )
            self.dark_cps = dark_cps, d_dark_cps
            raise StopIteration(entry)
        else:
            if self.dark_cps is None:
                raise ProcessingError(
                    "Cannot do dark background subtraction: no dark background measurement encountered yet."
                )
            intensity, unc = self.get_processed_intensity(entry)
            unc = (unc**2 + self.dark_cps[1] ** 2) ** 0.5
            intensity -= self.dark_cps[0]
            self.path_by_nxclass(entry, "processed/data/I")[:, :] = intensity
            self.path_by_nxclass(entry, "processed/data/Idev")[:, :] = unc
            self.add_process_entry(
                entry,
                "dark_subtraction",
                f"Subtracted {self.dark_cps[0]:.3g} \xb1 {self.dark_cps[1]:.3g} cps dark signal from each pixel.\n"
                f"(dark background level determined from {self.darkfilename})",
                dark_cps=self.dark_cps[0],
                dark_cps_errors=self.dark_cps[1],
                filename=self.darkfilename,
            )

    def normalize_by_transmission(self, entry: h5py.Group):
        transmission = self.get_float(
            self.path_by_nxclass(entry, "NXsample/transmission")
        ), self.get_float(self.path_by_nxclass(entry, "NXsample/transmission_errors"))
        if (transmission[0] > 1) or (transmission[0] < 0):
            raise ProcessingError(
                f"Invalid transmission value: "
                f"{transmission[0]:g} \xb1 {transmission[1]:g}."
            )
        intensity, unc = self.get_processed_intensity(entry)
        unc = (
            unc**2 / transmission[0] ** 2
            + transmission[1] ** 2 * intensity**2 / transmission[0] ** 4
        ) ** 0.5
        intensity /= transmission[0]
        self.path_by_nxclass(entry, "processed/data/I")[:, :] = intensity
        self.path_by_nxclass(entry, "processed/data/Idev")[:, :] = unc
        self.add_process_entry(
            entry,
            "normalize_by_transmission",
            f"Normalized by transmission {transmission[0]:.4g} \xb1 {transmission[1]:.4g}.",
            transmission=transmission[0],
            transmission_errors=transmission[1],
        )

    def subtract_empty_background(self, entry: h5py.Group):
        if (
            self.path_by_nxclass(entry, "NXsample/type")[()].decode("utf-8").lower()
            == "empty beam"
        ) or (self.path_by_nxclass(entry, "NXsample/name"))[()].decode(
            "utf-8"
        ).lower() == "empty_beam":
            # this is an empty beam measurement.
            self.emptyimage = self.get_processed_intensity(entry)
            self.emptyfilename = self.currentfile
            self.add_process_entry(
                entry,
                "empty_beam_subtraction",
                "This is an empty beam measurement",
                filename=self.currentfile,
            )
            raise StopIteration(entry)
        elif self.emptyimage is None:
            raise ProcessingError(
                "Empty-beam measurement not encountered yet, cannot correct for instrumental background."
            )
        else:
            intensity, unc = self.get_processed_intensity(entry)
            unc = (unc**2 + self.emptyimage[1] ** 2) ** 0.5
            intensity -= self.emptyimage[0]
            self.add_process_entry(
                entry,
                "empty_beam_subtraction",
                f"Subtracted empty beam image (from file {self.emptyfilename})",
                filename=self.emptyfilename,
            )
            self.path_by_nxclass(entry, "processed/data/I")[:, :] = intensity
            self.path_by_nxclass(entry, "processed/data/Idev")[:, :] = unc

    def correct_geometry(self, entry: h5py.Group):
        det = self.path_by_nxclass(entry, "NXinstrument/NXdetector")
        beamposx = self.get_float(det["beam_center_x"], "pixels")
        dbeamposx = self.get_float(det["beam_center_x_errors"], "pixels")
        beamposy = self.get_float(det["beam_center_y"], "pixels")
        dbeamposy = self.get_float(det["beam_center_y_errors"], "pixels")
        pixelsizex = self.get_float(det["x_pixel_size"], "mm")
        pixelsizey = self.get_float(det["y_pixel_size"], "mm")
        dpixelsizex = self.get_float(det["x_pixel_size_errors"], "mm")
        dpixelsizey = self.get_float(det["y_pixel_size_errors"], "mm")
        if (pixelsizex != pixelsizey) or (dpixelsizex != dpixelsizey):
            raise NotImplementedError("Only rectangular pixels are supported.")
        distance = self.get_float(det["SDD"], "mm")
        ddistance = self.get_float(det["SDD_errors"], "mm")
        transmission = self.get_float(
            self.path_by_nxclass(entry, "NXsample/transmission")
        )
        dtransmission = self.get_float(
            self.path_by_nxclass(entry, "NXsample/transmission_errors")
        )
        wavelength = self.get_float(
            self.path_by_nxclass(entry, "NXinstrument/NXmonochromator/wavelength"), "nm"
        )
        dwavelength = self.get_float(
            self.path_by_nxclass(
                entry, "NXinstrument/NXmonochromator/wavelength_errors"
            ),
            "nm",
        )
        intensity, unc = self.get_processed_intensity(entry)
        row = (np.arange(intensity.shape[0])[:, np.newaxis] - beamposy) * pixelsizey
        drow = (dbeamposy**2 * pixelsizey**2 + beamposy**2 * dpixelsizey**2) ** 0.5
        col = (np.arange(intensity.shape[1])[np.newaxis, :] - beamposx) * pixelsizex
        dcol = (dbeamposx**2 * pixelsizex**2 + beamposx**2 * dpixelsizex**2) ** 0.5
        radius = (row**2 + col**2) ** 0.5
        dradius = (row**2 * drow**2 + col**2 * dcol**2) ** 0.5 / radius
        tan2th = radius / distance
        dtan2th = (
            radius**2 * ddistance**2 / distance**4 + dradius**2 / distance**2
        ) ** 0.5
        twotheta = np.arctan(tan2th)
        dtwotheta = np.abs(dtan2th / (1 + tan2th**2))

        absq = 4 * np.pi * np.sin(twotheta * 0.5) / wavelength
        dabsq = (
            4
            * np.pi
            / wavelength
            * (
                np.sin(twotheta / 2) ** 2 / wavelength**2 * dwavelength**2
                + 0.25 * np.cos(twotheta / 2) ** 2 * dtwotheta**2
            )
            ** 0.5
        )

        sa, dsa = solidangle(
            twotheta, dtwotheta, distance, ddistance, pixelsizex, dpixelsizex
        )
        unc = (unc**2 * sa**2 + intensity**2 * dsa**2) ** 0.5
        intensity *= sa
        self.add_process_entry(
            entry,
            "solid_angle_correction",
            "Corrected for detector flatness (pixel solid angle)",
        )

        asa, dasa = angledependentabsorption(
            twotheta, dtwotheta, transmission, dtransmission
        )
        unc = (unc**2 * asa**2 + intensity**2 * dasa**2) ** 0.5
        intensity = intensity * asa
        self.add_process_entry(
            entry,
            "angle_dependent_absorption_correction",
            "Corrected for angle-dependent absorption on the sample.",
        )

        try:
            vacuum = self.get_float(
                self.path_by_nxclass(entry, "credo/chamber_vacuum/value"), "mbar"
            )
            aaa, daaa = angledependentairtransmission(
                twotheta, dtwotheta, vacuum, distance, ddistance
            )  # ToDo: mu_air non-default value
            unc = (unc**2 * aaa**2 + intensity**2 * daaa**2) ** 0.5
            intensity = intensity * aaa
            self.add_process_entry(
                entry,
                "air_absorption_correction",
                "Corrected for angle-dependent absorption of residual air in the vacuum path.",
                vacuum_pressure=vacuum,
            )
        except Exception:
            self.add_process_entry(
                entry,
                "no_air_absorption_correction",
                "Could not correct for angle-dependent absorption of residual air in the vacuum path.",
            )
        self.path_by_nxclass(entry, "processed/data/I")[:, :] = intensity
        self.path_by_nxclass(entry, "processed/data/Idev")[:, :] = unc

    def add_qmatrix(self, entry: h5py.Group):
        det = self.path_by_nxclass(entry, "NXinstrument/NXdetector")
        beamposx = self.get_float(det["beam_center_x"], "pixels")
        dbeamposx = self.get_float(det["beam_center_x_errors"], "pixels")
        beamposy = self.get_float(det["beam_center_y"], "pixels")
        dbeamposy = self.get_float(det["beam_center_y_errors"], "pixels")
        pixelsizex = self.get_float(det["x_pixel_size"], "mm")
        pixelsizey = self.get_float(det["y_pixel_size"], "mm")
        dpixelsizex = self.get_float(det["x_pixel_size_errors"], "mm")
        dpixelsizey = self.get_float(det["y_pixel_size_errors"], "mm")
        if (pixelsizex != pixelsizey) or (dpixelsizex != dpixelsizey):
            raise NotImplementedError("Only rectangular pixels are supported.")
        distance = self.get_float(det["SDD"], "mm")
        ddistance = self.get_float(det["SDD_errors"], "mm")
        wavelength = self.get_float(
            self.path_by_nxclass(entry, "NXinstrument/NXmonochromator/wavelength"), "nm"
        )
        dwavelength = self.get_float(
            self.path_by_nxclass(
                entry, "NXinstrument/NXmonochromator/wavelength_errors"
            ),
            "nm",
        )
        intensity, unc = self.get_processed_intensity(entry)
        row = (np.arange(intensity.shape[0])[:, np.newaxis] - beamposy) * pixelsizey
        drow = (dbeamposy**2 * pixelsizey**2 + beamposy**2 * dpixelsizey**2) ** 0.5
        col = (np.arange(intensity.shape[1])[np.newaxis, :] - beamposx) * pixelsizex
        dcol = (dbeamposx**2 * pixelsizex**2 + beamposx**2 * dpixelsizex**2) ** 0.5
        radius = (row**2 + col**2) ** 0.5
        dradius = (row**2 * drow**2 + col**2 * dcol**2) ** 0.5 / radius
        tan2th = radius / distance
        dtan2th = (
            radius**2 * ddistance**2 / distance**4 + dradius**2 / distance**2
        ) ** 0.5
        twotheta = np.arctan(tan2th)
        dtwotheta = np.abs(dtan2th / (1 + tan2th**2))

        absq = 4 * np.pi * np.sin(twotheta * 0.5) / wavelength
        dabsq = (
            4
            * np.pi
            / wavelength
            * (
                np.sin(twotheta / 2) ** 2 / wavelength**2 * dwavelength**2
                + 0.25 * np.cos(twotheta / 2) ** 2 * dtwotheta**2
            )
            ** 0.5
        )
        data = self.path_by_nxclass(entry, "processed/data")
        qds = data.create_dataset("Q", data=absq)
        dqds = data.create_dataset("Qdev", data=dabsq)
        qds.attrs.update({"units": "1/nm", "uncertainties": "Qdev"})
        dqds.attrs.update({"units": "1/nm"})


    def divide_by_thickness(self, entry: h5py.Group):
        intensity, unc = self.get_processed_intensity(entry)
        thickness = self.get_float(
            self.path_by_nxclass(entry, "NXsample/thickness"), "cm"
        )
        dthickness = self.get_float(
            self.path_by_nxclass(entry, "NXsample/thickness_errors"), "cm"
        )
        unc = (
            unc**2 / thickness**2 + dthickness**2 * intensity**2 / thickness**4
        ) ** 0.5
        intensity /= thickness
        self.add_process_entry(
            entry,
            "divided_by_sample_thickness",
            f"Divided by sample thickness {thickness:.3f} \xb1 {dthickness:.3f} cm",
            thickness=thickness,
            thickness_errors=dthickness,
        )
        ds_int = self.path_by_nxclass(entry, "processed/data/I")
        ds_int[:, :] = intensity
        ds_int.attrs["units"] = "cps/cm"
        ds_unc = self.path_by_nxclass(entry, "processed/data/Idev")
        ds_unc[:, :] = unc
        ds_unc.attrs["units"] = "cps/cm"

    def absolute_intensity_scaling(self, entry: h5py.Group):
        samplename = self.path_by_nxclass(entry, "NXsample/name")[()].decode("utf-8")
        sampletype = self.path_by_nxclass(entry, "NXsample/type")[()].decode("utf-8").lower().replace(" ", '')
        self.debug(f'Sample type: "{sampletype}"')
        if sampletype in [
            "calibrationsample",
            "normalizationsample",
        ]:
            # find corresponding reference
            with sqlalchemy.orm.Session(self.dbengine) as session:
                icalibrants: List[IntensityCalibrant] = list(
                    session.scalars(sqlalchemy.select(IntensityCalibrant))
                )
                matches = [
                    (m.end() - m.start(), ic)
                    for m, ic in [
                        (re.match(ic.regex, samplename), ic) for ic in icalibrants
                    ]
                    if m is not None
                ]
                bestmatchlength = max([length for length, ic in matches], default=0)
                bestmatches = sorted(
                    [ic for length, ic in matches if length == bestmatchlength],
                    key=lambda ic: ic.name,
                )
                if len(bestmatches) >= 1:
                    bestmatch = bestmatches[0]
                    datafile = bestmatch.datafile
                    calibdata = np.loadtxt(datafile)
                    self.info(
                        f"Using data file {datafile} (q from {np.nanmin(calibdata[:,0]):.5f} to "
                        f"{np.nanmax(calibdata[:,0]):.5f} 1/nm, {calibdata.shape[0]} points)"
                    )
                    q1d, i1d, di1d, dq1d, a1d, p1d = self.do_radial_average(
                        entry, calibdata[:, 0]
                    )
                    idx = (q1d > 0) & np.isfinite(i1d) & np.isfinite(di1d)
                    self.info(f"Radial average has {idx.sum()} points.")
                    self.info(
                        f"Measured q from {np.nanmin(q1d[idx]):.5f} to "
                        f"{np.nanmax(q1d[idx]):.5f} 1/nm, {len(q1d[idx])} points"
                    )
                    model = scipy.odr.Model(lambda params, x: x * params[0])
                    data = scipy.odr.RealData(
                        i1d[idx], calibdata[idx, 1], di1d[idx], calibdata[idx, 2]
                    )
                    odr = scipy.odr.ODR(data, model, [1.0])
                    result = odr.run()
                    factor = result.beta[0]
                    factor_unc = result.sd_beta[0]
                    chi2_red = result.res_var
                    dof = idx.sum() - 1
                    self.absintreffilename = self.currentfile
                    self.absintdata = {
                        "chi2_red": chi2_red,
                        "dof": dof,
                        "factor": factor,
                        "factor_unc": factor_unc,
                        "qmin": np.nanmin(q1d[idx]),
                        "qmax": np.nanmax(q1d[idx]),
                        "nq": idx.sum(),
                        "flux": 1 / factor,
                        "flux_unc": abs(factor_unc / factor**2),
                        "filename": self.currentfile,
                    }
                else:
                    raise ProcessingError(
                        f"No corresponding calibration data found for sample {samplename}."
                    )
        # no matching reference data, treat this sample as a normal sample.
        if self.absintdata is None:
            raise ProcessingError(
                "No absolute intensity reference measurement encountered up to now, cannot scale into"
                "absolute intensity units."
            )
        else:
            intensity, unc = self.get_processed_intensity(entry)
            unc = (
                unc**2 * self.absintdata["factor"] ** 2
                + intensity**2 * self.absintdata["factor_unc"] ** 2
            ) ** 0.5
            intensity = intensity * self.absintdata["factor"]
            self.add_process_entry(
                entry,
                "absolute_calibration",
                f"Calibrated into absolute intensity using reference data from {self.absintreffilename}.\n"
                "Common q-range with reference data: {qmin:.3f}..{qmax:.3f} ({nq} points).\n".format(
                    **self.absintdata
                )
                + "Cps/sec to 1/cm 1/sr factor: {factor:.3g} \xb1 {factor_unc:.3g}\n".format(
                    **self.absintdata
                )
                + "Fit statistics: reduced chi^2 = {chi2_red:.3g}; {dof} degrees of freedom".format(
                    **self.absintdata
                )
                + "Beam intensity at sample: {flux:.3g} \xb1 {flux_unc:.3g} photons*eta/sec".format(
                    **self.absintdata
                ),
                **self.absintdata,
            )
            ds_int = self.path_by_nxclass(entry, "processed/data/I")
            ds_int[:, :] = intensity
            ds_int.attrs["units"] = "cm^-1 sr^-1"
            ds_unc = self.path_by_nxclass(entry, "processed/data/Idev")
            ds_unc[:, :] = unc
            ds_unc.attrs["units"] = "cm^-1 sr^-1"
            try:
                del self.path_by_nxclass(entry, "NXinstrument")["intensity_at_sample"]
            except KeyError:
                pass
            beam = self.path_by_nxclass(entry, "NXinstrument").create_group(
                "intensity_at_sample"
            )
            beam.attrs.update({"NX_class": "NXbeam"})
            ds = beam.create_dataset("distance", data=0.0)
            ds.attrs["units"] = "mm"
            ds.attrs["type"] = "NX_LENGTH"
            ds = beam.create_dataset("flux", data=self.absintdata["flux"])
            ds.attrs["units"] = "sec^-1"
            ds.attrs["type"] = "NX_FLUX"
            ds = beam.create_dataset("flux_errors", data=self.absintdata["flux_unc"])
            ds.attrs["units"] = "sec^-1"
            ds.attrs["type"] = "NX_FLUX"
            try:
                del self.path_by_nxclass(entry, "NXsample")["beam"]
            except KeyError:
                pass
            self.path_by_nxclass(entry, "NXsample")["beam"] = h5py.SoftLink(self.path_by_nxclass(entry, "NXinstrument/intensity_at_sample").name)
            

    def write_sql(self, entry: h5py.Group):
        try:
            engine = self.dbengine_exposures
        except KeyError:
            self.warning("Could not find exposuredb engine url in Tango database.")
        else:
            self.debug('Creating Header object from entry')
            header = Header.fromNeXus(entry)
            self.debug('Writing exposure to SQL database')
            exposuredb.addExposure(
                header,
                engine,
                overwrite=True,
                mask=(self.get_processed_mask(entry) == 0).astype(np.uint8),
            )
            self.info('SQL database updated')

    def log(self, level: str, message: str):
        assert level in ["error", "warning", "info", "debug", "critical"]
        if self.resultqueue is not None:
            self.resultqueue.put_nowait((self.currentfile, "", level, message))

    def error(self, message: str):
        self.log("error", message)

    def debug(self, message: str):
        self.log("debug", message)

    def warning(self, message: str):
        self.log("warning", message)

    def info(self, message: str):
        self.log("info", message)
