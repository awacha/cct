import logging
import multiprocessing
import multiprocessing.synchronize
import queue
import sys
import os
from typing import Dict, List, Optional, Tuple, Type

import tango
from tango.server import Device, attribute, command, device_property

from .datareductionconfig import DataReductionConfig
from .datareductionpipeline import DataReductionPipeLine
from .datareductionpipeline_nexus import DataReductionPipeLineNeXus

logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)

logger_background = logging.getLogger(__name__ + ":background")
logger.setLevel(logging.INFO)


class DataReductionBackend:
    backend: multiprocessing.Process
    queueto: multiprocessing.Queue
    queuefrom: multiprocessing.Queue
    stopevent: multiprocessing.synchronize.Event
    submitted: int = 0

    def __init__(
        self,
        cfg: DataReductionConfig,
        backendclass: Type[DataReductionPipeLine] = DataReductionPipeLine,
    ) -> None:
        self.stopevent = multiprocessing.Event()
        self.queueto = multiprocessing.Queue()
        self.queuefrom = multiprocessing.Queue()
        self.backend = multiprocessing.Process(
            target=backendclass.run_in_background,
            args=(cfg, self.queueto, self.queuefrom, self.stopevent),
        )
        self.backend.daemon = True
        self.backend.start()

    def stop(self):
        self.stopevent.set()
        self.queueto.put_nowait(("exit"))
        self.queueto.close()

    def join(self) -> None:
        # self.queuefrom.join()
        self.backend.join()
        self.queuefrom = None
        self.queueto = None
        self.backend = None

    def getResults(self) -> List[Tuple[str, str, str, str]]:
        results = []
        while True:
            try:
                imgfile, metadatafile, what, result = self.queuefrom.get_nowait()
                if what == "done":
                    self.submitted -= 1
                elif what == "failed":
                    self.submitted -= 1
                results.append((imgfile, metadatafile, what, result))
            except queue.Empty:
                break
        return results

    def submit(self, imgfilename, metadatafilename):
        self.queueto.put_nowait(("process", imgfilename, metadatafilename))
        self.submitted += 1


class CCTDataReduction(Device):
    backends: Dict[str, DataReductionBackend] = None
    timerinterval: float = 0.1
    configsenddelay: float = 0.5
    _configsendtimer: Optional[int] = None
    timer: Optional[int] = None

    defaultmaskdir = device_property(
        dtype=tango.DevString,
        mandatory=True,
    )

    defaultoutputdir = device_property(
        dtype=tango.DevString,
        mandatory=True,
    )

    def init_device(self):
        super().init_device()
        self.backends = {}
        self._lastcompletedtask = ("", "", "", "")
        self._lastlog = ("", "", "", "", "")
        self.set_state(tango.DevState.ON)
        self.set_status("Waiting for images to process...")

    def delete_device(self):
        for name in self.backends:
            self.stopbackend(name)
        return super().delete_device()

    completedtask = attribute(
        dtype=("DevString",),
        max_dim_x=4,
        label="Last completed task",
        doc="Information on the most recently completed task in four strings: "
        "(input image file, as given at submission time), "
        "(input metadata file, as given at submission time), "
        "(backend name),"
        "(result message: either 'OK' or an error message)",
    )

    log = attribute(
        dtype=("DevString",),
        max_dim_x=5,
        label="Log messages from processing",
        doc="The most recent log message from data reduction in five strings:"
        "(input image file, as given at submission time), "
        "(input metadata file, as given at submission time), "
        "(backend name),"
        '(log level: "debug", "info", "warning", "error", "critical")'
        "(the log message)",
    )

    runningbackends = attribute(
        dtype=[str],
        label="Running backends",
    )

    runningjobs = attribute(dtype="DevULong64", label="Number of running jobs")

    def read_completedtask(self):
        return self._lastcompletedtask

    def read_log(self):
        return self._lastlog

    def read_runningbackends(self):
        return list(sorted(self.backends.keys()))

    def read_runningjobs(self):
        return sum([b.submitted for b in self.backends.values()], start=0)

    @command(
        dtype_in=tango.DevVarStringArray,
        doc_in="A list of strings: (absolute) path to the input image, (absolute) path to the input metadata file, "
        "the name of the backend to use",
        dtype_out=tango.DevVoid,
    )
    def submit(self, argin):
        imgfile, headerfile, backendname = argin
        if backendname not in self.backends:
            if os.path.splitext(imgfile)[-1].lower() in [
                ".nxs",
                ".nx5",
                ".h5",
                ".hdf5",
                ".nx",
            ]:
                pipelineclassname = "nexus"
            else:
                pipelineclassname = "pickle"
            self.startbackend(
                [
                    backendname,
                    self.defaultoutputdir,
                    self.defaultmaskdir,
                    pipelineclassname,
                ]
            )
        self.backends[backendname].submit(imgfile, headerfile)
        rj = self.read_runningjobs()
        self.set_state(tango.DevState.RUNNING if rj else tango.DevState.ON)
        self.set_status(f"Working on {rj} submitted jobs" if rj else "Waiting for jobs")

    @command(
        dtype_in=tango.DevVarStringArray,
        doc_in="backend name, output directory, mask director, [optional: pipeline class name]",
    )
    def startbackend(self, argin):
        """Start a new backend"""
        name, outputdir, maskdir, *other_ = argin
        if other_:
            pipelineclassname = other_[0]
            if pipelineclassname.lower() in ["datareductionpipeline", "cct", "pickle"]:
                pipelineclass = DataReductionPipeLine
            elif pipelineclassname.lower() in ["datareductionpipelinenexus", "nexus"]:
                pipelineclass = DataReductionPipeLineNeXus
        if name in self.backends:
            raise RuntimeError(
                f'Data reduction pipeline with name "{name}" already running.'
            )
        try:
            dburl = tango.Database().get_property("CREDO", "dburl")["dburl"][0]
        except Exception:
            raise ValueError(
                "Tango property CREDO->dburl must be a SQLAlchemy-compatible database URL"
            )
        try:
            dburl_expdb = tango.Database().get_property("CREDO", "dburl.exposures")[
                "dburl.exposures"
            ][0]
        except Exception:
            raise ValueError(
                "Tango property CREDO->dburl.exposures must be a SQLAlchemy-compatible database URL"
            )
        config = DataReductionConfig(outputdir, dburl, dburl_expdb, maskdir)
        self.backends[name] = DataReductionBackend(config, pipelineclass)

    @command(dtype_in=tango.DevString, doc_in="Stop a running backend")
    def stopbackend(self, name):
        self.backends[name].stop()
        self.backends[name].join()
        del self.backends[name]

    @command(dtype_in=tango.DevVoid, polling_period=200)
    def checkbackendresults(self):
        for name, backend in self.backends.items():
            for imgfile, metadatafile, what, result in backend.getResults():
                self.debug_stream(f'Result: {imgfile=}, {metadatafile=}, {what=}, {result=}')
                if what == "done":
                    self._lastcompletedtask = (imgfile, metadatafile, name, "OK")
                    self.completedtask.set_value(self._lastcompletedtask)
                elif what == "failed":
                    self._lastcompletedtask = (imgfile, metadatafile, name, result)
                elif what in ["debug", "info", "warning", "error", "critical"]:
                    self._lastlog = (imgfile, metadatafile, name, what, result)
                    if what == "info":
                        self.info_stream(
                            f"{imgfile=}, {metadatafile=}, {name=}: {result}"
                        )
                    elif what == "debug":
                        self.debug_stream(
                            f"{imgfile=}, {metadatafile=}, {name=}: {result}"
                        )
                    elif what == "error":
                        self.error_stream(
                            f"{imgfile=}, {metadatafile=}, {name=}: {result}"
                        )
                    elif what == "critical":
                        self.fatal_stream(
                            f"{imgfile=}, {metadatafile=}, {name=}: {result}"
                        )
                    elif what == "warning":
                        self.warn_stream(
                            f"{imgfile=}, {metadatafile=}, {name=}: {result}"
                        )
                elif what == "entrydone":
                    self.info_stream(
                        f"Entry {metadatafile} done in NeXus file {imgfile}."
                    )
                elif what == "entryfailed":
                    self.error_stream(
                        f"Error while processing entry {metadatafile} in NeXus file {imgfile}."
                    )
                elif what == "notfound":
                    self.error_stream(f"NeXus file {imgfile} not found.")
                else:
                    assert False
        rj = self.read_runningjobs()
        self.set_state(tango.DevState.RUNNING if rj else tango.DevState.ON)
        self.set_status(f"Working on {rj} submitted jobs" if rj else "Waiting for jobs")


def main():
    CCTDataReduction.run_server(msg_stream=sys.stderr, raises=True, verbose=True)
