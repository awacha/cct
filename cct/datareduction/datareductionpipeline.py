"""Data reduction pipeline for SAXS exposures"""
import logging
import multiprocessing
import multiprocessing.synchronize
import pickle
import re
import traceback
from typing import List, Optional, Dict
import os
import queue

import gzip
import numpy as np
import scipy.odr
import scipy.io
import fabio
import sqlalchemy

from ..core2.orm_schema import IntensityCalibrant

from .datareductionconfig import DataReductionConfig

from ..core2.algorithms.geometrycorrections import angledependentabsorption, angledependentairtransmission, solidangle
from ..core2.dataclasses import Exposure, Sample, Header
from ..dbutils2 import exposuredb

logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)


class ProcessingError(Exception):
    pass


class DataReductionPipeLine:
    dark: Optional[Exposure] = None
    emptybeam: Optional[Exposure] = None
    absintref: Optional[Exposure] = None
    commandqueue: Optional[multiprocessing.Queue] = None
    resultqueue: Optional[multiprocessing.Queue] = None
    config: DataReductionConfig
    currentimagefile: str
    currentmetadatafile: str

    def __init__(self, config: DataReductionConfig, commandqueue: multiprocessing.Queue,
                 resultqueue: multiprocessing.Queue):
        self.config = config
        self.commandqueue = commandqueue
        self.resultqueue = resultqueue
        self.currentimagefile = None
        self.currentmetadatafile = None

    def loadHeader(self, filename: str):
        return Header(filename)

    def loadExposure(self, imgfilename: str, header: Header):
        img = fabio.open(imgfilename).data
        unc = np.ones_like(img)
        unc[img > 0] = img[img > 0]**0.5
        return Exposure(img, header, unc, self.loadMask(header.maskname))

    def loadMask(self, maskname: str, find_file: bool = False):
        if find_file:
            path, maskname = os.path.split(maskname)
            if not (maskname.endswith('.npy') or maskname.endswith('.mat')):
                maskname = maskname + '.npy'
            for folder, folders, files in os.walk(self.config.maskdir):
                if maskname in files:
                    filename = os.path.join(folder, maskname)
                    break
            else:
                raise FileNotFoundError(
                    f'Could not find file {maskname} in mask directory {self.config.maskdir}')
        else:
            filename = maskname
        if filename.endswith('.npy'):
            return np.load(filename)
        elif filename.endswith('.mat'):
            m = scipy.io.loadmat(filename, appendmat=False)
            return m[[k for k in list(m.keys()) if not (k.startswith('__') or k.endswith('__'))][0]]
        else:
            raise ValueError(f'Unknown file format: {filename}')

    @classmethod
    def run_in_background(
            cls, config: DataReductionConfig, commandqueue: multiprocessing.Queue,
            resultqueue: multiprocessing.Queue, stopevent: Optional[multiprocessing.synchronize.Event] = None):
        self = cls(config, commandqueue, resultqueue)
        while True:
            if (stopevent is not None) and (stopevent.is_set()):
                break
            msg = commandqueue.get()
            if msg[0] == 'process':
                imgfilename, metadatafilename = msg[1:3]
                try:
                    header = Header(metadatafilename)
                    exposure = self.loadExposure(imgfilename, header)
                except Exception as exc:
                    self.resultqueue.put_nowait(
                        (imgfilename, metadatafilename, 'failed', f'{exc}'))
                    continue
                try:
                    self.currentimagefile = imgfilename
                    self.currentmetadatafile = metadatafilename
                    processed_okay = False
                    self.debug('Processing exposure')
                    exposure = self.process(exposure)
                    processed_okay = True
                except ProcessingError as pe:
                    self.error(pe.args[0])
                except Exception as exc:
                    self.error(repr(exc) + '\n' + traceback.format_exc())
                finally:
                    self.resultqueue.put_nowait(
                        (imgfilename, metadatafilename,
                         'done' if processed_okay else 'failed',
                         'OK' if processed_okay else 'ERROR'))
            elif msg[0] == 'exit':
                break
            else:
                self.error(f'Unknown command: {msg[0]}')
        self.debug('Emptying command queue')
        while True:
            try:
                commandqueue.get_nowait()
            except queue.Empty:
                break
        self.info('Finishing background thread.')

    def process(self, exposure: Exposure) -> Exposure:
        self.info(
            f'Starting data reduction on exposure #{exposure.header.fsn}: {exposure.header.title} @ '
            f'{exposure.header.distance[0]:.2f} mm, category={exposure.header.sample().category!r}, '
            f'{type(exposure.header.sample().category)=}')
        for operation in [self.sanitize_data, self.normalize_by_monitor, self.subtract_dark_background,
                          self.normalize_by_transmission, self.subtract_empty_background, self.correct_geometry,
                          self.divide_by_thickness, self.absolute_intensity_scaling]:
            try:
                exposure = operation(exposure)
            except StopIteration as si:
                exposure = si.args[0]
                break
        try:
            self.write_sql(exposure)
        except Exception as exc:
            self.error(f'Error while writing exposure to SQL DB: {exc}.')
        os.makedirs(self.config.outputdirectory, exist_ok=True)
        maskstat = self.get_statistics(exposure)
        self.info(f'Saving corrected image for fsn {exposure.header.fsn}.')
        self.debug(f'Mask statistics: {maskstat}')
        exposure.save(
            os.path.join(
                self.config.outputdirectory,
                os.path.splitext(os.path.split(self.currentimagefile)[1])[0] + '.npz')
        )
        with gzip.open(
                os.path.join(
                    self.config.outputdirectory,
                    os.path.split(self.currentmetadatafile)[1].split('.', 1)[0] + '.pickle.gz'),
                'w') as f:
            pickle.dump(exposure.header._data, f)
        return exposure

    def sanitize_data(self, exposure: Exposure) -> Exposure:
        exposure.mask[exposure.mask != 0] = 1  # set it to only 1 or 0.
        validbefore = exposure.mask.sum()
        exposure.mask[exposure.intensity < 0] = 0
        exposure.mask[~np.isfinite(exposure.intensity)] = 0
        exposure.mask[~np.isfinite(exposure.uncertainty)] = 0
        exposure.mask[exposure.uncertainty < 0] = 0
        # !!! REALLY IMPORTANT!!! Int and bool indexes work differently
        exposure.mask = exposure.mask.astype(bool)
        validafter = exposure.mask.sum()
        self.info(
            f'FSN #{exposure.header.fsn}: after sanitization {validbefore-validafter} more points are masked.')
        return exposure

    def normalize_by_monitor(self, exposure: Exposure) -> Exposure:
        exposure.uncertainty = (exposure.header.exposuretime[1] ** 2 * exposure.intensity ** 2 /
                                exposure.header.exposuretime[0] ** 4 + exposure.uncertainty ** 2 /
                                exposure.header.exposuretime[0] ** 2) ** 0.5
        exposure.intensity = exposure.intensity / \
            exposure.header.exposuretime[0]
        self.info(
            f'FSN #{exposure.header.fsn}: normalized by exposure time {exposure.header.exposuretime[0]} s.')
        return exposure

    def subtract_dark_background(self, exposure: Exposure) -> Exposure:
        if (exposure.header.sample().category == Sample.Categories.Dark) or (exposure.header.sample().title == 'Dark'):
            # this is a dark current measurement
            self.dark = exposure
            self.dark.header.fsn_dark = self.dark.header.fsn
            self.dark.header.dark_cps = exposure.intensity[exposure.mask].mean(
            ), (exposure.uncertainty[exposure.mask]**2).mean()**0.5
            self.debug(str(self.dark.header))
            self.info(
                f'FSN #{self.dark.header.fsn} is a dark background measurement. '
                f'Level: {self.dark.header.dark_cps[0]:.3g} \xb1 {self.dark.header.dark_cps[1]:.3g} cps per pixel '
                f'({self.dark.header.dark_cps[1]/self.dark.header.dark_cps[0]*100:.2f} % relative error, '
                f'{self.dark.header.dark_cps[0]*exposure.intensity.size:.3g} \xb1 '
                f'{self.dark.header.dark_cps[1]*exposure.intensity.size:.3g} cps on the whole detector surface) '
                f'Max counts: {exposure.intensity[exposure.mask].max()}, '
                f'min counts: {exposure.intensity[exposure.mask].min()}, '
                f'{exposure.mask.sum()} valid points'
            )
            raise StopIteration(exposure)
        else:
            if self.dark is None:
                raise ProcessingError(
                    'Cannot do dark background subtraction: no dark background measurement encountered yet.')
            exposure.uncertainty = (
                exposure.uncertainty ** 2 + self.dark.header.dark_cps[1] ** 2) ** 0.5
            exposure.intensity = exposure.intensity - \
                self.dark.header.dark_cps[0]
            exposure.header.fsn_dark = self.dark.header.fsn
            exposure.header.dark_cps = self.dark.header.dark_cps
            self.info(
                f'FSN #{exposure.header.fsn} corrected for dark background signal '
                f'{exposure.header.dark_cps[0]:.4f} \xb1 {exposure.header.dark_cps[1]}'
            )
        return exposure

    def normalize_by_transmission(self, exposure: Exposure) -> Exposure:
        if (exposure.header.transmission[0] > 1) or (exposure.header.transmission[0] < 0):
            raise ProcessingError(f'Invalid transmission value: '
                                  f'{exposure.header.transmission[0]:g} \xb1 {exposure.header.transmission[1]:g}.')
        exposure.uncertainty = (exposure.uncertainty ** 2 / exposure.header.transmission[0] ** 2 +
                                exposure.header.transmission[1] ** 2 * exposure.intensity ** 2 /
                                exposure.header.transmission[0] ** 4) ** 0.5
        exposure.intensity = exposure.intensity / \
            exposure.header.transmission[0]
        self.info(
            f'FSN #{exposure.header.fsn} has been normalized by transmission '
            f'{exposure.header.transmission[0]:.4g} \xb1 {exposure.header.transmission[1]:.4g}.'
        )
        return exposure

    def subtract_empty_background(self, exposure: Exposure) -> Exposure:
        if (exposure.header.sample().category == Sample.Categories.Empty_beam) or (
                exposure.header.title == 'Empty_Beam'):
            # this is an empty beam measurement.
            self.emptybeam = exposure
            self.info(
                f'FSN #{exposure.header.fsn} is an empty beam measurement.')
            raise StopIteration(exposure)
        elif self.emptybeam is None:
            raise ProcessingError(
                'Empty-beam measurement not encountered yet, cannot correct for instrumental background.')
        else:
            exposure.uncertainty = (
                exposure.uncertainty ** 2 + self.emptybeam.uncertainty ** 2) ** 0.5
            exposure.intensity = exposure.intensity - self.emptybeam.intensity
            exposure.header.fsn_emptybeam = self.emptybeam.header.fsn
            self.info(
                f'FSN #{exposure.header.fsn} has been corrected for instrumental background with image '
                f'#{self.emptybeam.header.fsn}')
        return exposure

    def correct_geometry(self, exposure: Exposure) -> Exposure:
        twotheta = exposure.twotheta()
        sa, dsa = solidangle(twotheta[0], twotheta[1], exposure.header.distance[0], exposure.header.distance[1],
                             exposure.header.pixelsize[0], exposure.header.pixelsize[1])
        asa, dasa = angledependentabsorption(twotheta[0], twotheta[1], exposure.header.transmission[0],
                                             exposure.header.transmission[1])
        try:
            aaa, daaa = angledependentairtransmission(twotheta[0], twotheta[1], exposure.header.vacuum[0],
                                                      exposure.header.distance[0],
                                                      exposure.header.distance[1])  # ToDo: mu_air non-default value
        except Exception as exc:
            self.warning(
                f'Error while getting angle dependent air transmission correction: {exc}. '
                f'Traceback: {traceback.format_exc()}')
            aaa, daaa = np.ones_like(twotheta[0]), np.zeros_like(twotheta[1])
        exposure.uncertainty = (
            exposure.uncertainty ** 2 * sa ** 2 + exposure.intensity ** 2 * dsa ** 2) ** 0.5
        exposure.intensity = exposure.intensity * sa
        self.info(
            f'FSN #{exposure.header.fsn} has been corrected for detector flatness (pixel solid angle)')
        exposure.uncertainty = (
            exposure.uncertainty ** 2 * asa ** 2 + exposure.intensity ** 2 * dasa ** 2) ** 0.5
        exposure.intensity = exposure.intensity * asa
        self.info(
            f'FSN #{exposure.header.fsn} has been corrected for angle-dependence of sample self-absorption.')
        exposure.uncertainty = (
            exposure.uncertainty ** 2 * aaa ** 2 + exposure.intensity ** 2 * daaa ** 2) ** 0.5
        exposure.intensity = exposure.intensity * aaa
        self.info(f'FSN #{exposure.header.fsn} has been corrected for angle-dependent absorption of residual air in '
                  f'the vacuum path.')
        return exposure

    def divide_by_thickness(self, exposure: Exposure) -> Exposure:
        exposure.uncertainty = (exposure.uncertainty ** 2 / exposure.header.thickness[0] ** 2 +
                                exposure.header.thickness[1] ** 2 * exposure.intensity ** 2 / exposure.header.thickness[
                                    0] ** 4) ** 0.5
        exposure.intensity = exposure.intensity / exposure.header.thickness[0]
        self.info(f'FSN #{exposure.header.fsn} has been divided by sample thickness of '
                  f'{exposure.header.thickness[0]:g} \xb1 {exposure.header.thickness[1]:g} cm.')
        return exposure

    def absolute_intensity_scaling(self, exposure: Exposure) -> Exposure:
        if exposure.header.sample().category in [Sample.Categories.NormalizationSample, Sample.Categories.Calibrant]:
            self.debug('This is a calibration sample')
            # find corresponding reference
            with sqlalchemy.orm.Session(
                sqlalchemy.create_engine(
                    self.config.dburl,
                    poolclass=sqlalchemy.pool.NullPool)) as session:
                icalibrants: List[IntensityCalibrant] = list(
                    session.scalars(sqlalchemy.select(IntensityCalibrant)))
                matches = [(m.end() - m.start(), ic)
                           for m, ic in [
                               (re.match(ic.regex, exposure.header.title), ic)
                               for ic in icalibrants] if m is not None]
                self.debug(
                    f'Matched calibrants: {", ".join([ic.name for l, ic in matches])}')
                bestmatchlength = max(
                    [length for length, ic in matches], default=0)
                bestmatches = sorted(
                    [ic for length, ic in matches if length == bestmatchlength], key=lambda ic: ic.name)
                if len(bestmatches) >= 1:
                    bestmatch = bestmatches[0]
                    datafile = bestmatch.datafile
                    calibdata = np.loadtxt(datafile)
                    self.info(
                        f'Using data file {datafile} (q from {np.nanmin(calibdata[:,0]):.5f} to '
                        f'{np.nanmax(calibdata[:,0]):.5f} 1/nm, {calibdata.shape[0]} points)')
                    rad0 = exposure.radial_average()
                    rad = exposure.radial_average(calibdata[:, 0]).sanitize()
                    self.info(f'Radial average has {len(rad.q)} points.')
                    qcalib = rad.q
                    self.info(
                        f'Measured q from {np.nanmin(rad0.q):.5f} to '
                        f'{np.nanmax(rad0.q):.5f} 1/nm, {len(rad0.q)} points')
                    icalib = np.interp(
                        qcalib, calibdata[:, 0], calibdata[:, 1])
                    ecalib = np.interp(
                        qcalib, calibdata[:, 0], calibdata[:, 2])
                    model = scipy.odr.Model(lambda params, x: x * params[0])
                    data = scipy.odr.RealData(
                        rad.intensity, icalib, rad.uncertainty, ecalib)
                    odr = scipy.odr.ODR(data, model, [1.0])
                    result = odr.run()
                    factor = result.beta[0]
                    factor_unc = result.sd_beta[0]
                    chi2_red = result.res_var
                    dof = len(rad) - 1
                    self.absintref = exposure
                    exposure.header.absintchi2 = chi2_red
                    exposure.header.absintdof = dof
                    exposure.header.absintfactor = factor, factor_unc
                    exposure.header.fsn_absintref = exposure.header.fsn
                    exposure.header.absintqmin = qcalib.min()
                    exposure.header.absintqmax = qcalib.max()
                    exposure.header.flux = 1 / \
                        factor, abs(factor_unc / factor ** 2)
                    exposure.uncertainty = (exposure.uncertainty ** 2 * exposure.header.absintfactor[
                        0] ** 2 + exposure.intensity ** 2 * exposure.header.absintfactor[1] ** 2) ** 0.5
                    exposure.intensity = exposure.intensity * \
                        exposure.header.absintfactor[0]
                    self.info(
                        f'FSN #{exposure.header.fsn} calibrated into absolute units using reference data from '
                        f'{datafile}. Common q-range from {qcalib.min():g} to {qcalib.max():g} 1/nm ({len(qcalib)} '
                        f'points). Reduced chi2: {chi2_red:g} (DoF: {dof}')
                    self.info(f'FSN #{exposure.header.fsn}: estimated beam flux {exposure.header.flux[0]:g} \xb1 '
                              f'{exposure.header.flux[1]} photons*eta/sec')
                    return exposure
        # no matching reference data, treat this sample as a normal sample.
        if self.absintref is None:
            raise ProcessingError('No absolute intensity reference measurement encountered up to now, cannot scale into'
                                  'absolute intensity units.')
        else:
            exposure.header.absintdof = self.absintref.header.absintdof
            exposure.header.absintchi2 = self.absintref.header.absintchi2
            exposure.header.absintfactor = self.absintref.header.absintfactor
            exposure.header.fsn_absintref = self.absintref.header.fsn
            exposure.header.absintqmax = self.absintref.header.absintqmax
            exposure.header.absintqmin = self.absintref.header.absintqmin
            exposure.header.flux = self.absintref.header.flux
            exposure.uncertainty = (exposure.uncertainty ** 2 * exposure.header.absintfactor[
                0] ** 2 + exposure.intensity ** 2 * exposure.header.absintfactor[1] ** 2) ** 0.5
            exposure.intensity = exposure.intensity * \
                exposure.header.absintfactor[0]
            self.info(
                f'FSN #{exposure.header.fsn} has been calibrated into absolute units using '
                f'exposure #{exposure.header.fsn_absintref}. '
                f'Absolute intensity factor: {exposure.header.absintfactor[0]:g} \xb1 '
                f'{exposure.header.absintfactor[1]:g}, estimated beam flux '
                f'{exposure.header.flux[0]:g} \xb1 {exposure.header.flux[1]:g}')
        return exposure

    def write_sql(self, exposure: Exposure):
        try:
            engine = sqlalchemy.create_engine(
                self.config.dburl_expdb,
                poolclass=sqlalchemy.pool.NullPool)
        except KeyError:
            self.warning(
                'Could not find exposuredb engine url in Tango database.')
        else:
            self.info(
                f'Writing exposure {exposure.header.prefix}/{exposure.header.fsn} to SQL DB.')
            exposuredb.addExposure(exposure.header, engine, overwrite=True, mask=exposure.mask)

    @staticmethod
    def get_statistics(exposure: Exposure) -> Dict[str, float]:
        return {
            'NaNs': np.isnan(exposure.intensity).sum(),
            'finites': np.isfinite(exposure.intensity).sum(),
            'negatives': (exposure.intensity < 0).sum(),
            'unmaskedNaNs': np.isnan(exposure.intensity[exposure.mask != 0]).sum(),
            'unmaskednegatives': (exposure.intensity[exposure.mask != 0] < 0).sum(),
            'masked': (exposure.mask == 0).sum(),
            'unmasked': (exposure.mask != 0).sum(),
        }

    def log(self, level: str, message: str):
        assert level in ['error', 'warning', 'info', 'debug', 'critical']
        if self.resultqueue is not None:
            self.resultqueue.put_nowait(
                (self.currentimagefile, self.currentmetadatafile, level, message))

    def error(self, message: str):
        self.log("error", message)

    def debug(self, message: str):
        self.log('debug', message)

    def warning(self, message: str):
        self.log('warning', message)

    def info(self, message: str):
        self.log('info', message)
