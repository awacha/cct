import dataclasses


@dataclasses.dataclass
class DataReductionConfig:
    # output directory for crd_?????.npz and crd_?????.pickle.gz files
    outputdirectory: str
    # SQL database url for retrieving calibrant information
    dburl: str
    # SQL database url for saving exposure information
    dburl_expdb: str
    # Mask file directory
    maskdir: str
