"""ORM schemata for exposure database"""

import datetime
import math
import time
from typing import List, Optional, Tuple

from ..core2.algorithms.radavg import autoq
from ..core2.dataclasses.header import Header

import sqlalchemy.dialects.mysql
import sqlalchemy.orm
import numpy as np


class Base(sqlalchemy.orm.DeclarativeBase):
    type_annotation_map = {
        int: sqlalchemy.BIGINT(),
        datetime.datetime: sqlalchemy.TIMESTAMP(timezone=True),
        str: sqlalchemy.String(1024),
    }


class Exposure(Base):
    __tablename__ = 'exposures'
    __table_args__ = (
        sqlalchemy.PrimaryKeyConstraint('fsn', 'year', 'prefix'),
        {'sqlite_autoincrement': True},
    )
    fsn: sqlalchemy.orm.Mapped[int] = sqlalchemy.orm.mapped_column(
        sqlalchemy.BIGINT().with_variant(sqlalchemy.INTEGER, "sqlite"), index=True)
    prefix: sqlalchemy.orm.Mapped[str] = sqlalchemy.orm.mapped_column(
        sqlalchemy.CHAR(6), index=True
    )
    year: sqlalchemy.orm.Mapped[int] = sqlalchemy.orm.mapped_column(index=True)
    date: sqlalchemy.orm.Mapped[datetime.datetime] = sqlalchemy.orm.mapped_column(
        sqlalchemy.dialects.mysql.TIMESTAMP(fsp=6))
    sample: sqlalchemy.orm.Mapped["Sample"] = sqlalchemy.orm.relationship()
    distance: sqlalchemy.orm.Mapped[float] = sqlalchemy.orm.mapped_column()
    exposuretime: sqlalchemy.orm.Mapped[float] = sqlalchemy.orm.mapped_column()
    project: sqlalchemy.orm.Mapped["Project"] = sqlalchemy.orm.relationship()
    vacuum: sqlalchemy.orm.Mapped[Optional[float]] = sqlalchemy.orm.mapped_column(nullable=True)
    username: sqlalchemy.orm.Mapped[Optional[str]] = sqlalchemy.orm.mapped_column(nullable=True)
    temperature: sqlalchemy.orm.Mapped[Optional[float]] = sqlalchemy.orm.mapped_column(nullable=True)
    transmission: sqlalchemy.orm.Mapped[Optional[float]] = sqlalchemy.orm.mapped_column(nullable=True)
    beamcenterx: sqlalchemy.orm.Mapped[float] = sqlalchemy.orm.mapped_column()
    beamcentery: sqlalchemy.orm.Mapped[float] = sqlalchemy.orm.mapped_column()
    mask: sqlalchemy.orm.Mapped[Optional[str]] = sqlalchemy.orm.mapped_column(nullable=True)
    emptybeam_fsn: sqlalchemy.orm.Mapped[Optional[int]] = sqlalchemy.orm.mapped_column(nullable=True)
    dark_fsn: sqlalchemy.orm.Mapped[Optional[int]] = sqlalchemy.orm.mapped_column(nullable=True)
    absintref_fsn: sqlalchemy.orm.Mapped[Optional[int]] = sqlalchemy.orm.mapped_column(nullable=True)
    absintfactor: sqlalchemy.orm.Mapped[Optional[float]] = sqlalchemy.orm.mapped_column(nullable=True)
    flux: sqlalchemy.orm.Mapped[Optional[float]] = sqlalchemy.orm.mapped_column(nullable=True)
    sample_id: sqlalchemy.orm.Mapped[int] = sqlalchemy.orm.mapped_column(sqlalchemy.ForeignKey("samples.id"))
    project_id: sqlalchemy.orm.Mapped[int] = sqlalchemy.orm.mapped_column(sqlalchemy.ForeignKey("projects.id"))
    qmin: sqlalchemy.orm.Mapped[Optional[float]] = sqlalchemy.orm.mapped_column(nullable=True)
    qmax: sqlalchemy.orm.Mapped[Optional[float]] = sqlalchemy.orm.mapped_column(nullable=True)
    qmin_estimate: sqlalchemy.orm.Mapped[Optional[float]] = sqlalchemy.orm.mapped_column(nullable=True)
    flux_estimate: sqlalchemy.orm.Mapped[Optional[float]] = sqlalchemy.orm.mapped_column(nullable=True)


class Project(Base):
    __tablename__ = 'projects'
    __table_args__ = (
        sqlalchemy.UniqueConstraint('year', 'projectid'),
        {'sqlite_autoincrement': True, },
    )
    id: sqlalchemy.orm.Mapped[int] = sqlalchemy.orm.mapped_column(primary_key=True, autoincrement=True)
    year: sqlalchemy.orm.Mapped[int] = sqlalchemy.orm.mapped_column(sqlalchemy.SmallInteger)
    projectid: sqlalchemy.orm.Mapped[str] = sqlalchemy.orm.mapped_column(index=True, nullable=True)
    name: sqlalchemy.orm.Mapped[str] = sqlalchemy.orm.mapped_column(nullable=True)
    proposer: sqlalchemy.orm.Mapped[Optional[str]] = sqlalchemy.orm.mapped_column(nullable=True)


class Sample(Base):
    __tablename__ = 'samples'
    __table_args__ = (
        sqlalchemy.UniqueConstraint('title', 'x', 'y', 'preparetime'),
        {'sqlite_autoincrement': True},
    )
    id: sqlalchemy.orm.Mapped[int] = sqlalchemy.orm.mapped_column(
        sqlalchemy.BIGINT().with_variant(sqlalchemy.INTEGER, "sqlite"), primary_key=True, autoincrement=True)
    title: sqlalchemy.orm.Mapped[Optional[str]] = sqlalchemy.orm.mapped_column(index=True, nullable=True)
    thickness: sqlalchemy.orm.Mapped[Optional[float]] = sqlalchemy.orm.mapped_column(nullable=True)
    x: sqlalchemy.orm.Mapped[Optional[float]] = sqlalchemy.orm.mapped_column(nullable=True)
    y: sqlalchemy.orm.Mapped[Optional[float]] = sqlalchemy.orm.mapped_column(nullable=True)
    description: sqlalchemy.orm.Mapped[Optional[str]] = sqlalchemy.orm.mapped_column(nullable=True)
    preparedby: sqlalchemy.orm.Mapped[Optional[str]] = sqlalchemy.orm.mapped_column(nullable=True)
    preparetime: sqlalchemy.orm.Mapped[Optional[datetime.datetime]] = sqlalchemy.orm.mapped_column(
        sqlalchemy.dialects.mysql.TIMESTAMP(fsp=6),
        nullable=True)


def _normalize_date(dt: datetime.datetime):
    if dt.timestamp() == 0:
        return '0000-00-00 00:00:00'
    else:
        return dt


def _normalize_valandunc(v: Tuple[float, float]):
    if v is None:
        return None
    if isinstance(v, float):
        v = (v, 0)
    if math.isnan(v[0]):
        return None
    else:
        return v[0]


def addExposure(header: Header, engine, overwrite: bool = False, mask: Optional[np.ndarray] = None):
    with sqlalchemy.orm.Session(engine) as session, session.begin():
        t0 = time.monotonic()
        exposure = session.scalar(sqlalchemy.select(Exposure).where(
            Exposure.fsn == header.fsn,
            Exposure.year == header.date.year,
            Exposure.prefix == header.prefix,
        ))
        texposurequery = time.monotonic() - t0
        if (exposure is not None) and not overwrite:
            return
        t0 = time.monotonic()
        sample = session.scalar(sqlalchemy.select(Sample).where(
            Sample.title == header.title and
            Sample.description == header.description and
            Sample.preparedby == header.preparedby and
            Sample.preparetime == _normalize_date(header.preparetime) and
            Sample.thickness == _normalize_valandunc(header.thickness) and
            Sample.x == _normalize_valandunc(header.samplex) and
            Sample.y == _normalize_valandunc(header.sampley)))
        tsamplequery = time.monotonic() - t0
        if sample is None:
            sample = Sample(
                title=header.title, description=header.description,
                preparedby=header.preparedby,
                preparetime=_normalize_date(header.preparetime),
                thickness=_normalize_valandunc(header.thickness),
                x=_normalize_valandunc(header.samplex),
                y=_normalize_valandunc(header.sampley))
            session.add(sample)
        t0 = time.monotonic()
        project = session.scalar(sqlalchemy.select(Project).where(
            Project.projectid == header.project,
            Project.year == header.date.year
        ))
        tprojectquery = time.monotonic() - t0
        if project is None:
            project = Project(
                projectid=header.project, name=header.projectname,
                proposer=header.proposer, year=header.date.year)
            session.add(project)
        #                        darkexposure = session.scalar(sqlalchemy.select(Exposure).where(
        #                            Exposure.fsn == darkfsn, Exposure.year == year, Exposure.prefix == prefix
        #                        ))
        #                        emptyexposure = session.scalar(sqlalchemy.select(Exposure).where(
        #                            Exposure.fsn == emptybeamfsn, Exposure.year == year, Exposure.prefix == prefix
        #                        ))
        #                        gcexposure = session.scalar(sqlalchemy.select(Exposure).where(
        #                            Exposure.fsn == absintreffsn, Exposure.year == year, Exposure.prefix == prefix
        #                        ))
        if mask is not None:
            qmin, qmax = autoq(mask, header.wavelength[0], header.distance[0], header.pixelsize[0], header.beamposrow[0], header.beamposcol[0], N=2)
        else:
            qmin, qmax = None, None
        try:
            qmin_estimate = header._data['geometry']['qmin']
        except KeyError:
            qmin_estimate = None
        try:
            flux_estimate = header._data['geometry']['intensity']
        except KeyError:
            flux_estimate = None
        if exposure is None:
            mps = []
            t0 = time.monotonic()
            exposure = Exposure(
                fsn=header.fsn, prefix=header.prefix, year=header.date.year, date=_normalize_date(header.date),
                sample=sample, distance=_normalize_valandunc(header.distance),
                exposuretime=_normalize_valandunc(header.exposuretime), project=project,
                vacuum=_normalize_valandunc(header.vacuum),
                username=header.username, temperature=_normalize_valandunc(header.temperature),
                transmission=_normalize_valandunc(header.transmission),
                beamcenterx=_normalize_valandunc(header.beamposcol),
                beamcentery=_normalize_valandunc(header.beamposrow), mask=header.maskname,
                emptybeam_fsn=header.fsn_emptybeam, dark_fsn=header.fsn_dark,
                absintref_fsn=header.fsn_absintref,
                absintfactor=_normalize_valandunc(header.absintfactor),
                flux=_normalize_valandunc(header.flux), 
                qmin=_normalize_valandunc(qmin), 
                qmax=_normalize_valandunc(qmax), 
                qmin_estimate=_normalize_valandunc(qmin_estimate),
                flux_estimate=_normalize_valandunc(flux_estimate)
            )
            session.add_all(mps + [exposure])
        else:
            assert isinstance(exposure, Exposure)
            exposure.date = _normalize_date(header.date)
            exposure.sample = sample
            exposure.distance = _normalize_valandunc(header.distance)
            exposure.exposuretime = _normalize_valandunc(header.exposuretime)
            exposure.project = project
            exposure.vacuum = _normalize_valandunc(header.vacuum)
            exposure.username = header.username
            exposure.temperature = _normalize_valandunc(header.temperature)
            exposure.transmission = _normalize_valandunc(header.transmission)
            exposure.beamcenterx = _normalize_valandunc(header.beamposcol)
            exposure.beamcentery = _normalize_valandunc(header.beamposrow)
            exposure.emptybeam_fsn = header.fsn_emptybeam
            exposure.dark_fsn = header.fsn_dark
            exposure.absintref_fsn = header.fsn_absintref
            exposure.absintfactor = _normalize_valandunc(header.absintfactor)
            exposure.flux = _normalize_valandunc(header.flux)
            exposure.qmin = _normalize_valandunc(qmin)
            exposure.qmax = _normalize_valandunc(qmax)
            exposure.qmin_estimate = _normalize_valandunc(qmin_estimate)
            exposure.flux_estimate = _normalize_valandunc(flux_estimate)
            session.add(exposure)
        t0 = time.monotonic()
        session.commit()
        tcommit = time.monotonic() - t0
        #logger.debug(f'{tsamplequery=:6.3f} {tprojectquery=:6.3f} {texposurequery=:6.3f} {tcommit=:6.3f}')
