
import contextlib
import os

import appdirs
import click

from .main import main

DEFAULTCONFFILE = os.path.join(
    appdirs.user_config_dir('cct'), 'cct4_exposuredb.conf')


@main.group()
@click.option('--conffile', '-f', help='Config file', required=True, default=DEFAULTCONFFILE,
              type=click.Path(exists=False, file_okay=True, dir_okay=False, writable=True, readable=True,
                              allow_dash=False, ))
@click.pass_context
def expdb(ctx, conffile):
    """Manipulate the exposure database"""
    import configparser
    ctx.ensure_object(dict)
    ctx.obj['cp'] = configparser.ConfigParser()
    ctx.obj['cp'].read(conffile)


@expdb.command()
@click.option('--dburl',
              help='Database url, typically "dialect+driver://username:password@host:port/database". ' +
              'See the SQLAlchemy documentation for details."',
              required=True, type=str)
@click.pass_context
def initconfig(ctx, dburl):
    """Initialize configuration"""
    import configparser
    import logging

    import sqlalchemy.exc
    import sqlalchemy.orm

    from ..dbutils2.exposuredb import Base

    logger = logging.getLogger(__name__)
    logger.setLevel(logging.DEBUG)

    try:
        ctx.parent.obj['cp'].add_section('SQL')
    except configparser.DuplicateSectionError:
        pass
    ctx.parent.obj['cp'].set('SQL', 'url', dburl)
    with open(ctx.parent.params['conffile'], 'wt') as f:
        ctx.parent.obj['cp'].write(f)
    engine = sqlalchemy.create_engine(ctx.parent.obj['cp'].get('SQL', 'url'))
    subclasses_to_delete = Base.__subclasses__()[:]
    while subclasses_to_delete:
        ndropped = 0
        for sc in subclasses_to_delete[:]:
            try:
                sc.__table__.drop(engine)
                subclasses_to_delete.remove(sc)
                logger.info(f'Dropped table {sc.__tablename__}')
                ndropped += 1
            except sqlalchemy.exc.IntegrityError as ie:
                logger.warning(
                    f'Could not drop table {sc.__tablename__}: {ie}')
            except sqlalchemy.exc.OperationalError as oe:
                logger.info(f'Could not drop table {sc.__tablename__}: {oe}')
                subclasses_to_delete.remove(sc)
                ndropped += 1
        if not ndropped:
            raise RuntimeError(
                'Could not drop all tables: remaining ones are '
                f'{", ".join([sc.__tablename__ for sc in subclasses_to_delete])}')
    Base.metadata.create_all(engine)


@contextlib.contextmanager
def open_file(filename, filemode):
    import gzip
    if filename.endswith('.gz'):
        f = gzip.open(filename, filemode)
    else:
        f = open(filename, filemode)
    yield f
    f.close()


@expdb.command()
@click.option('--firstfsn', '-f', help='First FSN to load', default=0, type=click.IntRange(0))
@click.option('--lastfsn', '-l', help='Last FSN to load (default: infinty)', default=None, type=click.IntRange(0))
@click.option('--prefix', '-p', help='Exposure prefix', default='crd', type=str)
@click.option('--fsndigits', '-d', help='Digits in the fsn', default=5, type=click.IntRange(min=1))
@click.option('--headertype', help='Header type', default='pickle',
              type=click.Choice(['pickle', 'param'], case_sensitive=False))
@click.option('--overwrite/--nooverwrite', help='Overwrite existing entries in the database', default=False,
              type=click.BOOL)
@click.argument('datadir', default='.')
@click.pass_context
def update(ctx, firstfsn, lastfsn, prefix, fsndigits, headertype, overwrite, datadir):
    """Update database"""
    import logging
    import re
    import time

    import sqlalchemy.exc
    import sqlalchemy.orm
    
    from scipy.io import loadmat

    import numpy as np
    import taurus
    taurus.Logger.disableLogOutput()

    from ..core2.dataclasses.header import Header
    from ..dbutils2.exposuredb import Base, addExposure

    logger = logging.getLogger(__name__)
    logger.setLevel(logging.DEBUG)

    engine = sqlalchemy.create_engine(ctx.parent.obj['cp'].get('SQL', 'url'))
    Base.metadata.create_all(engine)
    if headertype == 'pickle':
        filename_regex = re.compile(
            rf'{prefix}_(?P<fsn>\d{{{fsndigits},}})\.pickle(?P<gzip>\.gz)?')
    elif headertype == 'param':
        filename_regex = re.compile(
            rf'{prefix}_(?P<fsn>\d{{{fsndigits},}})\.param(?P<gzip>\.gz)?')
    else:
        raise ValueError(f'Invalid header type: {headertype}')
    logger.debug(f'Filename regex: {filename_regex.pattern}')
    tstart = time.monotonic()
    nadded = 0
    for subdir in ['param', 'param_override', 'eval2d']:
        logger.info(f'Reading from subdirectory {subdir}')
        for path, dirs, files in os.walk(os.path.join(datadir, subdir)):
            logger.debug(f'Path: {path}')
            for fn in sorted(files):
                if m := filename_regex.match(fn):
                    fsn = int(m['fsn'])
                    if fsn < firstfsn:
                        continue
                    if (lastfsn is not None) and (fsn > lastfsn):
                        continue
                    header = Header(filename=os.path.join(path, fn))
                    if subdir == 'eval2d' and np.isnan(header.flux[0]):
                        logger.warning(f'Flux is NaN for sample {header.title}, fsn {header.fsn}')
                    maskname = header.maskname
                    if not os.path.exists(maskname):
                        maskname = os.path.split(maskname)[-1]
                        for folder, subdirs, files in os.walk('mask'):
                            if maskname in files:
                                maskname = os.path.join(folder, maskname)
                    try:
                        if maskname.endswith('.mat'):
                            ms = loadmat(maskname)
                            mkeys = [k for k in ms if not k.startswith('_')]
                            mask = ms[mkeys[0]]
                        else:
                            mask = np.load(maskname)
                    except FileNotFoundError:
                        logger.warning(f'Cannot load mask file {header.maskname}')
                        mask = None

                    addExposure(header, engine, overwrite, mask)
                    nadded += 1
                    logger.debug(
                        f'Added {path}/{fn},  {(time.monotonic() - tstart) / nadded:.3f} seconds/file')
