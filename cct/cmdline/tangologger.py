import os
from typing import Optional

import appdirs
import click

from .main import main

DEFAULTCONFFILE = os.path.join(appdirs.user_config_dir("cct"), "cct4_tangologger.conf")


@main.group()
@click.option(
    "--conffile",
    "-f",
    help="Config file",
    required=True,
    type=click.Path(
        exists=False,
        file_okay=True,
        dir_okay=False,
        writable=True,
        readable=True,
        allow_dash=False,
    ),
    default=DEFAULTCONFFILE,
)
@click.pass_context
def tangologger(ctx, conffile):
    """Run a tango logger script"""
    import configparser

    ctx.ensure_object(dict)
    ctx.obj["cp"] = configparser.ConfigParser()
    ctx.obj["cp"].read(conffile)


@tangologger.command()
@click.option("--device", "-d", help="Tango device", required=True, type=str)
@click.option("--attribute", "-a", help="Tango attribute", required=True, type=str)
@click.option(
    "--interval",
    "-i",
    help="Recording interval (seconds)",
    required=False,
    type=click.IntRange(min=0, min_open=True),
)
@click.option(
    "--units",
    "-u",
    help="Measurement units",
    required=True,
    type=str,
)
@click.option(
    "--fieldname",
    "-n",
    help="Database field name",
    required=False,
    default=None,
    type=str,
)
@click.pass_context
def add(ctx, device, attribute, interval, units, fieldname: Optional[str] = None):
    import logging

    import sqlalchemy.orm

    from ..tangologger.orm_schema import QueriedAttribute

    logger = logging.getLogger(__name__)
    logger.setLevel(logging.DEBUG)

    engine = sqlalchemy.create_engine(ctx.parent.obj["cp"].get("SQL", "url"))
    with sqlalchemy.orm.Session(engine) as session, session.begin():
        qa = QueriedAttribute(
            tangodevice=device,
            tangoattribute=attribute,
            interval=interval,
            fieldname=fieldname if fieldname is not None else attribute,
            units=units
        )
        session.add(qa)


@tangologger.command()
@click.pass_context
def run(ctx):
    import datetime
    import logging
    import time

    import dateutil.tz
    import sqlalchemy.orm
    import tango

    from ..tangologger.orm_schema import QueriedAttribute, Reading

    logger = logging.getLogger(__name__)
    logger.setLevel(logging.DEBUG)

    engine = sqlalchemy.create_engine(ctx.parent.obj["cp"].get("SQL", "url"))
    while True:
        nextquerydate = None
        tz = dateutil.tz.gettz()
        with sqlalchemy.orm.Session(engine) as session, session.begin():
            t0 = time.monotonic()
            qas = session.query(QueriedAttribute).all()
            logger.debug(f"Querying attributes took {time.monotonic()-t0:.3f} seconds")
            t0 = time.monotonic()
            lastreadings = (
                session.query(sqlalchemy.func.max(Reading.timestamp), QueriedAttribute)
                .group_by(Reading.attribute_id)
                .join(Reading.attribute)
                .all()
            )
            logger.debug(
                f"Querying last readout times took {time.monotonic()-t0:.3f} seconds"
            )
            for qa in qas:
                if qa not in [qa_ for t_, qa_ in lastreadings]:
                    lastreadings.append(
                        (datetime.datetime.fromtimestamp(0).astimezone(tz), qa)
                    )
            if not lastreadings:
                logger.error("No attributes to query defined in the database. Exiting.")
                break
            for lasttime, qa in lastreadings:
                assert isinstance(qa, QueriedAttribute)
                if lasttime.tzinfo is None:
                    lasttime = lasttime.astimezone(tz)
                if (datetime.datetime.now(tz) - lasttime).total_seconds() > qa.interval:
                    t0 = time.monotonic()
                    try:
                        tdev = tango.DeviceProxy(qa.tangodevice)
                        value = getattr(tdev, qa.tangoattribute)
                    except AttributeError:
                        logger.error(
                            f'Cannot read attribute "{qa.tangoattribute}" from device "{qa.tangodevice}".'
                        )
                        continue
                    except tango.DevFailed as deverr:
                        logger.error(f"Tango exception: {deverr}")
                        continue
                    logger.debug(
                        f"Tango readout took {time.monotonic()-t0:.3f} seconds"
                    )
                    t0 = time.monotonic()
                    session.add(
                        Reading(
                            timestamp=datetime.datetime.now(tz),
                            attribute=qa,
                            value=value,
                        )
                    )
                    logger.debug(f"SQL add took {time.monotonic()-t0:.3f} seconds")
                    lasttime = datetime.datetime.now(tz)
                    logger.info(f"Read {qa.tangodevice}:{qa.tangoattribute}: {value}")
                nqd = lasttime + datetime.timedelta(seconds=qa.interval)
                if nextquerydate is None:
                    nextquerydate = nqd
                else:
                    if nqd < nextquerydate:
                        nextquerydate = nqd
            if nextquerydate is not None:
                delay = max(
                    (nextquerydate - datetime.datetime.now(tz)).total_seconds(), 0
                )
            else:
                delay = min([qa.interval for qa in qas])
        logger.debug(f"Sleeping for {delay} seconds until the next query.")
        time.sleep(delay)


@tangologger.command()
@click.pass_context
def oneshot(ctx):
    import datetime
    import logging

    import dateutil.tz
    import sqlalchemy.orm
    import tango

    from ..tangologger.orm_schema import QueriedAttribute, Reading

    logger = logging.getLogger(__name__)
    logger.setLevel(logging.DEBUG)

    engine = sqlalchemy.create_engine(ctx.parent.obj["cp"].get("SQL", "url"))
    with sqlalchemy.orm.Session(engine) as session, session.begin():
        for qa in session.scalars(sqlalchemy.select(QueriedAttribute)):
            tz = dateutil.tz.gettz()
            assert isinstance(qa, QueriedAttribute)
            try:
                tdev = tango.DeviceProxy(qa.tangodevice)
                value = getattr(tdev, qa.tangoattribute)
            except tango.DevFailed as deverr:
                logger.error(f"Tango exception: {deverr}")
                continue
            session.add(
                Reading(timestamp=datetime.datetime.now(tz), attribute=qa, value=value)
            )


@tangologger.command()
@click.pass_context
def show(ctx):
    import datetime
    import logging
    from typing import List, Tuple

    import dateutil.tz
    import sqlalchemy.orm

    from ..tangologger.orm_schema import QueriedAttribute, Reading

    logger = logging.getLogger(__name__)
    logger.setLevel(logging.DEBUG)

    engine = sqlalchemy.create_engine(ctx.parent.obj["cp"].get("SQL", "url"))

    with sqlalchemy.orm.Session(engine) as session, session.begin():
        subquery = sqlalchemy.select(
            sqlalchemy.func.max(Reading.timestamp).label("maxtimestamp"), 
            Reading.attribute_id).group_by(Reading.attribute_id).subquery()

        lastreadings: List[Tuple[datetime.datetime, QueriedAttribute]] = list(
            list(session.execute(
                sqlalchemy.select(
                    subquery.c.maxtimestamp, 
                    QueriedAttribute).join_from(QueriedAttribute, subquery)))
        )
        devnamelength = max(
            max([len(qa.tangodevice) for rod, qa in lastreadings]), len("Tango device")
        )
        attrnamelength = max(
            max([len(qa.tangoattribute) for rod, qa in lastreadings]),
            len("Tango attribute"),
        )
        fieldnamelength = max(
            max([len(qa.fieldname) for rod, qa in lastreadings]), len("Name")
        )
        print(
            f'{"Name":^{fieldnamelength}s} | {"Tango device":^{devnamelength}s} | '
            f'{"Tango attribute":^{attrnamelength}} | {"Last readout at":^30s}'
        )
        print(
            "-" * (fieldnamelength + 1)
            + "+"
            + "-" * (devnamelength + 2)
            + "+"
            + "-" * (attrnamelength + 2)
            + "+"
            + "-" * 31
        )
        tz = dateutil.tz.gettz()
        for rod, qa in lastreadings:
            assert isinstance(qa, QueriedAttribute)
            if rod is None:
                rod = "Never"
            else:
                assert isinstance(rod, datetime.datetime)
                if rod.tzinfo is None:
                    rod = rod.astimezone(tz)
            print(
                f"{qa.fieldname:^{fieldnamelength}s} | {qa.tangodevice:^{devnamelength}s} | "
                f"{qa.tangoattribute:^{attrnamelength}s} | {str(rod):^30s}"
            )


@tangologger.command()
@click.option(
    "--dburl",
    help='Database url, typically "dialect+driver://username:password@host:port/database". '
    'See the SQLAlchemy documentation for details."',
    required=True,
    type=str,
)
@click.pass_context
def initconfig(ctx, dburl):
    import configparser
    import logging

    import sqlalchemy
    import sqlalchemy.orm

    from ..tangologger.orm_schema import Base

    logger = logging.getLogger(__name__)
    logger.setLevel(logging.DEBUG)
    try:
        ctx.parent.obj["cp"].add_section("SQL")
    except configparser.DuplicateSectionError:
        pass
    ctx.parent.obj["cp"].set("SQL", "url", dburl)
    with open(ctx.parent.params["conffile"], "wt") as f:
        ctx.parent.obj["cp"].write(f)
    engine = sqlalchemy.create_engine(ctx.parent.obj["cp"].get("SQL", "url"))
    Base.metadata.create_all(engine)


@tangologger.command()
@click.option(
    "--name",
    "-n",
    "names",
    help="Name of the logged attribute",
    required=True,
    type=str,
    multiple=True,
)
@click.option(
    "--output",
    "-o",
    help="Output file",
    required=True,
    type=click.Path(
        exists=False, file_okay=True, dir_okay=False, writable=True, resolve_path=True
    ),
)
@click.pass_context
def dump(ctx, names, output):
    import datetime
    import logging

    import dateutil.tz
    import h5py
    import numpy as np
    import sqlalchemy.orm

    from ..tangologger.orm_schema import QueriedAttribute, Reading

    logger = logging.getLogger(__name__)
    logger.setLevel(logging.DEBUG)

    engine = sqlalchemy.create_engine(ctx.parent.obj["cp"].get("SQL", "url"))
    tz = dateutil.tz.gettz()
    with sqlalchemy.orm.Session(engine) as session, session.begin():
        for name in names:
            qa = session.scalar(
                sqlalchemy.select(QueriedAttribute).where(
                    QueriedAttribute.fieldname == name
                )
            )
            if qa is None:
                logger.error(f"Nonexistent field: {name}.")
            data = list(
                session.execute(
                    sqlalchemy.select(Reading.timestamp, Reading.value).where(
                        Reading.attribute == qa
                    )
                ).all()
            )
            dates = [d if d.tzinfo is not None else d.astimezone(tz) for d, v in data]
            values = [v for d, v in data]
            if any(
                [
                    output.lower().endswith(extn)
                    for extn in [".h5", ".hdf5", ".nx5", ".nxs"]
                ]
            ):
                with h5py.File(output, "a") as h5:
                    deltat = np.array([d.timestamp() for d in dates])
                    try:
                        del h5[name]
                    except KeyError:
                        pass
                    grp = h5.create_group(name)
                    grp.attrs["NX_class"] = "NXlog"
                    grp.attrs["signal"] = name
                    grp.attrs["axis"] = "time"
                    grp.create_dataset("value", data=np.array(values))
                    t = grp.create_dataset("time", data=np.array(deltat))
                    t.attrs["units"] = "s"
                    t.attrs["start"] = datetime.datetime.fromtimestamp(
                        0, tz
                    ).isoformat()
            else:
                if len(names) > 1:
                    raise ValueError(
                        "ASCII file output is only allowed for one logged attribute at a time."
                    )
                with open(output, "wt") as f:
                    f.write(f"# Time\t{name}\n")
                    for d, v in zip(dates, values):
                        f.write(f"{d}\t{v:.18f}\n")


@tangologger.command()
@click.option(
    "--name",
    "-n",
    "names",
    help="Name of the logged attribute",
    required=True,
    type=str,
    multiple=True,
)
@click.option(
    "--output",
    "-o",
    help="Output graph file (or \"dokuwikirpc://<hostname>/<medianame>\")",
    required=False,
    default=None,
    type=click.STRING,
)
@click.option(
    "--last",
    "interval",
    help='The time interval before now, in which the graph should be drawn. E.g. "1 day", "2 years" etc. Default: all data',
    default=None,
)
@click.option(
    "--lines/--nolines", "lines", is_flag=True, help="Connect the data with lines"
)
@click.option(
    "--ylabel",
    "ylabel",
    help="Label on the y axis (without measurement units declaration)",
    default=None,
    type=click.STRING,
)
@click.option(
    "--timestamp/--notimestamp",
    "timestamp",
    is_flag=True,
    help="Print a timestamp in the bottom right corner",
    default=True,
)
@click.option("--grid/--nogrid", "grid", is_flag=True, help="Draw a grid", default=True)
@click.option(
    "--logscale/--linscale",
    "logscale",
    is_flag=True,
    help="Logarithmic scale on the vertical axis",
)
@click.option(
    "--ymin", "ymin", type=click.FLOAT, help="Lowest value on the y axis", default=None
)
@click.option(
    "--ymax", "ymax", type=click.FLOAT, help="Highest value on the y axis", default=None
)
@click.option(
    "--gui/--nogui", "gui", is_flag=True, help="Show a graphical user interface"
)
@click.option(
    "--marker/--nomarker", "showmarkers", is_flag=True, help="Show markers"
)
@click.option(
    "--dokuwiki_api_token", type=click.STRING, help="API token for dokuwiki upload", default=None, envvar="TANGOLOGGER_DW_TOKEN",
)
@click.option(
    "--dokuwiki_api_client_cert", type=click.STRING, help="Client TLS certificate file", default=None, envvar="TANGOLOGGER_DW_CERT",
)
@click.option(
    "--dokuwiki_api_client_key", type=click.STRING, help="Client TLS key file", default=None, envvar="TANGOLOGGER_DW_KEY",
)
@click.pass_context
def plot(
    ctx, names, output, interval, lines, ylabel, timestamp, grid, logscale, ymin, ymax, gui, showmarkers, dokuwiki_api_token,
    dokuwiki_api_client_cert, dokuwiki_api_client_key,
):
    import logging
    import re
    import tempfile

    import dateparser
    import sqlalchemy.orm
    import matplotlib
    if not gui:
        matplotlib.use('agg')

    import matplotlib.pyplot as plt
    import datetime
    import requests
    import base64
    import json

    from ..tangologger.orm_schema import QueriedAttribute, Reading

    logger = logging.getLogger(__name__)
    logger.setLevel(logging.DEBUG)
    engine = sqlalchemy.create_engine(ctx.parent.obj["cp"].get("SQL", "url"))
    fig, ax = plt.subplots(constrained_layout=True)
    with sqlalchemy.orm.Session(engine) as session, session.begin():
        qas = list(
            session.scalars(
                sqlalchemy.select(QueriedAttribute).where(
                    QueriedAttribute.fieldname.in_(names)
                )
            )
        )
        if len({qa.units for qa in qas}) > 1:
            raise RuntimeError(
                "Invalid selection of logged attributes: they do not have the same units of measurement."
            )
        if interval is not None:
            whereclauses_interval = (Reading.timestamp > dateparser.parse(interval),)
        else:
            whereclauses_interval = ()
        for qa, marker in zip(
            qas,
            [
                ".",
                "o",
                "v",
                "s",
                "*",
                "p",
                "h",
                "+",
                "x",
                "D",
                "1",
                "2",
                "3",
                "4",
                "^",
                "<",
                ">",
                "^",
                "H",
                "d",
            ],
        ):
            data = list(
                session.execute(
                    sqlalchemy.select(Reading.timestamp, Reading.value).where(
                        *((Reading.attribute == qa,) + whereclauses_interval)
                    )
                ).all()
            )
            dates = [d for d, v in data]
            values = [v for d, v in data]
            ax.plot(
                dates,
                values,
                label=qa.fieldname,
                linestyle="-" if lines else "",
                marker=marker if showmarkers else None,
            )
        ax.set_xlabel("Date of readout")
        ax.set_ylabel(
            f"{ylabel} ({qas[0].units})"
            if ylabel is not None
            else f"Values ({qas[0].units})"
        )
        ax.tick_params(axis="x", labelrotation=90)
        ax.legend(loc="best")
        if logscale:
            ax.set_yscale("log")
        if ymin is not None:
            ax.axis(ymin=ymin)
        if ymax is not None:
            ax.axis(ymax=ymax)
        if grid:
            ax.grid(True, which="both")
        if timestamp:
            fig.text(0.99, 0.01, str(datetime.datetime.now()), ha="right", va="bottom")
        if output is not None:
            print(f'Output is {output}')
            if (m := re.match('dokuwikirpc://(?P<hostname>.+)/(?P<medianame>.+)', output)):
                with tempfile.TemporaryDirectory() as td:
                    fig.savefig(os.path.join(td, 'graph.png'), dpi=600, transparent=True, bbox_inches='tight')
                    with open(os.path.join(td, 'graph.png'), 'rb') as f:
                        b64data = base64.b64encode(f.read())
                if dokuwiki_api_client_cert is not None:
                    if dokuwiki_api_client_key is not None:
                        cert = (dokuwiki_api_client_cert, dokuwiki_api_client_key)
                    else:
                        cert = dokuwiki_api_client_cert
                else:
                    cert = None
                response = requests.post(
                    url=f'https://{m["hostname"]}/lib/exe/jsonrpc.php/core.saveMedia',
                    headers={'content-type': 'application/json',
                             'Accept': 'application/json',
                             'Authorization': f'Bearer {dokuwiki_api_token}',
                             },
                    data=json.dumps({
                        "media": m["medianame"],
                        "base64": b64data.decode('ascii'),
                        "overwrite": True,
                    }),
                    cert=cert)
                print('Results of dokuwiki upload:')
                print(f'   site: {m["hostname"]}')
                print(f'   media: {m["medianame"]}')
                print(f'   elapsed time: {response.elapsed}')
                print(f'   status code: {response.status_code}')
                print(f'   reason: {response.reason}')
                print(f'   data:')
                print(response.json())
            else:
                fig.savefig(output, dpi=600)
        if gui:
            plt.show()
