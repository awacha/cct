from typing import Optional

import click

from .main import main


@main.command()
@click.option("--project", "-p", default=None, help="Project file to load")
@click.option("--icontheme", default=None, help="Icon theme name", type=str)
def processing4(project: Optional[str], icontheme: Optional[str]):
    """Open the data processing GUI (obsolete version 4)"""
    import logging
    import multiprocessing
    import sys

    import taurus

    taurus.Logger.disableLogOutput()
    from taurus.external.qt import QtGui, QtWidgets

    from ..qtgui2.processingmain.main import Main

    logger = logging.getLogger(__name__)
    logger.setLevel(logging.DEBUG)

    app = QtWidgets.QApplication(sys.argv)
    if icontheme is not None:
        QtGui.QIcon.setThemeName(icontheme)
    mw = Main()
    if project is not None:
        mw.openProject(project)
    mw.show()
    logger.debug("Starting event loop")
    result = app.exec_()
    mw.deleteLater()
    app.deleteLater()
    sys.exit(result)


@main.command()
@click.option("--project", "-p", default=None, help="Project file to load")
@click.option("--icontheme", default=None, help="Icon theme name", type=str)
def processing(project: Optional[str], icontheme: Optional[str]):
    """Open the data processing GUI"""
    import logging
    import sys

    import taurus

    taurus.Logger.disableLogOutput()
    from taurus.external.qt import QtGui, QtWidgets, QtCore

    from ..qtgui2.processingmain.processing_entry import ProcessingEntryDialog
    from ..qtgui2.processingmain.main5 import ProcessingMainWindow

    logger = logging.getLogger(__name__)
    logger.setLevel(logging.DEBUG)

    app = QtWidgets.QApplication(sys.argv)
    if icontheme is not None:
        QtGui.QIcon.setThemeName(icontheme)
    if project is None:
        ped = ProcessingEntryDialog(None, QtCore.Qt.Dialog)
        if ped.exec_():
            project = ped.chosen_file
        if project is not None:
            ped.saveRecentFileList()
    if project is not None:
        main = ProcessingMainWindow(None, QtCore.Qt.Window, project)
        main.show()
        return app.exec_()


@main.command()
@click.option("--project", "-p", default=None, help="Project file to load")
@click.option("--icontheme", default=None, help="Icon theme name", type=str)
def processing5(project: Optional[str], icontheme: Optional[str]):
    """Open the data processing GUI (version 5)"""
    return processing(project, icontheme)
