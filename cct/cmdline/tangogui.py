import click

from .main import main


@main.command()
@click.argument('device')
def tangogui(device):
    """Open a GUI for the Tango device DEVICE"""
    import sys

    import tango
    import taurus
    taurus.Logger.disableLogOutput()
    import taurus.core.tango.tangodatabase
    from taurus.external.qt import QtWidgets
    from taurus.qt.qtgui.application import TaurusApplication

    from .. import _version
    from ..tangogui import TangoDeviceUI

    app = TaurusApplication(sys.argv, app_name='cct', app_version=_version.version, org_name='CREDO',
                            cmd_line_parser=None)
    auth: taurus.core.tango.tangodatabase.TangoAuthority = taurus.Authority()
    print(sys.argv[0])
    try:
        classname = auth.getDevice(device).klass().name()
    except AttributeError:
        QtWidgets.QMessageBox.critical(
            None, 'Device not found', f'Tango device {device} does not exist')
        return 1
    try:
        GuiClass = [sc for sc in TangoDeviceUI.__subclasses__(
        ) if sc.deviceserverclass == classname][0]
    except IndexError:
        QtWidgets.QMessageBox.critical(
            None, 'Class not found', f'Could not find UI for device class {classname}')
        return 2
    try:
        taurus.Device(device).getDeviceProxy().ping()
    except tango.ConnectionFailed as cf:
        QtWidgets.QMessageBox.critical(
            None, 'Device unreachable', cf.args[0].desc)
        return 3
    print('Ping OK', flush=True)
    w = GuiClass(parent=None)
    w.setModel(device)
    w.show()
    return app.exec_()
