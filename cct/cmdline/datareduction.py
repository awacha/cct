
import click

from .main import main


@main.command()
@click.option('--firstfsn', '-f', default=None, help='First file sequence number', type=click.IntRange(0),
              prompt='First file sequence number')
@click.option('--lastfsn', '-l', default=None, help='Last file sequence number', type=click.IntRange(0),
              prompt='Last file sequence number')
@click.option('--filenameformat', default='crd_%05d', help='File name format',
              type=str)
@click.option('--nexus/--pickle', is_flag=True, default=False, help='Process NeXus files instead of pickled headers and cbfs')
def datareduction(firstfsn: int, lastfsn: int, filenameformat: str, nexus: bool):
    """Command-line data reduction routine"""
    import logging
    import taurus
    import tango
    import time
    import os
    from ..datareduction import DataReductionBackend, DataReductionConfig, DataReductionPipeLineNeXus, DataReductionPipeLine
    from ..core2.fastfindfile import fastfindfile
    taurus.Logger.disableLogOutput()

    logger = logging.getLogger(__name__)
    logger.setLevel(logging.INFO)

    if nexus:
        pipelineclass = DataReductionPipeLineNeXus
    else:
        pipelineclass = DataReductionPipeLine

    try:
        dburl = tango.Database().get_property('CREDO', 'dburl')['dburl'][0]
    except Exception:
        raise ValueError(
            'Tango property CREDO->dburl must be a SQLAlchemy-compatible database URL')
    try:
        dburl_expdb = tango.Database().get_property(
            'CREDO', 'dburl.exposures')['dburl.exposures'][0]
    except Exception:
        raise ValueError(
            'Tango property CREDO->dburl.exposures must be a SQLAlchemy-compatible database URL')
    for subdir in ['eval2d', 'param', 'param_override', 'mask', 'images']:
        if not os.path.exists(subdir) or not os.path.isdir(subdir):
            raise RuntimeError(f'Required subdirectory {subdir} not found.')
    drc = DataReductionConfig('eval2d', dburl, dburl_expdb, 'mask')
    drb = DataReductionBackend(drc, pipelineclass)
    
    for fsn in range(firstfsn, lastfsn + 1):
        name = filenameformat % fsn
        if nexus:
            try:
                imgfile = fastfindfile('nexus', name, ['.nxs', '.nx5', '.h5', '.hdf5', '.nx'])
            except FileNotFoundError:
                logger.warning(f'Could not find NeXus file for name {name}.')
                continue
            drb.submit(os.path.abspath(imgfile), '')
        for paramdir in ['param_override', 'param']:
            try:
                metadatafile = fastfindfile(paramdir, name, ['.pickle', '.pickle.gz', '.param', '.param.gz'])
                break
            except FileNotFoundError:
                pass
        else:
            logger.warning(f'Could not find metadata file for {name}')
            continue

        imgfile = fastfindfile('images', name, ['.cbf'])

        drb.submit(os.path.abspath(imgfile), os.path.abspath(metadatafile))

    try:
        while True:
            for imgfile, metadatafile, what, result in drb.getResults():

                msg = f'{os.path.split(imgfile)[1]}: {result}'
                if what == 'debug':
                    logger.debug(msg)
                elif what == 'info':
                    logger.info(msg)
                elif what == 'warning':
                    logger.warning(msg)
                elif what == 'error':
                    logger.error(msg)
                elif what in ['done', 'failed']:
                    pass
                else:
                    logger.critical(msg)
            if drb.submitted <= 0:
                logger.info(f'{drb.submitted=}')
                break
    finally:
        logger.info('Stopping data reduction background thread')
        drb.stop()
        logger.info('Joining data reduction background thread')
        drb.join()
        
        