import click

from .main import main


@main.command()
@click.option('-d', '--door', 'door', help='Name of the Sardana door to use. If not given, finds the first one in the Tango database.', default=None, required=False)
@click.option('--verbose/--quiet', 'verbose', help='Verbose operation')
@click.argument('windowclassname', default = None, required=False)
def sardanagui(door, windowclassname, verbose):
    """Open a GUI window"""
    import sys
    import sardana.taurus.core.tango.sardana
    import sardana.taurus.qt.qtcore.tango.sardana
    import sardana.util.parser
    import taurus
    import taurus.core.tango.tangodatabase

    sardana.taurus.core.tango.sardana.registerExtensions()
    sardana.taurus.qt.qtcore.tango.sardana.registerExtensions()

#    taurus.Logger.disableLogOutput()
    import taurus.core.tango.tangodatabase
    from taurus.qt.qtgui.application import TaurusApplication

    from .. import _version
    from .. import sardanagui
    app = TaurusApplication(sys.argv, app_name='cct', app_version=_version.version, org_name='CREDO',
                            cmd_line_parser=None)
    if windowclassname is None:
        print('Supported GUI utilities:')
        for wc in dir(sardanagui):
            try:
                if issubclass(getattr(sardanagui, wc), sardanagui.SardanaGUIBase) and getattr(sardanagui, wc) is not sardanagui.SardanaGUIBase:
                    print(f'  {wc}')
            except TypeError:
                pass
        return 0
    windowclass = getattr(sardanagui, windowclassname)
    if door is None:
        door = sardanagui.utils.getDoor()
    w = windowclass(parent=None, door=door)
    if verbose:
        w.setLogLevel(w.Debug)
    w.show()
    return app.exec_()
