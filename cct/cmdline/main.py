import sys
import click


def excepthook(exctype, exc, tb):
    import logging
    import traceback
    formatted_tb = "".join(traceback.format_exception(exctype, exc, tb))
    logging.root.critical(
        f'Uncaught exception: {repr(exc)}\nTraceback:\n{formatted_tb}')


@click.group()
@click.option('--die-on-error/--dont-die-on-error', '-d', default=False,
              help='Die on an unhandled exception (for debugging)', type=bool, is_flag=True)
def main(die_on_error: bool):
    import logging
    logging.captureWarnings(True)
    import sys

    import colorlog
#    import taurus

#    taurus.Logger.disableLogOutput()
    if not die_on_error:
        sys.excepthook = excepthook
    logging.getLogger('matplotlib').setLevel(logging.INFO)
    # logging.getLogger('matplotlib.font_manager').setLevel(logging.INFO)
    handler = colorlog.StreamHandler()
    handler.setFormatter(colorlog.ColoredFormatter(
        '%(log_color)s%(asctime)s %(levelname)s:%(name)s:%(message)s'
    ))
    logging.root.addHandler(handler)
    warnings_logger = logging.getLogger("py.warnings")
    warnings_logger.addHandler(handler)


if __name__ == '__main__':
    sys.exit(main())