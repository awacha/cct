
from typing import Optional

import click

from .main import main


@main.command()
@click.option('--config', '-c', default='config/cct.pickle', help='Config file',
              type=click.Path(file_okay=True, dir_okay=False, writable=True, readable=True,
                              allow_dash=False, ))
@click.option('--online/--offline', default=False, help='Connect to devices')
@click.option('--root/--no-root', '-r', default=False, help='Skip login', type=bool, is_flag=True)
@click.option('--icontheme', default=None, help='Icon theme name', type=str)
@click.option('--door', default=None, help='Sardana door name', type=str, required=False)
def daq(config: str, online: bool, root: bool, icontheme: Optional[str] = None, door: Optional[str] = None):
    """Open the data acquisition GUI mode"""

    # put as many imports here as possible. They'll only be imported when the subcommand is run, so
    # they won't slow down initialization of other parts of the program, nor clutter the memory with
    # unneeded code.
    import logging
    import logging.handlers
    import os
    import sys
    import taurus

    from taurus.external.qt import QtGui, QtWidgets
    from taurus.qt.qtgui.application import TaurusApplication

    from .. import _version
    from ..core2.instrument.instrument import Instrument
    from ..qtgui2.main.logindialog import LoginDialog
    from ..qtgui2.main.mainwindow import MainWindow
    from ..sardanagui.utils import getDoor

    taurus.Logger.disableLogOutput()

    logger = logging.getLogger(__name__)
    logger.setLevel(logging.DEBUG)

    os.makedirs('log', exist_ok=True)
    handler = logging.handlers.TimedRotatingFileHandler(
        'log/cct4.log', 'D', 1, encoding='utf-8', backupCount=0)
    handler.addFilter(logging.Filter('cct'))
    formatter = logging.Formatter(
        '%(asctime)s:%(name)s:%(levelname)s:%(message)s')
    handler.setFormatter(formatter)
    logging.root.addHandler(handler)

#    multiprocessing.set_start_method(
#        'forkserver')  # the default 'fork' method is not appropriate for multi-threaded programs, e.g. with PyQt.
    app = TaurusApplication(sys.argv, app_name='CCT',
                            app_version=_version.version, org_name='CREDO', cmd_line_parser=None)
    if icontheme is not None:
        QtGui.QIcon.setThemeName(icontheme)
    logger.debug('Instantiating Instrument()')
    instrument = Instrument(configfile=config)
    logger.debug('Instrument() done.')
    instrument.auth.setRoot()
    instrument.setOnline(online)
    mw = MainWindow(instrument=instrument, door=door if door is not None else getDoor())
    mw.show()
    instrument.start()

    def onAboutToQuit():
        pass
    app.aboutToQuit.connect(onAboutToQuit)

    logger.debug('Starting event loop')
    result = app.exec_()
#    print('Now exiting...')
    sys.exit(result)
