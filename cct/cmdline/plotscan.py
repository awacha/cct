import click
from typing import Optional
from .main import main


@main.command()
@click.option(
    '-d', '--door', 'door',
    help='Name of the Sardana MacroServer Door to use. If empty, use the first one available.',
    default=None)
@click.option(
    '-f', '--file', 'filename',
    help='The scan file to open',
    type=click.Path(exists=True, dir_okay=False),
    default=None,
    required=False,
)
@click.option(
    '-i', '--index', 'index',
    help='The index of the scan to open from the file',
    type=click.INT,
    default=None,
    required=False
)
@click.option(
    '-l', '--last', 'openlast', is_flag=True,
    help='Just open the last completed scan',
    type=click.BOOL,
    default=False,
    required=False,
)
def plotscan(door: str, filename: Optional[str], index: Optional[int], openlast: bool = False):
    """Open a GUI for the Tango device DEVICE"""
    import sys
    import os

    import taurus
    taurus.Logger.disableLogOutput()
    import taurus.core.tango.tangodatabase
    from taurus.qt.qtgui.application import TaurusApplication
    import sardana.taurus.core.tango.sardana
    import sardana.taurus.qt.qtcore.tango.sardana
    from sardana.taurus.qt.qtcore.tango.sardana.macroserver import QDoor

    from .. import _version
    from ..sardanagui.plotscan import PlotScanSardana
    from ..sardanagui.utils import getDoor
    from ..core2.dataclasses import Scan

    sardana.taurus.core.tango.sardana.registerExtensions()
    sardana.taurus.qt.qtcore.tango.sardana.registerExtensions()

    app = TaurusApplication(sys.argv, app_name='cct', app_version=_version.version, org_name='CREDO',
                            cmd_line_parser=None)

    if door is None:
        door = getDoor()
    w = PlotScanSardana(door=door)
    if openlast:
        d = taurus.Device(door)
        assert isinstance(d, QDoor)
        scandir = d.getEnvironment('ScanDir')
        scanfile = d.getEnvironment('ScanFile')[0]
        scanid = d.getEnvironment('ScanID')
        scan = Scan.fromh5file(os.path.join(scandir, scanfile), scanid)
        w.setScan(scan._data, scan.motorname, scan.command)
        w.setWaitForNewScans(True)
    w.show()
    return app.exec_()
