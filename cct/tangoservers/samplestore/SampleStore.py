# -*- coding: utf-8 -*-
#
# This file is part of the SampleStore project
#
#
#
# Distributed under the terms of the BSD3 license.
# See LICENSE.txt for more info.

"""
SampleStore

A device for storing data of samples in a SQL database
"""

# PROTECTED REGION ID(SampleStore.system_imports) ENABLED START #
# PROTECTED REGION END #    //  SampleStore.system_imports

# PyTango imports
import tango
from tango import DebugIt
from tango.server import run
from tango.server import Device
from tango.server import command
from tango.server import device_property
from tango import AttrQuality, DispLevel, DevState
from tango import AttrWriteType, PipeWriteType
# Additional import
# PROTECTED REGION ID(SampleStore.additionnal_import) ENABLED START #
import sqlalchemy.orm
from ...core2.orm_schema import Sample, Project, Categories, Situations, Base
from typing import Dict, Any, List, Final, Optional, Union
import dateutil.parser
import datetime
import numpy as np
import json
import traceback
from collections import namedtuple
import re

ProjectTuple = namedtuple('ProjectTuple', ['year', 'category', 'id'])
# PROTECTED REGION END #    //  SampleStore.additionnal_import

__all__ = ["SampleStore", "main"]


class SampleStore(Device):
    """
    A device for storing data of samples in a SQL database

    **Properties:**

    - Device Property
        sqlurl
            - SQL url (as understood by sqlalchemy)
            - Type:'str'
    """
    # PROTECTED REGION ID(SampleStore.class_variable) ENABLED START #
    sqlengine: sqlalchemy.engine.base.Engine
    _sample_properties: Final[List[str]] = [
        'title', 'description', 'positionx', 'positiony', 'positionx_error', 'positiony_error', 
        'thickness', 'thickness_error', 'distminus', 'distminus_error', 
        'transmission', 'transmission_error', 'project', 'maskoverride', 
        'category', 'situation', 'preparedby', 'preparetime']
    # PROTECTED REGION END #    //  SampleStore.class_variable

    # -----------------
    # Device Properties
    # -----------------

    sqlurl = device_property(
        dtype='str',
        doc="SQL url (as understood by sqlalchemy)",
        mandatory=True
    )

    # ---------------
    # General methods
    # ---------------

    def init_device(self):
        """Initializes the attributes and properties of the SampleStore."""
        Device.init_device(self)
        # PROTECTED REGION ID(SampleStore.init_device) ENABLED START #
        self.sqlengine = sqlalchemy.create_engine(self.sqlurl, pool_pre_ping=True)
        try:
            Base.metadata.create_all(self.sqlengine)
        except sqlalchemy.exc.OperationalError as exc:
            self.set_state(tango.DevState.FAULT)
            self.set_status(str(exc))
        else:
            self.set_state(tango.DevState.ON)
            self.set_status('Connected to the database')
        # PROTECTED REGION END #    //  SampleStore.init_device

    def always_executed_hook(self):
        """Method always executed before any TANGO command is executed."""
        # PROTECTED REGION ID(SampleStore.always_executed_hook) ENABLED START #
        # PROTECTED REGION END #    //  SampleStore.always_executed_hook

    def delete_device(self):
        """Hook to delete resources allocated in init_device.

        This method allows for any memory or other resources allocated in the
        init_device method to be released.  This method is called by the device
        destructor and by the device Init command.
        """
        # PROTECTED REGION ID(SampleStore.delete_device) ENABLED START #
        self.sqlengine.dispose()
        # PROTECTED REGION END #    //  SampleStore.delete_device

    # --------
    # Commands
    # --------


    @command(
        dtype_in='DevVarStringArray',
        doc_in="Sample name and other property-value pairs, e.g.:"
               "[`test`, `description`, `test sample`, `positionx`, `0.31`, `positiony`, `45`]",
    )
    @DebugIt()
    def CreateSample(self, argin):
        # PROTECTED REGION ID(SampleStore.CreateSample) ENABLED START #
        """
        Create a sample with the name given
        :param argin:     Sample name and other property-value pairs, e.g.:
            [`test`, `description`, `test sample`, `positionx`, `0.31`, `positiony`, `45`]
        :type argin: PyTango.DevVarCharArray

        :rtype: PyTango.DevVoid
        """
        sampledict = {"title": argin[0]}
        if not len(argin) % 2:
            raise ValueError('number of input arguments must be odd (title + property-value pairs)')
        for i in range(len(argin[1:]) // 2):
            key = argin[1+2*i]
            value = argin[2+2*i]
            if key == 'title' and value != argin[0]:
                raise ValueError('Sample title must not be given twice')
            if key not in self._sample_properties:
                raise ValueError(f'Unknown property: {key}')
            sampledict[key] = value
        sampledict = self.initialize_sample_properties_with_defaults(sampledict)
        sampledict = self.parse_sample_properties_from_json(sampledict)
        self.info_stream(str(sampledict))
        with sqlalchemy.orm.Session(self.sqlengine) as session:
            session.add(self.dict_to_orm_sample(sampledict))
            try:
                session.commit()
            except sqlalchemy.exc.IntegrityError:
                raise ValueError(f'Sample with name {argin[0]} already exists.')
        # PROTECTED REGION END #    //  SampleStore.CreateSample

    @command(
        dtype_in='DevString',
        doc_in="Name of the sample to be deleted",
    )
    @DebugIt()
    def DeleteSample(self, argin):
        # PROTECTED REGION ID(SampleStore.DeleteSample) ENABLED START #
        """
        Delete the named sample
        :param argin: Name of the sample to be deleted
        :type argin: PyTango.DevString

        :rtype: PyTango.DevVoid
        """
        with sqlalchemy.orm.Session(self.sqlengine) as session:
            sample = session.scalar(sqlalchemy.select(Sample).where(Sample.title == argin))
            if not sample:
                raise ValueError(f'Unknown sample {argin}')
            session.delete(sample)
            session.commit()
        # PROTECTED REGION END #    //  SampleStore.DeleteSample

    @command(
        dtype_in='DevString',
        doc_in="Name of the sample",
        dtype_out='DevEncoded',
        doc_out="JSON-formatted sample data",
    )
    @DebugIt()
    def GetSampleJSON(self, argin):
        # PROTECTED REGION ID(SampleStore.GetSampleJSON) ENABLED START #
        """
        Get the sample as a JSON-formatted dictionary
        :param argin: Name of the sample
        :type argin: PyTango.DevString

        :return: JSON-formatted sample data
        :rtype: PyTango.DevEncoded
        """
        with sqlalchemy.orm.Session(self.sqlengine) as session:
            samples = list(session.scalars(sqlalchemy.select(Sample).where(Sample.title == argin)))
            if len(samples) > 1:
                # this should not happen
                raise ValueError('Multiple samples found')
            elif not samples:
                raise ValueError(f'No such sample: {argin}')
            sample: Sample = samples[0]
            return "json", self.jsonify_sample_properties(self.orm_sample_to_dict(sample))
        # PROTECTED REGION END #    //  SampleStore.GetSampleJSON

    @command(
        dtype_in='DevString',
        doc_in="Regular expression",
        dtype_out='DevVarStringArray',
        doc_out="List of sample names",
    )
    @DebugIt()
    def GetSampleListRegex(self, argin):
        # PROTECTED REGION ID(SampleStore.GetSampleListRegex) ENABLED START #
        """
        Get a list of the samples defined currently whose name match the supplied regular expression
        :param argin: Regular expression
        :type argin: PyTango.DevString

        :return: List of sample names
        :rtype: PyTango.DevVarStringArray
        """
        with sqlalchemy.orm.Session(self.sqlengine) as session:
            return list(session.scalars(sqlalchemy.select(Sample.title).where(Sample.title.op("regexp")(argin))))
        # PROTECTED REGION END #    //  SampleStore.GetSampleListRegex

    @command(
        dtype_in='DevEncoded',
        doc_in="JSON-formatted sample dictionary",
    )
    @DebugIt()
    def SetSampleJSON(self, argin):
        # PROTECTED REGION ID(SampleStore.SetSampleJSON) ENABLED START #
        """
        Add a sample (or replace an existing one with the same title) specified by JSON data
        :param argin: JSON-formatted sample dictionary
        :type argin: PyTango.DevEncoded

        :rtype: PyTango.DevVoid
        """
        if argin[0] != 'json':
            raise ValueError(f'Invalid encoding, "json" expected, got "{argin[0]}"')
        self.info_stream(str(argin))
        sample = self.parse_sample_properties_from_json(argin[1])
        with sqlalchemy.orm.Session(self.sqlengine) as session:
            presentsample = session.scalar(sqlalchemy.select(Sample).where(Sample.title == sample["title"]))
            sample = self.dict_to_orm_sample(sample, presentsample)
            session.add(sample)
            session.commit()
        # PROTECTED REGION END #    //  SampleStore.SetSampleJSON

    @command(
        dtype_in='DevVarStringArray',
        doc_in="Sample title, then (property, value) pairs, like"
               "[`test`, `positionx`, `10.0`, `description`, `test sample`].",
    )
    @DebugIt()
    def ModifySample(self, argin):
        # PROTECTED REGION ID(SampleStore.ModifySample) ENABLED START #
        """
        Modify parameters of a sample.
        :param argin:     Sample title, then (property, value) pairs, like
            [`test`, `positionx`, `10.0`, `description`, `test sample`].
        :type argin: PyTango.DevVarStringArray

        :rtype: PyTango.DevVoid
        """
        if not len(argin) % 2:
            raise ValueError('number of input arguments must be odd (title + property-value pairs)')
        with sqlalchemy.orm.Session(self.sqlengine) as session:
            sample = session.scalar(sqlalchemy.select(Sample).where(Sample.title == argin[0]))
            if sample is None:
                raise ValueError(f'Unknown sample: {argin[0]}')
            for i in range(len(argin[1:]) // 2):
                key = argin[1+2*i]
                value = argin[2+2*i]
                if key not in self._sample_properties:
                    raise ValueError(f'Unknown property: {key}')
                value = self.parse_parameter(key, value)
                if (key == 'project') and (value is not None) and isinstance(value, ProjectTuple):
                    project = session.scalar(
                        sqlalchemy.select(Project).where(
                            sqlalchemy.or_(
                                Project.year == (2000 + value.year%100), 
                                Project.year == (value.year%100)),
                            Project.category == value.category,
                            Project.id == value.id))
                    if project is None:
                        raise ValueError(f'Nonexistent project {value.category} {value.year}/{value.id}')
                    value = project
                setattr(sample, key, value)
            try:
                session.commit()
            except sqlalchemy.exc.IntegrityError:
                raise ValueError(f'Sample with name {argin[0]} already exists.')
            
        # PROTECTED REGION END #    //  SampleStore.ModifySample

    @command(
        dtype_out='DevVarStringArray',
        doc_out="List of sample names",
    )
    @DebugIt()
    def GetSampleList(self):
        # PROTECTED REGION ID(SampleStore.GetSampleList) ENABLED START #
        """
        Get a list of all sample names present
        :return: List of sample names
        :rtype: PyTango.DevVarStringArray
        """
        with sqlalchemy.orm.Session(self.sqlengine) as session:
            return list(session.scalars(sqlalchemy.select(Sample.title)))
        # PROTECTED REGION END #    //  SampleStore.GetSampleList

    @command(
        dtype_in='DevString',
        doc_in="Pattern, as used by SQL`s ``LIKE`` operator",
        dtype_out='DevVarStringArray',
        doc_out="List of matching sample names",
    )
    @DebugIt()
    def GetSampleListLike(self, argin):
        # PROTECTED REGION ID(SampleStore.GetSampleListLike) ENABLED START #
        """
        Get a list of sample names which match the pattern in the sense of SQL-s ``LIKE`` operator
        :param argin: Pattern, as used by SQL`s ``LIKE`` operator
        :type argin: PyTango.DevString

        :return: List of matching sample names
        :rtype: PyTango.DevVarStringArray
        """
        with sqlalchemy.orm.Session(self.sqlengine) as session:
            return list(session.scalars(sqlalchemy.select(Sample.title).where(Sample.title.op("like")(argin))))
        # PROTECTED REGION END #    //  SampleStore.GetSampleListLike

    @command(
        dtype_in='DevString',
        doc_in="Sample name prefix",
        dtype_out='DevString',
    )
    @DebugIt()
    def NextFreeSampleName(self, argin):
        # PROTECTED REGION ID(SampleStore.NextFreeSampleName) ENABLED START #
        """
        Get the next free sample name with the given prefix.
        :param argin: Sample name prefix
        :type argin: PyTango.DevString

        :rtype: PyTango.DevString
        """
        samplenames = self.GetSampleList()
        prefix = argin
        if m := re.match(r'(?P<prefix>.*?)(?P<number>\d+)$', prefix):
            # if the sample name ends with a number
            ndigits = len(m['number'])
            prefix = m['prefix']
            i = int(m['number'])
        else:
            if prefix not in samplenames:
                return prefix
            i = 0
            prefix = prefix+'_'
            ndigits = 0
        while (sn := f'{prefix}{i:0{ndigits}d}') in samplenames:
            i += 1
        return sn

        # PROTECTED REGION END #    //  SampleStore.NextFreeSampleName

# ----------
# Run server
# ----------

# PROTECTED REGION ID(SampleStore.custom_code) ENABLED START #
    def parse_parameter(self, key: str, value: Any) -> Any:
        if key in ['title', 'description', 'preparedby', 'maskoverride']:
            if str(value) in ['""', "''"]:
                value = ""
            return str(value)
        elif key in ['positionx', 'positiony', 'positionx_error', 'positiony_error', 'thickness', 'thickness_error', 'distminus', 'distminus_error', 'transmission', 'transmission_error']:
            return float(value)
        elif key == 'preparetime':
            if isinstance(value, str):
                return dateutil.parser.parse(value)
            elif not isinstance(value, datetime.datetime):
                raise ValueError(f'Invalid type for sample property "preparetime": {type(value)}')
            return value
        elif key == 'category':
            if isinstance(value, Categories):
                return value
            for c in Categories:
                possible_values = {c.value.lower(), c.name.lower()}
                possible_values |= {v.replace('_and_', '+') for v in possible_values}
                possible_values |= {v.replace('_', ' ') for v in possible_values}
                possible_values |= {v.replace(' ', '_') for v in possible_values}
                possible_values |= {v.replace('+', '_and_') for v in possible_values}
                if value.lower() in possible_values:
                    return c
            else:
                raise ValueError(f'Invalid value for sample property "category": {value}')
        elif key == 'situation':
            if isinstance(value, Situations):
                return value
            for c in Situations:
                possible_values = {c.value.lower(), c.name.lower()}
                possible_values |= {v.replace('_and_', '+') for v in possible_values}
                possible_values |= {v.replace('_', ' ') for v in possible_values}
                possible_values |= {v.replace(' ', '_') for v in possible_values}
                possible_values |= {v.replace('+', '_and_') for v in possible_values}
                if value.lower() in possible_values:
                    return c
            else:
                raise ValueError(f'Invalid value for sample property "situation": {value}')
        elif key == 'project':
            if (value is None):
                return None
            if isinstance(value, str) and ((not value.strip()) or value in ["''", '""']):
                return None
            elif isinstance(value, ProjectTuple):
                return value
            else:
                m = re.match(r'^(?P<category>\S+)\s+(?P<year>[0-9]{2}([0-9]{2})?)\s*/\s*(?P<id>[0-9]+)$', value)
                if m is None:
                    raise ValueError(f'Invalid value for sample property "project": should be of the form "<category> <year>/<id>", but got "{value}"')
                return ProjectTuple(category=m["category"], year=int(m["year"])%100+2000, id=int(m["id"]))
        else:
            raise ValueError(f'Unknown key: {key}')

    def unparse_parameter(self, key: str, value: Any) -> Any:
        if isinstance(value, str):
            return value
        if key in ['title', 'description', 'preparedby', 'maskoverride']:
            return str(value)
        elif key in ['positionx', 'positiony', 'positionx_error', 'positiony_error', 'thickness', 'thickness_error', 'distminus', 'distminus_error', 'transmission', 'transmission_error']:
            if not isinstance(value, (int, float, np.number)):
                raise TypeError(f'Invalid type for sample property {key}: {type(value)}')
            return value
        elif key == 'preparetime':
            if not isinstance(value, (datetime.datetime, datetime.date, datetime.time)):
                raise TypeError(f'Invalid type for sample property {key}: {type(value)}')
            return str(value)
        elif key == 'category':
            if not isinstance(value, Categories):
                raise TypeError(f'Invalid type for sample property {key}: {type(value)}')
            return value.value.lower().replace('_and_','+').replace('_', ' ')
        elif key == 'situation':
            if not isinstance(value, Situations):
                raise TypeError(f'Invalid type for sample property {key}: {type(value)}')
            return value.value.lower().replace('_and_','+').replace('_', ' ')
        elif key == 'project':
            if value is None:
                return ''
            elif isinstance(value, ProjectTuple):
                return f'{value.category} {value.year%100:02d}/{value.id:02d}'
        else:
            raise ValueError(f'Unknown key: {key}')


    def parse_sample_properties_from_json(self, jsonstr: Union[str, Dict[str, str]]) -> Dict[str, Any]:
        if isinstance(jsonstr, bytes):
            dic = json.loads(jsonstr)
        elif isinstance(jsonstr, dict):
            dic = jsonstr.copy()
        else:
            raise TypeError(f'Invalid type for jsonstr: {type(jsonstr)}')
        for key, value in dic.items():
            dic[key] = self.parse_parameter(key, value)
        return dic


    def jsonify_sample_properties(self, dic: Dict[str, Any]) -> str:
        dic = dic.copy()
        for key, value in dic.items():
            dic[key] = self.unparse_parameter(key, value)
        if set(dic) != set(self._sample_properties):
            raise ValueError(f'Dictionary keys are different than the ones needed: {set(dic)}')
        self.info_stream(f'Dumping dict to JSON: {dic.keys()}')
        jsonstr = json.dumps(dic)
        self.info_stream(f'JSON: {jsonstr}')
        return jsonstr

    def initialize_sample_properties_with_defaults(self, dic: Dict[str, Any]):
        if 'title' not in dic:
            raise ValueError('Sample must have a unique title at least')
        dic.setdefault('description', '')
        dic.setdefault('preparedby', 'Anonymous')
        dic.setdefault('maskoverride', '')
        dic.setdefault('positionx', 0.0)
        dic.setdefault('positiony', 0.0)
        dic.setdefault('positionx_error', 0.0)
        dic.setdefault('positiony_error', 0.0)
        dic.setdefault('thickness', 1.0)
        dic.setdefault('thickness_error', 0.0)
        dic.setdefault('distminus', 0.0)
        dic.setdefault('distminus_error', 0.0)
        dic.setdefault('transmission', 0.0)
        dic.setdefault('transmission_error', 0.0)
        dic.setdefault('preparetime', datetime.datetime.now())
        dic.setdefault('category', Categories.Sample)
        dic.setdefault('situation', Situations.Vacuum)
        dic.setdefault('project', None)
        return dic
    
    def orm_sample_to_dict(self, sample: Sample) -> Dict[str, Any]:
        dic = {k: getattr(sample, k) for k in self._sample_properties}
        if dic["project"] is not None:
            dic["project"] = ProjectTuple(dic["project"].year, dic["project"].category, dic["project"].id)
        self.info_stream(f'ORM_sample_to_dict: {dic.keys()}')
        return dic

    def dict_to_orm_sample(self, dic: Dict[str, Any], sample: Optional[Sample] = None):
        dic = self.parse_sample_properties_from_json(dic)
        if missing := [k for k in self._sample_properties if k not in dic]:
            raise ValueError(f'Some sample properties missing: {missing}')
        elif superfluous := [k for k in dic if k not in self._sample_properties]:
            raise ValueError(f'Superfluous sample properties: {superfluous}')
        if dic['project'] is not None:
            assert isinstance(dic["project"], ProjectTuple)
            with sqlalchemy.orm.Session(self.sqlengine) as session:
                project = session.scalar(
                    sqlalchemy.select(Project).where(
                        sqlalchemy.or_(
                            Project.year == (2000 + dic["project"].year%100), 
                            Project.year == (dic["project"].year%100)),
                        Project.category == dic["project"].category,
                        Project.id == dic["project"].id))
                if project is None:
                    raise ValueError(f'Nonexistent project {dic["project"].category} {dic["project"].year}/{dic["project"].id}')
                dic['project'] = project
        self.info_stream(f'PrepareTime = {dic["preparetime"]}')
        if sample is None:
            sample = Sample(**dic)
        else:
            for k, v in dic.items():
                setattr(sample, k, v)
        self.info_stream(f"Sample.preparetime = {sample.preparetime}")
        return sample
# PROTECTED REGION END #    //  SampleStore.custom_code


def main(args=None, **kwargs):
    """Main function of the SampleStore module."""
    # PROTECTED REGION ID(SampleStore.main) ENABLED START #
    return run((SampleStore,), args=args, **kwargs)
    # PROTECTED REGION END #    //  SampleStore.main

# PROTECTED REGION ID(SampleStore.custom_functions) ENABLED START #
# PROTECTED REGION END #    //  SampleStore.custom_functions


if __name__ == '__main__':
    main()
