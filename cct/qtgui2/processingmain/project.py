import logging
import os

from taurus.external.qt.QtCore import Slot
from taurus.external.qt import QtWidgets
from .project_ui import Ui_Form
from .processingwindow import ProcessingWindow
from ..utils.filebrowsers import getDirectory
from ..utils.blocksignalscontextmanager import SignalsBlocked
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)


class ProjectWindow(ProcessingWindow, Ui_Form):
    def setupUi(self, Form):
        super().setupUi(Form)
        self.fsnsTreeView.setModel(self.project)

        datarootdir, yeardir = (
            self.project.settings.dataroot,
            self.project.settings.datayear,
        )

        self.rootPathLineEdit.setText(datarootdir)
        self.rootPathLineEdit.editingFinished.emit()

        logger.debug(f'Setting year {yeardir} in year combobox')
        self.yearComboBox.setCurrentIndex(self.yearComboBox.findText(yeardir))
        logger.debug(f'Year combobox setting is now {self.yearComboBox.currentText()}')

        for task in [
            self.project.headers,
            self.project.subtraction,
            self.project.summarization,
            self.project.merging,
        ]:
            task.started.connect(self.onTaskStarted)
            task.finished.connect(self.onTaskFinished)

    @Slot(bool)
    def on_loadHeadersPushButton_clicked(self, checked: bool):
        return self.project.headers.start()

    @Slot(bool)
    def on_averagingPushButton_clicked(self, checked: bool):
        return self.project.summarization.start()

    @Slot(bool)
    def on_subtractionPushButton_clicked(self, checked: bool):
        return self.project.subtraction.start()

    @Slot(bool)
    def on_mergingPushButton_clicked(self, checked: bool):
        return self.project.merging.start()

    @Slot()
    def onTaskStarted(self):
        for pushbutton in [
            self.loadHeadersPushButton,
            self.averagingPushButton,
            self.subtractionPushButton,
            self.mergingPushButton,
        ]:
            pushbutton.setEnabled(False)

    @Slot(bool)
    def onTaskFinished(self, success: bool):
        for pushbutton in [
            self.loadHeadersPushButton,
            self.averagingPushButton,
            self.subtractionPushButton,
            self.mergingPushButton,
        ]:
            pushbutton.setEnabled(True)

    @Slot()
    def on_rootPathLineEdit_editingFinished(self):
        logger.debug("on_rootPathLineEdot_editingFinished")
        datarootdir = os.path.abspath(self.rootPathLineEdit.text())
        logger.debug(f'Trying {datarootdir}, finding year directories')
        yeardirs = sorted(
            [
                s
                for s in os.listdir(datarootdir)
                if os.path.isdir(os.path.join(datarootdir, s))
                and os.path.exists(os.path.join(datarootdir, s, "eval2d"))
            ]
        )
        if not yeardirs:
            QtWidgets.QMessageBox.critical(
                self,
                "Invalid data root directory",
                "Choose another data root directory: this one "
                "doesn't contain any yearly subdirectories!",
            )
            return
        with SignalsBlocked(self.yearComboBox):
            currentyear = self.yearComboBox.currentText() if self.yearComboBox.currentIndex() >= 0 else None
            self.yearComboBox.clear()
            self.yearComboBox.addItems(yeardirs)
            if currentyear is not None:
                self.yearComboBox.setCurrentIndex(self.yearComboBox.findText(currentyear))
            else:
                self.yearComboBox.setCurrentIndex(0)
        self.project.settings.dataroot = datarootdir
        self.project.settings.datayear = self.yearComboBox.currentText()
        self.project.settings.emitSettingsChanged()

    @Slot(str)
    def on_yearComboBox_currentIndexChanged(self, currenttext: str):
        self.project.settings.datayear = self.yearComboBox.currentText()
        self.project.settings.emitSettingsChanged()

    @Slot(bool)
    def on_addFSNRangeToolButton_clicked(self, checked: bool):
        self.project.insertRow(self.project.rowCount())

    @Slot(bool)
    def on_removeFSNRangeToolButton_clicked(self, checked: bool):
        while selectedrows := self.fsnsTreeView.selectionModel().selectedRows(0):
            self.project.removeRow(selectedrows[0].row())

    @Slot(bool)
    def on_clearFSNRangesToolButton_clicked(self, checked: bool):
        self.project.modelReset()

    @Slot(bool)
    def on_rootPathToolButton_clicked(self, checked: bool):
        dn = getDirectory(self, "Select measurement root directory", "")
        if not dn:
            return
        self.rootPathLineEdit.setText(dn)
        self.rootPathLineEdit.editingFinished.emit()
