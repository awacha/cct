from taurus.external.qt import QtWidgets, QtGui

from ...core2.processing.h5io.utils import h5_get_float, h5_get_string, h5_nxpath

from .resultviewwindow import ResultViewWindow
from ...sardanagui.plotimage import PlotImage


class ShowImageWindow(ResultViewWindow):
    plotImage: PlotImage = None

    def setupUi(self, Form: QtWidgets.QWidget):
        self.plotImage = PlotImage(self)
        self.plotImage.setMinimumSize(600, 700)
        layout = QtWidgets.QVBoxLayout()
        layout.setContentsMargins(0, 0, 0, 0, )
        self.setLayout(layout)
        layout.addWidget(self.plotImage)
        self.onResultItemChanged(self.resultitems[0][0], self.resultitems[0][1])
        self.setWindowIcon(QtGui.QIcon(':/icons/saxspattern.svg'))
        self.updateGeometry()

    def onResultItemChanged(self, samplename: str, distancekey: str):
        if not samplename:
            # samplename is empty or None, distancekey is a FSN
            fsn = int(distancekey)
            if self.project.is_canSAS_NeXus():
                path, index = self.project.h5io.find_fsn(fsn)
                with self.project.h5io.reader(path) as h5group:
                    matrix = h5group['all_patterns/I'][index, :, :]
                    mask = h5group["all_patterns/mask"][index, :, :]
                    samplename = h5_get_string(h5group["sample/name"])
                    distance = h5_get_float(h5_nxpath(h5group, "NXinstrument/NXdetector/SDD"), 'mm')
                    wavelength = h5_get_float(h5_nxpath(h5group, "NXinstrument/NXmonochromator/wavelength"), 'nm')
                    pixelsize = h5_get_float(h5_nxpath(h5group, "NXinstrument/NXdetector/x_pixel_size"), "mm")
                    beamx = h5_get_float(h5_nxpath(h5group, "NXinstrument/NXdetector/beam_center_x"), "pixels")
                    beamy = h5_get_float(h5_nxpath(h5group, "NXinstrument/NXdetector/beam_center_y"), "pixels")
                    title =  f'#{fsn}: {samplename} @ {distance:.2f} mm'
                self.plotImage.setImage(matrix, mask, title, wavelength, pixelsize, beamx, beamy, distance)
            else:
                try:
                    ex = self.project.loader().loadExposure(fsn)
                except Exception as exc:
                    QtWidgets.QMessageBox.critical(self, 'Error', f'Cannot load exposure #{fsn}: {exc}')
                    return
                title = ex.header.title
                distance = ex.header.distance[0]
                fsn = ex.header.fsn
                self.plotImage.setExposure(ex, keepzoom=True)
            self.setWindowTitle(
                f'Scattering pattern of sample {title}, {distance:.2f} mm, FSN {fsn}')
        else:
            if self.project.is_canSAS_NeXus():
                path = self.project.h5io.getH5Path(samplename, distancekey)
                with self.project.h5io.reader(path) as h5group:
                    matrix = h5group['pattern/I'][()]
                    mask = h5group["pattern/mask"][()]
                    samplename = h5_get_string(h5group["sample/name"])
                    distance = h5_get_float(h5_nxpath(h5group, "NXinstrument/NXdetector/SDD"), 'mm')
                    wavelength = h5_get_float(h5_nxpath(h5group, "NXinstrument/NXmonochromator/wavelength"), 'nm')
                    pixelsize = h5_get_float(h5_nxpath(h5group, "NXinstrument/NXdetector/x_pixel_size"), "mm")
                    beamx = h5_get_float(h5_nxpath(h5group, "NXinstrument/NXdetector/beam_center_x"), "pixels")
                    beamy = h5_get_float(h5_nxpath(h5group, "NXinstrument/NXdetector/beam_center_y"), "pixels")
                self.plotImage.setImage(matrix, mask, f'{samplename} @ {distance:.2f} mm', wavelength, pixelsize, beamx, beamy, distance)
            else:
                ex = self.project.settings.h5io.readExposure(f'Samples/{samplename}/{distancekey}')
                self.plotImage.setExposure(ex, keepzoom=True)
            self.setWindowTitle(f'Scattering pattern of {samplename} @ {distancekey} mm')

