import os
from taurus.external.qt import QtWidgets, QtCore, QtGui

from .headers import HeadersWindow
from .merging import MergingWindow
from .averaging import AveragingWindow
from .subtraction import SubtractionWindow
from .project import ProjectWindow
from .results import ResultsWindow
from ...core2.processing.processing import Processing

from .main5_ui import Ui_Form


class ProcessingMainWindow(QtWidgets.QWidget, Ui_Form):
    projectwidget: ProjectWindow
    metadatawidget: HeadersWindow
    averagingwidget: AveragingWindow
    subtractionwidget: SubtractionWindow
    mergingwidget: MergingWindow
    resultswidget: ResultsWindow
    filename: str
    project: Processing

    def __init__(
        self,
        parent: QtWidgets.QWidget | None = ...,
        f: QtCore.Qt.WindowFlags = ...,
        filename: str | None = None,
    ) -> None:
        self.filename = filename
        super().__init__(parent, f)
        self.setupUi(self)
        self.setWindowTitle(
            f"{self.windowTitle()} {'(NeXus version)' if self.project.is_canSAS_NeXus() else ''}:: {os.path.split(self.filename)[1]}"
        )

    def setupUi(self, form):
        super().setupUi(form)
        self.setWindowFilePath(self.filename)
        self.project = Processing(self.filename)
        self.projectwidget = ProjectWindow(self.project, self)
        self.metadatawidget = HeadersWindow(self.project, self)
        self.averagingwidget = AveragingWindow(self.project, self)
        self.subtractionwidget = SubtractionWindow(self.project, self)
        self.mergingwidget = MergingWindow(self.project, self)
        self.resultswidget = ResultsWindow(self.project, self)
        for tab, widget in [
            (self.projectTab, self.projectwidget),
            (self.metadataTab, self.metadatawidget),
            (self.averagingTab, self.averagingwidget),
            (self.subtractionTab, self.subtractionwidget),
            (self.mergingTab, self.mergingwidget),
            (self.resultsTab, self.resultswidget),
        ]:
            layout = QtWidgets.QVBoxLayout()
            layout.setContentsMargins(0, 0, 0, 0)
            tab.setLayout(layout)
            layout.addWidget(widget)
            widget.setParent(tab)

    def closeEvent(self, event: QtGui.QCloseEvent) -> None:
        app = QtWidgets.QApplication.instance()
        app.closeAllWindows()
        return super().closeEvent(event)
