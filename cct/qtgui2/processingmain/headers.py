from .processingwindow import ProcessingWindow
from .headers_ui import Ui_Form
from taurus.external.qt import QtGui, QtCore
from taurus.external.qt.QtCore import Slot

import logging

logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)


class HeadersWindow(ProcessingWindow, Ui_Form):
    def setupUi(self, Form):
        super().setupUi(Form)
        self.treeView.setModel(self.project.headers)
        self.reloadPushButton.clicked.connect(self.startStopReload)
        self.forceReloadPushButton.setVisible(self.project.is_canSAS_NeXus())
        self.project.headers.finished.connect(self.onFinished)
        self.project.headers.started.connect(self.onStarted)
        self.project.headers.progress.connect(self.onProgress)
        self.markBadToolButton.clicked.connect(self.markSelectedAsGoodOrBad)
        self.markGoodToolButton.clicked.connect(self.markSelectedAsGoodOrBad)
        self.progressBar.setVisible(False)
        # Do not resize treeview columns automatically: if a lot of headers are loaded, it takes too much time!
        # self.treeView.model().modelReset.connect(self.resizeTreeViewColumns)

    @Slot()
    def on_forceReloadPushButton_clicked(self):
        self.startStopReload(force=True)

    @Slot()
    def resizeTreeViewColumns(self):
        logger.debug("Resize treeview columns")
        for c in range(self.treeView.model().columnCount()):
            self.treeView.resizeColumnToContents(c)
        logger.debug("Treeview columns resized.")

    @Slot()
    def markSelectedAsGoodOrBad(self):
        for selectedindex in self.treeView.selectionModel().selectedRows(0):
            self.project.headers.setData(
                index=selectedindex,
                value=(
                    QtCore.Qt.CheckState.Checked
                    if self.sender() is self.markBadToolButton
                    else QtCore.Qt.CheckState.Unchecked
                ),
                role=QtCore.Qt.ItemDataRole.CheckStateRole,
            )

    @Slot()
    def startStopReload(self, force: bool = False):
        if self.reloadPushButton.text() == "Stop":
            self.project.headers.stop()
        else:
            if self.project.is_canSAS_NeXus():
                self.project.headers.start(overwrite_if_present=force)
            else:
                self.project.headers.start()

    @Slot()
    def onFinished(self):
        for pb in [self.reloadPushButton, self.forceReloadPushButton]:
            pb.setIcon(QtGui.QIcon(QtGui.QPixmap(":/icons/start.svg")))
        self.reloadPushButton.setText("(Re)load")
        self.forceReloadPushButton.setText('Force reload')
        self.progressBar.setVisible(False)

    @Slot()
    def onStarted(self):
        for pb in [self.reloadPushButton, self.forceReloadPushButton]:
            pb.setIcon(QtGui.QIcon(QtGui.QPixmap(":/icons/stop.svg")))
            pb.setText("Stop")
        self.progressBar.setVisible(True)
        self.progressBar.setMinimum(0)
        self.progressBar.setMaximum(0)

    @Slot(int, int)
    def onProgress(self, current: int, total: int):
        self.progressBar.setMaximum(total)
        self.progressBar.setValue(current)
