import datetime
from taurus.external.qt.QtCore import Qt, Slot
from taurus.external.qt import QtWidgets, QtCore, QtGui
from taurus import Logger
import os
import appdirs


from .processing_entry_ui import Ui_Dialog


class ProcessingEntryDialog(QtWidgets.QDialog, Ui_Dialog, Logger):
    chosen_file: str | None = None

    def __init__(
        self, parent: QtWidgets.QWidget | None = ..., f: QtCore.Qt.WindowFlags = ...
    ) -> None:
        super().__init__(parent, f)
        self.call__init__(Logger)
        self.setupUi(self)
        self.setLogLevel(self.Info)

    def setupUi(self, Dialog):
        super().setupUi(Dialog)
        assert isinstance(self.buttonBox, QtWidgets.QDialogButtonBox)
        self.newButton = QtWidgets.QPushButton(
            QtGui.QIcon.fromTheme("document-new"), "New", self
        )
        self.buttonBox.addButton(
            self.newButton, QtWidgets.QDialogButtonBox.ButtonRole.ActionRole
        )
        self.newButton.clicked.connect(self.on_newButton_clicked)
        assert isinstance(self.treeWidget, QtWidgets.QTreeWidget)

        try:
            with open(
                os.path.join(appdirs.user_config_dir("cct"), "cpt4.recents"), "rt"
            ) as f:
                for line in f:
                    if os.path.isfile(line.strip()):
                        path, filename = os.path.split(line.strip())
                        ctime = datetime.datetime.fromtimestamp(
                            os.stat(line.strip()).st_ctime
                        )
                        self.treeWidget.addTopLevelItem(
                            QtWidgets.QTreeWidgetItem(
                                self.treeWidget, [str(ctime), path, filename]
                            )
                        )
        except FileNotFoundError:
            return
        self.accepted.connect(self.on_self_accepted)
        self.buttonBox.button(self.buttonBox.Open).clicked.connect(
            self.on_openButton_clicked
        )

    @Slot(bool)
    def on_newButton_clicked(self, state: bool):
        self.filedialog = QtWidgets.QFileDialog(
            self, "Create a new CPT project", "", "CPT4 projects (*.cpt4.nxs *.credo.nxs)"
        )
#        self.filedialog.setOption(QtWidgets.QFileDialog.Option.DontUseNativeDialog)
        self.filedialog.setModal(True)
        self.filedialog.setDefaultSuffix(".cpt4.nxs")
        self.filedialog.setFileMode(QtWidgets.QFileDialog.FileMode.AnyFile)
        self.filedialog.setAcceptMode(QtWidgets.QFileDialog.AcceptMode.AcceptSave)
        self.filedialog.fileSelected.connect(self.on_fileDialog_fileSelected)
        self.filedialog.open()

    @Slot(str)
    def on_fileDialog_fileSelected(self, filename):
        self.filedialog.destroy()
        self.filedialog = None
        if not filename:
            self.done(self.Rejected)
        else:
            self.chosen_file = filename
            self.debug(f"chosen_file -> {filename} (selected)")
            self.treeWidget.insertTopLevelItem(
                0,
                QtWidgets.QTreeWidgetItem(
                    self.treeWidget,
                    [str(datetime.datetime.now())] + list(os.path.split(filename)),
                ),
            )
            self.treeWidget.setCurrentItem(self.treeWidget.topLevelItem(0))
            self.done(self.Accepted)

    @Slot(QtWidgets.QTreeWidgetItem, int)
    def on_treeWidget_itemActivated(self, item: QtWidgets.QTreeWidgetItem, column: int):
        filename = os.path.join(
            item.data(1, Qt.ItemDataRole.DisplayRole),
            item.data(2, Qt.ItemDataRole.DisplayRole),
        )
        self.debug(f"chosen_file -> {filename} (act)")
        self.chosen_file = filename
        self.treeWidget.insertTopLevelItem(
            0,
            QtWidgets.QTreeWidgetItem(
                self.treeWidget,
                [str(datetime.datetime.now())] + list(os.path.split(filename)),
            ),
        )
        self.treeWidget.setCurrentItem(self.treeWidget.topLevelItem(0))
        self.done(self.Accepted)

    @Slot(bool)
    def on_openButton_clicked(self, state: bool):
        filename, filter = QtWidgets.QFileDialog.getOpenFileName(
            self,
            "Open an existing CPT project",
            "",
            "CPT4 projects (*.cpt4.nxs *.cpt4 *.credo.nxs)",
            "CPT4 projects (*.cpt4.nxs *.cpt4 *.credo.nxs)",
        )
        if not filename:
            self.done(self.Rejected)
        else:
            self.chosen_file = filename
            self.debug(f"chosen_file -> {filename} (open)")
            self.treeWidget.insertTopLevelItem(
                0,
                QtWidgets.QTreeWidgetItem(
                    self.treeWidget,
                    [str(datetime.datetime.now())] + list(os.path.split(filename)),
                ),
            )
            self.treeWidget.setCurrentItem(self.treeWidget.topLevelItem(0))
            self.done(self.Accepted)

    @Slot()
    def on_self_accepted(self):
        self.debug("on_self_accepted()")
        item = self.treeWidget.currentItem()
        filename = os.path.join(
            item.data(1, Qt.ItemDataRole.DisplayRole),
            item.data(2, Qt.ItemDataRole.DisplayRole),
        )
        self.chosen_file = filename
        self.debug(f"chosen_file -> {filename} (accpt)")

    def saveRecentFileList(self):
        os.makedirs(appdirs.user_config_dir("cct"), exist_ok=True)
        filelist = []
        for i in range(self.treeWidget.topLevelItemCount()):
            tli: QtWidgets.QTreeWidgetItem = self.treeWidget.topLevelItem(i)
            fn = os.path.join(
                tli.data(1, QtCore.Qt.ItemDataRole.DisplayRole),
                tli.data(2, QtCore.Qt.ItemDataRole.DisplayRole),
            )
            if fn not in filelist:
                filelist.append(fn)
        with open(
            os.path.join(appdirs.user_config_dir("cct"), "cpt4.recents"), "wt"
        ) as f:
            for fn in filelist:
                f.write(fn + "\n")
