import logging
import multiprocessing

from taurus.external.qt import QtWidgets, QtGui, QtCore
from taurus.external.qt.QtCore import Slot
from .averaging_ui import Ui_Form
from .processingwindow import ProcessingWindow
from .settings import SettingsWindow
from ...core2.processing.tasks.summarization import SummaryData

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)


def logentry(fcn):
    import time
    def function(*args, **kwargs):
        logger.debug(f'-> {fcn.__name__}')
        t0 = time.monotonic()
        try:
            return fcn(*args, **kwargs)
        except Exception as exc:
            logger.error(f'!!! Exception {exc} in {fcn.__name__}')
            raise
        finally:
            logger.debug(f'<- {fcn.__name__}, time {time.monotonic() - t0:.3f} s')
    return function


class AveragingWindow(ProcessingWindow, Ui_Form):
    settingswindow: SettingsWindow | None = None

    @logentry
    def setupUi(self, Form):
        super().setupUi(Form)
        self.treeView.setModel(self.project.summarization)
        self.project.summarization.started.connect(self.onAveragingStarted)
        self.project.summarization.finished.connect(self.onAveragingStopped)
        self.project.summarization.progress.connect(self.onProgress)
        self.progressBar.setVisible(False)
        self.treeView.model().modelReset.connect(self.resizeTreeViewColumns)
        self.processCountSpinBox.setValue(self.project.summarization.maxprocesscount)
        self.processCountSpinBox.setMinimum(1)
        self.processCountSpinBox.setMaximum(multiprocessing.cpu_count())
        self.treeView.selectionModel().currentChanged.connect(self.onCurrentChanged)

    @logentry
    @Slot(QtCore.QModelIndex, QtCore.QModelIndex)
    def onCurrentChanged(
        self, current: QtCore.QModelIndex, previous: QtCore.QModelIndex
    ):
        self.changeSettingsPushButton.setEnabled(
            (not self.project.summarization.isBusy()) and current.isValid()
        )

    @Slot(bool)
    def on_changeSettingsPushButton_clicked(self, checked: bool | None=None):
        if self.settingswindow is None:
            sd: SummaryData = (
                self.treeView.selectionModel()
                .currentIndex()
                .data(QtCore.Qt.ItemDataRole.UserRole)
            )
            self.settingswindow = SettingsWindow(
                project=self.project,
                mainwindow=self.mainwindow,
                closable=True,
                samplename=sd.samplename,
                distkey=f"{sd.distance:.2f}",
            )
            self.settingswindow.show()
            self.settingswindow.destroyed.connect(self.on_settingswindow_destroyed)

    @Slot(int)
    def on_processCountSpinBox_valueChanged(self, value: int):
        if self.project.summarization.isBusy():
            raise ValueError("Cannot change process count: summarization is busy.")
        else:
            self.project.summarization.maxprocesscount = value

    @logentry
    @Slot()
    def resizeTreeViewColumns(self):
        logger.debug("Resizing treeview columns of averaging window")
        for c in range(self.treeView.model().columnCount()):
            self.treeView.resizeColumnToContents(c)
        logger.debug("Resized treeview columns of averaging window.")

    @Slot(bool)
    def on_runPushButton_clicked(self, checked: bool):
        if self.runPushButton.text() == "Run":
            logger.debug('Starting summarization')
            self.project.summarization.start()
            logger.debug('Started summarization')
        else:
            self.project.summarization.stop()

    @logentry
    @Slot(bool)
    def on_runOnlySelectedPushButton_clicked(self, checked: bool):
        if self.runOnlySelectedPushButton.text() == "Run only selected":
            summaryjobs = [
                (sd.samplename, sd.distance)
                for sd in [
                    index.data(QtCore.Qt.ItemDataRole.UserRole)
                    for index in self.treeView.selectionModel().selectedRows()
                ]
            ]
            self.project.summarization.start(startonly=summaryjobs)
        else:
            self.project.summarization.stop()

    @logentry
    @Slot()
    def onAveragingStarted(self):
        for pb in [self.runPushButton, self.runOnlySelectedPushButton]:
            pb.setText("Stop")
            pb.setIcon(QtGui.QIcon(QtGui.QPixmap(":/icons/stop.svg")))
        self.progressBar.setVisible(True)
        self.progressBar.setMinimum(0)
        self.progressBar.setMaximum(0)
        self.processCountSpinBox.setEnabled(False)
        self.changeSettingsPushButton.setEnabled(False)

    @logentry
    @Slot(bool)
    def onAveragingStopped(self, success: bool):
        self.processCountSpinBox.setEnabled(True)
        for pb, text in [
            (self.runPushButton, "Run"),
            (self.runOnlySelectedPushButton, "Run only selected"),
        ]:
            pb.setText(text)
            pb.setIcon(QtGui.QIcon(QtGui.QPixmap(":/icons/start.svg")))
        self.progressBar.setVisible(False)
        self.changeSettingsPushButton.setEnabled(self.treeView.currentIndex().isValid())
        if not success:
            QtWidgets.QMessageBox.critical(
                self, "Averaging stopped", "Averaging stopped unexpectedly."
            )
        else:
            badfsnstext = "\n".join(
                [
                    f'  {sd.samplename}: {", ".join([str(f) for f in sorted(sd.lastfoundbadfsns)])}'
                    for sd in self.project.summarization
                    if sd.lastfoundbadfsns
                ]
            )
            QtWidgets.QMessageBox.information(
                self,
                "Averaging finished",
                f"Finished averaging images in {self.project.summarization.elapsedTimeFromStart():.3f} seconds."
                + (
                    f" New bad exposures found: \n{badfsnstext}"
                    if badfsnstext
                    else " No new bad exposures found."
                ),
            )

    @logentry
    @Slot(int, int)
    def onProgress(self, current: int, total: int):
        logger.debug(f"Averaging progress: {current=}, {total=}")
        self.progressBar.setMaximum(total)
        self.progressBar.setValue(current)

    @Slot()
    def on_settingswindow_destroyed(self, obj = None):
        logger.debug("Settings window destroyed")
        self.settingswindow = None
