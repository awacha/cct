import traceback
from taurus.external.qt import QtWidgets, QtGui

from .resultviewwindow import ResultViewWindow
from ...sardanagui.plotcurve import PlotCurve

import logging
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

class ShowCurveWindow(ResultViewWindow):
    plotCurve: PlotCurve = None

    def setupUi(self, Form: QtWidgets.QWidget):
        self.plotCurve = PlotCurve(self)
        layout = QtWidgets.QVBoxLayout()
        layout.setContentsMargins(0, 0, 0, 0, )
        self.setLayout(layout)
        layout.addWidget(self.plotCurve)
        self.onResultItemChanged('', '')  # at present only full redraw is supported
        self.plotCurve.setPixelMode(False)
        self.plotCurve.setShowErrorBars(False)
        self.plotCurve.setMinimumSize(600, 600)
        self.setWindowIcon(QtGui.QIcon(':/icons/saxscurve.svg'))

    def onResultItemChanged(self, samplename: str, distancekey: str):
        # full redraw every time. This is suboptimal, ToDo
        self.plotCurve.clear()
        for sn, dist in self.resultitems:
            if not sn:  # then dist is the FSN!
                try:
                    curve = self.project.h5io.readCurveFSN(dist, None, None)
                    label = f'#{dist}'
                except Exception:
                    logger.error(f'Exception {traceback.format_exc()}')
                    continue
            else:
                curve = self.project.settings.h5io.readCurve(self.project.settings.h5io.getH5Path(sn, dist))
                label = f'{sn} @ {dist} mm'
            self.plotCurve.addCurve(curve, label=label)
        self.plotCurve.replot()
        self.setWindowTitle(f'Scattering curves')

    def clear(self):
        self.plotCurve.clear()
