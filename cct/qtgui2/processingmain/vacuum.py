from taurus.external.qt import QtWidgets
import datetime
import dateutil.parser

from .resultviewwindow import ResultViewWindow
from ...core2.processing.calculations.resultsentry import SampleDistanceEntry
from .vacuum_ui import Ui_Form
from matplotlib.figure import Figure
from matplotlib.backends.backend_qt5agg import NavigationToolbar2QT, FigureCanvasQTAgg
from matplotlib.axes import Axes

import logging

logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)


class VacuumWindow(ResultViewWindow, Ui_Form):
    figure: Figure
    canvas: FigureCanvasQTAgg
    figtoolbar: NavigationToolbar2QT
    axesvacuum: Axes
    axesflux: Axes

    def setupUi(self, Form):
        super().setupUi(self)
        self.figure = Figure(constrained_layout=True, figsize=(4, 3))
        self.canvas = FigureCanvasQTAgg(self.figure)
        self.figtoolbar = NavigationToolbar2QT(self.canvas, self)
        self.axesvacuum = self.figure.add_subplot(self.figure.add_gridspec(1, 1)[:, :])
        self.axesflux = self.axesvacuum.twinx()
        layout = QtWidgets.QVBoxLayout()
        self.setLayout(layout)
        layout.addWidget(self.figtoolbar)
        layout.addWidget(self.canvas, stretch=1)
        self.onResultItemChanged("", "")

    def onResultItemChanged(self, samplename: str, distancekey: str):
        dates = []
        flux = []
        vacuum = []
        for samplename, distancekey in self.resultitems:
            sde: SampleDistanceEntry = self.project.results.get(samplename, distancekey)
            logger.debug(f"Reading metadata series from {samplename=}, {distancekey=}")
            try:
                f = sde.getMetaDataSeries(sde.MetaDataField.Flux)
                d = sde.getMetaDataSeries(sde.MetaDataField.Date)
                v = sde.getMetaDataSeries(sde.MetaDataField.Vacuum)
            except KeyError as ke:
                # if any of the three datasets does not exist in the HDF5 file
                logger.warning(
                    f"KeyError while reading metadata series from {samplename=}, {distancekey=}: {ke.args[0]}"
                )
                continue
            logger.debug(f"{d=}, {f=}, {v=}")
            dates.extend(d)
            flux.extend(f)
            vacuum.extend(v)
        dates = [
            d if isinstance(d, datetime.datetime) else dateutil.parser.parse(d)
            for d in dates
        ]
        logger.debug(str(dates))
        self.axesvacuum.clear()
        self.axesvacuum.plot(
            dates, [v[0] if isinstance(v, tuple) else v for v in vacuum], "bo"
        )
        self.axesflux.plot(
            dates, [f[0] if isinstance(f, tuple) else f for f in flux], "rs"
        )
        self.axesvacuum.set_xlabel("Date of measurement")
        self.axesvacuum.set_ylabel("Vacuum (mbar)", color="b")
        self.axesflux.set_ylabel("Flux (photons/sec)", color="r")
        self.axesvacuum.set_xlim(min(dates), max(dates))
        self.axesvacuum.xaxis.set_tick_params(labelrotation=90)
        self.axesvacuum.yaxis.set_tick_params(colors="b")
        self.axesflux.yaxis.set_tick_params(colors="r")
        self.canvas.draw_idle()
