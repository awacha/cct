from typing import Optional
import logging
from taurus.external.qt import QtWidgets
from .currentsample_ui import Ui_Frame

from ....core2.instrument.instrument import Instrument

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)


class CurrentSampleIndicator(QtWidgets.QFrame, Ui_Frame):
    def __init__(self, parent: QtWidgets.QWidget | None = ...) -> None:
        super().__init__(parent)
        self.setupUi(self)

    def setupUi(self, Form):
        super().setupUi(self)
        Instrument.instance().samplestore.currentSampleChanged.connect(
            self.onSampleStoreCurrentSampleChanged)
        Instrument.instance().sardana.environmentChanged.connect(
            self.onSardanaEnvironmentChanged)
        self.onSampleStoreCurrentSampleChanged(
            Instrument.instance().samplestore.currentSampleName())
        try:
            self.onSardanaEnvironmentChanged(
                'SampleInfo', Instrument.instance().sardana.getEnvironment('SampleInfo'))
        except AttributeError:
            # happens when the Sardana component has not yet been started.
            pass

    def onSampleStoreCurrentSampleChanged(self, sample: Optional[str]):
        logger.debug(f'onSampleStoreCurrentSampleChanged({type(sample)}: {sample})')
        if sample is None:
            self.currentSampleCCTLabel.setText('-- None --')
        else:
            self.currentSampleCCTLabel.setText(sample)

    def onSardanaEnvironmentChanged(self, name, value):
        if name == 'SampleInfo':
            samplename = value['currentsample']
            if samplename is None:
                self.currentSampleSardanaLabel.setText('-- None --')
            else:
                self.currentSampleSardanaLabel.setText(samplename)
