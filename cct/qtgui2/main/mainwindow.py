import logging
import subprocess
import sys
import time
from typing import Dict, Optional, Union, Type

import taurus
from taurus.external.qt import QtWidgets, QtGui, QtCore
from taurus.external.qt.QtCore import Slot
from sardana.taurus.qt.qtcore.tango.sardana.macroserver import QDoor
#from sardana.taurus.qt.qtgui.extra_sardana.qtspock import QtSpockWidget


from ...sardanagui.plotscan import PlotScanSardana

from ...sardanagui.accountingindicator import AccountingIndicator
from ...sardanagui.beamstopindicator import BeamstopIndicator
from ...sardanagui.vacuumindicator import VacuumIndicator
from ...sardanagui.lastfsnindicator import LastFSNIndicator
from ...sardanagui.currentsampleindicator import CurrentSampleIndicator
from ...sardanagui.shutterindicator import ShutterIndicator
from .mainwindow_ui import Ui_MainWindow
from .tangodevicestatus import TangoDeviceStatus
from ...sardanagui.motorview import MotorView
#from ..listing.headerview import HeaderView
# from ..measurement.monitor import MonitorMeasurement
# from ..measurement.simpleexposure.simpleexposure import SimpleExposure
# from ..measurement.transmission import TransmissionUi
# from ..setup.calibrants.calibrants import Calibrants
from ...sardanagui.calibration import Calibration
from ...sardanagui.centering import Centering
from ...sardanagui.geometryeditor import GeometryEditor
from ...sardanagui.projectmanager import ProjectManagerUI
from ...sardanagui.sampleeditor import SampleEditor
from ...sardanagui.capillarysizer import CapillarySizer
from ...sardanagui.maskeditor import MaskEditor
from ...sardanagui.samplepositionchecker import SamplePositionChecker
from ...sardanagui.sensors import SensorsWindow
# from ..utils.anisotropy import AnisotropyEvaluator
from ...sardanagui.imageandcurvemonitor import ImageMonitor, CurveMonitor, TwinImageAndCurveMonitor
from ...sardanagui.plotcurve import PlotCurve
from ...sardanagui.plotimage import PlotImage
from ...sardanagui.models.tangodevicemanager import TangoDeviceManager
from ...sardanagui.flagui import FlagUI
from ...sardanagui.scan import ScanMeasurement
from ...sardanagui.scanviewer import ScanViewer
from ...sardanagui.sardanaguibase import SardanaGUIBase
from ...sardanagui.sequencewizard import SequenceWizard
from ... import _version
from ...core2.instrument.instrument import Instrument
from ...tangogui import TangoDeviceUI

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)


class MainWindow(QtWidgets.QMainWindow, Ui_MainWindow):
    instrument: Instrument
    plotimage: PlotImage
    plotcurve: PlotCurve
    lastfsnindicator: LastFSNIndicator
    accountingindicator: AccountingIndicator
    beamstopindicator: BeamstopIndicator
    currentsampleindicator: CurrentSampleIndicator
    shutterindicator: ShutterIndicator
    tangodevicamanagermodel: TangoDeviceManager
    vacuumindicator: VacuumIndicator
    flagindicator: FlagUI

    _action2windowclass = {
        'actionMotors': MotorView,
        'actionSample_editor': SampleEditor,
        'actionGeometry_editor': GeometryEditor,
        'actionCalibration': Calibration,
        'actionSamplePositionChecker': SamplePositionChecker,
        'actionView_scans': ScanViewer,
        'actionMask_editor': MaskEditor,
        'actionProject_management': ProjectManagerUI,
        'actionCapillary_sizing': CapillarySizer,
        #        'actionSingle_exposure': SimpleExposure,
#        'actionView_images_and_curves': HeaderView,
#        'actionData_reduction': HeaderView,
        'actionScan': ScanMeasurement,
        #        'actionTransmission': TransmissionUi,
        'actionSensors': SensorsWindow,
        #        'actionBeam_Monitor': MonitorMeasurement,
        'actionImage_monitor': ImageMonitor,
        'actionCurve_monitor': CurveMonitor,
        'actionBeam_centering': Centering,
#        'actionScan_wizard': '',
        'actionMeasurement_sequence_wizard': SequenceWizard,
        'actionTwinImageAndCurveMonitor': TwinImageAndCurveMonitor,

    }
    _windows: Dict[str, QtWidgets.QWidget]
#    _devicestatuswidgets: List[DeviceStatus]
    _tangodevicestatuswidgets: Dict[str, TangoDeviceStatus]
    _tangodeviceuiactions: Dict[QtWidgets.QAction, Type[TangoDeviceUI]]
    door: QDoor

    def __init__(self, **kwargs):
        logger.debug(f'MainWindow.__init__({kwargs})')
        self.door = taurus.Device(kwargs.pop('door'))
        logger.debug('We have a door instance')
        self._devicestatuswidgets = []
        self._tangodevicestatuswidgets = {}
        logger.debug('Calling superclass __init__')
        super().__init__(parent=kwargs['parent']
                         if 'parent' in kwargs else None)
        logger.debug('Getting a tango device manager')
        self.tangodevicamanagermodel = TangoDeviceManager(
            parent=self, door=self.door.name)
        logger.debug('Getting the instrument')
        self.instrument = kwargs['instrument']
        self.instrument.shutdown.connect(self.onInstrumentShutdown)
#        self.tangodevicamanagermodel.devicesChanged.connect(
#            self.onTangoDevicesChanged)
        self._windows = {}
        self._tangodeviceuiactions = {}
        logger.debug('Running setupUi')
        self.setupUi(self)
        logger.debug('__init__ of MainWindow done.')

    def insertTangoDeviceActions(self):
        """Insert actions in the toolbar for opening tango device UIs"""
        if self._tangodeviceuiactions:
            raise ValueError(
                'Tango device UI actions have already been added to the toolbar')
        firstseparator = [a for a in self.toolBar.actions()
                          if a.isSeparator()][0]
        self.toolBar.insertSeparator(firstseparator)
        for sc in TangoDeviceUI.__subclasses__():
            icon = QtGui.QIcon(sc.iconresource)
            action = QtWidgets.QAction(icon, '', parent=self)
            self.toolBar.insertAction(firstseparator, action)
            self._tangodeviceuiactions[action] = sc
            action.triggered.connect(self.onTangoDeviceUIActionTriggered)

    @Slot()
    def onTangoDeviceUIActionTriggered(self):
        action = self.sender()
        try:
            guiclass: Type[TangoDeviceUI] = self._tangodeviceuiactions[action]
        except KeyError:
            logger.error(
                f'Cannot find tango device UI class for action {action}.')
            return
        devs = guiclass.findAvailableDevices()
        if not devs:
            QtWidgets.QMessageBox.critical(self, 'No devices found',
                                           f'No devices are exported with server class {guiclass.deviceserverclass}')
            return
        if len(devs) > 2:
            logger.warning(
                f'More than two devices available for class {guiclass.deviceserverclass}: {devs}')
        subprocess.Popen([sys.argv[0], 'tangogui', devs[0]])

    def setupUi(self, Form):
        super().setupUi(Form)
        self.tangoDeviceTreeView.setModel(self.tangodevicamanagermodel)
        for icol in range(self.tangodevicamanagermodel.columnCount()):
            self.tangoDeviceTreeView.resizeColumnToContents(icol)
        self.actionQuit.triggered.connect(self.close)
#        self.plotimage = PlotImage(self.patternTab)
#        self.patternTab.setLayout(QtWidgets.QVBoxLayout())
#        self.patternTab.layout().addWidget(self.plotimage)
#        self.plotcurve = PlotCurve(self.curveTab)
#        self.curveTab.setLayout(QtWidgets.QVBoxLayout())
#        self.curveTab.layout().addWidget(self.plotcurve)
        for actionname, windowclass in self._action2windowclass.items():
            action = getattr(self, actionname)
            assert isinstance(action, QtGui.QAction)
            action.triggered.connect(self.onActionTriggered)
        self.setWindowTitle(
            f'Credo Control Tool v{_version.version} User: {self.instrument.auth.username()}')
        self.accountingindicator = AccountingIndicator(
            parent=self.centralwidget, door=self.door.name)
        self.lastfsnindicator = LastFSNIndicator(
            parent=self.centralwidget, door=self.door.name)
        self.vacuumindicator = VacuumIndicator(
            parent=self.centralwidget, door=self.door.name)
        self.beamstopindicator = BeamstopIndicator(
            parent=self.centralwidget, door=self.door.name)
        self.shutterindicator = ShutterIndicator(
            parent=self.centralwidget, door=self.door.name)
        self.currentsampleindicator = CurrentSampleIndicator(
            parent=self.centralwidget, door=self.door.name)
        self.flagindicator = FlagUI(
            parent=self.centralwidget, door=self.door.name)
        for indicator in [
                self.accountingindicator, self.lastfsnindicator,
                self.beamstopindicator, self.shutterindicator,
                self.currentsampleindicator, self.flagindicator, 1, self.vacuumindicator]:
            if isinstance(indicator, int):
                self.indicatorHorizontalLayout.addStretch(indicator)
            else:
                frame = QtWidgets.QFrame(parent=self.centralwidget)
#                frame.setContentsMargins(0, 0, 0, 0)
                frame.setFrameShape(QtWidgets.QFrame.Shape.StyledPanel)
                frame.setLayout(QtWidgets.QHBoxLayout())
                frame.layout().setContentsMargins(0, 0, 0, 0)
                indicator.setParent(frame)
                frame.layout().addWidget(indicator)
                self.indicatorHorizontalLayout.addWidget(frame)

        self.actionSave_settings.triggered.connect(self.saveSettings)
#        self.onTangoDevicesChanged()
        self.insertTangoDeviceActions()
#        self.spockWidget = QtSpockWidget(use_model_from_profile=True)
#        self.terminalTab.setLayout(QtWidgets.QVBoxLayout())
#        self.terminalTab.layout().addWidget(self.spockWidget)
#        self.spockWidget.start_kernel()

        self.plotscan = PlotScanSardana(parent=self.centralWidget(), door=self.door.name)
        self.plotScanTab.setLayout(QtWidgets.QVBoxLayout())
        self.plotScanTab.layout().addWidget(self.plotscan)
        self.plotscan.setWaitForNewScans(True)

    @Slot()
    def onTangoDevicesChanged(self):
        for devname, (device, attrname) in self.instrument.tangodevicemanager.devicesAndAttributes().items():
            assert isinstance(device, taurus.core.tango.TangoDevice)
            if devname not in self._tangodevicestatuswidgets:
                self._tangodevicestatuswidgets[devname] = TangoDeviceStatus(
                    parent=self)
                self._tangodevicestatuswidgets[devname].setModel(
                    f'{devname}', attrname=attrname)
                self.deviceStatusBarLayout.insertWidget(self.deviceStatusBarLayout.count() - 1,
                                                        self._tangodevicestatuswidgets[devname])
                self._tangodevicestatuswidgets[devname].show()
        for devname in self._tangodevicestatuswidgets:
            if devname not in self.instrument.tangodevicemanager:
                self._tangodevicestatuswidgets[devname].destroy()
                self._tangodevicestatuswidgets[devname].deleteLater()
                del self._tangodevicestatuswidgets[devname]

    @Slot()
    def saveSettings(self):
        self.instrument.saveConfig()

    @Slot()
    def onInstrumentShutdown(self):
        logger.debug('Instrument shutdown signal received.')
        self.close()

    @Slot(bool)
    def onActionTriggered(self, toggled: bool):
        action = self.sender()
        windowclass = self._action2windowclass[action.objectName()]
        logger.debug(f'Window class: {windowclass}')
        assert issubclass(windowclass, QtWidgets.QWidget)
        try:
            win = self._windows[windowclass.__name__]
            win.show()
            if win.isMinimized():
                win.showNormal()
            win.raise_()
            win.setFocus()
        except KeyError:
            assert issubclass(windowclass, QtWidgets.QWidget)
            self.addSubWindow(windowclass, singleton=True)

    def addSubWindow(self, windowclass, singleton: bool = True):
        if singleton and windowclass.__name__ in self._windows:
            raise ValueError(
                f'Window class {windowclass} has already an active instance.')
        if not singleton:
            objectname = windowclass.__name__ + str(time.monotonic())
        else:
            objectname = windowclass.__name__
        if issubclass(windowclass, (SardanaGUIBase, SequenceWizard)):
            w = windowclass(parent=None, mainwindow=self,
                            door=self.door.name)
        else:
            w = windowclass(
                parent=None, instrument=self.instrument, mainwindow=self)
        w.setAttribute(QtCore.Qt.WidgetAttribute.WA_DeleteOnClose, True)
        w.destroyed.connect(self.onWindowDestroyed)
        w.setObjectName(objectname)
        self._windows[objectname] = w
        w.show()
        w.raise_()
        w.setFocus()
        return w

    @Slot(QtCore.QObject)
    def onWindowDestroyed(self, window: QtWidgets.QWidget):
        logger.debug(
            f'Window with object name {window.objectName()} destroyed.')
        del self._windows[window.objectName()]

    def closeEvent(self, event: QtGui.QCloseEvent) -> None:
        logger.debug('closeEvent in Main window')
#        self.spockWidget.shutdown_kernel()

        if not self.instrument.stopping:
            logger.debug('Instrument is running.')
            event.ignore()
            result = QtWidgets.QMessageBox.question(
                self, 'Confirm quit', 'Do you really want to quit CCT?',
                QtWidgets.QMessageBox.StandardButton.Yes | QtWidgets.QMessageBox.StandardButton.No,
                QtWidgets.QMessageBox.StandardButton.No)
            if result == QtWidgets.QMessageBox.StandardButton.Yes:
                logger.debug('Stopping instrument')
                self.instrument.stop()
        if not self.instrument.running:
            for name in self._windows:
                self._windows[name].close()
            logger.debug('All windows closed, accepting close event.')
            self.instrument.cfg.save()
            logger.info('Detaching GUI logger')
            for name, tds in self._tangodevicestatuswidgets.items():
                tds.deleteLater()
                tds.destroy()
            event.accept()
        logger.debug('Exiting closeEvent')

#    def showPattern(self, exposure: Exposure, keepzoom: Optional[bool] = None, title: Optional[str] = None):
#        if title is None:
#            title = f'{exposure.header.prefix}/{exposure.header.fsn}: '\
#                f'{exposure.header.title} @ {exposure.header.distance[0]:.2f} mm'
#        self.plotimage.setExposure(exposure, keepzoom, title)
#        self.tabWidget.setCurrentWidget(self.patternTab)

#    def showCurve(self, curve: Union[Curve, Exposure]):
#        if isinstance(curve, Exposure):
#            title = f'{curve.header.prefix}/{curve.header.fsn}: '\
#                f'{curve.header.title} @ {curve.header.distance[0]:.2f} mm'
#            curve = curve.radial_average()
#        else:
#            title = None
#        self.plotcurve.clear()
#        self.plotcurve.addCurve(curve, label=title)
#        self.plotcurve.replot()
#        self.tabWidget.setCurrentWidget(self.curveTab)
