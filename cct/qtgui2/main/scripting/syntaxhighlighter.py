from taurus.external.qt import QtGui, QtCore
import re
from typing import List, Tuple, Pattern

from ....core2.commands import Command


class ScriptSyntaxHighlighter(QtGui.QSyntaxHighlighter):
    formats: List[Tuple[List[Pattern], QtGui.QTextCharFormat]] = [
    ]

    def __init__(self, textdocument: QtGui.QTextDocument):
        super().__init__(textdocument)
        # character format for commands
        f = QtGui.QTextCharFormat()
        f.setFontWeight(QtGui.QFont.Weight.Bold)
        f.setForeground(QtCore.Qt.GlobalColor.darkMagenta)
        self.formats.append(([re.compile(r'\b' + c.name + r'\b') for c in Command.subclasses() if isinstance(c.name, str)], f))
        # character format for comments
        f = QtGui.QTextCharFormat()
        f.setForeground(QtCore.Qt.GlobalColor.lightGray)
        self.formats.append(([re.compile('#.*$')], f))
        # character format for labels
        f = QtGui.QTextCharFormat()
        f.setForeground(QtCore.Qt.GlobalColor.blue)
        self.formats.append(([re.compile(r'^\s*@.*$')], f))
        # character format for strings
        f = QtGui.QTextCharFormat()
        f.setForeground(QtCore.Qt.GlobalColor.darkCyan)
        f.setFontItalic(True)
        self.formats.append(([re.compile("(?P<delimiter>[\"\']).*?(?P=delimiter)")], f))

    def highlightBlock(self, text: str) -> None:
        for regexes, textcharformat in self.formats:
            for regex in regexes:
                for m in regex.finditer(text):
                    self.setFormat(m.start(), m.end() - m.start(), textcharformat)
