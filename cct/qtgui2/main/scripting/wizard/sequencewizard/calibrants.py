from taurus.external.qt import QtWidgets, QtCore

from .calibrants_ui import Ui_WizardPage
from ......core2.instrument.instrument import Instrument


class CalibrantsPage(QtWidgets.QWizardPage, Ui_WizardPage):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.setupUi(self)

    def setupUi(self, WizardPage):
        super().setupUi(WizardPage)
        instrument = Instrument.instance()
        for combobox, regexp, fieldname in [
            (self.darkComboBox, "Dark", 'darkSample*'),
            (self.emptyComboBox, "Empty beam", 'emptySample*'),
            (self.intensityComboBox, "normalization sample", 'absintSample*'),
            (self.qComboBox, "calibration sample", 'qCalibrantSample*'),
        ]:
            sortmodel = QtCore.QSortFilterProxyModel(self)
            sortmodel.setSourceModel(instrument.samplestore)
            sortmodel.setFilterKeyColumn(8)
            sortmodel.setFilterRegularExpression(QtCore.QRegularExpression(regexp))
            sortmodel.sort(0, QtCore.Qt.SortOrder.AscendingOrder)
            combobox.setModel(sortmodel)
            self.registerField(fieldname, combobox)
            combobox.setCurrentIndex(-1)
        for spinbox, fieldname in [
            (self.darkExpTimeDoubleSpinBox, 'darkTime'),
            (self.emptyExpTimeDoubleSpinBox, 'emptyTime'),
            (self.intensityExpTimeDoubleSpinBox, 'absintTime'),
            (self.qExpTimeDoubleSpinBox, 'qCalibrantTime'),
        ]:
            self.registerField(fieldname, spinbox, 'value', spinbox.valueChanged)
        self.initializePage()

    def darkSample(self) -> str:
        return self.darkComboBox.currentText()

    def emptySample(self) -> str:
        return self.emptyComboBox.currentText()

    def intensitySample(self) -> str:
        return self.intensityComboBox.currentText()

    def qSample(self) -> str:
        return self.qComboBox.currentText()