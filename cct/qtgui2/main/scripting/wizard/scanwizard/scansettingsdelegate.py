from typing import Union

from taurus.external.qt import QtWidgets, QtCore
import logging

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)


class ScanSettingsDelegate(QtWidgets.QStyledItemDelegate):
    """A treeview edit delegate for editing the settings of a relative scan

    The current value is taken from the EditRole: (steps, half width, counting time)
    """
    def createEditor(self, parent: QtWidgets.QWidget, option: QtWidgets.QStyleOptionViewItem,
                     index: QtCore.QModelIndex) -> QtWidgets.QWidget:
        logger.debug(f'Creating editor for index {index.row()=}, {index.column()=}')
        w = QtWidgets.QWidget(parent)
        layout = QtWidgets.QHBoxLayout(w)
        layout.setContentsMargins(0, 0, 0, 0)
        layout.setSpacing(0)
        w.setLayout(layout)
        npointsspinbox = QtWidgets.QSpinBox(w)
        npointsspinbox.setObjectName('npointsSpinBox')
        npointsspinbox.setRange(2, 10000)
        npointsspinbox.setAutoFillBackground(True)
        layout.addWidget(npointsspinbox, 1)
        npointsspinbox.setFrame(False)
        label1 = QtWidgets.QLabel(text='points, \xb1', parent=w)
        label1.setAutoFillBackground(True)
        layout.addWidget(label1)
        hwdoublespinbox = QtWidgets.QDoubleSpinBox(w)
        hwdoublespinbox.setObjectName('hwDoubleSpinBox')
        hwdoublespinbox.setRange(0.001, 10000)
        hwdoublespinbox.setDecimals(3)
        hwdoublespinbox.setFrame(False)
        hwdoublespinbox.setAutoFillBackground(True)
        layout.addWidget(hwdoublespinbox, 1)
        label2 = QtWidgets.QLabel(text=' half width, ', parent=w)
        label2.setAutoFillBackground(True)
        layout.addWidget(label2)
        ctdoublespinbox = QtWidgets.QDoubleSpinBox(w)
        ctdoublespinbox.setObjectName('ctDoubleSpinBox')
        ctdoublespinbox.setRange(0.001, 10000)
        ctdoublespinbox.setDecimals(3)
        ctdoublespinbox.setFrame(False)
        ctdoublespinbox.setSuffix(' secs')
        ctdoublespinbox.setAutoFillBackground(True)
        layout.addWidget(ctdoublespinbox, 1)
        npointsspinbox.show()
        hwdoublespinbox.show()
        ctdoublespinbox.show()
        label2.show()
        label1.show()
        w.setAutoFillBackground(True)
        return w

    def sizeHint(self, option: QtWidgets.QStyleOptionViewItem,
                 index: Union[QtCore.QModelIndex, QtCore.QPersistentModelIndex]) -> QtCore.QSize:
        return QtCore.QSize(200, 0)

    def updateEditorGeometry(self, editor: QtWidgets.QWidget, option: QtWidgets.QStyleOptionViewItem,
                             index: QtCore.QModelIndex) -> None:
        logger.debug(f'Updating editor geometry to {option.rect}')
        editor.setGeometry(option.rect)

    def setEditorData(self, editor: QtWidgets.QComboBox, index: QtCore.QModelIndex) -> None:
        assert isinstance(editor, QtWidgets.QWidget)
        npointsspinbox = editor.findChild(QtWidgets.QSpinBox, 'npointsSpinBox')
        assert isinstance(npointsspinbox, QtWidgets.QSpinBox)
        hwdoublespinbox = editor.findChild(QtWidgets.QDoubleSpinBox, 'hwDoubleSpinBox')
        assert isinstance(hwdoublespinbox, QtWidgets.QDoubleSpinBox)
        ctdoublespinbox = editor.findChild(QtWidgets.QDoubleSpinBox, 'ctDoubleSpinBox')
        assert isinstance(ctdoublespinbox, QtWidgets.QDoubleSpinBox)
        npoints, hw, ct = index.data(QtCore.Qt.ItemDataRole.EditRole)
        logger.debug(f'Setting editor data to {npoints, hw, ct=}')
        npointsspinbox.setValue(npoints)
        hwdoublespinbox.setValue(hw)
        ctdoublespinbox.setValue(ct)

    def setModelData(self, editor: QtWidgets.QComboBox, model: QtCore.QAbstractItemModel,
                     index: QtCore.QModelIndex) -> None:
        assert isinstance(editor, QtWidgets.QWidget)
        npointsspinbox = editor.findChild(QtWidgets.QSpinBox, 'npointsSpinBox')
        assert isinstance(npointsspinbox, QtWidgets.QSpinBox)
        hwdoublespinbox = editor.findChild(QtWidgets.QDoubleSpinBox, 'hwDoubleSpinBox')
        assert isinstance(hwdoublespinbox, QtWidgets.QDoubleSpinBox)
        ctdoublespinbox = editor.findChild(QtWidgets.QDoubleSpinBox, 'ctDoubleSpinBox')
        assert isinstance(ctdoublespinbox, QtWidgets.QDoubleSpinBox)
        model.setData(index, (npointsspinbox.value(), hwdoublespinbox.value(), ctdoublespinbox.value()), role=QtCore.Qt.ItemDataRole.EditRole)
