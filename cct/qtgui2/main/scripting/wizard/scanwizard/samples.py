import pickle
from typing import Tuple, List, Iterable, Any
import logging

from taurus.external.qt import QtWidgets, QtCore
from taurus.external.qt.QtCore import Slot

from .samplescanmodel import SampleScanModel, SampleType, SampleScanEntry
from .scansettingsdelegate import ScanSettingsDelegate
from .samples_ui import Ui_WizardPage
from ......core2.instrument.instrument import Instrument
from .....utils.blocksignalscontextmanager import SignalsBlocked
from .....utils.comboboxdelegate import ComboBoxDelegate

logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)


class SamplesPage(QtWidgets.QWizardPage, Ui_WizardPage):
    samplescanmodel: SampleScanModel
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.setupUi(self)

    def setupUi(self, WizardPage):
        super().setupUi(WizardPage)
        instrument = Instrument.instance()
        self.sampleListView.setModel(instrument.samplestore.sortedmodel)
        self.sampleListView.setSelectionModel(QtCore.QItemSelectionModel(self.sampleListView.model(), self.sampleListView))
        self.samplescanmodel = SampleScanModel(parent=self)
        self.exposureTreeView.setModel(self.samplescanmodel)
        self.exposureTreeView.setSelectionModel(QtCore.QItemSelectionModel(self.exposureTreeView.model(), self.exposureTreeView))
        with SignalsBlocked(self.sampleTypeComboBox):
            self.sampleTypeComboBox.clear()
            self.sampleTypeComboBox.addItems([st.value for st in SampleType])
            self.sampleTypeComboBox.setCurrentIndex(0)
            self.sampleTypeCheckBox.setChecked(False)
        self.samplescanmodel.defaultvalues.sampletype = None
        self.samplescanmodel.defaultvalues.xscan = self.xScanCheckBox.isChecked()
        self.samplescanmodel.defaultvalues.yscan = self.yScanCheckBox.isChecked()
        self.samplescanmodel.defaultvalues.xsteps = self.xScanPointCountSpinBox.value()
        self.samplescanmodel.defaultvalues.ysteps = self.yScanPointCountSpinBox.value()
        self.samplescanmodel.defaultvalues.xhalfwidth = self.xHalfWidthDoubleSpinBox.value()
        self.samplescanmodel.defaultvalues.yhalfwidth = self.yHalfWidthDoubleSpinBox.value()
        self.samplescanmodel.defaultvalues.xcountingtime = self.xCountingTimeDoubleSpinBox.value()
        self.samplescanmodel.defaultvalues.ycountingtime = self.yCountingTimeDoubleSpinBox.value()
        self.exposureTreeView.setItemDelegateForColumn(1, ComboBoxDelegate(self.exposureTreeView))
        self.exposureTreeView.setItemDelegateForColumn(2, ScanSettingsDelegate(self.exposureTreeView))
        self.exposureTreeView.setItemDelegateForColumn(3, ScanSettingsDelegate(self.exposureTreeView))
        self.exposureTreeView.setColumnWidth(2, 200)
        self.exposureTreeView.setColumnWidth(3, 200)

    @Slot(bool)
    def on_sampleTypeCheckBox_toggled(self, checked: bool):
        self.samplescanmodel.defaultvalues.sampletype = None if not checked else SampleType.Flat

    @Slot(str)
    def on_sampleTypeComboBox_currentTextChanged(self, text: str):
        self.samplescanmodel.defaultvalues.sampletype = SampleType(text)

    @Slot(bool)
    def on_xScanCheckBox_toggled(self, checked: bool):
        self.samplescanmodel.defaultvalues.xscan = checked

    @Slot(int)
    def on_xScanPointCountSpinBox_valueChanged(self, value: int):
        self.samplescanmodel.defaultvalues.xsteps = value
        with SignalsBlocked(self.xStepSizeDoubleSpinBox):
            self.xStepSizeDoubleSpinBox.setValue(self.samplescanmodel.defaultvalues.xstepsize)

    @Slot(float)
    def on_xHalfWidthDoubleSpinBox_valueChanged(self, value: float):
        self.samplescanmodel.defaultvalues.xhalfwidth = value
        with SignalsBlocked(self.xStepSizeDoubleSpinBox):
            self.xStepSizeDoubleSpinBox.setValue(self.samplescanmodel.defaultvalues.xstepsize)

    @Slot(float)
    def on_xStepSizeDoubleSpinBox_valueChanged(self, value: float):
        self.samplescanmodel.defaultvalues.xstepsize = value
        self.xScanPointCountSpinBox.setValue(self.samplescanmodel.defaultvalues.xsteps)

    @Slot(float)
    def on_xCountingTimeDoubleSpinBox_valueChanged(self, value: float):
        self.samplescanmodel.defaultvalues.xcountingtime = value

    @Slot(bool)
    def on_yScanCheckBox_toggled(self, checked: bool):
        self.samplescanmodel.defaultvalues.yscan = checked

    @Slot(int)
    def on_yScanPointCountSpinBox_valueChanged(self, value: int):
        self.samplescanmodel.defaultvalues.ysteps = value
        with SignalsBlocked(self.yStepSizeDoubleSpinBox):
            self.yStepSizeDoubleSpinBox.setValue(self.samplescanmodel.defaultvalues.ystepsize)

    @Slot(float)
    def on_yHalfWidthDoubleSpinBox_valueChanged(self, value: float):
        self.samplescanmodel.defaultvalues.yhalfwidth = value
        with SignalsBlocked(self.yStepSizeDoubleSpinBox):
            self.yStepSizeDoubleSpinBox.setValue(self.samplescanmodel.defaultvalues.ystepsize)

    @Slot(float)
    def on_yStepSizeDoubleSpinBox_valueChanged(self, value: float):
        self.samplescanmodel.defaultvalues.ystepsize = value
        self.yScanPointCountSpinBox.setValue(self.samplescanmodel.defaultvalues.ysteps)

    @Slot(float)
    def on_yCountingTimeDoubleSpinBox_valueChanged(self, value: float):
        self.samplescanmodel.defaultvalues.ycountingtime = value

    @Slot()
    def on_addSampleToolButton_clicked(self):
        logger.debug(f'Adding {len(self.sampleListView.selectedIndexes())} samples')
        for index in self.sampleListView.selectedIndexes():
            samplename = index.data(QtCore.Qt.ItemDataRole.DisplayRole)
            logger.debug(f'Adding sample {samplename}')
            self.samplescanmodel.addSample(samplename)
        self.sampleListView.selectionModel().clearSelection()

    @Slot()
    def on_removeSamplesToolButton_clicked(self):
        while (indexlist := self.exposureTreeView.selectionModel().selectedRows(0)):
            self.exposureTreeView.model().removeRow(indexlist[0].row(), QtCore.QModelIndex())

    @Slot()
    def on_clearExposureListToolButton_clicked(self):
        self.exposureTreeView.model().clear()

    def samples(self) -> List[SampleScanEntry]:
        return self.samplescanmodel.samples()

