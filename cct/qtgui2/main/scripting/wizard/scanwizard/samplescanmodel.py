# coding: utf-8

import enum
import logging
import pickle
from typing import List, Any, Optional, Iterable

from taurus.external.qt import QtCore

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)


class SampleType(enum.Enum):
    Flat = 'Flat'
    HorizontalCapillary = 'Horizontal capillary'
    VerticalCapillary = 'Vertical capillary'


class SampleScanEntry:
    sampletype: Optional[SampleType] = None
    title: str
    xcountingtime: float = 0.1
    xhalfwidth: float = 1
    xsteps: int = 21
    ycountingtime: float = 0.1
    yhalfwidth: float = 1
    ysteps: int = 21
    xscan: bool = True
    yscan: bool = True

    def __init__(self, title: str, sampletype: Optional[SampleType] = None,
                 xscan: bool = True, xcountingtime: float = 0.1, xhalfwidth: float = 1, xsteps: int = 21,
                 yscan: bool = True, ycountingtime: float = 0.1, yhalfwidth: float = 1, ysteps: int = 21):
        self.sampletype = sampletype
        self.title = title
        self.xscan = xscan
        self.yscan = yscan
        self.xcountingtime = xcountingtime
        self.ycountingtime = ycountingtime
        self.xhalfwidth = xhalfwidth
        self.yhalfwidth = yhalfwidth
        self.xsteps = xsteps
        self.ysteps = ysteps

    @property
    def xstepsize(self) -> float:
        return 2 * self.xhalfwidth / (self.xsteps - 1)

    @property
    def ystepsize(self) -> float:
        return 2 * self.yhalfwidth / (self.ysteps - 1)

    @xstepsize.setter
    def xstepsize(self, value: float):
        if self.xhalfwidth > 0:
            self.xsteps = int(2 * self.xhalfwidth / value) + 1
        else:
            pass

    @ystepsize.setter
    def ystepsize(self, value: float):
        if self.yhalfwidth > 0:
            self.ysteps = int(2 * self.yhalfwidth / value) + 1
        else:
            pass


class SampleScanModel(QtCore.QAbstractItemModel):
    _data: List[SampleScanEntry]
    defaultvalues: SampleScanEntry = SampleScanEntry('__default__')

    def __init__(self, **kwargs):
        self._data = []
        super().__init__(**kwargs)

    def rowCount(self, parent: QtCore.QModelIndex = ...) -> int:
        return len(self._data)

    def columnCount(self, parent: QtCore.QModelIndex = ...) -> int:
        return 4

    def headerData(self, section: int, orientation: QtCore.Qt.Orientation, role: int = ...) -> Any:
        if (orientation == QtCore.Qt.Orientation.Horizontal) and (role == QtCore.Qt.ItemDataRole.DisplayRole):
            return ['Sample', 'Type', 'X scan', 'Y scan'][section]
        return None

    def parent(self, child: QtCore.QModelIndex) -> QtCore.QModelIndex:
        return QtCore.QModelIndex()

    def index(self, row: int, column: int, parent: QtCore.QModelIndex = ...) -> QtCore.QModelIndex:
        return self.createIndex(row, column, None)

    def flags(self, index: QtCore.QModelIndex) -> QtCore.Qt.ItemFlag:
#        logger.debug(f'{index.row(), index.column(), index.parent().isValid()=}')
        if not index.isValid():
            return QtCore.Qt.ItemFlag.ItemIsDropEnabled | QtCore.Qt.ItemFlag.ItemIsDropEnabled
        entry = self._data[index.row()]
        if index.column() == 0:
            return QtCore.Qt.ItemFlag.ItemNeverHasChildren | QtCore.Qt.ItemFlag.ItemIsEnabled | \
                   QtCore.Qt.ItemFlag.ItemIsSelectable | QtCore.Qt.ItemFlag.ItemIsDropEnabled
        elif ((index.column() == 1) and (entry.sampletype is None)) or ((index.column() == 1) and not entry.xscan) or ((index.column() == 2) and not entry.yscan):
            return QtCore.Qt.ItemFlag.ItemNeverHasChildren | QtCore.Qt.ItemFlag.ItemIsEnabled | \
                   QtCore.Qt.ItemFlag.ItemIsSelectable | QtCore.Qt.ItemFlag.ItemIsDropEnabled | \
                   QtCore.Qt.ItemFlag.ItemIsUserCheckable
        elif index.column() in [1,2,3]:
            return QtCore.Qt.ItemFlag.ItemNeverHasChildren | QtCore.Qt.ItemFlag.ItemIsEnabled | \
                   QtCore.Qt.ItemFlag.ItemIsSelectable | QtCore.Qt.ItemFlag.ItemIsDropEnabled | \
                   QtCore.Qt.ItemFlag.ItemIsUserCheckable | QtCore.Qt.ItemFlag.ItemIsEditable
        else:
            return QtCore.Qt.ItemFlag.ItemNeverHasChildren | QtCore.Qt.ItemFlag.ItemIsEnabled | \
                   QtCore.Qt.ItemFlag.ItemIsSelectable | QtCore.Qt.ItemFlag.ItemIsDropEnabled | \
                   QtCore.Qt.ItemFlag.ItemIsEditable

    def data(self, index: QtCore.QModelIndex, role: int = ...) -> Any:
        entry = self._data[index.row()]
        if (index.column() == 0) and (role == QtCore.Qt.ItemDataRole.DisplayRole):
            return entry.title
        elif (index.column() == 1) and (role == QtCore.Qt.ItemDataRole.DisplayRole):
            return entry.sampletype.value if entry.sampletype is not None else 'None'
        elif (index.column() == 1) and (role == QtCore.Qt.ItemDataRole.EditRole):
            return entry.sampletype.value if entry.sampletype is not None else None
        elif (index.column() == 1) and (role == QtCore.Qt.ItemDataRole.UserRole):
            return [st.value for st in SampleType]
        elif (index.column() == 1) and (role == QtCore.Qt.ItemDataRole.CheckStateRole):
            return QtCore.Qt.CheckState.Checked if entry.sampletype is not None else QtCore.Qt.CheckState.Unchecked
        elif (index.column() == 2) and (role == QtCore.Qt.ItemDataRole.DisplayRole):
            return f'{entry.xsteps} points, \xb1 {entry.xhalfwidth}, {entry.xcountingtime} s exposure' if entry.xscan else 'Not scanned'
        elif (index.column() == 3) and (role == QtCore.Qt.ItemDataRole.DisplayRole):
            return f'{entry.ysteps} points, \xb1 {entry.yhalfwidth}, {entry.ycountingtime} s exposure' if entry.yscan else 'Not scanned'
        elif (index.column() == 2) and (role == QtCore.Qt.ItemDataRole.EditRole):
            return entry.xsteps, entry.xhalfwidth, entry.xcountingtime
        elif (index.column() == 3) and (role == QtCore.Qt.ItemDataRole.EditRole):
            return entry.ysteps, entry.yhalfwidth, entry.ycountingtime
        elif (index.column() == 2) and (role == QtCore.Qt.ItemDataRole.CheckStateRole):
            return QtCore.Qt.CheckState.Checked if entry.xscan else QtCore.Qt.CheckState.Unchecked
        elif (index.column() == 3) and (role == QtCore.Qt.ItemDataRole.CheckStateRole):
            return QtCore.Qt.CheckState.Checked if entry.yscan else QtCore.Qt.CheckState.Unchecked
        else:
            return None

    def setData(self, index: QtCore.QModelIndex, value: Any, role: int = ...) -> bool:
        entry = self._data[index.row()]
        logger.debug(f'setData(row: {index.row()}, column: {index.column()}, value: {value} of type {type(value)}')
        if (index.column() == 1) and (role == QtCore.Qt.ItemDataRole.EditRole):
            entry.sampletype = SampleType(value) if value is not None else None
        elif (index.column() == 1) and (role == QtCore.Qt.ItemDataRole.CheckStateRole):
            entry.sampletype = None if value == QtCore.Qt.CheckState.Unchecked else SampleType.Flat
        elif (index.column() == 2) and (role == QtCore.Qt.ItemDataRole.CheckStateRole):
            entry.xscan = value == QtCore.Qt.CheckState.Checked
        elif (index.column() == 3) and (role == QtCore.Qt.ItemDataRole.CheckStateRole):
            entry.yscan = value == QtCore.Qt.CheckState.Checked
        elif (index.column() == 2) and (role == QtCore.Qt.ItemDataRole.EditRole):
            entry.xsteps, entry.xhalfwidth, entry.xcountingtime = value
        elif (index.column() == 3) and (role == QtCore.Qt.ItemDataRole.EditRole):
            entry.ysteps, entry.yhalfwidth, entry.ycountingtime = value
        else:
            return False
        self.dataChanged.emit(index, index)
        return True

    def removeRows(self, row: int, count: int, parent: QtCore.QModelIndex = ...) -> bool:
        self.beginRemoveRows(parent, row, row + count - 1)
        self._data = self._data[:row] + self._data[row + count:]
        self.endRemoveRows()
        return True

    def removeRow(self, row: int, parent: QtCore.QModelIndex = ...) -> bool:
        return self.removeRows(row, 1, parent)

    def dropMimeData(self, data: QtCore.QMimeData, action: QtCore.Qt.DropAction, row: int, column: int,
                     parent: QtCore.QModelIndex) -> bool:
        logger.debug(f'dropMimeData({data.formats()}, {action=}, {row=}, {column=}, {parent.isValid()=}')
        if parent.isValid():
            return False
        if row < 0:
            row = len(self._data)
        if data.hasFormat('application/x-cctsamplescanlist'):
            lis = pickle.loads(data.data('application/x-cctsamplescanlist'))
            logger.debug(f'Adding {len(lis)} samplescanlist elements')
            if not lis:
                return False
            if (not isinstance(lis, list)) or not all([isinstance(x, SampleScanEntry) for x in lis]):
                raise ValueError('Invalid drop data')
            self.beginInsertRows(parent, row, row + len(lis) - 1)
            self._data = self._data[:row] + lis + self._data[row:]
            self.endInsertRows()
        elif data.hasFormat('application/x-cctsampledictlist'):
            lis = pickle.loads(data.data('application/x-cctsampledictlist'))
            logger.debug(f'Adding {len(lis)} samples')
            if not lis:
                return False
            self.beginInsertRows(parent, row, row + len(lis) - 1)
            self._data = self._data[:row] + [
                SampleScanEntry(
                    s["title"], sampletype=self.defaultvalues.sampletype,
                    xscan=self.defaultvalues.xscan, xcountingtime=self.defaultvalues.xcountingtime,
                    xhalfwidth=self.defaultvalues.xhalfwidth, xsteps=self.defaultvalues.xsteps,
                    yscan=self.defaultvalues.yscan, ycountingtime=self.defaultvalues.ycountingtime,
                    yhalfwidth=self.defaultvalues.yhalfwidth, ysteps=self.defaultvalues.ysteps,
                )
                for s in lis] + self._data[row:]
            self.endInsertRows()
        else:
            return False
        return True

    def supportedDragActions(self) -> QtCore.Qt.DropAction:
        return QtCore.Qt.DropAction.MoveAction

    def supportedDropActions(self) -> QtCore.Qt.DropAction:
        return QtCore.Qt.DropAction.CopyAction | QtCore.Qt.DropAction.MoveAction

    def mimeData(self, indexes: Iterable[QtCore.QModelIndex]) -> QtCore.QMimeData:
        md = QtCore.QMimeData()
        rows = sorted({i.row() for i in indexes})
        md.setData('application/x-cctsamplescanlist', pickle.dumps([self._data[r] for r in rows]))
        return md

    def clear(self):
        self.beginResetModel()
        self._data = []
        self.endResetModel()

    def mimeTypes(self) -> List[str]:
        return ['application/x-cctsequenceexposurelist', 'application/x-cctsampledictlist']

    def addSample(self, title: str):
        self.beginInsertRows(QtCore.QModelIndex(), len(self._data), len(self._data))
        self._data.append(SampleScanEntry(
            title,
            sampletype=self.defaultvalues.sampletype,
            xscan=self.defaultvalues.xscan,
            xcountingtime=self.defaultvalues.xcountingtime,
            xhalfwidth=self.defaultvalues.xhalfwidth,
            xsteps=self.defaultvalues.xsteps,
            yscan=self.defaultvalues.yscan,
            ycountingtime=self.defaultvalues.ycountingtime,
            yhalfwidth=self.defaultvalues.yhalfwidth,
            ysteps=self.defaultvalues.ysteps
        ))
        self.endInsertRows()

    def samples(self) -> List[SampleScanEntry]:
        return self._data[:]