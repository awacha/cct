from taurus.external.qt import QtWidgets
from taurus.external.qt.QtCore import Slot

from .initialization_ui import Ui_WizardPage
from ......core2.devices.detector import PilatusGain, PilatusDetector


class InitPage(QtWidgets.QWizardPage, Ui_WizardPage):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.setupUi(self)

    def setupUi(self, WizardPage):
        super().setupUi(WizardPage)
        self.xRayPowerInitComboBox.addItems(['off', 'standby', 'full'])
        self.xRayPowerInitComboBox.setCurrentIndex(1)
        self.xrayPowerFinalComboBox.addItems(['off', 'standby', 'full'])
        self.xrayPowerFinalComboBox.setCurrentIndex(1)

        self.registerField('shutterAlways', self.shutterAlwaysRadioButton)
        self.registerField('shutterNever', self.shutterNeverRadioButton)
        self.registerField('shutterOnce', self.shutterOnceRadioButton)
        self.registerField('beamstopOut', self.beamstopOutCheckBox)
        self.registerField('beamstopIn', self.beamstopInCheckBox)
        self.registerField('xRayPowerInit', self.xRayPowerInitComboBox, 'currentText')
        self.registerField('xRayPowerFinal', self.xrayPowerFinalComboBox, 'currentText')
        self.registerField('doXrayPowerInit', self.xRayPowerInitCheckBox)
        self.registerField('doXrayPowerFinal', self.xRayPowerFinalCheckBox)
