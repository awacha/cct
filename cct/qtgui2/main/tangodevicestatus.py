from taurus.qt.qtgui.base.taurusbase import _DEFAULT
from taurus.qt.qtgui.container import TaurusWidget

from .tangodevicestatus_ui import Ui_Form


class TangoDeviceStatus(TaurusWidget, Ui_Form):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.setupUi(self)

    def setupUi(self, Form):
        super().setupUi(Form)
        self.taurusLabel.setBgRole('none')
        self.taurusLabel.setAutoTrim(False)

    def setModel(self, model, *, key=_DEFAULT, attrname='Status'):
        super().setModel(model, key=key)
        self.taurusGroupBox.setModel(model)
        if '/' in model:
            self.taurusGroupBox.setTitle(model.split('/', 1)[1])
        else:
            self.taurusGroupBox.setTitle(model)
        self.taurusLed.setModel(model + '/State')
        self.taurusLabel.setModel(model + f'/{attrname}')
        self.taurusLabel.setAutoTrim(True)
