# coding: utf-8
"""UI for evaluating anisotropy"""

from .anisotropy import AnisotropyEvaluator
