import logging
from typing import List, Tuple, Optional

import numpy as np
from taurus.external.qt import QtWidgets, QtGui, QtCore
from taurus.external.qt.QtCore import Slot
from matplotlib.axes import Axes
from matplotlib.backends.backend_qt5agg import NavigationToolbar2QT, FigureCanvasQTAgg
from matplotlib.figure import Figure
from matplotlib.lines import Line2D
from matplotlib.patches import Circle, Polygon
from matplotlib.widgets import SpanSelector

from .anisotropy_ui import Ui_Form
from .slicesmodel import SectorModel
from ....sardanagui.fsnselector import FSNSelector
from ....sardanagui.h5selector import H5Selector
from ....sardanagui.plotimage import PlotImage
from ..window import WindowRequiresDevices
from ....core2.algorithms.radavg import maskforannulus, maskforsectors
from ....core2.dataclasses import Exposure

logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)


class AnisotropyEvaluator(WindowRequiresDevices, QtWidgets.QWidget, Ui_Form):
    plotimage: PlotImage
    sectorModel: SectorModel

    fig_full: Figure
    figtoolbar_full: NavigationToolbar2QT
    canvas_full: FigureCanvasQTAgg
    axes_full: Axes

    fig_azim: Figure
    figtoolbar_azim: NavigationToolbar2QT
    canvas_azim: FigureCanvasQTAgg
    axes_azim: Axes

    fig_slice: Figure
    figtoolbar_slice: NavigationToolbar2QT
    canvas_slice: FigureCanvasQTAgg
    axes_slice: Axes

    selectorGrid: QtWidgets.QGridLayout
    fsnSelector: FSNSelector
    h5Selector: H5Selector
    fullSpanSelector: SpanSelector
    azimSpanSelector: SpanSelector
    exposure: Exposure

    _qrangecircles: Tuple[Optional[Circle], Optional[Circle]]
    _slicelines: List[Line2D]
    _slicearcs: List[Polygon]

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._qrangecircles = (None, None)
        self._slicelines = []
        self._slicearcs = []
        self.setupUi(self)

    def setupUi(self, MainWindow):
        super().setupUi(MainWindow)
        self.setWindowTitle('Anisotropy Evaluator [*]')
        self.plotimage = PlotImage(self)
        self.plotimage.figure.set_size_inches(3, 2)
        self.patternVerticalLayout.addWidget(self.plotimage)
        self.plotimage.axes.set_facecolor('black')
        self.plotimage.axes.set_title('2D scattering pattern')
        self.plotimage.axesComboBox.setCurrentIndex(self.plotimage.axesComboBox.findText('q'))

        for graphname, vbox in [('full', self.radialVerticalLayout), ('azim', self.azimuthalVerticalLayout),
                                ('slice', self.sliceVerticalLayout)]:
            setattr(self, f'fig_{graphname}', Figure(figsize=(3, 2), constrained_layout=True))
            setattr(self, f'canvas_{graphname}', FigureCanvasQTAgg(getattr(self, f'fig_{graphname}')))
            setattr(self, f'figtoolbar_{graphname}', NavigationToolbar2QT(getattr(self, f'canvas_{graphname}'), self))
            getattr(self, f'canvas_{graphname}').setSizePolicy(
                QtWidgets.QSizePolicy.Policy.MinimumExpanding, QtWidgets.QSizePolicy.Policy.MinimumExpanding)
            vbox.addWidget(getattr(self, f'figtoolbar_{graphname}'), stretch=0)
            vbox.addWidget(getattr(self, f'canvas_{graphname}'), stretch=1)
            setattr(self, f'axes_{graphname}', getattr(self, f'fig_{graphname}').add_subplot(1, 1, 1))
        self.h5Selector = H5Selector(self.selectorStackedWidget)
        self.h5Selector.setSizePolicy(QtWidgets.QSizePolicy.Policy.MinimumExpanding,
                                      QtWidgets.QSizePolicy.Policy.Preferred)
        self.selectorStackedWidget.addWidget(self.h5Selector)
        self.h5Selector.datasetSelected.connect(self.onH5Selected)
        if self.instrument is not None:
            self.fsnSelector = FSNSelector(self.selectorStackedWidget)
            self.fsnSelector.setSizePolicy(QtWidgets.QSizePolicy.Policy.MinimumExpanding,
                                           QtWidgets.QSizePolicy.Policy.Preferred)
            self.selectorStackedWidget.addWidget(self.fsnSelector)
            self.fsnSelector.fsnSelected.connect(self.onFSNSelected)
            self.selectorStackedWidget.setCurrentWidget(self.fsnSelector)
            self.fsnToolButton.setChecked(True)
        else:
            self.fsnToolButton.setVisible(False)
            self.hdf5ToolButton.setVisible(False)
            self.selectorStackedWidget.setCurrentWidget(self.h5Selector)
            self.fsnSelector = None
        self.sectorModel = SectorModel()
        self.slicesTreeView.setModel(self.sectorModel)
        self.sectorModel.dataChanged.connect(self.onSectorsChanged)
        self.sectorModel.rowsRemoved.connect(self.onSectorsChanged)
        self.sectorModel.rowsInserted.connect(self.onSectorsChanged)
        self.sectorModel.modelReset.connect(self.onSectorsChanged)
        self.addSliceToolButton.clicked.connect(self.addNewSlice)
        self.removeSliceToolButton.clicked.connect(self.removeSlice)
        self.cleanSlicesToolButton.clicked.connect(self.clearSlices)
        self.nAzimSpinBox.valueChanged.connect(self.onAzimuthalCountChanged)
        self.nRadialSpinBox.valueChanged.connect(self.onRadialCountChanged)
        self.fanPushButton.clicked.connect(self.createFan)
        self.beamPosXDoubleSpinBox.valueChanged.connect(self.onBeamXChanged)
        self.beamPosYDoubleSpinBox.valueChanged.connect(self.onBeamYChanged)

    @Slot(bool)
    def on_fsnToolButton_toggled(self, checked: bool):
        if checked:
            self.selectorStackedWidget.setCurrentWidget(self.fsnSelector)

    @Slot(bool)
    def on_hdf5ToolButton_toggled(self, checked: bool):
        if checked:
            self.selectorStackedWidget.setCurrentWidget(self.h5Selector)

    @Slot(int, name='onAzimuthalCountChanged')
    def onAzimuthalCountChanged(self, value: int):
        self.onQRangeSelected(*self.fullSpanSelector.extents)

    @Slot(int, name='onRadialCountChanged')
    def onRadialCountChanged(self, value: int):
        self.onSectorsChanged()

    def enableH5Selector(self, enable: bool = True):
        self.h5Selector.setEnabled(enable)
        self.h5Selector.setVisible(enable)

    def enableFSNSelector(self, enable: bool = True):
        if self.fsnSelector is not None:
            self.fsnSelector.setEnabled(enable)
            self.fsnSelector.setVisible(enable)

    @Slot(str, int)
    def onFSNSelected(self, prefix: str, fsn: int):
        self.setExposure(self.fsnSelector.loadExposure())

    @Slot(str, str, str)
    def onH5Selected(self, filename: str, sample: str, distancekey: str):
        self.setExposure(self.h5Selector.loadExposure())

    def setExposure(self, exposure: Exposure):
        self.exposure = exposure
        self.removeCircles()
        self.removeSliceLines()
        self.axes_azim.clear()
        self.axes_slice.clear()
        self.plotimage.setExposure(exposure)
        self.redrawFullRadialAverage()
        self.sectorModel.clear()
        self.beamPosYDoubleSpinBox.blockSignals(True)
        try:
            self.beamPosYDoubleSpinBox.setValue(exposure.header.beamposrow[0])
        finally:
            self.beamPosYDoubleSpinBox.blockSignals(False)
        self.beamPosXDoubleSpinBox.blockSignals(True)
        try:
            self.beamPosXDoubleSpinBox.setValue(exposure.header.beamposcol[0])
        finally:
            self.beamPosXDoubleSpinBox.blockSignals(False)

    def redrawFullRadialAverage(self):
        self.axes_full.clear()
        rad = self.exposure.radial_average(self.nRadialSpinBox.value() if self.nRadialSpinBox.value() > 0 else None)
        self.axes_full.loglog(rad.q, rad.intensity, label='Full radial average')
        self.axes_full.set_xlabel('q (nm$^{-1}$)')
        self.axes_full.set_ylabel(r'$d\sigma/d\Omega$ (cm$^{-1}$ sr$^{-1}$)')
        self.axes_full.set_title('Circular average')
        self.axes_full.set_xlabel('q (nm$^{-1}$)')
        self.axes_full.set_ylabel(r'$d\sigma/d\Omega$ (cm$^{-1}$ sr$^{-1}$)')
        self.fullSpanSelector = SpanSelector(self.axes_full, self.onQRangeSelected, 'horizontal', interactive=True)
        self.canvas_full.draw_idle()

    def removeCircles(self):
        for c in self._qrangecircles:
            try:
                c.remove()
            except (ValueError, AttributeError, NotImplementedError):
                pass
        self._qrangecircles = (None, None)

    def removeSliceLines(self):
        for l in self._slicelines:
            try:
                l.remove()
            except Exception:
                pass
        self._slicelines = []
        for a in self._slicearcs:
            try:
                a.remove()
            except Exception:
                pass
        self._slicearcs = []

    def onQRangeSelected(self, qmin: float, qmax: float):
        self.removeCircles()
        self.removeSliceLines()
        self._qrangecircles = (
            Circle((0, 0), radius=qmin, color='white', fill=False, linestyle='--', zorder=100),
            Circle((0, 0), radius=qmax, color='white', fill=False, linestyle='--', zorder=100)
        )
        self.plotimage.axes.add_patch(self._qrangecircles[0])
        self.plotimage.axes.add_patch(self._qrangecircles[1])
        self.plotimage.canvas.draw_idle()
        ex = self.exposure
        # ex.mask_nonfinite()
        prevmask = ex.mask
        logger.debug(f'{qmin=}, {qmax=}, pixmin={ex.qtopixel(qmin)}, pixmax={ex.qtopixel(qmax)}')
        try:
            ex.mask = maskforannulus(mask=ex.mask, center_row=ex.header.beamposrow[0],
                                     center_col=ex.header.beamposcol[0], pixmin=ex.qtopixel(qmin),
                                     pixmax=ex.qtopixel(qmax))
            azimcurve = ex.azim_average(self.nAzimSpinBox.value()).sanitize()
            logger.debug(f'{ex.mask.sum()=}')
        finally:
            ex.mask = prevmask
        self.axes_azim.clear()
        self.axes_azim.plot(azimcurve.phi * 180.0 / np.pi, azimcurve.intensity, 'o', label='Azimuthal curve')
        self.azimSpanSelector = SpanSelector(self.axes_azim, self.onPhiRangeSelected, 'horizontal', interactive=True)
        self.axes_azim.set_xlabel(r'$\phi$ (°)')
        self.axes_azim.set_ylabel(r'$d\sigma/d\Omega$ (cm$^{-1}$ sr$^{-1}$)')
        self.axes_azim.set_title('Azimuthal scattering curve')
        self.canvas_azim.draw_idle()

    def onPhiRangeSelected(self, phimin: float, phimax: float):
        self.sectorModel.appendSector(0.5 * (phimin + phimax), 0.5*(phimax - phimin), True, None)

    @Slot(name='onSectorsChanged')  # QAbstractItemModel.modelReset
    @Slot(QtCore.QModelIndex, int, int, name='onSectorsChanged')  # QAbstractItemModel.rowsInserted and rowsRemoved
    @Slot(QtCore.QModelIndex, QtCore.QModelIndex, 'QVector<int>',
          name='onSectorsChanged')  # QAbstractItemModel.dataChanged
    def onSectorsChanged(self, *args, **kwargs):
        self.removeSliceLines()
        self.axes_slice.clear()
        ex = self.exposure
        originalmask = ex.mask
        qdata = ex.q()
        try:
            for si in self.sectorModel:
                ex.mask = maskforsectors(originalmask, ex.header.beamposrow[0], ex.header.beamposcol[0],
                                         si.phi0 * np.pi / 180., si.dphi * np.pi / 180, symmetric=si.symmetric)
                sliced = ex.radial_average(
                    self.nRadialSpinBox.value() if self.nRadialSpinBox.value() > 0 else None).sanitize()
                line2d = self.axes_slice.loglog(
                    sliced.q, sliced.intensity,
                    label=rf'$\phi_0={si.phi0:.2f}^\circ$, $\Delta\phi = {si.dphi:.2f}^\circ$',
                    color=si.color.name(QtGui.QColor.NameFormat.HexRgb))[0]
                self._slicelines.append(line2d)
                qmax = np.nanmax(ex.q()[0])
                ax = self.plotimage.axes.axis()
                points = np.zeros((101, 2), np.double)
                points[0, :] = 0
                phi = np.linspace(si.phi0 - si.dphi, si.phi0 + si.dphi, 100) * np.pi / 180.
                points[1:101, 0] = qmax * np.cos(phi)
                points[1:101, 1] = - qmax * np.sin(phi)
                self._slicearcs.append(
                    Polygon(points, closed=True, color=line2d.get_color(), zorder=100, alpha=0.5, linewidth=1,
                            fill=True)
                )
                if si.symmetric:
                    self._slicearcs.append(
                        Polygon(-points, closed=True, color=line2d.get_color(), zorder=100, alpha=0.5, linewidth=1,
                                fill=True))
        finally:
            ex.mask = originalmask
        #        self.axes_slice.legend(loc='best')
        self.axes_slice.set_xlabel('q (nm$^{-1}$)')
        self.axes_slice.set_ylabel(r'$d\sigma/d\Omega$ (cm$^{-1}$ sr$^{-1}$)')
        self.axes_slice.set_title('Slices')
        self.canvas_slice.draw_idle()
        for patch in self._slicearcs:
            self.plotimage.axes.add_patch(patch)
        self.plotimage.canvas.draw_idle()

    @Slot(name='addNewSlice')
    def addNewSlice(self):
        self.sectorModel.insertRow(self.sectorModel.rowCount(QtCore.QModelIndex()), QtCore.QModelIndex())

    @Slot(name='removeSlice')
    def removeSlice(self):
        for row in reversed(sorted({index.row() for index in self.slicesTreeView.selectionModel().selectedRows(0)})):
            self.slicesTreeView.model().removeRow(row, QtCore.QModelIndex())

    @Slot(name='clearSlices')
    def clearSlices(self):
        self.sectorModel.clear()

    @Slot(name='createFan')
    def createFan(self):
        phi0 = self.fanPhi0DoubleSpinBox.value()
        dphi = 360.0 / self.fanCountSpinBox.value()
        for i in range(self.fanCountSpinBox.value()):
            self.sectorModel.appendSector(phi0 + dphi * i, dphi, False)

    @Slot(float, name='onBeamXChanged')
    def onBeamXChanged(self, value: float):
        self.exposure.header.beamposcol = (value, 0.0)
        self.plotimage.setExposure(self.exposure, keepzoom=True)
        self.redrawFullRadialAverage()
        self.onAzimuthalCountChanged(self.nAzimSpinBox.value())
        self.onSectorsChanged()

    @Slot(float, name='onBeamYChanged')
    def onBeamYChanged(self, value: float):
        self.exposure.header.beamposrow = (value, 0.0)
        self.plotimage.setExposure(self.exposure, keepzoom=True)
        self.redrawFullRadialAverage()
        self.onAzimuthalCountChanged(self.nAzimSpinBox.value())
        self.onSectorsChanged()
