import logging
from typing import List, Optional, Any, Tuple, final, Union

from taurus.external.qt import QtWidgets, QtGui
from taurus.external.qt.QtCore import Slot

from ...core2.instrument.instrument import Instrument

logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)


class WindowRequiresDevices:
    """Convenience mixin class for UI windows

    Some GUI widgets require the availibility of certain devices (e.g. a motor mover window). By specifying this class
    among its parents, the following operations are automated:

    - checking the availability of the required devices on initialization
    - connecting to the most important signals of the devices:
        - variableChanged -> onVariableChanged
        - allVariablesReady -> onAllVariablesReady
        - connectionLost -> onConnectionLost
        - connectionEnded -> onConnectionEnded
        - commandResult -> onCommandResult
        - destroyed -> onDeviceDestroyed
    - disconnecting the signal handlers when the device is disconnected

    In addition to devices, motors can be required in a similar way. Their callback functions are:
        - started -> onMotorStarted: motor started moving
        - stopped -> onMotorStopped: motor stopped moving
        - positionChanged -> onMotorPositionChanged: motor position updated
        - moving -> onMotorMoving: motor position is updated while moving
        - variableChanged -> onVariableChanged: motor variable changed, common callback with device variable changed
        - destroyed -> onMotorDestroyed: motor is destroyed

    Motors and devices can be required using the `required_devicenames` or `required_devicetypes` attributes, set before
    calling __init__. These are lists of strings.

    Even after initialization, the widget keeps a look out on devices/motors: if a required device becomes unavailable,
    either temporarily (unexpected communication error) or terminally (disconnection), the widget becomes disabled.
    Whenever they become available, the widget is reenabled.

    The concept of 'idle' and 'busy' states are also implemented. If the widget is set 'busy' with the `setBusy()`
    method, any attempt to close it will result in a question to the user for confirmation.



    :ivar required_devicenames: list of device names which are required for
    """
    _idle: bool = True  # True if the widget is 'idle'. If False, the user will be asked for confirmation for closing the widget
    instrument: Optional[Instrument] = None  # the `Instrument` instance
    mainwindow: Optional["MainWindow"] = None  # the `MainWindow` instance
    _requirements_satisfied: bool = False  # True if all requirements (devices, motors, privileges) are satisfied
    _sensitive: bool = True  # User-requested enabled/disabled state of the widget. The actual enabled/disabled state is the AND of this and `_requirements_satisfied`

    def __init__(self, **kwargs):
        """Instantiate a device/motor/privilege-aware window

        Keyword arguments:

        :param instrument: the Instrument instance (optional, by default None)
        :type instrument: `Instrument`
        :param mainwindow: the main window instance
        :param mainwindow: the main window instance
        :type mainwindow: `MainWindow`
        """
        try:
            instrument = kwargs.pop('instrument')
        except KeyError:
            instrument = None
        mainwindow = kwargs.pop('mainwindow')
        super().__init__(**kwargs)
        if instrument is not None:
            self.instrument = instrument

            self.instrument.auth.currentUserChanged.connect(self.onUserOrPrivilegesChanged)
        else:
            self.instrument = None
        self.mainwindow = mainwindow

    # ------------ Checking and handling requirements ------------------------------------------------------------------

    @classmethod
    @final
    def checkRequirements(cls) -> bool:
        """Check if the requirements are satisfied for this widget class"""
        return True

    def setSensitive(self, sensitive: Optional[bool] = True):
        """An extended version of QtWidget.setEnabled(): only set the widget enabled (sensitive) if all the requirements
        are satisfied."""
        if (self.instrument is None) and (sensitive is not None):
            return self.setEnabled(sensitive)
        else:
            self._sensitive = sensitive if sensitive is not None else self._sensitive
            return self.setEnabled(self._sensitive and self.checkRequirements())

    def setUnsensitive(self, unsensitive: Optional[bool] = True):
        return self.setSensitive(not unsensitive)

    # -------------------------- callback functions from devices and motors -------------------------------------------

    def closeEvent(self, closeEvent: QtGui.QCloseEvent):
        if self.isIdle():
            logger.debug(f'Accepting close event because we are idle. {self.objectName()=}')
            closeEvent.accept()
            self.destroy()
        else:
            if QtWidgets.QMessageBox.question(
                    self.window(), 'Confirm close', 'Really close this window? There is a background process working!'):
                closeEvent.accept()
                self.destroy()
            else:
                closeEvent.ignore()
