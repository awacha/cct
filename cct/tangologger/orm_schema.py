import sqlalchemy.orm
import datetime
from typing import List, Optional


class Base(sqlalchemy.orm.DeclarativeBase):
    type_annotation_map = {
        int: sqlalchemy.BIGINT(),
        datetime.datetime: sqlalchemy.TIMESTAMP(timezone=True),
        str: sqlalchemy.String(1024),
    }


class QueriedAttribute(Base):
    __tablename__ = 'queried_attributes'
    __table_args__ = {'sqlite_autoincrement': True, 'extend_existing': True}
    id: sqlalchemy.orm.Mapped[int] = sqlalchemy.orm.mapped_column(
        sqlalchemy.BIGINT().with_variant(sqlalchemy.INTEGER, "sqlite"), primary_key=True, autoincrement=True)
    tangodevice: sqlalchemy.orm.Mapped[str] = sqlalchemy.orm.mapped_column()
    tangoattribute: sqlalchemy.orm.Mapped[str] = sqlalchemy.orm.mapped_column()
    interval: sqlalchemy.orm.Mapped[float] = sqlalchemy.orm.mapped_column()
    fieldname: sqlalchemy.orm.Mapped[str] = sqlalchemy.orm.mapped_column(
        sqlalchemy.String(1024), unique=True)
    units: sqlalchemy.orm.Mapped[str] = sqlalchemy.orm.mapped_column(
        sqlalchemy.String(1024), unique=False, default='', nullable=True)
    readings: sqlalchemy.orm.Mapped[List["Reading"]] = sqlalchemy.orm.relationship(
        back_populates="attribute", lazy='noload')
    lastquery: sqlalchemy.orm.Mapped[datetime.datetime] = sqlalchemy.orm.mapped_column(
        nullable=True, default=None)


class Reading(Base):
    __tablename__ = 'readings'
    __table_args__ = {'sqlite_autoincrement': True, 'extend_existing': True}
    id: sqlalchemy.orm.Mapped[int] = sqlalchemy.orm.mapped_column(
        sqlalchemy.BIGINT().with_variant(sqlalchemy.INTEGER, "sqlite"), primary_key=True, autoincrement=True)
    timestamp: sqlalchemy.orm.Mapped[datetime.datetime] = sqlalchemy.orm.mapped_column(
    )
    attribute: sqlalchemy.orm.Mapped["QueriedAttribute"] = sqlalchemy.orm.relationship(
        back_populates="readings")
    attribute_id: sqlalchemy.orm.Mapped[int] = sqlalchemy.orm.mapped_column(
        sqlalchemy.ForeignKey("queried_attributes.id"))
    value: sqlalchemy.orm.Mapped[Optional[float]
                                 ] = sqlalchemy.orm.mapped_column()
