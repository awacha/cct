import datetime
import json
import math
import sys
import time
import traceback
from typing import Tuple, Optional

import dateutil.tz
import sqlalchemy.orm
import tango
import tango.server
from tango.server import attribute, command, device_property

from cct.tangologger.orm_schema import Reading, QueriedAttribute, Base


class CCTTangoLogger(tango.server.Device):
    _next_wake_time: Optional[float] = None

    dburl = device_property(
        dtype=str, doc='Database URL a la SQLAlchemy', mandatory=True, update_db=True)
    querywindow = device_property(
        dtype=float, doc='Early query time window: query attributes which are due in this time (seconds).',
        mandatory=False, update_db=True, default_value=60)

    queriedattributes = attribute(
        dtype=tango.DevEncoded, access=tango.AttrWriteType.READ, label='Attributes that are queried')
    engine: sqlalchemy.Engine = None

    def init_device(self):
        self.get_device_properties()
        self._next_wake_time = 0.0
        self.engine = sqlalchemy.create_engine(self.dburl, pool_pre_ping=True)
        Base.metadata.create_all(self.engine)
        self.set_state(tango.DevState.RUNNING)

    def read_queriedattributes(self):
        with sqlalchemy.orm.Session(self.engine) as session:
            qas = session.scalars(sqlalchemy.select(QueriedAttribute))
            result = [
                {'device': qa.tangodevice,
                 'attribute': qa.tangoattribute,
                 'interval': qa.interval,
                 'fieldname': qa.fieldname,
                 'units': qa.units} for qa in qas]
            return 'json', json.dumps(result)

    @command(dtype_in=tango.DevEncoded, dtype_out=tango.DevVoid)
    def addQueriedAttribute(self, data: Tuple[str, bytes]):
        """Add a queried attribute"""
        encoding, encoded = data
        if encoding != 'json':
            raise ValueError('Only JSON encoding is supported')

        attr = json.loads(encoded)
        try:
            qa = QueriedAttribute(tangodevice=attr['device'], tangoattribute=attr['attribute'],
                                  interval=attr.setdefault('interval', 60),
                                  fieldname=attr.setdefault(
                                      'fieldname', attr['attribute']),
                                  units=attr.setdefault('units', ''))
        except Exception:
            raise ValueError(
                'Invalid parameter: the parameter must be a JSON-encoded dictionary with '
                'fields "device", "attribute", "interval" and "fieldname".')
        with sqlalchemy.orm.Session(self.engine) as session, session.begin():
            session.add(qa)
        self._next_wake_time = None

    @command(dtype_in=tango.DevVoid, dtype_out=tango.DevVoid, polling_period=10000, )
    def Query(self):
        tz = dateutil.tz.gettz()
        if (self._next_wake_time is None) or (time.monotonic() >= self._next_wake_time):
            self.info_stream('Starting Query')
            t0 = time.monotonic()
            tznow = datetime.datetime.now(tz)
            try:
                with sqlalchemy.orm.Session(self.engine) as session, session.begin():
                    # find all the queried attributes
                    qas = list(session.scalars(
                        sqlalchemy.select(QueriedAttribute)))

                    # find the last readout times for each attribute
                    lastreadings = session.query(QueriedAttribute, sqlalchemy.func.max(Reading.timestamp)).group_by(
                        Reading.attribute_id).join(Reading.attribute).where(QueriedAttribute.lastquery.is_(None)).all()
                    readouttimes = dict(lastreadings)
                    for qa in qas:
                        if qa not in readouttimes:
                            readouttimes[qa] = qa.lastquery
                    for qa in qas:
                        if readouttimes[qa] is None:
                            readouttimes[qa] = datetime.datetime.fromtimestamp(0).astimezone(tz)
                    attrstoquery = [
                        qa
                        for qa in qas
                        if (tznow - readouttimes.setdefault(
                            qa,
                            datetime.datetime.fromtimestamp(0).astimezone(tz)).astimezone(tz)).total_seconds()
                        > qa.interval]
                    self.info_stream('Attributes to query: '+", ".join(
                        [qa.tangodevice+'/'+qa.tangoattribute for qa in attrstoquery]))
                    for qa in attrstoquery:
                        assert isinstance(qa, QueriedAttribute)
                        try:
                            value = tango.AttributeProxy(
                                f'{qa.tangodevice}/{qa.tangoattribute}').read().value
                        except tango.DevFailed as deverr:
                            value = math.nan
                            self.error_stream(f'Tango exception: {deverr}')
                        except Exception:
                            self.error_stream(
                                f'Cannot read attribute "{qa.tangoattribute}" from device "{qa.tangodevice}".')
                            value = math.nan
                        self.debug_stream(
                            f'{qa.tangodevice}/{qa.tangoattribute}: {value}')
                        lasttime = datetime.datetime.now(tz)
                        session.add(Reading(timestamp=lasttime, attribute=qa,
                                    value=value if not math.isnan(value) else None))
                        qa.lastquery = lasttime
                        session.add(qa)
                        readouttimes[qa] = lasttime
                    # now all attributes are queried
                    nextquerydate = min([readouttime.astimezone(
                        tz) + datetime.timedelta(seconds=qa.interval) for qa, readouttime in readouttimes.items()])
                    self._next_wake_time = time.monotonic() + (nextquerydate -
                                                               datetime.datetime.now(tz)).total_seconds()
                    self.debug_stream(
                        f'Next wake will happen at {nextquerydate}, '
                        f'{self._next_wake_time - time.monotonic():.3f} seconds from now')
                    self.set_state(tango.DevState.RUNNING)
                    self.set_status(f'Last readout: {tznow}')
            except Exception as exc:
                self.error_stream(
                    f'Exception in SQL session: {exc}, {traceback.format_exc()}')
                self.set_state(tango.DevState.FAULT)
                self.set_status(
                    f'Exception in SQL session: {exc}, {traceback.format_exc()}')
            self.info_stream(
                f'Query of attributes ended in {time.monotonic()-t0:.3f} seconds.')
        else:
            self.debug_stream(
                f'Not querying, next wake time is in {self._next_wake_time-time.monotonic():.1f} seconds')


def main():
    CCTTangoLogger.run_server(msg_stream=sys.stderr, raises=True, verbose=True)
