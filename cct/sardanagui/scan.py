import enum
from typing import Optional

import tango
import taurus
import taurus.core.util.codecs
from taurus.core.tango import TangoAttribute, TangoDevice
from taurus.external.qt import QtGui, compat
from taurus.external.qt.QtCore import Slot

from .plotscan import PlotScanSardana
from .sardanaguibase import SardanaGUIBase, UILoadable


class RangeType(enum.Enum):
    Absolute = 'Absolute'
    Relative = 'Relative'
    SymmetricRelative = 'Symmetric'


@UILoadable()
class ScanMeasurement(SardanaGUIBase):
    motordevice: Optional[TangoDevice] = None
    positionattr: Optional[TangoAttribute] = None
    scangraph: Optional[PlotScanSardana] = None

    def __init__(self, **kwargs):
        super().__init__(**kwargs)

    def setupUi(self):
        self._door.stateObj.addListener(self)
        self._door.recordDataUpdated.connect(self.onRecordDataUpdated)
        self.motorComboBox.addItems(self.getMotors())
        self.motorComboBox.currentIndexChanged.connect(self.onMotorChanged)
        self.motorComboBox.setCurrentIndex(-1)
        self.rangeTypeComboBox.addItems([rt.value for rt in RangeType])
        self.rangeTypeComboBox.currentIndexChanged.connect(
            self.onRangeTypeSelected)
        for widget in [self.rangeTypeComboBox, self.rangeMinDoubleSpinBox, self.rangeMaxDoubleSpinBox,
                       self.stepSizeDoubleSpinBox, self.stepCountSpinBox, self.countingTimeDoubleSpinBox]:
            widget.setEnabled(False)
        self.rangeTypeComboBox.setEnabled(False)
        self.rangeMaxDoubleSpinBox.setEnabled(False)
        self.progressBar.hide()
        self.rangeMinDoubleSpinBox.valueChanged.connect(self.onRangeChanged)
        self.rangeMaxDoubleSpinBox.valueChanged.connect(self.onRangeChanged)
        self.stepCountSpinBox.valueChanged.connect(self.onStepsChanged)
        self.stepSizeDoubleSpinBox.valueChanged.connect(self.onStepsChanged)
        self.startStopPushButton.clicked.connect(self.onStartStopClicked)
        self.startStopPushButton.setEnabled(self.isStartStopable())
        self.shrinkWindow()

    def getMotors(self):
        return sorted([e['name'] for e in taurus.core.util.codecs.CodecFactory().decode(
            self._door.macro_server.Elements)['new'] if e['type'] == 'Motor'])

    @Slot(compat.PY_OBJECT)
    def onRecordDataUpdated(self, rd):
        data = taurus.core.util.codecs.CodecFactory().decode(rd)
        if data['type'] == 'data_desc':
            # scan just started.
            for widget in [self.motorComboBox, self.rangeTypeComboBox, self.rangeMinDoubleSpinBox,
                           self.rangeMaxDoubleSpinBox, self.stepCountSpinBox, self.stepSizeDoubleSpinBox,
                           self.countingTimeDoubleSpinBox, self.shutterCheckBox,
                           self.resetMotorCheckBox]:
                widget.setEnabled(False)
            self.startStopPushButton.setEnabled(True)
            self.startStopPushButton.setText('Stop')
            self.startStopPushButton.setIcon(
                QtGui.QIcon(QtGui.QPixmap(":/icons/stop.svg")))
            self.progressBar.show()
            self.progressBar.setRange(0, data['data']['total_scan_intervals'])
            self.progressBar.setValue(0)
            self.progressBar.setFormat('Starting scan...')
        elif data['type'] == 'record_data':
            self.progressBar.setValue(self.progressBar.value() + 1)
            self.progressBar.setFormat(
                f'Scanning, {self.progressBar.value()}/{self.progressBar.maximum()}')
        elif data['type'] == 'record_end':
            for widget in [self.motorComboBox, self.rangeTypeComboBox, self.rangeMinDoubleSpinBox,
                           self.rangeMaxDoubleSpinBox, self.stepCountSpinBox, self.stepSizeDoubleSpinBox,
                           self.countingTimeDoubleSpinBox, self.shutterCheckBox,
                           self.resetMotorCheckBox]:
                widget.setEnabled(True)
            self.startStopPushButton.setEnabled(True)
            self.startStopPushButton.setText('Start')
            self.startStopPushButton.setIcon(
                QtGui.QIcon(QtGui.QPixmap(":/icons/start.svg")))
            if self.scangraph is not None:
                try:
                    self.scangraph.setRecording(False)
                    self.scangraph.setDoor(None)
                except RuntimeError:
                    # this can happen when the user closes the scan graph prematurely
                    pass
                self.scangraph = None
            self.progressBar.hide()
            self.shrinkWindow()

    @Slot()
    def onRangeTypeSelected(self):
        if self.rangeTypeComboBox.currentIndex() < 0:
            return
        rt = RangeType(self.rangeTypeComboBox.currentText())
        self.fromLabel.setText('Half width:' if rt ==
                               RangeType.SymmetricRelative else 'From:')
        if rt == RangeType.SymmetricRelative:
            self.toLabel.hide()
            self.rangeMaxDoubleSpinBox.hide()
        else:
            self.toLabel.show()
            self.rangeMaxDoubleSpinBox.show()
        if self.motordevice is not None:
            self.onMotorPositionChanged()
        self.shrinkWindow()

    def shrinkWindow(self):
        self.resize(self.minimumSizeHint())

    def handleEvent(self, evt_src, evt_type, evt_value: taurus.core.tango.TangoAttrValue):
        if (self.motordevice is not None) and (evt_src == self.motordevice.stateObj) and (evt_type == taurus.core.taurusbasetypes.TaurusEventType.Change):
            self.setMotorButtonsEnabled()
        elif (self.positionattr is not None) and (evt_src == self.positionattr) and (evt_type == taurus.core.taurusbasetypes.TaurusEventType.Change):
            self.onMotorPositionChanged(float(evt_value.rvalue))
        elif (evt_src == self._door.stateObj) and (evt_type == taurus.core.taurusbasetypes.TaurusEventType.Change):
            self.startStopPushButton.setEnabled(self.isStartStopable())

    @Slot()
    def onMotorChanged(self):
        if self.motordevice is not None:
            self.motordevice.removeListener(self)
            self.positionattr.removeListener(self)
            self.motordevice = None
            self.positionattr = None
        if self.motorComboBox.currentIndex() < 0:
            return
        motorname = self.motorComboBox.currentText()
        self.motordevice = taurus.core.tango.TangoDevice(motorname)
        self.positionattr = self.motordevice.getAttribute('Position')
        self.motordevice.addListener(self)
        self.positionattr.addListener(self)
        self.onMotorPositionChanged(float(self.positionattr.read().rvalue))
        for widget in [self.rangeTypeComboBox, self.rangeMinDoubleSpinBox, self.rangeMaxDoubleSpinBox,
                       self.stepSizeDoubleSpinBox, self.stepCountSpinBox, self.countingTimeDoubleSpinBox]:
            widget.setEnabled(True)
        self.startStopPushButton.setEnabled(self.isStartStopable())

    def isStartStopable(self):
        if self.startStopPushButton.text() == 'Start':
            return (self._door.State() != tango.DevState.RUNNING) and (self.motordevice is not None)
        elif self.startStopPushButton.text() == 'Stop':
            return True

    @Slot(float)
    def onMotorPositionChanged(self, newposition: float):
        if self.startStopPushButton.text() != 'Start':
            # scan is running, do not update motor limits in the input boxes
            return
        rt = RangeType(self.rangeTypeComboBox.currentText())
        left, right = [float(x) for x in self.positionattr.getRange()]
        if rt == RangeType.Absolute:
            self.rangeMinDoubleSpinBox.setRange(left, right)
            self.rangeMaxDoubleSpinBox.setRange(left, right)
        elif rt == RangeType.Relative:
            self.rangeMinDoubleSpinBox.setRange(
                left - newposition, right - newposition)
            self.rangeMaxDoubleSpinBox.setRange(
                left - newposition, right - newposition)
        elif rt == RangeType.SymmetricRelative:
            radius = min(abs(left - newposition), abs(right - newposition))
            self.rangeMinDoubleSpinBox.setRange(0, radius)
            self.rangeMaxDoubleSpinBox.setRange(0, radius)

    @Slot()
    def onRangeChanged(self):
        self.onStepsChanged()

    @Slot()
    def onStepsChanged(self):
        rt = RangeType(self.rangeTypeComboBox.currentText())
        length = (self.rangeMinDoubleSpinBox.value() * 2) if rt == RangeType.SymmetricRelative else (
            self.rangeMaxDoubleSpinBox.value() - self.rangeMinDoubleSpinBox.value())
        if self.sender() is self.stepCountSpinBox:
            stepsize = length / (self.stepCountSpinBox.value() - 1)
            self.stepSizeDoubleSpinBox.blockSignals(True)
            self.stepSizeDoubleSpinBox.setValue(stepsize)
            self.stepSizeDoubleSpinBox.blockSignals(False)
        else:
            if self.stepSizeDoubleSpinBox.value() == 0:
                self.stepSizeDoubleSpinBox.setValue(0.1)
            stepcount = max(
                1, abs(int(length / self.stepSizeDoubleSpinBox.value()))) + 1
            # do not monkey around with blocksignals: stepcount is the definitive
            self.stepCountSpinBox.setValue(stepcount)

    @Slot()
    def onStartStopClicked(self):
        if self.startStopPushButton.text() == 'Start':
            rt = RangeType(self.rangeTypeComboBox.currentText())
            if rt == RangeType.Absolute:
                rangemin, rangemax = self.rangeMinDoubleSpinBox.value(
                ), self.rangeMaxDoubleSpinBox.value()
                relative = False
            elif rt == RangeType.Relative:
                rangemin, rangemax = self.rangeMinDoubleSpinBox.value(
                ), self.rangeMaxDoubleSpinBox.value()
                relative = True
            elif rt == RangeType.SymmetricRelative:
                rangemin, rangemax = - \
                    self.rangeMinDoubleSpinBox.value(), self.rangeMinDoubleSpinBox.value()
                relative = True
            else:
                assert False

            self._door.setEnvironment(
                'OpenShutterOnScan', self.shutterCheckBox.isChecked())
            if self.openPlotScanCheckBox.isChecked():
                self.scangraph = PlotScanSardana(
                    door=self._door.name, mainwindow=self.mainwindow)
                self.scangraph.setWaitForNewScans(True)
            else:
                self.scangraph = None
            try:
                self._door.runMacro('dscan' if relative else 'ascan',
                                    [self.motordevice.name, rangemin, rangemax,
                                     self.stepCountSpinBox.value() - 1,
                                     self.countingTimeDoubleSpinBox.value()], synch=False)
            except Exception:
                if self.scangraph is not None:
                    self.scangraph.destroy()
                raise
            if self.scangraph is not None:
                self.scangraph.show()
            self.startStopPushButton.setEnabled(False)
        else:
            self.startStopPushButton.setEnabled(False)
            self._door.StopMacro()

    @Slot(float, float, float, str)
    def onScanProgress(self, start: float, end: float, current: float, message: str):
        if end == start:
            self.progressBar.setRange(0, 0)
        else:
            self.progressBar.setRange(0, 1000)
            self.progressBar.setValue(
                int((current - start) / (end - start) * 1000))
        self.progressBar.setFormat(message)
