from typing import List
import os

from taurus.external.qt import QtCore, QtWidgets
from taurus.external.qt.QtCore import Slot

from ..core2.dataclasses import Scan
from .models.scanstore import ScanStore
from .plotscan import PlotScanSardana
from .sardanaguibase import SardanaGUIBase, UILoadable
import sardana.macroserver.msexception


@UILoadable()
class ScanViewer(SardanaGUIBase):
    plotscanwindows: List[PlotScanSardana] = None

    def __init__(self, **kwargs):
        self.plotscanwindows = []
        super().__init__(**kwargs)

    def setupUi(self):
        self._door.stateObj.addListener(self)
        try:
            scandir = self._door.getEnvironment('ScanDir')
            scanfile = self._door.getEnvironment('ScanFile')
        except sardana.macroserver.msexception.UnknownEnv:
            scandir = ''
            scanfile = []

        self.scanFileComboBox.blockSignals(True)
        self.scanFileComboBox.clear()
        self.scanFileComboBox.addItem('<ScanHistory>')
        if scandir is not None and scanfile is not None:
            if isinstance(scanfile, str):
                scanfile = [scanfile]
            self.scanFileComboBox.addItems([os.path.join(scandir, sf) for sf in scanfile if sf.rsplit('.', 1)[1] in ['spec', 'h5', 'nxs', 'nx5']])
        self.scanFileComboBox.setCurrentIndex(self.scanFileComboBox.count()-1)
        self.scanFileComboBox.blockSignals(False)

        self.treemodel = ScanStore(
            door=self._door.name,
            parent=self,
            scanfile=None if self.scanFileComboBox.currentIndex() == 0 else self.scanFileComboBox.currentText())
        self.treeView.setModel(self.treemodel)
        self.showPushButton.clicked.connect(self.onShowPushButtonClicked)
        self.treeView.activated.connect(self.showScan)
        self.resizeTreeColumns()

    @Slot(str)
    def on_scanFileComboBox_currentTextChanged(self, currenttext: str):
        if currenttext == '<ScanHistory>':
            self.treemodel.scanfile = None
            self.treemodel.reload()
        elif os.path.exists(currenttext):
            self.treemodel.scanfile = currenttext
            self.treemodel.reload()

    @Slot(bool)
    def on_reloadToolButton_clicked(self, checked: bool):
        self.treemodel.reload()

    @Slot(bool)
    def on_browseToolButton_clicked(self, checked: bool):
        filename, filter_ = QtWidgets.QFileDialog.getOpenFileName(
            self, 'Open scan file', '',
            'HDF5 files (*.h5 *.hdf5 *.nxs *.nx5);;SPEC files (*.spec *.dat);;All files (*)')
        if not filename:
            return
        self.scanFileComboBox.addItem(filename)
        self.scanFileComboBox.setCurrentText(filename)
#        self.treemodel.scanfile = filename
#        self.treemodel.reload()

    @Slot()
    def resizeTreeColumns(self):
        for c in range(self.treemodel.columnCount()):
            self.treeView.resizeColumnToContents(c)

    @Slot(bool)
    def onShowPushButtonClicked(self, checked: bool):
        self.showScan(self.treeView.currentIndex())

    @Slot(QtCore.QModelIndex)
    def showScan(self, index: QtCore.QModelIndex):
        scan = self.treemodel.loadscan(index)
        assert isinstance(scan, Scan)
        if self.mainwindow is not None:
            plotscan: PlotScanSardana = self.mainwindow.addSubWindow(
                PlotScanSardana, singleton=False)
        else:
            plotscan = PlotScanSardana(
                door=self._door.name if self._door is not None else None, mainwindow=None)
            self.plotscanwindows.append(plotscan)
            plotscan.destroyed.connect(self.onPlotScanDestroyed)
            plotscan.show()
            plotscan.setAttribute(QtCore.Qt.WA_DeleteOnClose, True)
        plotscan.setScan(scan._data, scan.motorname, scan.command)
        plotscan.setWaitForNewScans(False)

    def onPlotScanDestroyed(self, object):
        # note that object is not the PlotScanSardana instance, but some QWidget.
        # We cannot directly remove the object, but find out where has the
        # "wrapped C/C33 object of type PlotScanSardana" been deleted and remove
        # those items from the list.
        for x in self.plotscanwindows[:]:
            try:
                x.objectName()
            except RuntimeError:
                self.plotscanwindows.remove(x)
