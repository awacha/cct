from taurus.external.qt import QtWidgets, QtCore
from taurus.external.qt.QtCore import Slot

from .sardanaguibase import UILoadable, SardanaGUIBase
from .models.projectmanager import ProjectManager
from .addprojectdialog import AddProjectDialog


@UILoadable()
class ProjectManagerUI(SardanaGUIBase):
    _projectmanager: ProjectManager

    def setupUi(self):
        self._projectmanager = ProjectManager(self, door=self._door.name)
        self.projectListTreeView.setModel(self._projectmanager)
        self.projectListTreeView.selectionModel(
        ).selectionChanged.connect(self.projectSelected)
        for c in range(self.projectListTreeView.model().columnCount()):
            self.projectListTreeView.resizeColumnToContents(c)
        self.addProjectPushButton.setEnabled(True)
        self.removeProjectPushButton.setEnabled(True)

    @Slot()
    def on_addProjectPushButton_clicked(self):
        dlg = AddProjectDialog(self, prjman=self._projectmanager)
        result = dlg.exec_()
        if result == QtWidgets.QDialog.DialogCode.Accepted:
            try:
                self._projectmanager.addProject(
                    dlg.yearSpinBox.value(), dlg.categoryComboBox.currentText(),
                    dlg.idSpinBox.value(), dlg.titleLineEdit.text(), dlg.proposerLineEdit.text())
            except ValueError as ve:
                QtWidgets.QMessageBox.critical(
                    self, 'Error', f'Cannot add project: {ve.args[0]}')
        dlg.destroy()

    @Slot()
    def on_removeProjectPushButton_clicked(self):
        if self.projectListTreeView.selectionModel().currentIndex().isValid():
            prj = self.projectListTreeView.selectionModel().currentIndex().data(
                QtCore.Qt.ItemDataRole.UserRole)
            self._projectmanager.removeProject(
                prj.year, prj.category, prj.id)

    @Slot()
    def projectSelected(self):
        if not self.projectListTreeView.selectionModel().currentIndex().isValid():
            self.removeProjectPushButton.setEnabled(False)
        else:
            self.removeProjectPushButton.setEnabled(
                (self.projectListTreeView.selectionModel().currentIndex().data(
                    QtCore.Qt.ItemDataRole.UserRole).projectid !=
                 self.instrument.projects.projectID()))
