from .models.sensors import Sensors
from .sardanaguibase import SardanaGUIBase, UILoadable


@UILoadable()
class SensorsWindow(SardanaGUIBase):
    sensors: Sensors

    def setupUi(self):
        self.sensors = Sensors(parent=self, door=self._door.name)
        self.treeView.setModel(self.sensors)
