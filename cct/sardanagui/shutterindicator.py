from typing import Any

from taurus.external.qt import QtGui
from taurus.external.qt.QtCore import Slot

from .sardanaguibase import SardanaGUIBase, UILoadable
from taurus.core.tango import TangoAttribute
import taurus


@UILoadable()
class ShutterIndicator(SardanaGUIBase):
    shutterattr: TangoAttribute = None
    interlockattr: TangoAttribute = None

    def setupUi(self):
        self.on_environment_changed(
            '_BeamShutter', self._door.getEnvironment('_BeamShutter'))
        self.setToolButtonState()

    def on_environment_changed(self, envname: str, envdata: Any):
        if envname == '_BeamShutter':
            if self.shutterattr is not None:
                self.shutterattr.removeListener(self)
                del self.shutterattr
            if self.interlockattr is not None:
                self.interlockattr.removeListener(self)
                del self.interlockattr
            self.shutterattr = taurus.Attribute(envdata['shutter'])
            self.interlockattr = taurus.Attribute(envdata['interlock'])
            self.shutterattr.addListener(self)
            self.interlockattr.addListener(self)
            xraydev = self.shutterattr.parentObj.name
            self.htTaurusLCD.setModel(f'{xraydev}/ht')
            self.currentTaurusLCD.setModel(f'{xraydev}/current')
            self.powerTaurusLCD.setModel(f'{xraydev}/power')

    def handleEvent(self, evt_src, evt_type, evt_value):
        if (evt_src is self.shutterattr or evt_src is self.interlockattr) and \
                (evt_type == taurus.core.taurusbasetypes.TaurusEventType.Change):
            self.setToolButtonState()

    def setToolButtonState(self):
        shutterstate = bool(
            self.shutterattr.rvalue) if self.shutterattr is not None else False
        interlockstate = bool(
            self.interlockattr.rvalue) if self.interlockattr is not None else False
        self.debug(f'{interlockstate=}, {shutterstate=}')
        self.toolButton.setEnabled(interlockstate)
        self.toolButton.setIcon(
            QtGui.QIcon(
                QtGui.QPixmap(
                    ':/icons/beamshutter_open.svg' if shutterstate else ':/icons/beamshutter_closed.svg')))
        self.toolButton.blockSignals(True)
        self.toolButton.setChecked(shutterstate)
        self.toolButton.blockSignals(False)

    @Slot(bool)
    def on_toolButton_toggled(self, state: bool):
        self.shutterattr.write(state)
