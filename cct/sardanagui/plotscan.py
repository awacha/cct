from .sardanaguibase import SardanaGUIBase, UILoadable
from typing import Dict, Optional, Tuple, Any
import tango
import numpy as np
from taurus.external.qt import QtWidgets, QtCore
from taurus.external.qt.QtCore import Slot
from matplotlib.axes import Axes
from matplotlib.backend_bases import key_press_handler, KeyEvent
from matplotlib.backends.backend_qt5agg import NavigationToolbar2QT, FigureCanvasQTAgg
from matplotlib.figure import Figure
from matplotlib.legend import Legend
from matplotlib.lines import Line2D
from matplotlib.text import Text
import sardana.taurus.core.tango.sardana
import sardana.taurus.qt.qtcore.tango.sardana.macroserver
from sardana.macroserver.msexception import UnknownEnv
import taurus
from taurus.core.tango import TangoDevice, TangoAttribute
import taurus.core.util.codecs
import lmfit
from ..core2.algorithms.schilling import longest_run, cormap_pval

from ..core2.dataclasses import Scan
from ..resource import icons_rc

icons_rc.qInitResources()


@UILoadable()
class PlotScanSardana(SardanaGUIBase):
    _recording: bool = False
    _data: Optional[np.ndarray] = None
    _recorddataptr: Optional[int] = None
    _recordingnames = []
    _title: Optional[str] = None
    motorname: Optional[str] = None
    figure: Figure
    canvas: FigureCanvasQTAgg
    figtoolbar: NavigationToolbar2QT
    axes: Axes
    scan: Scan = None
    lines: Dict[str, Line2D]
    legend: Optional[Legend] = None
    cursor: Optional[Line2D] = None
    peakcurve: Optional[Line2D] = None
    peakvline: Optional[Line2D] = None
    peaktext: Optional[Text] = None
    motorvline: Optional[Line2D] = None
    _door: Optional[sardana.taurus.qt.qtcore.tango.sardana.macroserver.QDoor] = None

    motordevice: Optional[TangoDevice] = None
    positionattr: Optional[TangoAttribute] = None

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.setLogLevel(self.Debug)

    def setupUi(self):
        if self._door is not None:
            self.setWaitForNewScans(True)
            self.setDoor(self._door.name)

        self.figure = Figure(constrained_layout=True)
        self.axes = self.figure.add_subplot(self.figure.add_gridspec(1, 1)[:, :])
        self.canvas = FigureCanvasQTAgg(self.figure)
        self.canvas.mpl_connect("key_press_event", self.onCanvasKeyPress)
        self.canvas.setFocusPolicy(QtCore.Qt.FocusPolicy.StrongFocus)
        self.figtoolbar = NavigationToolbar2QT(self.canvas, self)
        self.figureVerticalLayout.addWidget(self.figtoolbar)
        self.figureVerticalLayout.addWidget(self.canvas)

        self.listWidget.itemChanged.connect(self.setPlotVisibility)
        self.autoScaleToolButton.clicked.connect(self.autoScaleToggled)
        self.fitAsymmetricPositiveToolButton.clicked.connect(self.fitPeak)
        self.fitSymmetricPositiveToolButton.clicked.connect(self.fitPeak)
        self.fitAsymmetricNegativeToolButton.clicked.connect(self.fitPeak)
        self.fitSymmetricNegativeToolButton.clicked.connect(self.fitPeak)
        self.motorToPeakToolButton.clicked.connect(self.motorToPeak)
        self.motorToCursorToolButton.clicked.connect(self.motorToCursor)
        self.show2DToolButton.toggled.connect(self.show2DToggled)
        self.goBackToolButton.clicked.connect(self.onSliderButton)
        self.goForwardToolButton.clicked.connect(self.onSliderButton)
        self.gotoFirstToolButton.clicked.connect(self.onSliderButton)
        self.gotoLastToolButton.clicked.connect(self.onSliderButton)
        self.listWidget.currentRowChanged.connect(self.emphasizeCurrentLine)
        self.cursorHorizontalSlider.valueChanged.connect(self.updateCursor)
        self.cursorToMaximumToolButton.clicked.connect(self.cursorToMax)
        self.cursorToMinimumToolButton.clicked.connect(self.cursorToMin)
        self.legend = None
        self.lines = {}
        self.setRecording(self._recording)
        self.canvas.setFocus(QtCore.Qt.FocusReason.OtherFocusReason)

    def setWaitForNewScans(self, waitfornewscans: bool = True):
        self.waitForScansToolButton.setChecked(waitfornewscans)

    def setDoor(self, doorname: str):
        self.info(f'Setting door to "{doorname}"')
        if self._door is not None:
            try:
                self._door.recordDataUpdated.disconnect(self.onRecordDataUpdated)
            except TypeError:
                pass
            self._door = None
        if doorname is not None:
            self._door = taurus.Factory().getDevice(doorname)
            assert isinstance(
                self._door, sardana.taurus.qt.qtcore.tango.sardana.macroserver.QDoor
            )
            self._door.recordDataUpdated.connect(self.onRecordDataUpdated)
        else:
            self.warning("Running without a Sardana door.")
            self._door = None

    def onRecordDataUpdated(self, data: Tuple[str, Dict[str, Any]]):
        data = taurus.core.util.codecs.CodecFactory().decode(data)
        if data["type"] == "data_desc":
            if self.waitForScansToolButton.isChecked():
                _data = np.zeros(
                    data["data"]["total_scan_intervals"] + 1,
                    dtype=[
                        (cd["label"], cd["dtype"])
                        for cd in data["data"]["column_desc"]
                        if not cd["shape"]
                    ],
                )
                motorname = data["data"]["ref_moveables"][0]
                self.setScan(
                    _data,
                    motorname,
                    f"{data['data']['scanfile'][0]} / {data['data']['serialno']}: "
                    f"{data['data']['title']}",
                )
                self.setRecording(True)
                self._recorddataptr = 0
                self._recordingnames = [
                    cd["name"] for cd in data["data"]["column_desc"] if not cd["shape"]
                ]
                self.replot()
            else:
                self.setRecording(False)
        elif data["type"] == "record_data":
            if self.isRecording():
                values = tuple([data["data"][name] for name in self._recordingnames])
                self._data[self._recorddataptr] = values
                self._recorddataptr += 1
                self.replot()
        elif data["type"] == "record_end":
            if self.isRecording():
                self._data = self._data[: self._recorddataptr]
                self.setRecording(False)
                self.figtoolbar.update()
                self.replot()

    @property
    def columnnames(self) -> Tuple[str]:
        return tuple([n for n in self._data.dtype.names if n != self.motorname])

    def onCanvasKeyPress(self, event: KeyEvent):
        self.debug("Key pressed on canvas: {}".format(event.key))
        if event.key == "left":
            self.cursorHorizontalSlider.triggerAction(
                self.cursorHorizontalSlider.SliderSingleStepSub
            )
        elif event.key == "right":
            self.cursorHorizontalSlider.triggerAction(
                self.cursorHorizontalSlider.SliderSingleStepAdd
            )
        elif event.key in ["shift+left", "pagedown"]:
            self.cursorHorizontalSlider.triggerAction(
                self.cursorHorizontalSlider.SliderPageStepSub
            )
        elif event.key in ["shift+right", "pageup"]:
            self.cursorHorizontalSlider.triggerAction(
                self.cursorHorizontalSlider.SliderPageStepAdd
            )
        else:
            key_press_handler(event, self.canvas, self.figtoolbar)
        return True

    @Slot()
    def updateCursor(self):
        if self.cursor is not None:
            cursorpos = self._data[self.motorname][self.cursorHorizontalSlider.value()]
            self.cursor.set_xdata([cursorpos])
            self.canvas.draw_idle()
            if self.show2DToolButton.isChecked():
                self.showImage(keepzoom=True)
            self.cursorPositionLabel.setText(str(cursorpos))

    @Slot()
    def on_showAllPushButton_clicked(self):
        self.listWidget.blockSignals(True)
        for row in range(self.listWidget.count()):
            self.listWidget.item(row).setCheckState(QtCore.Qt.CheckState.Checked)
        self.listWidget.blockSignals(False)
        self.setPlotVisibility()

    @Slot()
    def on_hideAllPushButton_clicked(self):
        self.listWidget.blockSignals(True)
        for row in range(self.listWidget.count()):
            self.listWidget.item(row).setCheckState(QtCore.Qt.CheckState.Unchecked)
        self.listWidget.blockSignals(False)
        self.setPlotVisibility()

    @Slot()
    def emphasizeCurrentLine(self):
        item = self.listWidget.currentItem()
        if item is None:
            return
        signal = item.text()
        for key in self.lines:
            self.lines[key].set_linewidth(3 if signal == key else 1)
        self.canvas.draw_idle()

    @Slot()
    def onSliderButton(self):
        if self.sender() is self.goBackToolButton:
            self.cursorHorizontalSlider.triggerAction(
                QtWidgets.QSlider.SliderAction.SliderSingleStepSub
            )
        elif self.sender() is self.goForwardToolButton:
            self.cursorHorizontalSlider.triggerAction(
                QtWidgets.QSlider.SliderAction.SliderSingleStepAdd
            )
        elif self.sender() is self.gotoFirstToolButton:
            self.cursorHorizontalSlider.triggerAction(
                QtWidgets.QSlider.SliderAction.SliderToMinimum
            )
        else:
            assert self.sender() is self.gotoLastToolButton
            self.cursorHorizontalSlider.triggerAction(
                QtWidgets.QSlider.SliderAction.SliderToMaximum
            )

    @Slot()
    def autoScaleToggled(self):
        self.axes.relim(visible_only=True)
        self.axes.autoscale(self.autoScaleToolButton.isChecked())

    def setMotorButtonsEnabled(self):
        self.motorToCursorToolButton.setEnabled(
            (self.motordevice is not None)
            and (not self._recording)
            and (self.motordevice.State() != tango.DevState.MOVING)
        )
        self.motorToPeakToolButton.setEnabled(
            self.motorToCursorToolButton.isEnabled()
            and (self.peakposition() is not None)
        )

    @Slot()
    def motorToPeak(self):
        if self.peakposition() is not None:
            self.motordevice.getAttribute("Position").write(self.peakposition())

    @Slot()
    def motorToCursor(self):
        self.motordevice.getAttribute("Position").write(
            self._data[self.motorname][self.cursorHorizontalSlider.value()]
        )

    @Slot()
    def cursorToMin(self):
        x, y = self.currentData(onlyzoomed=False)
        self.cursorHorizontalSlider.setValue(np.argmin(y))

    @Slot()
    def cursorToMax(self):
        x, y = self.currentData(onlyzoomed=False)
        self.cursorHorizontalSlider.setValue(np.argmax(y))

    @Slot()
    def show2DToggled(self):
        if self.show2DToolButton.isChecked():
            self.showImage(keepzoom=False)

    @Slot(bool)
    def showImage(self, keepzoom: Optional[bool] = None):
        self.mainwindow.showPattern(
            self.instrument.io.loadExposure(
                self.instrument.cfg["path", "prefixes", "scn"],
                int(self.scan["FSN"][self.cursorHorizontalSlider.value()]),
                raw=True,
                check_local=True,
            ),
            keepzoom,
        )

    def currentData(self, onlyzoomed: bool = False):
        x = self._data[self.motorname][: self._recorddataptr]
        y = self._data[self.listWidget.currentItem().text()][: self._recorddataptr]
        if self.derivativeToolButton.isChecked():
            y = (y[1:] - y[:-1]) / (x[1:] - x[:-1])
            x = 0.5 * (x[1:] + x[:-1])
        if onlyzoomed:
            xmin, xmax, ymin, ymax = self.axes.axis()
            idx = np.logical_and(
                np.logical_and(x >= xmin, x <= xmax),
                np.logical_and(y >= ymin, y <= ymax),
            )
            x = x[idx]
            y = y[idx]
        return x, y

    @Slot()
    def fitPeak(self):
        x, y = self.currentData(onlyzoomed=True)
        positive = self.sender() in [
            self.fitSymmetricPositiveToolButton,
            self.fitAsymmetricPositiveToolButton,
        ]
        
        if self.sender() in [self.fitSymmetricNegativeToolButton, self.fitSymmetricPositiveToolButton]:
            model = lmfit.models.LorentzianModel() + lmfit.models.ConstantModel()
        else:
            model = lmfit.models.SplitLorentzianModel() + lmfit.models.ConstantModel()

        params = model.make_params(
            amplitude = (y.max() - y.min()) if positive else (y.min() - y.max()),
            c=y.min() if positive else y.max(),
            center = x[np.argmax(y)] if positive else x[np.argmin(y)],
            sigma=0.3*(x.max() - x.min()),
            sigma_r=0.3*(x.max() - x.min()))
        if not positive:
            params['amplitude'].max = 0
        else:
            params['amplitude'].min = 0
        params['sigma'].min = 0
        if 'sigma_r' in params:
            params['sigma_r'].min = 0
        params['center'].min = x.min()
        params['center'].max = x.max()
        result = model.fit(y, params, x=x)

        xplot = np.linspace(x.min(), x.max(), 100)
        yplot = result.eval(x=xplot)
        if self.peakcurve is not None:
            try:
                self.peakcurve.remove()
            except Exception:
                pass
        self.peakcurve = self.axes.plot(xplot, yplot, "r-", lw=1)[0]
        if self.peaktext is not None:
            try:
                self.peaktext.remove()
            except Exception:
                pass
        self.peaktext = self.axes.text(
            result.params['center'].value,
            yplot.max() if positive else yplot.min(),
            f"{result.params['center'].value:g}",
            ha="center",
            va="bottom" if positive else "top",
        )
        if self.peakvline is not None:
            try:
                self.peakvline.remove()
            except Exception:
                pass
        self.peakvline = self.axes.axvline(result.params["center"].value, lw=1, color="r", ls=":")
        self.canvas.draw_idle()
        self.setMotorButtonsEnabled()

        # update the peak fit statistics tree widget
        tw: QtWidgets.QTreeWidget = self.peakStatisticsTreeWidget
        tw.clear()
        for param in result.params:
            tw.addTopLevelItem(QtWidgets.QTreeWidgetItem(tw, [param, f'{result.params[param].value:.3f}']))
            tw.addTopLevelItem(QtWidgets.QTreeWidgetItem(tw, ['Δ'+param, f'{result.params[param].stderr:.3f}' if result.params[param].stderr is not None else '--']))

        # Find the extent of monotonous decrease (positive peak) or increase (negative peak) 
        # on both sides of the maximum. For the sake of simplicity, we make the peak positive
        # and do everything on a concave curve.
        ipeak = np.argmax(y) if positive else np.argmin(-y)
        self.debug(f'{ipeak=}')
        diffy = (y[1:] - y[:-1]) / (x[1:] - x[:-1]) * (1 if positive else -1)
        diffx = 0.5*(x[:-1] + x[1:])
        # if the peak is at the #ipeak-th point in the original curve, 
        # the #ipeak-1 -th point in the derivative is on its left side, 
        # and the #ipeak-th point is on its right side. 
        ileft = ipeak-1
        self.debug(f'{ileft=}')
        while (ileft > 0) and (diffy[ileft-1] >= 0):
            self.debug(f'{ileft=}, {diffy[ileft-1]=} -> ileft -= 1')
            ileft -= 1
        self.debug(f'{ileft=}')
        iright = ipeak
        self.debug(f'{iright=}, {diffy[iright]=}, {diffy[iright-1]=}, {diffy[iright+1]=}')
        while (iright < diffy.size - 2) and (diffy[iright+1] <= 0):
            self.debug(f'{iright=}, {diffy[iright+1]=}')
            iright += 1
        self.debug(f'{iright=}')
        # Find the extent of convexness/concaveness on both sides of the maximum.
        # This is concaveness, as the peak has been "positivized" above.
        # In other words, find the inflexion points
        diff2x = 0.5*(diffx[:-1] + diffx[1:])
        diff2y = (diffy[1:] - diffy[:-1]) / (diffx[1:] - diffx[:-1])
        # if the peak is at the #ipeak-th point in the original curve,
        # the #ipeak-1 -th point in the second derivative is on its left side,
        # and the #ipeak -th point is on its right.
        ileft2 = ipeak-1
        while (ileft2 > 0) and (diff2y[ileft2-1] <= 0):
            ileft2 -= 1
        iright2 = ipeak-1
        while (iright2 < diff2y.size - 2) and (diff2y[iright2+1] <= 0):
            iright2 += 1

        for label, value in [
            ('Akaike info crit', result.aic),
            ('chi-square', result.chisqr),
            ('Bayesian info crit', result.bic),
            ('R-squared', result.rsquared),
            ('Reduced chi-squared', result.redchi),
            ('Schilling test p', cormap_pval(x.size, longest_run(y - result.eval(x=x)))),
            ('Monotonous decrease left (points)', ipeak - ileft),
            ('Monotonous decrease right (points)', iright - ipeak),
            ('Extent of concaveness left (points)', ipeak - 1 - ileft2),
            ('Extent of concaveness right (points)', iright2 - ipeak + 1),
            ('Derivative Schilling test p', cormap_pval(x.size, longest_run(diffy.astype(np.double)))),
        ]:
            tw.addTopLevelItem(QtWidgets.QTreeWidgetItem(tw, [label, str(value)]))

    def peakposition(self) -> Optional[float]:
        if self.peakvline is None:
            return None
        else:
            return self.peakvline.get_xdata()[0]

    def setRecording(self, recording: bool):
        self._recording = recording
        self.cursorVerticalLayout.setEnabled(not recording)
        if self.cursor is not None:
            self.cursor.set_visible(not recording)
            if not recording:
                self.updateCursor()
        for button in [
            self.fitAsymmetricNegativeToolButton,
            self.fitAsymmetricPositiveToolButton,
            self.fitSymmetricNegativeToolButton,
            self.fitSymmetricPositiveToolButton,
            self.cursorToMaximumToolButton,
            self.cursorToMinimumToolButton,
            self.motorToCursorToolButton,
            self.motorToPeakToolButton,
        ]:
            button.setEnabled(not recording)

    def isRecording(self) -> bool:
        return self._recording

    @Slot(name="on_listwidget_itemChanged")
    def setPlotVisibility(self):
        if not self.lines:
            return
        selected = [
            self.listWidget.item(row).text()
            for row in range(self.listWidget.count())
            if self.listWidget.item(row).checkState() == QtCore.Qt.CheckState.Checked
        ]
        for counter in self.lines:
            self.lines[counter].set_visible(counter in selected)
        if self.autoScaleToolButton.isChecked():
            self.axes.relim(visible_only=True)
            self.axes.autoscale_view()
        if self.legend is not None:
            try:
                self.legend.remove()
            except Exception:
                pass
        self.legend = self.axes.legend(
            [
                self.lines[name]
                for name in self.columnnames[1:]
                if self.lines[name].get_visible()
            ],
            [name for name in self.columnnames[1:] if self.lines[name].get_visible()],
        )
        self.legend.set_visible(self.showLegendToolButton.isChecked())
        self.canvas.draw_idle()

    @Slot()
    def on_showLegendToolButton_toggled(self):
        if self.legend is not None:
            self.legend.set_visible(self.showLegendToolButton.isChecked())
            self.canvas.draw_idle()

    @Slot(bool)
    def on_derivativeToolButton_toggled(self, checked: bool):
        self.replot()

    @Slot(name="on_replotToolButton_clicked")
    def replot(self):
        self.axes.clear()
        motor = self.motorname
        xdata = self._data[motor][: self._recorddataptr]
        self.lines = {}
        for counter in self.columnnames[1:]:
            ydata = self._data[counter][: self._recorddataptr]
            if self.derivativeToolButton.isChecked():
                x = 0.5*(xdata[1:]+xdata[:-1])
                y = (ydata[1:] - ydata[:-1]) / (xdata[1:] - xdata[:-1])
            else:
                x = xdata
                y = ydata
            self.lines[counter] = self.axes.plot(
                x, y, ".-", label=counter, linewidth=1
            )[0]
        self.axes.set_xlabel(motor)
        if self.derivativeToolButton.isChecked():
            self.axes.set_ylabel("Derivative of counters (cps/mm)")
        else:
            self.axes.set_ylabel("Counters (cps)")
        self.axes.grid(True, which="both")
        self.axes.set_title(f"{self._title}")
        self.setPlotVisibility()  # this also plots the legend
        self.emphasizeCurrentLine()
        self.cursorHorizontalSlider.setMinimum(0)
        self.cursorHorizontalSlider.setMaximum(len(self._data) - 1)
        if self.isRecording():
            cursorpos = len(xdata) - 1
        else:
            cursorpos = self.cursorHorizontalSlider.value()
        self.cursor = self.axes.axvline(
            self._data[self.motorname][cursorpos], lw=2, ls="dashed", color="k"
        )
        self.cursor.set_visible(not self.isRecording())
        limits = self.axes.axis()
        try:
            self.motorvline = self.axes.axvline(
                float(self.motordevice.getAttribute("Position").read().rvalue),
                lw=2,
                ls="dotted",
                color="g",
            )
            self.axes.axis(limits)
        except Exception as exc:
            self.warning(f"Cannot plot motor vline: {exc}")
            self.motorvline = None

    def handleEvent(
        self, evt_src, evt_type, evt_value: taurus.core.tango.TangoAttrValue
    ):
        if (evt_src == self.motordevice.stateObj) and (
            evt_type == taurus.core.taurusbasetypes.TaurusEventType.Change
        ):
            self.setMotorButtonsEnabled()
        elif (evt_src == self.positionattr) and (
            evt_type == taurus.core.taurusbasetypes.TaurusEventType.Change
        ):
            if self.motorvline is not None:
                newpos = float(evt_value.rvalue)
                self.motorvline.set_xdata([newpos])
                self.canvas.draw_idle()
        else:
            self.debug(f"{evt_src=}")
            self.debug(f"{evt_type=}")
            self.debug(f"{evt_value=}")

    def setScan(
        self,
        data: Optional[np.ndarray] = None,
        motorname: Optional[str] = None,
        title: Optional[str] = None,
    ):
        if self.motordevice is not None:
            self.motordevice.stateObj.removeListener(self)
            self.positionattr.removeListener(self)

        self._data = data
        if self._data is None:
            self.motorname = None
        else:
            if motorname is None:
                motorname = [
                    c for c in self._data.dtype.names if self._data.dtype[c] == float
                ][0]
            self.motorname = motorname
            self._recorddataptr = len(self._data)
        if self._door is not None:
            try:
                self.motordevice = taurus.Device(self.motorname)
            except taurus.core.taurusexception.TaurusException:
                self.motordevice = None
            else:
                self.positionattr = self.motordevice.getAttribute("Position")
                self.positionattr.addListener(self)
                self.motordevice.stateObj.addListener(self)
            try:
                defaultvisibility = self._door.getEnvironment("_PlotScanInfo")[
                    "signalvisibility"
                ]
            except (UnknownEnv, KeyError):
                defaultvisibility = {}
        else:
            defaultvisibility = {}

        self.listWidget.clear()
        if self._data is not None:
            self.listWidget.addItems(self.columnnames[1:])
            for row in range(self.listWidget.count()):
                item = self.listWidget.item(row)
                item.setFlags(
                    QtCore.Qt.ItemFlag.ItemIsEnabled
                    | QtCore.Qt.ItemFlag.ItemNeverHasChildren
                    | QtCore.Qt.ItemFlag.ItemIsSelectable
                    | QtCore.Qt.ItemFlag.ItemIsUserCheckable
                )
                visible = defaultvisibility.get(self.columnnames[1 + row], True)
                item.setCheckState(
                    QtCore.Qt.CheckState.Checked
                    if bool(visible)
                    else QtCore.Qt.CheckState.Unchecked
                )
            self.listWidget.setCurrentItem(self.listWidget.item(0))
            self.cursorHorizontalSlider.setMinimum(0)
            self.cursorHorizontalSlider.setMaximum(len(self._data) - 1)
            self.cursorHorizontalSlider.setValue(len(self._data) // 2)
            self.setWindowTitle(title if title is not None else "Scan Viewer")
            self._title = title

        self.replot()

    @Slot()
    def on_saveDefaultSignalVisibilityPushButton_clicked(self):
        self.debug("Save default signal visibility")
        if self._data is None:
            self.debug("_data is None")
            return
        if self._door is None:
            self.debug("_door is None")
            return
        try:
            plotscaninfo = self._door.getEnvironment("_PlotScanInfo")
        except (UnknownEnv, KeyError):
            plotscaninfo = {"signalvisibility": {}}

        plotscaninfo["signalvisibility"].update(
            {
                label: self.listWidget.item(row).checkState()
                == QtCore.Qt.CheckState.Checked
                for row, label in enumerate(self.columnnames[1:])
            }
        )
        self.debug(f"updated plotscaninfo: {plotscaninfo}")
        self._door.setEnvironment("_PlotScanInfo", plotscaninfo)
