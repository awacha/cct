from typing import Any

import sqlalchemy.orm
from taurus.external.qt.QtCore import Slot

from ..dbutils2 import exposuredb
from .sardanaguibase import SardanaGUIBase, UILoadable


@UILoadable()
class LastFSNIndicator(SardanaGUIBase):
    def setupUi(self):
        with self.getSQLSession(True) as session:
            years = sorted(list(session.scalars(
                sqlalchemy.select(exposuredb.Exposure.year).distinct())))
            self.yearSpinBox.setRange(min(years), max(years))
        self.yearSpinBox.setValue(self.yearSpinBox.maximum())
        self.on_environment_changed('LastReadyFSNs', self._door.getEnvironment('LastReadyFSNs'))

    def on_environment_changed(self, envname: str, envdata: Any):
        if envname == 'LastReadyFSNs':
            year, prefix, fsn = envdata[None]
            self.yearLabel.setText(str(year))
            self.prefixLabel.setText(str(prefix))
            self.lastExposedFSNLcdNumber.display(fsn)
        self.on_prefixComboBox_currentIndexChanged(
            self.prefixComboBox.currentText())
        return super().on_environment_changed(envname, envdata)

    @Slot(str)
    def on_prefixComboBox_currentIndexChanged(self, prefix: str):
        with self.getSQLSession(True) as session:
            maxfsn = session.scalar(
                sqlalchemy.select(
                    sqlalchemy.func.max(exposuredb.Exposure.fsn)).where(
                        exposuredb.Exposure.year == self.yearSpinBox.value(),
                        exposuredb.Exposure.prefix == self.prefixComboBox.currentText()))
            if maxfsn is None:
                self.lastFSNLcdNumber.display(-1)
            else:
                self.lastFSNLcdNumber.display(maxfsn)

    @Slot(int)
    def on_yearSpinBox_valueChanged(self, year: int):
        with self.getSQLSession(True) as session:
            prefixes = sorted(list(session.scalars(
                sqlalchemy.select(exposuredb.Exposure.prefix).distinct())))
        currentprefix = self.prefixComboBox.currentText()
        self.prefixComboBox.blockSignals(True)
        try:
            self.prefixComboBox.clear()
            self.prefixComboBox.addItems(prefixes)
            self.prefixComboBox.setCurrentIndex(
                self.prefixComboBox.findText(currentprefix))
        finally:
            self.prefixComboBox.blockSignals(False)
        if self.prefixComboBox.currentIndex() < 0:
            self.prefixComboBox.setCurrentIndex(0)
