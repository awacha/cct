from .sardanaguibase import UILoadable, SardanaGUIBase

from taurus.external.qt.QtCore import Slot
import taurus


@UILoadable()
class BeamStopCalibrator(SardanaGUIBase):
    def setupUi(self):
        self.on_resetXInFromMotorToolButton_clicked(False)
        self.on_resetXOutFromMotorToolButton_clicked(False)
        self.on_resetYInFromMotorToolButton_clicked(False)
        self.on_resetYOutFromMotorToolButton_clicked(False)
        return super().setupUi()
    
    @property
    def bsinfo(self):
        return self._door.getEnvironment('BeamstopInfo')

    @Slot(bool)
    def on_fetchXInFromMotorToolButton_clicked(self, checked: bool):
        self.xInDoubleSpinBox.setValue(taurus.Device(self.bsinfo['xmotor']).getAttribute('Position').rvalue.magnitude)

    @Slot(bool)
    def on_fetchYInFromMotorToolButton_clicked(self, checked: bool):
        self.yInDoubleSpinBox.setValue(taurus.Device(self.bsinfo['ymotor']).getAttribute('Position').rvalue.magnitude)

    @Slot(bool)
    def on_fetchXOutFromMotorToolButton_clicked(self, checked: bool):
        self.xOutDoubleSpinBox.setValue(taurus.Device(self.bsinfo['xmotor']).getAttribute('Position').rvalue.magnitude)

    @Slot(bool)
    def on_fetchYOutFromMotorToolButton_clicked(self, checked: bool):
        self.yOutDoubleSpinBox.setValue(taurus.Device(self.bsinfo['ymotor']).getAttribute('Position').rvalue.magnitude)

    @Slot(bool)
    def on_resetXInFromMotorToolButton_clicked(self, checked: bool):
        self.xInDoubleSpinBox.setValue(self.bsinfo['in'][0])

    @Slot(bool)
    def on_resetYInFromMotorToolButton_clicked(self, checked: bool):
        self.yInDoubleSpinBox.setValue(self.bsinfo['in'][1])

    @Slot(bool)
    def on_resetXOutFromMotorToolButton_clicked(self, checked: bool):
        self.xOutDoubleSpinBox.setValue(self.bsinfo['out'][0])

    @Slot(bool)
    def on_resetYOutFromMotorToolButton_clicked(self, checked: bool):
        self.yOutDoubleSpinBox.setValue(self.bsinfo['out'][1])

    @Slot(bool)
    def on_saveInToolButton_clicked(self, checked: bool):
        bsinfo = self.bsinfo
        bsinfo['in'] = self.xInDoubleSpinBox.value(), self.yInDoubleSpinBox.value()
        self._door.setEnvironment('BeamstopInfo', bsinfo)

    @Slot(bool)
    def on_saveOutToolButton_clicked(self, checked: bool):
        bsinfo = self.bsinfo
        bsinfo['out'] = self.xOutDoubleSpinBox.value(), self.yOutDoubleSpinBox.value()
        self._door.setEnvironment('BeamstopInfo', bsinfo)
