from .models.motors import Motors
from .sardanaguibase import SardanaGUIBase, UILoadable
from .motormover import MotorMover
from .beamstopcalibrator import BeamStopCalibrator
from .samplechanger import SampleChanger


@UILoadable()
class MotorView(SardanaGUIBase):
    motorsmodel: Motors
    motormover: MotorMover
    beamstopcalibrator: BeamStopCalibrator
    samplechanger: SampleChanger

    def __init__(self, **kwargs):
        super().__init__(**kwargs)

    def setupUi(self):
        self.motorsmodel = Motors(door=self._door.name, parent=None)
        self.treeView.setModel(self.motorsmodel)
        self.motormover = MotorMover(parent=self, door=self._door.name)
        self.beamstopcalibrator = BeamStopCalibrator(parent=self, door=self._door.name)
        self.samplechanger = SampleChanger(parent=self, door=self._door.name)
        self.samplechanger.setEnabled(False)  # ToDo
        self.horizontalLayout.addWidget(self.motormover)
        self.horizontalLayout.addWidget(self.samplechanger)
        self.horizontalLayout.addWidget(self.beamstopcalibrator)
        self.horizontalLayout.addStretch(1)
