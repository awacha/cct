import os
from typing import List, Optional, Tuple

import lmfit
import tango
from matplotlib.axes import Axes
import numpy as np
from matplotlib.backends.backend_qt5agg import (FigureCanvasQTAgg,
                                                NavigationToolbar2QT)
from matplotlib.figure import Figure
from matplotlib.lines import Line2D
from matplotlib.patches import Polygon
from sardana.macroserver.msexception import UnknownEnv
from taurus.external.qt import QtCore, QtGui, QtWidgets
from taurus.external.qt.QtCore import Slot

from ..core2.algorithms.peakfit import PeakType, fitpeak
from ..core2.dataclasses import Scan
from .utils.filebrowsers import browseScanFile
from .sardanaguibase import SardanaGUIBase, UILoadable
from .models.samplestore import SampleStore


@UILoadable()
class CapillarySizer(SardanaGUIBase):
    figure: Figure
    figtoolbar: NavigationToolbar2QT
    canvas: FigureCanvasQTAgg
    axes: Axes
    scan: Optional[Scan] = None
    positive: Tuple[float, float] = (.0, .0)
    negative: Tuple[float, float] = (.0, .0)
    line: Line2D = None
    positivepeakline: Optional[Line2D] = None
    negativepeakline: Optional[Line2D] = None
    profilefitline: Optional[Line2D] = None
    center: Tuple[float, float] = (.0, .0)
    thickness: Tuple[float, float] = (.0, .0)
    peaksFitWidgets: List[Tuple[QtWidgets.QRadioButton,
                                QtWidgets.QRadioButton, QtWidgets.QLabel, QtWidgets.QLabel]]
    peaksFitLeftButtonGroup: QtWidgets.QButtonGroup
    peaksFitRightButtonGroup: QtWidgets.QButtonGroup
    fittedpeaks: List[Tuple[float, float, float]] = None
    centerline: Optional[Line2D] = None
    thicknessspan: Optional[Polygon] = None
    samplestore: SampleStore

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.setLogLevel(self.Info)

    def setupUi(self):
        if self._door is not None:
            scanfiles = sorted(
                [x for x in self._door.getEnvironment('ScanFile')
                 if x.rsplit('.', 1)[1].lower() in ['spec', 'dat', 'h5', 'hdf5', 'nxs']],
                key=lambda fn: ['spec', 'dat', 'h5', 'hdf5', 'nxs'].index(fn.rsplit('.', 1)[1].lower()))

            self.scanFileLineEdit.setText(
                os.path.join(self._door.getEnvironment('ScanDir'),
                             scanfiles[0]))
            try:
                self.on_reloadScanFileToolButton_clicked()
            except Exception:
                pass
        self.samplestore = SampleStore(parent=self, door=self._door.name)
        self.sampleNameComboBox.setModel(self.samplestore.sortedmodel)
        self.figure = Figure(constrained_layout=True)
        self.canvas = FigureCanvasQTAgg(self.figure)
        self.figtoolbar = NavigationToolbar2QT(self.canvas, self)
        self.figureVerticalLayout.addWidget(self.canvas)
        self.figureVerticalLayout.addWidget(self.figtoolbar)
        self.axes = self.figure.add_subplot(
            self.figure.add_gridspec(1, 1)[:, :])
        self.canvas.setSizePolicy(
            QtWidgets.QSizePolicy.Policy.MinimumExpanding, QtWidgets.QSizePolicy.Policy.MinimumExpanding)
        self.canvas.draw_idle()

        self.fitNegativeToolButton.clicked.connect(self.fitPeak)
        self.fitPositiveToolButton.clicked.connect(self.fitPeak)

        self.negativeValDoubleSpinBox.valueChanged.connect(
            self.setValuesFromSpinboxes)
        self.positiveValDoubleSpinBox.valueChanged.connect(
            self.setValuesFromSpinboxes)
        self.negativeErrDoubleSpinBox.valueChanged.connect(
            self.setValuesFromSpinboxes)
        self.positiveErrDoubleSpinBox.valueChanged.connect(
            self.setValuesFromSpinboxes)
        self.updateCenterToolButton.setIcon(
            QtWidgets.QApplication.instance().style().standardIcon(QtWidgets.QStyle.StandardPixmap.SP_ArrowRight))
        self.updateThicknessToolButton.setIcon(
            QtWidgets.QApplication.instance().style().standardIcon(QtWidgets.QStyle.StandardPixmap.SP_ArrowRight))
        self.signalNameComboBox.setCurrentIndex(0)

        self.peaksFitWidgets = []
        self.peaksFitLeftButtonGroup = QtWidgets.QButtonGroup(self)
        self.peaksFitRightButtonGroup = QtWidgets.QButtonGroup(self)
        self.peaksFitLeftButtonGroup.idToggled.connect(
            self.on_peaksFitLeftButtonGroup_idToggled)
        self.peaksFitRightButtonGroup.idToggled.connect(
            self.on_peaksFitRightButtonGroup_idToggled)
        for i in range(4):
            left = QtWidgets.QRadioButton(self.peaksFitTab)
            right = QtWidgets.QRadioButton(self.peaksFitTab)
            left.setText('')
            right.setText('')
            self.peaksFitLeftButtonGroup.addButton(left, i)
            self.peaksFitRightButtonGroup.addButton(right, i)
            valuelabel = QtWidgets.QLabel(self.peaksFitTab)
            uncertaintylabel = QtWidgets.QLabel(self.peaksFitTab)
            valuelabel.setText('--')
            uncertaintylabel.setText('--')
            self.peaksFitGridLayout.addWidget(
                left, i+1, 0, alignment=QtCore.Qt.AlignmentFlag.AlignCenter)
            self.peaksFitGridLayout.addWidget(
                right, i+1, 1, alignment=QtCore.Qt.AlignmentFlag.AlignCenter)
            self.peaksFitGridLayout.addWidget(
                valuelabel, i+1, 2, alignment=QtCore.Qt.AlignmentFlag.AlignCenter)
            self.peaksFitGridLayout.addWidget(
                uncertaintylabel, i + 1, 3, alignment=QtCore.Qt.AlignmentFlag.AlignCenter)
            self.peaksFitWidgets.append(
                (left, right, valuelabel, uncertaintylabel))

        self.peaksFitFitPushButton.clicked.connect(
            self.on_peaksFitFitPushButton_clicked)
        self.replot()

    def on_reloadScanFileToolButton_clicked(self):
        scanfile = self.scanFileLineEdit.text()
        indices = Scan.scanindicesinfile(scanfile)
        self.scanIndexSpinBox.setRange(min(indices), max(indices))
        self.scanIndexSpinBox.setValue(max(indices))

    @Slot()
    def on_loadScanFileToolButton_clicked(self):
        scanfile = browseScanFile(self)
        if scanfile is None:
            return
        self.scanFileLineEdit.setText(scanfile)
        self.on_reloadScanFileToolButton_clicked()

    @Slot()
    def on_refreshSampleListToolButton_clicked(self) -> None:
        currentsample = self.sampleNameComboBox.currentText()
        assert isinstance(self.sampleNameComboBox, QtWidgets.QComboBox)
        self.sampleNameComboBox.blockSignals(True)
        try:
            self.samplestore.loadFromDB()
            self.sampleNameComboBox.setCurrentIndex(
                self.sampleNameComboBox.findText(currentsample))
        finally:
            self.sampleNameComboBox.blockSignals(False)
        if (self.sampleNameComboBox.currentIndex() < 0):
            self.sampleNameComboBox.setCurrentIndex(0)

    @Slot(int, bool)
    def on_peaksFitLeftButtonGroup_idToggled(self, id: int, checked: bool):
        if checked:
            self.negativeValDoubleSpinBox.setValue(self.fittedpeaks[id][0])
            self.negativeErrDoubleSpinBox.setValue(self.fittedpeaks[id][1])

    @Slot(int, bool)
    def on_peaksFitRightButtonGroup_idToggled(self, id: int, checked: bool):
        if checked:
            self.positiveValDoubleSpinBox.setValue(self.fittedpeaks[id][0])
            self.positiveErrDoubleSpinBox.setValue(self.fittedpeaks[id][1])

    def resetMultiPeakFitter(self):
        self.fittedpeaks = []
        for i in range(len(self.fittedpeaks), 4):
            leftradio, rightradio, valuelabel, uncertaintylabel = self.peaksFitWidgets[i]
            valuelabel.setText('--')
            uncertaintylabel.setText('--')
            pal: QtGui.QPalette = valuelabel.palette()
            pal.setColor(QtGui.QPalette.WindowText,
                         self.label.palette().color(QtGui.QPalette.WindowText))
            valuelabel.setPalette(pal)
            pal: QtGui.QPalette = uncertaintylabel.palette()
            pal.setColor(QtGui.QPalette.WindowText,
                         self.label.palette().color(QtGui.QPalette.WindowText))
            uncertaintylabel.setPalette(pal)
            leftradio.setChecked(False)
            rightradio.setChecked(False)
            valuelabel.setEnabled(False)
            uncertaintylabel.setEnabled(False)
            leftradio.setEnabled(False)
            rightradio.setEnabled(False)

    @Slot()
    def on_peaksFitFitPushButton_clicked(self):
        x = self.line.get_xdata()
        y = self.line.get_ydata()
        xmin, xmax, ymin, ymax = self.axes.axis()
        idx = np.isfinite(x) & np.isfinite(y) & (x >= xmin) & (x <= xmax)
        x, y = x[idx], y[idx]
        if not self.derivativeToolButton.isChecked():
            # ascertain that we are fitting the derivative of the scan
            x_ = 0.5*(x[1:] + x[:-1])
            y_ = (y[1:] - y[:-1]) / (x[1:] - x[:-1])
            x, y = x_, y_

        # now find four peaks if possible
        results = []
        # take a copy of y, since we will modify it
        yorig = np.array(y).copy()
        for i in range(4):
            # find another peak: take the highest (or deepest) one
            if abs(y.min()) > y.max():
                sign = -1
            else:
                sign = 1
            # find first guesses for the position, height and width
            ipos_crude = np.argmax(sign * y)
            height_crude = sign * y[ipos_crude]
            ihalfleft = ipos_crude
            while (ihalfleft > 0) and (sign * y[ihalfleft] > height_crude * 0.5):
                ihalfleft -= 1
            ihalfright = ipos_crude
            while (ihalfright < len(x) - 1) and (sign * y[ihalfright] > height_crude * 0.5):
                ihalfright += 1
            width_crude = x[ihalfright] - x[ihalfleft]
            # refine the parameters of the peak using least-squares fitting in its vicinity
            model = lmfit.Model(lmfit.models.gaussian)
            params = model.make_params(
                amplitude=height_crude, sigma=width_crude * 0.5, center=x[ipos_crude])
            result = model.fit(y[ihalfleft:ihalfright + 1],
                               params=params, x=x[ihalfleft:ihalfright + 1])
            vd = result.params.valuesdict()
            if (vd['center'] < x.min()) or (vd['center'] > x.max()):
                # if the peak went out of the data range, there are no more peaks.
                break
            elif results and (abs(vd['amplitude']) < abs(results[0]['amplitude']) * 0.5):
                # if the peak is too low / shallow: this is noise, not a peak.
                break
            results.append(vd)
            # subtract the peak curve from the dataset and iterate again
            y = y - result.eval(x=x)
        # now fit the whole dataset using the peaks
        fullmodel = None
        params_initdict = {}
        for i, vd in enumerate(results):
            peakmodel = lmfit.Model(
                lmfit.models.gaussian, prefix=f'peak_{i:03d}_')
            if fullmodel is None:
                fullmodel = peakmodel
            else:
                fullmodel = fullmodel + peakmodel
            for pname, pval in vd.items():
                params_initdict[f'peak_{i:03d}_{pname}'] = pval
        params = fullmodel.make_params(**params_initdict)
        # first fit with the centers fixed, adjust only heights and widths
        for name in params:
            if name.endswith('_center'):
                params[name].vary = False
        result = fullmodel.fit(yorig, params, x=x)
        # free the centers and repeat the fit
        params = result.params
        for name in params:
            params[name].vary = True
        result = fullmodel.fit(yorig, params, x=x)

        self.resetMultiPeakFitter()
        for i in range(4):
            try:
                value = result.params[f'peak_{i:03d}_center'].value
                unc = result.params[f'peak_{i:03d}_center'].stderr
                height = result.params[f'peak_{i:03d}_amplitude'].value
                self.fittedpeaks.append((value, unc, height))
            except KeyError:
                pass

        self.fittedpeaks = list(sorted(self.fittedpeaks))

        for i, (value, unc, height) in enumerate(self.fittedpeaks):
            leftradio, rightradio, valuelabel, uncertaintylabel = self.peaksFitWidgets[i]
            valuelabel.setText(f'{value:.4f}')
            valuelabel.setEnabled(True)
            pal: QtGui.QPalette = valuelabel.palette()
            pal.setColor(QtGui.QPalette.WindowText, QtGui.QColor(
                QtCore.Qt.GlobalColor.red if height > 0 else QtCore.Qt.GlobalColor.blue))
            valuelabel.setPalette(pal)
            pal: QtGui.QPalette = uncertaintylabel.palette()
            pal.setColor(QtGui.QPalette.WindowText, QtGui.QColor(
                QtCore.Qt.GlobalColor.red if height > 0 else QtCore.Qt.GlobalColor.blue))
            uncertaintylabel.setPalette(pal)
            uncertaintylabel.setText(f'{unc:.4f}' if unc is not None else '--')
            uncertaintylabel.setEnabled(True)
            leftradio.setEnabled(True)
            rightradio.setEnabled(True)

        if (len(self.fittedpeaks) == 4) and (self.fittedpeaks[0][2] > 0) and \
            (self.fittedpeaks[1][2] < 0) and (self.fittedpeaks[2][2] > 0) and \
                (self.fittedpeaks[3][2] < 0):
            # this is a capillary scan
            self.peaksFitLeftButtonGroup.button(1).setChecked(True)
            self.peaksFitRightButtonGroup.button(2).setChecked(True)
        elif len(self.fittedpeaks) == 2:
            self.peaksFitLeftButtonGroup.button(0).setChecked(True)
            self.peaksFitRightButtonGroup.button(1).setChecked(True)
        else:
            self.peaksFitLeftButtonGroup.button(0).setChecked(True)
            self.peaksFitRightButtonGroup.button(1).setChecked(True)
        self.on_peaksFitLeftButtonGroup_idToggled(
            self.peaksFitLeftButtonGroup.checkedId(), True)
        self.on_peaksFitRightButtonGroup_idToggled(
            self.peaksFitRightButtonGroup.checkedId(), True)
        return result

    def getFittingData(self) -> Tuple[np.ndarray, np.ndarray]:
        x = self.line.get_xdata()
        y = self.line.get_ydata()
        xmin, xmax, ymin, ymax = self.axes.axis()
        idx = np.logical_and(
            np.logical_and(x >= xmin, x <= xmax),
            np.logical_and(y >= ymin, y <= ymax)
        )
        return x[idx], y[idx]

    @Slot()
    def fitPeak(self):
        if self.line is None:
            return
        x = self.line.get_xdata()
        y = self.line.get_ydata()
        xmin, xmax, ymin, ymax = self.axes.axis()
        idx = np.logical_and(
            np.logical_and(x >= xmin, x <= xmax),
            np.logical_and(y >= ymin, y <= ymax)
        )
        if self.sender() == self.fitNegativeToolButton:
            y = -y
        try:
            # do not use y error bars: if y<0, y**0.5 is NaN, which will break the fitting routine
            pars, covar, peakfunc = fitpeak(
                x[idx], y[idx], None, None, PeakType.AsymmetricLorentzian)
        except ValueError as ve:
            QtWidgets.QMessageBox.critical(self, 'Error while fitting',
                                           f'Cannot fit peak, please try another range. The error message was: {ve}')
            return
        self.debug(f'Peak parameters: {pars}')
        self.debug(f'Covariance matrix: {covar}')
        xfit = np.linspace(x[idx].min(), x[idx].max(), 100)
        yfit = peakfunc(xfit)
        if self.profilefitline is not None:
            try:
                self.profilefitline.remove()
            except Exception:
                pass
            self.profilefitline = None
        if self.sender() == self.fitNegativeToolButton:
            if self.negativepeakline is None:
                self.negativepeakline = self.axes.plot(
                    xfit, - yfit, 'b-', lw=3)[0]
            else:
                self.negativepeakline.set_xdata(xfit)
                self.negativepeakline.set_ydata(-yfit)
            self.negative = (pars[1], covar[1, 1] ** 0.5)
            self.negativeValDoubleSpinBox.blockSignals(True)
            self.negativeErrDoubleSpinBox.blockSignals(True)
            self.negativeValDoubleSpinBox.setValue(pars[1])
            self.negativeErrDoubleSpinBox.setValue(covar[1, 1] ** 0.5)
            self.negativeValDoubleSpinBox.blockSignals(False)
            self.negativeErrDoubleSpinBox.blockSignals(False)
        else:
            if self.positivepeakline is None:
                self.positivepeakline = self.axes.plot(
                    xfit, yfit, 'r-', lw=3)[0]
            else:
                self.positivepeakline.set_xdata(xfit)
                self.positivepeakline.set_ydata(yfit)
            self.positive = (pars[1], covar[1, 1] ** 0.5)
            self.positiveValDoubleSpinBox.blockSignals(True)
            self.positiveErrDoubleSpinBox.blockSignals(True)
            self.positiveValDoubleSpinBox.setValue(pars[1])
            self.positiveErrDoubleSpinBox.setValue(covar[1, 1] ** 0.5)
            self.positiveValDoubleSpinBox.blockSignals(False)
            self.positiveErrDoubleSpinBox.blockSignals(False)
        self.canvas.draw_idle()
        self.recalculate()

    @Slot()
    def setValuesFromSpinboxes(self):
        self.positive = (self.positiveValDoubleSpinBox.value(),
                         self.positiveErrDoubleSpinBox.value())
        self.negative = (self.negativeValDoubleSpinBox.value(),
                         self.negativeErrDoubleSpinBox.value())
        self.recalculate()

    @Slot()
    def recalculate(self):
        positionval = 0.5 * (self.positive[0] + self.negative[0])
        positionerr = 0.5 * \
            (self.positive[1] ** 2 + self.negative[1] ** 2) ** 0.5
        thicknessval = abs(self.positive[0] - self.negative[0])
        thicknesserr = (self.positive[1] ** 2 + self.negative[1] ** 2) ** 0.5
        self.center = (positionval, positionerr)
        self.thickness = (thicknessval, thicknesserr)
        self.newPositionLabel.setText(
            f'{positionval:.4f} \xb1 {positionerr:.4f}')
        self.newThicknessLabel.setText(
            f'{thicknessval:.4f} \xb1 {thicknesserr:.4f} mm')
        if self.centerline is not None:
            try:
                self.centerline.remove()
            except Exception:
                pass
            self.centerline = None
        if self.thicknessspan is not None:
            try:
                self.thicknessspan.remove()
            except Exception:
                pass
            self.thicknessspan = None
        ax = self.axes.axis()
        self.centerline = self.axes.axvline(
            self.center[0], ls='--', color='green')
        self.thicknessspan = self.axes.axvspan(
            self.center[0] - self.thickness[0]*0.5, self.center[0] + self.thickness[0]*0.5, color='green', alpha=0.3)
        self.axes.axis(ax)
        self.canvas.draw_idle()

    @property
    def dburl(self) -> str:
        return tango.Database().get_property('CREDO', 'dburl')['dburl'][0]

    @Slot(int)
    def on_sampleNameComboBox_currentIndexChanged(self, index: int = None):
        if self.sampleNameComboBox.currentIndex() < 0:
            return
        if self.scan is not None:
            is_x = self.is_x_motor(self.scan.motorname)
        else:
            is_x = None
        assert isinstance(self.samplestore, SampleStore)
        sample = self.samplestore.get(
            self.sampleNameComboBox.currentText(), True)
        if is_x is True:
            self.oldPositionLabel.setText(
                f'{sample["positionx"]:.4f} \xb1 {sample["positionx_error"]:.4f}')
        elif is_x is False:
            self.oldPositionLabel.setText(
                f'{sample["positiony"]:.4f} \xb1 {sample["positiony_error"]:.4f}')
        self.oldThicknessLabel.setText(
            f'{sample["thickness"] * 10.0:.4f} \xb1 {sample["thickness_error"] * 10.0:.4f} mm')

    @Slot(int)
    def on_scanIndexSpinBox_valueChanged(self, value: int):
        scanfile = self.scanFileLineEdit.text()
        scannr = self.scanIndexSpinBox.value()
        if scanfile.endswith('.h5') or scanfile.endswith('.nxs') or \
                scanfile.endswith('.nx5') or scanfile.endswith('.hdf5'):
            self.scan = Scan.fromh5file(scanfile, scannr)
        else:
            self.scan = Scan.fromspecfile(scanfile, scannr)

        self.signalNameComboBox.blockSignals(True)
        oldsignal = self.signalNameComboBox.currentText()
        self.signalNameComboBox.clear()
        self.signalNameComboBox.addItems(self.scan.columnnames[2:])
        self.signalNameComboBox.setCurrentIndex(
            self.signalNameComboBox.findText(oldsignal))
        self.signalNameComboBox.blockSignals(False)
        self.on_signalNameComboBox_currentIndexChanged()

    @Slot(int)
    def on_signalNameComboBox_currentIndexChanged(self, index: int = None):
        if self.signalNameComboBox.currentIndex() >= 0:
            self.replot()
            self.figtoolbar.update()
        # we force a refresh of the sample: if the scan motor changed from x to y, the appropriate
        # sample position needs to be shown.
        self.on_sampleNameComboBox_currentIndexChanged()

    @Slot(bool)
    def on_derivativeToolButton_toggled(self, state: bool):
        return self.on_signalNameComboBox_currentIndexChanged(self.signalNameComboBox.currentIndex())

    @Slot(bool)
    def on_reloadToolButton_clicked(self, state: bool):
        return self.on_scanIndexSpinBox_valueChanged(self.scanIndexSpinBox.value())

    @Slot()
    def replot(self):
        if self.scan is None:
            return
        if self.centerline is not None:
            try:
                self.centerline.remove()
            except Exception:
                pass
            self.centerline = None
        if self.thicknessspan is not None:
            try:
                self.thicknessspan.remove()
            except Exception:
                pass
            self.thicknessspan = None
        if self.positivepeakline is not None:
            try:
                self.positivepeakline.remove()
            except Exception:
                pass
            self.positivepeakline = None
        if self.negativepeakline is not None:
            try:
                self.negativepeakline.remove()
            except Exception:
                pass
            self.negativepeakline = None
        self.debug(
            f'{self.scan.motorname=}, {self.signalNameComboBox.currentText()=}')
        x = self.scan[self.scan.motorname]
        y = self.scan[self.signalNameComboBox.currentText()]
        self.debug(f'{x=}, {y=}')
        self.debug(f'{self.scan._data=}, {self.scan._nextpoint=}')
        if self.derivativeToolButton.isChecked():
            y = (y[1:] - y[:-1]) / (x[1:] - x[:-1])
            x = 0.5 * (x[1:] + x[:-1])
        if self.line is None:
            self.line = self.axes.plot(x, y, 'k.-')[0]
        else:
            self.line.set_xdata(x)
            self.line.set_ydata(y)
            self.axes.relim()
            self.axes.autoscale(True)
        self.axes.set_xlabel(self.scan.motorname)
        self.axes.set_ylabel(
            'Derivative of ' + self.signalNameComboBox.currentText()
            if self.derivativeToolButton.isChecked() else self.signalNameComboBox.currentText())
        self.axes.set_title(self.scan.comment)
        self.axes.grid(True, which='both')
        self.canvas.draw_idle()
        self.fittedpeaks

    def is_x_motor(self, motorname: str) -> Optional[bool]:
        if self._door is not None:
            try:
                env = self._door.getEnvironment('SampleInfo')
                if env['motx'] == motorname:
                    return True
                elif env['moty'] == motorname:
                    return False
            except (KeyError, UnknownEnv):
                return None
        return None

    @Slot()
    def on_updateCenterToolButton_clicked(self):
        if self._door is None:
            QtWidgets.QMessageBox.critical(
                self, 'Error',
                'Cannot set sample position: no Sardana door defined')

        positionval = 0.5 * (self.positive[0] + self.negative[0])
        positionerr = 0.5 * \
            (self.positive[1] ** 2 + self.negative[1] ** 2) ** 0.5
        is_x = self.is_x_motor(self.scan.motorname)
        if is_x is None:
            # ask the user which direction this is
            msgbox = QtWidgets.QMessageBox(self.window())
            msgbox.setIcon(QtWidgets.QMessageBox.Icon.Question)
            msgbox.setWindowTitle('Select direction')
            msgbox.setText(
                'Please select X or Y direction to save the determined sample center to:')
            btnX = msgbox.addButton(
                'X', QtWidgets.QMessageBox.ButtonRole.YesRole)
            btnY = msgbox.addButton(
                'Y', QtWidgets.QMessageBox.ButtonRole.NoRole)
            msgbox.addButton(QtWidgets.QMessageBox.StandardButton.Cancel)
            result = msgbox.exec_()
            self.debug(f'{result=}')
            if msgbox.clickedButton() == btnX:
                is_x = True
            elif msgbox.clickedButton() == btnY:
                is_x = False
            else:
                return
        try:
            assert isinstance(self.samplestore, SampleStore)
            if is_x is True:
                posattr = 'positionx'
            else:
                posattr = 'positiony'
            self.samplestore.updateSample(
                self.sampleNameComboBox.currentText(), posattr, positionval)
            self.samplestore.updateSample(
                self.sampleNameComboBox.currentText(), posattr+'_error', positionerr)
            self.info(
                f'Updated {"X" if is_x else "Y"} '
                f'position of sample {self.sampleNameComboBox.currentText()} '
                f'to {positionval:.4f} \xb1 {positionerr:.4f}.')
        except Exception as exc:
            QtWidgets.QMessageBox.critical(
                self, 'Error',
                f'Cannot set position for sample {self.sampleNameComboBox.currentText()}: {exc}')
        self.on_sampleNameComboBox_currentIndexChanged()

    @Slot()
    def on_updateThicknessToolButton_clicked(self):
        thicknessval = abs(self.positive[0] - self.negative[0]) / 10.0
        thicknesserr = (self.positive[1] ** 2 +
                        self.negative[1] ** 2) ** 0.5 / 10.0

        if self._door is None:
            QtWidgets.QMessageBox.critical(
                self, 'Error',
                'Cannot set sample thickness: no Sardana door defined')
        else:
            try:
                self.samplestore.updateSample(self.sampleNameComboBox.currentText(
                ), 'thickness', thicknessval)
                self.samplestore.updateSample(self.sampleNameComboBox.currentText(
                ), 'thickness_error', thicknesserr)
#                self.samplestore.updateSample(self.sampleNameComboBox.currentText(), 'thickness_error', thicknesserr)
                self.info(
                    f'Updated thickness of sample {self.sampleNameComboBox.currentText()} to '
                    f'{thicknessval:.4f} \xb1 {thicknesserr:.4f} cm.')
            except Exception as exc:
                QtWidgets.QMessageBox.critical(
                    self, 'Error',
                    f'Cannot set thickness for sample {self.sampleNameComboBox.currentText()}: {exc}')
        self.on_sampleNameComboBox_currentIndexChanged()
