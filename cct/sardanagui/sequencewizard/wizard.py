import datetime

from taurus import Logger
from taurus.external.qt import QtWidgets
from taurus.external.qt.QtCore import Slot

from ..sardanaguibase import UILoadable
from ..utils.filebrowsers import getSaveFile
from .calibrants import CalibrantsPage
from .cyclemodel import IlluminationMode, PeristalticMode
from .cyclespage import Cycle, CyclesPage
from .endpage import EndPage
from .initialization import InitPage
from .resultpage import ResultPage
from .samples import SamplesPage
from .startpage import StartPage


@UILoadable()
class SequenceWizard(QtWidgets.QWizard, Logger):
    startpage: StartPage
    initpage: InitPage
    calibrantspage: CalibrantsPage
    samplespage: SamplesPage
    cyclespage: CyclesPage
    endpage: EndPage
    resultpage: ResultPage

    def __init__(self, **kwargs):
        self.door = kwargs.pop("door")
        self.mainwindow = kwargs.pop("mainwindow")
        super().__init__(**kwargs)
        self.call__init__(Logger)
        self.loadUi()

        self.startpage = StartPage(parent=self)
        self.addPage(self.startpage)
        self.initpage = InitPage(parent=self)
        self.addPage(self.initpage)
        self.calibrantspage = CalibrantsPage(parent=self, door=self.door)
        self.addPage(self.calibrantspage)
        self.samplespage = SamplesPage(parent=self, door=self.door)
        self.addPage(self.samplespage)
        self.cyclespage = CyclesPage(parent=self)
        self.addPage(self.cyclespage)
        self.endpage = EndPage(parent=self)
        self.addPage(self.endpage)
        self.resultpage = ResultPage(parent=self)
        self.addPage(self.resultpage)
        self.currentIdChanged.connect(self.on_currentIdChanged)
        self.finished.connect(self.on_finished)

    @Slot(int)
    def on_currentIdChanged(self, currentId: int):
        if self.page(currentId) is self.resultpage:
            self.resultpage.setScript(self.script())

    def script(self) -> str:
        self.debug(f'{self.field("darkSample")}')
        t = f"# CREDO experiment sequence generated on {datetime.datetime.now()}\n"
        t += "# Run it in Spock with the command\n"
        t += "#    saxsseq <absolute path to script file>\n"
        t += "\n\n"
        # write the config section

        t += "[config]\n"
        t += "iterations = 1\n"
        t += "breakflagname = 'Break'\n"
        t += "advanceflagname = 'Advance'\n"
        t += (
            "reordersamples = "
            + ("true" if self.field("orderSamples") else "false")
            + "\n"
        )
        t += "\n"
        t += "[init]\n"
        t += (
            "beamstopin = true\n"
            if self.field("beamstopIn")
            else "beamstopin = false\n"
        )
        if self.field("initializeXraySource"):
            t += f'xraypower = "{self.field("initializeXraySourceTo")}"\n'

        t += (
            "closeshutter = true\n"
            if self.field("closeShutterBefore")
            else "closeshutter = false\n"
        )
        t += (
            "openshutter = true\n"
            if self.field("openShutterBefore")
            else "openshutter = false\n"
        )
        t += "\n"
        if self.field("trimDetector"):
            t += "[trim]\n"
            t += f'energy = {self.field("threshold")}  # eV\n'
            t += f'gain = "{self.field("gain")}G"\n'
            t += "\n"

        t += "[finish]\n"
        if self.field("setXraySourcePowerAfter"):
            t += f'xraypower = "{self.field("setXraySourcePowerAfterTo")}"\n'

        t += "\n"

        t += "[references]\n"
        t += f'dark.name = "{self.calibrantspage.darkSample()}"\n'
        t += f'dark.time = {self.field("darkTime"):.3f}  # sec\n'
        t += f'empty.name = "{self.calibrantspage.emptySample()}"\n'
        t += f'empty.time = {self.field("emptyTime"):.3f}  # sec\n'
        t += f'absint.name = "{self.calibrantspage.intensitySample()}"\n'
        t += f'absint.time = {self.field("absintTime"):.3f}  # sec\n'
        t += f'qrange.name = "{self.calibrantspage.qSample()}"\n'
        t += f'qrange.time = {self.field("qCalibrantTime"):.3f}  # sec\n'
        t += "\n"

        for title, exptime, count, settings in self.samplespage.exposures():
            t += "[[sample]]\n"
            t += f'name = "{title}"\n'
            t += f"time = {exptime:.3f}  #sec\n"
            t += f"repeats = {count}\n"
            if 'matrix' in settings:
                for key, value in settings["matrix"].items():
                    if isinstance(value, float):
                        t += f"matrix.{key} = {value:.4f}\n"
                    elif isinstance(value, bool):
                        t += f"matrix.{key} = {'true' if value else 'false'}\n"
                    else:
                        assert False
            t += "\n"

        for cycle in self.cyclespage.cycles():
            assert isinstance(cycle, Cycle)
            t += "[[cycle]]\n"
            t += f'iterations = {cycle.acq_iterations if cycle.acq_iterations is not None else "inf"}\n'
            if cycle.do_references:
                if cycle.references_only_once:
                    t += "refs = \"once\"\n"
                else:
                    t += "refs = \"always\"\n"
            else:
                t += "refs = \"never\"\n"
            t += "samples = true\n" if cycle.do_samples else "samples = false\n"

            if cycle.settemperature is not None:
                t += f"temperature.set = {cycle.settemperature:.3f}\n"
                t += f"temperature.stabilitycheckradius = {cycle.temptolradius:.3f}  # °C\n"
                t += (
                    f"temperature.stabilitychecktime = {cycle.temptoltime:.3f}  # sec\n"
                )
            if cycle.peristalticmode != PeristalticMode.NoPeristaltic:
                t += f"peristaltic.flowrate = {cycle.peristalticflowrate:.3f}  # rpm\n"
                t += f"peristaltic.dispensetime = {cycle.peristalticdispense:.3f}  # sec\n"
                if cycle.peristalticmode == PeristalticMode.Continuous:
                    t += "peristaltic.continuous = true\n"
                elif cycle.peristalticmode == PeristalticMode.Dispense:
                    t += "peristaltic.continuous = false\n"
                else:
                    assert False
            if cycle.illuminationmode != IlluminationMode.NoIllumination:
                t += f"illumination.level = {cycle.illuminationlevel:.2f}  # %\n"
                t += f'illumination.onlywhileexposing = {"true" if IlluminationMode.DuringExposure else "false"}'

        return t

    @Slot(int)
    def on_finished(self, result: int):
        if result == self.Accepted:
            filename = getSaveFile(
                self,
                "Save sequence configuration to",
                "",
                filters="Sequence files (*.toml);;All files (*)",
                defaultsuffix=".toml",
            )
            if filename:
                with open(filename, "wt") as f:
                    f.write(self.resultpage.script())
