from taurus.external.qt import QtWidgets
from taurus import Logger
from ..sardanaguibase import UILoadable


@UILoadable()
class EndPage(QtWidgets.QWizardPage, Logger):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.loadUi()
        self.xRaySourcePowerModeComboBox.addItems(['off', 'standby', 'full'])
        self.registerField(
            'iterationCount', self.iterationCountSpinBox,
            'value', self.iterationCountSpinBox.valueChanged)
        self.registerField(
            'setXraySourcePowerAfter', self.setXraySourcePowerModeCheckBox,
            'checked', self.setXraySourcePowerModeCheckBox.toggled)
        self.registerField(
            'setXraySourcePowerAfterTo', self.xRaySourcePowerModeComboBox,
            'currentText', self.xRaySourcePowerModeComboBox.currentTextChanged)
