from taurus.external.qt import QtWidgets
from taurus import Logger
from ..sardanaguibase import UILoadable
from ..utils import ScriptEditor


@UILoadable()
class ResultPage(QtWidgets.QWizardPage, Logger):
    def __init__(self, parent: QtWidgets.QWidget | None = ...) -> None:
        super().__init__(parent)
        self.loadUi()
        self.scriptEditor = ScriptEditor(self)
        self.setLayout(QtWidgets.QVBoxLayout())
        self.layout().addWidget(self.scriptEditor)

    def setScript(self, text):
        self.scriptEditor.setPlainText(text)

    def script(self):
        return self.scriptEditor.toPlainText()
