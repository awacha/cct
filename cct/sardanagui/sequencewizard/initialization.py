from taurus.external.qt import QtWidgets
from taurus.external.qt.QtCore import Slot
from taurus import Logger
from ..sardanaguibase import UILoadable


@UILoadable()
class InitPage(QtWidgets.QWizardPage, Logger):
    thresholdranges = {'low': (6685, 20202),
                       'mid': (4425, 14328),
                       'high': (3814, 11614), }

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.loadUi()
        self.gainComboBox.addItems(list(self.thresholdranges))
        self.gainComboBox.currentIndexChanged.connect(self.onGainChanged)
        self.gainComboBox.setCurrentIndex(
            self.gainComboBox.findText(list(self.thresholdranges)[2]))
        self.thresholdSpinBox.setValue(4024)
        self.xRayPowerComboBox.addItems(['off', 'standby', 'full'])
        self.xRayPowerComboBox.setCurrentIndex(2)
        self.registerField('closeShutterBefore', self.closeShutterCheckBox,
                           'checked', self.closeShutterCheckBox.toggled)
        self.registerField('beamstopIn', self.beamstopInCheckBox,
                           'checked', self.beamstopInCheckBox.toggled)
        self.registerField('initializeXraySourceTo', self.xRayPowerComboBox,
                           'currentText', self.xRayPowerComboBox.currentTextChanged)
        self.registerField('initializeXraySource', self.xRayPowerCheckBox,
                           'checked', self.xRayPowerCheckBox.toggled)
        self.registerField('trimDetector', self.trimCheckBox,
                           'checked', self.trimCheckBox.toggled)
        self.registerField('gain', self.gainComboBox,
                           'currentText', self.gainComboBox.currentTextChanged)
        self.registerField('threshold', self.thresholdSpinBox,
                           'value', self.thresholdSpinBox.valueChanged)
        self.onGainChanged(self.gainComboBox.currentIndex())

    @Slot(int)
    def onGainChanged(self, index: int):
        self.thresholdSpinBox.setRange(*self.thresholdranges[self.gainComboBox.currentText()])
