import pickle
from typing import Dict, Tuple, List, Iterable, Any

from taurus import Logger
from taurus.external.qt import QtWidgets, QtCore
from taurus.external.qt.QtCore import Slot

from ...dbutils2.exposuredb import Exposure

from ..models.samplestore import SampleStore
from ..sardanaguibase import UILoadable
from .exposureeditdialog import ExposureGeometryDialog


class Model(QtCore.QAbstractItemModel, Logger):
    _data: List[Tuple[str, float, int, Dict[str, Any]]]
    exposuretime: float = 300
    exposurecount: int = 1
    exposuregeometry: Dict[str, Any]

    def __init__(self):
        self.exposuregeometry = {}
        self._data = []
        super().__init__()
        self.call__init__(Logger)

    def setDefaultExposureGeometry(self, settings: Dict[str, Any]):
        self.exposuregeometry = settings.copy()

    def setDefaultExposureTime(self, exptime: float):
        self.exposuretime = exptime

    def setDefaultExposureCount(self, expcount: int):
        self.exposurecount = expcount

    def rowCount(self, parent: QtCore.QModelIndex = ...) -> int:
        return len(self._data)

    def columnCount(self, parent: QtCore.QModelIndex = ...) -> int:
        return 4

    def parent(self, child: QtCore.QModelIndex) -> QtCore.QModelIndex:
        return QtCore.QModelIndex()

    def index(
        self, row: int, column: int, parent: QtCore.QModelIndex = ...
    ) -> QtCore.QModelIndex:
        return self.createIndex(row, column, None)

    def flags(self, index: QtCore.QModelIndex) -> QtCore.Qt.ItemFlag:
        if not index.isValid():
            return (
                QtCore.Qt.ItemFlag.ItemIsDropEnabled
                | QtCore.Qt.ItemFlag.ItemIsDropEnabled
            )
        elif index.column() == 0:
            return (
                QtCore.Qt.ItemFlag.ItemNeverHasChildren
                | QtCore.Qt.ItemFlag.ItemIsEnabled
                | QtCore.Qt.ItemFlag.ItemIsSelectable
                | QtCore.Qt.ItemFlag.ItemIsDropEnabled
            )
        else:
            return (
                QtCore.Qt.ItemFlag.ItemNeverHasChildren
                | QtCore.Qt.ItemFlag.ItemIsEnabled
                | QtCore.Qt.ItemFlag.ItemIsSelectable
                | QtCore.Qt.ItemFlag.ItemIsDropEnabled
                | QtCore.Qt.ItemFlag.ItemIsEditable
            )

    def data(self, index: QtCore.QModelIndex, role: int = ...) -> Any:
        if (index.column() == 0) and (
            role
            in [QtCore.Qt.ItemDataRole.DisplayRole, QtCore.Qt.ItemDataRole.EditRole]
        ):
            return self._data[index.row()][0]
        elif (index.column() == 1) and (role == QtCore.Qt.ItemDataRole.DisplayRole):
            return f"{self._data[index.row()][1]:.2f} sec"
        elif (index.column() == 1) and (role == QtCore.Qt.ItemDataRole.EditRole):
            return self._data[index.row()][1]
        elif (index.column() == 2) and (role == QtCore.Qt.ItemDataRole.DisplayRole):
            return f"{self._data[index.row()][2]:d}"
        elif (index.column() == 2) and (role == QtCore.Qt.ItemDataRole.EditRole):
            return self._data[index.row()][2]
        elif (index.column() == 3) and (role == QtCore.Qt.ItemDataRole.DisplayRole):
            geo = self._data[index.row()][3]
            if 'matrix' not in geo:
                return "Single point"
            elif 'ellipse' not in geo["matrix"] or not geo["matrix"]["ellipse"]:
                return "Rectangular"
            else:
                return "Elliptic"
        elif (index.column() == 3) and (role == QtCore.Qt.ItemDataRole.EditRole):
            return self._data[index.row()][3]
        else:
            return None

    def setData(self, index: QtCore.QModelIndex, value: Any, role: int = ...) -> bool:
        if role != QtCore.Qt.ItemDataRole.EditRole:
            self.warning(
                f"setdata(row={index.row()}, column={index.column()}, {value=}, "
                f"{type(value)=} role={role} != EditRole)"
            )
            return False
        data = self._data[index.row()]
        if index.column() == 0:
            self._data[index.row()] = value, data[1], data[2], data[3]
        elif (index.column() == 1) and (value > 0):
            self._data[index.row()] = data[0], float(value), data[2], data[3]
        elif (index.column() == 2) and (value > 0):
            self._data[index.row()] = data[0], data[1], int(value), data[3]
        elif (index.column() == 3):
            self._data[index.row()] = data[0], data[1], data[2], value.copy()
        else:
            return False
        self.dataChanged.emit(index, index)
        return True

    def insertRow(self, row: int, parent: QtCore.QModelIndex = ...) -> bool:
        return self.insertRows(row, 1, parent)

    def insertRows(
        self, row: int, count: int, parent: QtCore.QModelIndex = ...
    ) -> bool:
        self.beginInsertRows(parent, row, row + count - 1)
        self._data = (
            self._data[:row]
            + [("", self.exposuretime, self.exposurecount, self.exposuregeometry.copy()) for i in range(count)]
            + self._data[row:]
        )
        self.endInsertRows()
        return True

    def removeRows(
        self, row: int, count: int, parent: QtCore.QModelIndex = ...
    ) -> bool:
        self.beginRemoveRows(parent, row, row + count - 1)
        self._data = self._data[:row] + self._data[row + count :]
        self.endRemoveRows()
        return True

    def removeRow(self, row: int, parent: QtCore.QModelIndex = ...) -> bool:
        return self.removeRows(row, 1, parent)

    def headerData(
        self, section: int, orientation: QtCore.Qt.Orientation, role: int = ...
    ) -> Any:
        if (orientation == QtCore.Qt.Orientation.Horizontal) and (
            role == QtCore.Qt.ItemDataRole.DisplayRole
        ):
            return ["Sample", "Exposure time", "Exposure count", "Geometry"][section]
        return None

    def dropMimeData(
        self,
        data: QtCore.QMimeData,
        action: QtCore.Qt.DropAction,
        row: int,
        column: int,
        parent: QtCore.QModelIndex,
    ) -> bool:
        self.debug(
            f"dropMimeData({data.formats()}, {action=}, {row=}, {column=}, {parent.isValid()=}"
        )
        if parent.isValid():
            return False
        if row < 0:
            row = len(self._data)
        if data.hasFormat("application/x-cctsequenceexposurelist"):
            lis = pickle.loads(data.data("application/x-cctsequenceexposurelist"))
            self.debug(f"Adding {len(lis)} exposurelist elements")
            if not lis:
                return False
            for r_ in [r for r, l in lis]:
                self._data[r_] = "", -1, -1
            self.beginInsertRows(parent, row, row + len(lis) - 1)
            self._data = self._data[:row] + [l for r, l in lis] + self._data[row:]
            self.endInsertRows()
            while rowstoremove := [
                r for r, d in enumerate(self._data) if d == ("", -1, -1)
            ]:
                self.removeRow(rowstoremove[0], QtCore.QModelIndex())
        elif data.hasFormat("application/x-cctsampledictlist"):
            lis = pickle.loads(data.data("application/x-cctsampledictlist"))
            self.debug(f"Adding {len(lis)} samples")
            if not lis:
                return False
            self.beginInsertRows(parent, row, row + len(lis) - 1)
            self._data = (
                self._data[:row]
                + [(s["title"], self.exposuretime, self.exposurecount, self.exposuregeometry) for s in lis]
                + self._data[row:]
            )
            self.endInsertRows()
        else:
            return False
        return True

    def supportedDragActions(self) -> QtCore.Qt.DropAction:
        return QtCore.Qt.DropAction.MoveAction

    def supportedDropActions(self) -> QtCore.Qt.DropAction:
        return QtCore.Qt.DropAction.CopyAction | QtCore.Qt.DropAction.MoveAction

    def mimeData(self, indexes: Iterable[QtCore.QModelIndex]) -> QtCore.QMimeData:
        md = QtCore.QMimeData()
        rows = {i.row() for i in indexes}
        md.setData(
            "application/x-cctsequenceexposurelist",
            pickle.dumps([(r, self._data[r]) for r in rows]),
        )
        return md

    def clear(self):
        self.beginResetModel()
        self._data = []
        self.endResetModel()

    def mimeTypes(self) -> List[str]:
        return ["application/x-cctsequenceexposurelist", "application/x-cctsampledictlist"]

    def exposures(self) -> List[Tuple[str, float, int, Dict[str, Any]]]:
        return self._data

    def setExpTimes(self):
        for i in range(len(self._data)):
            self._data[i] = self._data[i][0], self.exposuretime, self._data[i][2], self._data[i][3]
        self.dataChanged.emit(
            self.index(0, 1, QtCore.QModelIndex()),
            self.index(len(self._data), 1, QtCore.QModelIndex()),
        )

    def setExpCounts(self):
        for i in range(len(self._data)):
            self._data[i] = self._data[i][0], self._data[i][1], self.exposurecount, self._data[i][3]
        self.dataChanged.emit(
            self.index(0, 2, QtCore.QModelIndex()),
            self.index(len(self._data), 2, QtCore.QModelIndex()),
        )

    def setExpGeometries(self):
        for i in range(len(self._data)):
            self._data[i] = self._data[i][0], self._data[i][1], self._data[i][2], self.exposuregeometry
        self.dataChanged.emit(
            self.index(0, 3, QtCore.QModelIndex()),
            self.index(len(self._data), 3, QtCore.QModelIndex()),
        )


@UILoadable()
class SamplesPage(QtWidgets.QWizardPage, Logger):
    def __init__(self, **kwargs):
        door = kwargs.pop("door")
        super().__init__(**kwargs)
        self.loadUi()

        self.samplestore = SampleStore(parent=self, door=door)
        self.sampleListView.setModel(self.samplestore.sortedmodel)
        self.sampleListView.setSelectionModel(
            QtCore.QItemSelectionModel(self.sampleListView.model(), self.sampleListView)
        )
        self.exposureTreeView.setModel(Model())
        self.exposureTreeView.setSelectionModel(
            QtCore.QItemSelectionModel(
                self.exposureTreeView.model(), self.exposureTreeView
            )
        )
        self.exposureTimeDoubleSpinBox.valueChanged.connect(
            self.exposureTreeView.model().setDefaultExposureTime
        )
        self.exposureCountSpinBox.valueChanged.connect(
            self.exposureTreeView.model().setDefaultExposureCount
        )
        self.setExpCountToolButton.clicked.connect(
            self.exposureTreeView.model().setExpCounts
        )
        self.setExpTimeToolButton.clicked.connect(
            self.exposureTreeView.model().setExpTimes
        )
        self.registerField(
            "orderSamples",
            self.orderSamplesCheckBox,
            "checked",
            self.orderSamplesCheckBox.toggled,
        )

    def exposures(self) -> List[Tuple[str, float, int, Dict[str, Any]]]:
        return self.exposureTreeView.model().exposures()

    @Slot()
    def on_addSampleToolButton_clicked(self):
        self.debug(f"Adding {len(self.sampleListView.selectedIndexes())} samples")
        for index in self.sampleListView.selectedIndexes():
            samplename = index.data(QtCore.Qt.ItemDataRole.DisplayRole)
            self.debug(f"Adding sample {samplename}")
            model = self.exposureTreeView.model()
            model.insertRow(
                model.rowCount(QtCore.QModelIndex()) + 1, QtCore.QModelIndex()
            )
            model.setData(
                model.index(
                    model.rowCount(QtCore.QModelIndex()) - 1, 0, QtCore.QModelIndex()
                ),
                samplename,
                QtCore.Qt.ItemDataRole.EditRole,
            )
        self.sampleListView.selectionModel().clearSelection()

    @Slot()
    def on_removeSamplesToolButton_clicked(self):
        while indexlist := self.exposureTreeView.selectionModel().selectedRows(0):
            self.exposureTreeView.model().removeRow(
                indexlist[0].row(), QtCore.QModelIndex()
            )

    @Slot()
    def on_clearExposureListToolButton_clicked(self):
        self.exposureTreeView.model().clear()

    @Slot()
    def on_changeExposureGeometryToolButton_clicked(self):
        for index in self.exposureTreeView.selectionModel().selectedRows(3):
            settings = index.data(QtCore.Qt.EditRole)
            sample = index.model().index(index.row(), 0, QtCore.QModelIndex()).data(QtCore.Qt.ItemDataRole.DisplayRole)
            dialog = ExposureGeometryDialog(self, QtCore.Qt.WindowType.Dialog)
            dialog.setModal(True)
            dialog.setWindowTitle(f'Set exposure geometry for sample {sample}')
            dialog.set_settings(settings)
            result = dialog.exec_()
            if result == dialog.Accepted:
                index.model().setData(index, dialog.get_settings(), QtCore.Qt.ItemDataRole.EditRole)
            dialog.destroy()
