from taurus.external.qt import QtWidgets
from taurus import Logger
from ..sardanaguibase import UILoadable


@UILoadable()
class StartPage(QtWidgets.QWizardPage, Logger):
    def __init__(self, parent: QtWidgets.QWidget | None = ...) -> None:
        super().__init__(parent)
        self.loadUi()
