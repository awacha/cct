from taurus import Logger
from taurus.external.qt import QtWidgets, QtCore
from taurus.external.qt.QtCore import Slot, Qt, QModelIndex
from ..sardanaguibase import UILoadable
from .cyclemodel import CyclesModel, Cycle
from .cycleeditdialog import CycleEditDialog


@UILoadable()
class CyclesPage(QtWidgets.QWizardPage, Logger):
    _editdialog: CycleEditDialog | None = None
    _editedindex: QModelIndex | None = None
    _cyclemodel: CyclesModel

    def __init__(self, parent: QtWidgets.QWidget | None = ...) -> None:
        super().__init__(parent)
        self.loadUi()
        self._cyclemodel = CyclesModel(parent=self)
        self.treeView.setModel(self._cyclemodel)

    @Slot()
    def on_addToolButton_clicked(self):
        self._cyclemodel.insertRow(
            self._cyclemodel.rowCount(QModelIndex()), QModelIndex())
        self.treeView.selectionModel().setCurrentIndex(self._cyclemodel.index(
            self._cyclemodel.rowCount(QModelIndex())-1, 0, QModelIndex()),
            QtCore.QItemSelectionModel.SelectionFlag.SelectCurrent)
        self.on_editToolButton_clicked()

    @Slot(int)
    def on_editdialog_finished(self, result: int):
        if result == QtWidgets.QDialog.Accepted:
            cycle: Cycle = self._editdialog.cycle()
            self._cyclemodel.setData(self._editedindex, cycle, Qt.ItemDataRole.UserRole)
        self._editdialog.destroy()
        self._editdialog = None
        self._editedindex = None

    @Slot()
    def on_removeToolButton_clicked(self):
        for row in reversed(sorted({index.row() for index in self.treeView.selectionModel().selectedRows()})):
            self._cyclemodel.removeRow(row, QModelIndex())

    @Slot()
    def on_editToolButton_clicked(self):
        assert self._editdialog is None
        self._editedindex = self.treeView.selectionModel().currentIndex()
        self._editdialog = CycleEditDialog(self, Qt.Dialog)
        self._editdialog.setCycle(
            self._editedindex.data(Qt.ItemDataRole.UserRole))
        self._editdialog.setModal(True)
        self._editdialog.finished.connect(self.on_editdialog_finished)
        self._editdialog.show()

    @Slot()
    def on_clearToolButton_clicked(self):
        self._cyclemodel.removeRows(0, self._cyclemodel.rowCount(QModelIndex()), QModelIndex())

    def cycles(self) -> list[Cycle]:
        return [self._cyclemodel.data(self._cyclemodel.index(row, 0, QModelIndex()), Qt.ItemDataRole.UserRole)
                for row in range(self._cyclemodel.rowCount(QModelIndex()))]
