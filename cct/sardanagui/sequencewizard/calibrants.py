from queue import Empty
from taurus.external.qt import QtWidgets, QtCore
from taurus.external.qt.QtCore import Slot
import taurus
from taurus import Logger
from sardana.taurus.qt.qtcore.tango.sardana.macroserver import QDoor

from ..sardanaguibase import UILoadable
from ..models.samplestore import SampleStore
from ...core2.orm_schema import Categories


@UILoadable()
class CalibrantsPage(QtWidgets.QWizardPage, Logger):
    samplestore: SampleStore

    def __init__(self, **kwargs):
        self.door = taurus.Device(kwargs.pop('door'))
        assert isinstance(self.door, QDoor)
        try:
            env = self.door.getEnvironment('_MeasurementSetup')
        except KeyError:
            env = {}

        super().__init__(**kwargs)
        self.loadUi()
        self.samplestore = SampleStore(parent=self, door=self.door.name)
        for combobox, category, fieldname, envkeyname in [
            (self.darkComboBox, Categories.Dark, 'darkSample', 'DarkSample'),
            (self.emptyComboBox, Categories.Empty_beam, 'emptySample', 'EmptySample'),
            (self.intensityComboBox, Categories.NormalizationSample,
             'absintSample', 'AbsIntRefSample'),
            (self.qComboBox, Categories.Calibrant,
             'qCalibrantSample', 'QRefSample'),
        ]:
            sortmodel = self.samplestore.sortedSamplesOfCategory(category)
            combobox.setModel(sortmodel)
            self.registerField(fieldname, combobox)
            try:
                combobox.setCurrentIndex(combobox.findText(env[envkeyname]))
            except KeyError:
                combobox.setCurrentIndex(-1)
        for spinbox, fieldname in [
            (self.darkExpTimeDoubleSpinBox, 'darkTime'),
            (self.emptyExpTimeDoubleSpinBox, 'emptyTime'),
            (self.intensityExpTimeDoubleSpinBox, 'absintTime'),
            (self.qExpTimeDoubleSpinBox, 'qCalibrantTime'),
        ]:
            self.registerField(fieldname, spinbox, 'value',
                               spinbox.valueChanged)
        self.initializePage()

    def darkSample(self) -> str:
        return self.darkComboBox.currentText()

    def emptySample(self) -> str:
        return self.emptyComboBox.currentText()

    def intensitySample(self) -> str:
        return self.intensityComboBox.currentText()

    def qSample(self) -> str:
        return self.qComboBox.currentText()

    def isComplete(self) -> bool:
        return all([cb.currentIndex() >= 0 for cb in [
            self.darkComboBox, self.emptyComboBox, self.intensityComboBox, self.qComboBox]])

    @Slot(int)
    def on_darkComboBox_currentIndexChanged(self, currentIndex):
        self.completeChanged.emit()

    @Slot(int)
    def on_emptyComboBox_currentIndexChanged(self, currentIndex):
        self.completeChanged.emit()

    @Slot(int)
    def on_intensityComboBox_currentIndexChanged(self, currentIndex):
        self.completeChanged.emit()

    @Slot(int)
    def on_qComboBox_currentIndexChanged(self, currentIndex):
        self.completeChanged.emit()
