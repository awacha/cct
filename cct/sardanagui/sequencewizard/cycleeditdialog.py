import re
from ..sardanaguibase import UILoadable
from taurus.external.qt import QtWidgets, QtCore
from taurus import Logger
from .cyclemodel import Cycle, IlluminationMode, PeristalticMode


@UILoadable()
class CycleEditDialog(QtWidgets.QDialog, Logger):
    def __init__(self, parent: QtWidgets.QWidget | None = ..., f: QtCore.Qt.WindowFlags = ...) -> None:
        super().__init__(parent, f)
        self.loadUi()
        self.setWindowTitle("Cycle Editor")

    def setCycle(self, cycle: Cycle):
        self.measureReferencesCheckBox.setChecked(cycle.do_references)
        self.measureReferencesOnceRadioButton.setChecked(cycle.references_only_once)
        self.measureReferencesBeforeEachSampleRadioButton.setChecked(not cycle.references_only_once)
        self.measureSamplesCheckBox.setChecked(cycle.do_samples)
        self.acqLoopIterationsSpinBox.setValue(
            0 if cycle.acq_iterations is None else cycle.acq_iterations)
        self.setTemperatureCheckBox.setChecked(
            cycle.settemperature is not None)
        self.temperatureSetPointDoubleSpinBox.setValue(
            cycle.settemperature if cycle.settemperature is not None else 0.0)
        self.temperatureStabilityTimeDoubleSpinBox.setValue(cycle.temptoltime)
        self.temperatureToleranceRadiusDoubleSpinBox.setValue(
            cycle.temptolradius)
        if cycle.illuminationmode == IlluminationMode.NoIllumination:
            self.noIlluminationRadioButton.setChecked(True)
        elif cycle.illuminationmode == IlluminationMode.DuringCycle:
            self.illuminateDuringCycleRadioButton.setChecked(True)
        elif cycle.illuminationmode == IlluminationMode.DuringExposure:
            self.illuminateDuringExposureRadioButton.setChecked(True)
        else:
            assert False
        self.illuminationIntensityDoubleSpinBox.setValue(
            cycle.illuminationlevel)

        if cycle.peristalticmode == PeristalticMode.NoPeristaltic:
            self.noFlowRadioButton.setChecked(True)
        elif cycle.peristalticmode == PeristalticMode.Continuous:
            self.continuousFlowRadioButton.setChecked(True)
        elif cycle.peristalticmode == PeristalticMode.Dispense:
            self.dispenseBeforeExposingRadioButton.setChecked(True)
        self.flowSpeedDoubleSpinBox.setValue(cycle.peristalticflowrate)
        self.dispenseTimeDoubleSpinBox.setValue(cycle.peristalticdispense)

    def cycle(self) -> Cycle:
        return Cycle(
            do_references=self.measureReferencesCheckBox.isChecked(),
            references_only_once=self.measureReferencesOnceRadioButton.isChecked(),
            do_samples=self.measureSamplesCheckBox.isChecked(),
            acq_iterations=self.acqLoopIterationsSpinBox.value()
            if self.acqLoopIterationsSpinBox.value() > 0 else None,
            settemperature=self.temperatureSetPointDoubleSpinBox.value()
            if self.setTemperatureCheckBox.isChecked() else None,
            temptoltime=self.temperatureStabilityTimeDoubleSpinBox.value(),
            temptolradius=self.temperatureToleranceRadiusDoubleSpinBox.value(),
            illuminationmode=IlluminationMode.NoIllumination
            if self.noIlluminationRadioButton.isChecked()
            else (IlluminationMode.DuringCycle if self.illuminateDuringCycleRadioButton.isChecked()
                  else IlluminationMode.DuringExposure),
            illuminationlevel=self.illuminationIntensityDoubleSpinBox.value(),
            peristalticmode=PeristalticMode.NoPeristaltic if self.noFlowRadioButton.isChecked() else (
                PeristalticMode.Continuous if self.continuousFlowRadioButton.isChecked() else PeristalticMode.Dispense),
            peristalticflowrate=self.flowSpeedDoubleSpinBox.value(),
            peristalticdispense=self.dispenseTimeDoubleSpinBox.value(),

        )
