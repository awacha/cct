from typing import Any, Dict
from ..sardanaguibase import UILoadable
from taurus.external.qt import QtWidgets, QtCore
from taurus.external.qt.QtCore import Signal, Slot
from taurus import Logger
from matplotlib.figure import Figure
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg, NavigationToolbar2QT
from matplotlib.axes import Axes
import math
from matplotlib.patches import Ellipse, Rectangle


@UILoadable()
class ExposureGeometryDialog(QtWidgets.QDialog, Logger):
    figure: Figure
    canvas: FigureCanvasQTAgg
    axes: Axes
    figtoolbar: NavigationToolbar2QT

    def __init__(
        self, parent: QtWidgets.QWidget | None = ..., f: QtCore.Qt.WindowFlags = ...
    ) -> None:
        super().__init__(parent, f)
        self.loadUi()
        self.figure = Figure(constrained_layout=True, figsize=(3, 3))
        self.canvas = FigureCanvasQTAgg(self.figure)
        self.figtoolbar = NavigationToolbar2QT(self.canvas, self)
        self.axes = self.figure.add_subplot(1, 1, 1)
        self.figureVerticalLayout.addWidget(self.figtoolbar)
        self.figureVerticalLayout.addWidget(self.canvas)
        self.replot()

    @Slot(bool)
    def on_ellipticRadioButton_toggled(self, checked):
        self.debug("on_ellipticRadioButton_toggled")
        if checked:
            self.replot()

    @Slot(bool)
    def on_rectangularRadioButton_toggled(self, checked):
        self.debug("on_rectangularRadioButton_toggled")
        if checked:
            self.replot()

    @Slot(bool)
    def on_singlePointRadioButton_toggled(self, checked):
        self.debug("on_singlePointRadioButton_toggled")
        if checked:
            self.replot()

    @Slot(float)
    def on_heightDoubleSpinBox_valueChanged(self, value):
        self.replot()

    @Slot(float)
    def on_widthDoubleSpinBox_valueChanged(self, value):
        self.replot()

    @Slot(float)
    def on_hspacingDoubleSpinBox_valueChanged(self, value):
        self.replot()

    @Slot(float)
    def on_vspacingDoubleSpinBox_valueChanged(self, value):
        self.replot()

    def replot(self):
        self.debug("Replotting...")
        self.axes.clear()
        if self.singlePointRadioButton.isChecked():
            points = {(0, 0)}
        else:
            width = self.widthDoubleSpinBox.value()
            height = self.heightDoubleSpinBox.value()
            hspacing = self.hspacingDoubleSpinBox.value()
            vspacing = self.vspacingDoubleSpinBox.value()
            xpos = 0
            points = set()
            while xpos <= width / 2:
                ypos = 0
                while ypos <= height / 2:
                    points.add((xpos, ypos))
                    points.add((xpos, -ypos))
                    points.add((-xpos, ypos))
                    points.add((-xpos, -ypos))
                    ypos += vspacing
                    if (height == 0) or (vspacing == 0):
                        break
                xpos += hspacing
                if (width == 0) or (hspacing == 0):
                    break
            if self.ellipticRadioButton.isChecked():
                points = {
                    (x, y)
                    for (x, y) in points
                    if (2 * x / width) ** 2 + (2 * y / height) ** 2 <= 1
                }
        points = sorted(points)
        self.axes.plot([x for x, y in points], [y for x, y in points], "C0o")
        self.axes.set_title(f"Total number of points: {len(points)}")
        if self.ellipticRadioButton.isChecked():
            self.axes.add_patch(
                Ellipse((0, 0), width, height, color="gray", zorder=-100)
            )
        elif self.rectangularRadioButton.isChecked():
            self.axes.add_patch(
                Rectangle(-0.5 * width, -0.5 * height),
                width,
                height,
                color="gray",
                zorder=-100,
            )
        self.canvas.draw_idle()
        self.debug("Replotting done.")

    def get_settings(self):
        if self.singlePointRadioButton.isChecked():
            return {}
        else:
            return {
                "matrix": {
                    "width": self.widthDoubleSpinBox.value(),
                    "height": self.heightDoubleSpinBox.value(),
                    "hspacing": self.hspacingDoubleSpinBox.value(),
                    "vspacing": self.vspacingDoubleSpinBox.value(),
                    "ellipse": self.ellipticRadioButton.isChecked(),
                }
            }

    def set_settings(self, settings: Dict[str, Any]):
        if "matrix" not in settings:
            self.singlePointRadioButton.setChecked(True)
            return
        elif settings["matrix"]["ellipse"]:
            self.ellipticRadioButton.setChecked(True)
        else:
            self.rectangularRadioButton.setChecked(True)
        self.widthDoubleSpinBox.setValue(settings["matrix"]["width"])
        self.heightDoubleSpinBox.setValue(settings["matrix"]["height"])
        self.hspacingDoubleSpinBox.setValue(settings["matrix"]["hspacing"])
        self.vspacingDoubleSpinBox.setValue(settings["matrix"]["vspacing"])
