from typing import Any, Optional, List
from taurus.external.qt import QtCore
from taurus.external.qt.QtCore import QModelIndex, Qt, QMimeData
import pickle
import dataclasses
import enum
from taurus import Logger

class PeristalticMode(enum.Enum):
    NoPeristaltic = 0
    Continuous = 1
    Dispense = 2


class IlluminationMode(enum.Enum):
    NoIllumination = 0
    DuringCycle = 1
    DuringExposure = 2


@dataclasses.dataclass
class Cycle:
    do_references: bool = True
    references_only_once: bool=True
    do_samples: bool = True
    acq_iterations: Optional[int] = None

    settemperature: Optional[float] = None
    temptolradius: float = 0.1
    temptoltime: float = 30

    peristalticflowrate: float = 45
    peristalticdispense: float = 10
    peristalticmode: PeristalticMode = PeristalticMode.NoPeristaltic

    illuminationlevel: float = 100.0
    illuminationmode: IlluminationMode = IlluminationMode.NoIllumination

    def __str__(self):
        t = ""
        t += f"Reference measuremens: {self.do_references} ({'once' if self.references_only_once else 'before each sample'})\n"
        t += f"Sample measurements: {self.do_samples}\n"
        t += f"Acquisition loop repeats: {'infinite' if self.acq_iterations is None else self.acq_iterations}\n"

        if self.settemperature is None:
            t += "No temperature control\n"
        else:
            t += f"Temperature setpoint: {self.settemperature:.2f}°C\n"
            t += f"Temperature tolerance radius: {self.temptolradius:.2f}°C\n"
            t += f"Temperature stability check time: {self.temptoltime:.2f} sec\n"

        if self.peristalticmode == PeristalticMode.NoPeristaltic:
            t += "Peristaltic pump won't be used\n"
        else:
            if self.peristalticmode == PeristalticMode.Continuous:
                t += f'Peristaltic pump in continuous mode at ({self.peristalticflowrate:.2f} rpm\n'
            elif self.peristalticmode == PeristalticMode.Dispense:
                t += f'Peristaltic pump will run for {self.peristalticdispense:.2f} sec at '\
                    f'{self.peristalticflowrate:.2f} rpm before each sample exposure\n'
            else:
                assert False

        if self.illuminationmode == IlluminationMode.NoIllumination:
            t += "No illumination will be used"
        elif self.illuminationmode == IlluminationMode.DuringCycle:
            t += f"Sample will be illuminated throughout the whole cycle at {self.illuminationlevel:.2f} % power."
        elif self.illuminationlevel == IlluminationMode.DuringExposure:
            t += f"Sample will be illuminated during exposure at {self.illuminationlevel:.2f} % power"
        else:
            assert False
        return t


class CyclesModel(QtCore.QAbstractItemModel, Logger):
    _data: List[Cycle]

    def __init__(self, parent: QtCore.QObject | None = None) -> None:
        self._data = []
        super().__init__(parent)

    def rowCount(self, parent: QtCore.QModelIndex = ...) -> int:
        if parent.isValid():
            return 0
        else:
            return len(self._data)

    def headerData(self, section: int, orientation: QtCore.Qt.Orientation, role: int = ...) -> Any:
        if (role == Qt.ItemDataRole.DisplayRole) and (orientation == Qt.Orientation.Horizontal):
            return ['Cycle'][section]
        return None

    def columnCount(self, parent: QModelIndex = ...) -> int:
        return 1

    def parent(self, index: QtCore.QModelIndex) -> QModelIndex:
        return QtCore.QModelIndex()

    def flags(self, index: QModelIndex) -> Qt.ItemFlags:
        return Qt.ItemFlag.ItemNeverHasChildren | Qt.ItemFlag.ItemIsSelectable \
            | Qt.ItemFlag.ItemIsEnabled | Qt.ItemFlag.ItemIsDragEnabled

    def data(self, index: QModelIndex, role: int = ...) -> Any:
        cycle = self._data[index.row()]
        if (index.column() == 0) and (role == Qt.ItemDataRole.DisplayRole):
            return str(cycle)
        elif role == Qt.ItemDataRole.UserRole:
            return cycle
        return None

    def index(self, row: int, column: int, parent: QModelIndex = ...) -> QModelIndex:
        return self.createIndex(row, column, None)

    def removeRows(self, row: int, count: int, parent: QModelIndex = ...) -> bool:
        assert not parent.isValid()
        self.beginRemoveRows(parent, row, row+count-1)
        del self._data[row:row+count]
        self.endRemoveRows()
        return True

    def removeRow(self, row: int, parent: QModelIndex = ...) -> bool:
        return self.removeRows(row, 1, parent)

    def insertRows(self, row: int, count: int, parent: QModelIndex = ...) -> bool:
        assert not parent.isValid()
        self.beginInsertRows(QtCore.QModelIndex(), row, row+count-1)
        for c in range(count):
            self._data.insert(row, Cycle())
        self.endInsertRows()
        return True

    def insertRow(self, row: int, parent: QModelIndex = ...) -> bool:
        return self.insertRows(row, 1, parent)

    def supportedDragActions(self) -> Qt.DropActions:
        return Qt.DropAction.MoveAction | Qt.DropAction.CopyAction

    def supportedDropActions(self) -> Qt.DropActions:
        return Qt.DropAction.MoveAction | Qt.DropAction.CopyAction

    def dropMimeData(self, data: QMimeData, action: Qt.DropAction, row: int, column: int, parent: QModelIndex) -> bool:
        if not self.canDropMimeData(data, action, row, column, parent):
            return False

        if action == Qt.DropAction.IgnoreAction:
            pass

        cycles: List[Cycle] = data.data('application/vnd.cct.saxsseqcycle')
        self.beginInsertRows(parent, row, row+len(cycles)-1)
        self._data = self._data[:row] + cycles[:] + self._data[row:]
        self.endInsertRows()

    def mimeData(self, indexes: List[QtCore.QModelIndex]) -> QMimeData:
        md = QtCore.QMimeData()
        md.setData("application/vnd.cct.saxsseqcycle",
                   pickle.dumps(
                       [
                           index.data(QtCore.Qt.ItemDataRole.UserRole)
                           for index in indexes]))
        return md

    def mimeTypes(self) -> List[str]:
        return ["application/vnd.cct.saxsseqcycle"]

    def canDropMimeData(self, data: QtCore.QMimeData, action: Qt.DropAction,
                        row: int, column: int, parent: QtCore.QModelIndex) -> bool:
        if parent.isValid():
            return False
        if data.hasFormat('application/vnd.cct.saxsseqcycle'):
            return True
        return False

    def setData(self, index: QModelIndex, value: Any, role: int = ...) -> bool:
        if (role == Qt.ItemDataRole.UserRole) and isinstance(value, Cycle):
            self._data[index.row()] = value
            self.dataChanged.emit(self.index(index.row(), 0, index.parent()),
                                  self.index(index.row(), self.columnCount(QModelIndex()), index.parent()))
            return True
        return False
