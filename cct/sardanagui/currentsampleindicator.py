from typing import Any
from .sardanaguibase import UILoadable, SardanaGUIBase
import taurus
import json


@UILoadable()
class CurrentSampleIndicator(SardanaGUIBase):
    def setupUi(self):
        self.setLogLevel(self.Debug)
        self.on_environment_changed(
            'SampleInfo', self._door.getEnvironment('SampleInfo'))
        self.on_environment_changed(
            'ActiveMntGrp', self._door.getEnvironment('ActiveMntGrp')
        )

    def on_environment_changed(self, envname: str, envdata: Any):
        if envname == 'SampleInfo':
            self.currentSampleLabel.setText(envdata['currentsample'])
        elif envname == 'ActiveMntGrp':
            mgconf = json.loads(taurus.Device(
                envdata).getAttribute('Configuration').rvalue)
            for controllername, controllerdata in mgconf['controllers'].items():
                self.debug(f'{controllername=}')
                for channelname, channel in controllerdata['channels'].items():
                    self.debug(f'{controllername=}, {channelname=}, {channel["ndim"]=}')
                    if channel['ndim'] == 2:
                        expchan = taurus.Device(channel['full_name'])
                        try:
                            tangodevicename = getattr(expchan, 'TangoDeviceName')
                            if tangodevicename is not None:
                                self.timeLeftTaurusLabel.setModel(
                                    tangodevicename+'/timeleft')
                                self.info(f'Setting model for timeLeftTauruLabel to {tangodevicename}/timeleft')
                                return
                            else:
                                self.warning('tangodevicename attribute is None')
                        except KeyError as ke:
                            self.warning(f'Key error: {ke}')
                            pass
            self.warning(f'Could not find 2D channel in measurement group {envdata}')
            self.timeLeftTaurusLabel.setModel('')
