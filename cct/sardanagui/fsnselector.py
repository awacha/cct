import time
from typing import Any, Optional
import sqlalchemy
import os

import tango
import numpy as np
import scipy.io
import fabio

from taurus.external.qt import QtWidgets, QtGui
from taurus.external.qt.QtCore import Signal, Slot

from .sardanaguibase import SardanaGUIBase, UILoadable
from ..dbutils2 import exposuredb
from ..core2.dataclasses import Header, Exposure
from ..core2.fastfindfile import fastfindfile


@UILoadable()
class FSNSelector(SardanaGUIBase):
    fsnSelected = Signal(int, str, int)  # year, prefix, fsn
    horizontal: bool = True

    def __init__(self, parent: QtWidgets.QWidget, **kwargs):
        self.horizontal = kwargs.pop('horizontal', True)
        super().__init__(parent, **kwargs)

    def setupUi(self):
        pal: QtGui.QPalette = self.palette()
        pal.setColor(QtGui.QPalette.ColorRole.Window,
                     QtGui.QColor('lightblue'))
        self.setPalette(pal)
        if not self.horizontal:
            self.horizontalLayout.removeWidget(self.yearSpinBox)
            self.horizontalLayout.removeWidget(self.prefixComboBox)
            self.horizontalLayout.removeWidget(self.reloadToolButton)
            self.horizontalLayout.removeItem(self.horizontalLayout_2)
            self.horizontalLayout.removeWidget(self.prefixLabel)
            self.horizontalLayout.removeWidget(self.indexLabel)
            self.horizontalLayout.removeWidget(self.yearLabel)
            grid: QtWidgets.QGridLayout = QtWidgets.QGridLayout(self)
            self.horizontalLayout.addLayout(grid, 1)
            grid.addWidget(self.yearLabel, 0, 0, 1, 1)
            grid.addWidget(self.yearSpinBox, 0, 1, 1, 1)
            grid.addWidget(self.prefixLabel, 1, 0, 1, 1)
            grid.addWidget(self.prefixComboBox, 1, 1, 1, 1)
            grid.addWidget(self.indexLabel, 2, 0, 1, 1)
            grid.addLayout(self.horizontalLayout_2, 2, 1, 1, 1)
            grid.addWidget(self.reloadToolButton, 0, 2, 1, 1)
            grid.setContentsMargins(0, 0, 0, 0)
            self.setSizePolicy(QtWidgets.QSizePolicy.Policy.MinimumExpanding,
                               QtWidgets.QSizePolicy.Policy.MinimumExpanding)

        self.updateFromSQL(True, True, True)

    def updateFromSQL(self, update_years: bool = True, update_prefixes: bool = True, update_fsns: bool = True):
        self.debug(
            f'Updating from SQL database: {update_years=}, {update_prefixes=}, {update_fsns=}')
        t0 = time.monotonic()
        with self.getSQLSession(True) as session:
            if update_years:
                years = sorted(list(session.scalars(
                    sqlalchemy.select(exposuredb.Exposure.year).distinct())))
                self.yearSpinBox.blockSignals(True)
                try:
                    self.yearSpinBox.setRange(min(years), max(years))
                    self.yearSpinBox.setValue(max(years))
                finally:
                    self.yearSpinBox.blockSignals(False)
            if update_prefixes:
                prefixes = sorted(list(session.scalars(
                    sqlalchemy.select(exposuredb.Exposure.prefix).where(
                        exposuredb.Exposure.year == self.yearSpinBox.value()).distinct())))
                currentprefix = self.prefixComboBox.currentText()
                self.prefixComboBox.blockSignals(True)
                try:
                    self.prefixComboBox.clear()
                    self.prefixComboBox.addItems(prefixes)
                    self.prefixComboBox.setCurrentIndex(
                        self.prefixComboBox.findText(currentprefix))
                    if (self.prefixComboBox.currentIndex() < 0) and prefixes:
                        self.prefixComboBox.setCurrentIndex(0)
                finally:
                    self.prefixComboBox.blockSignals(False)
            if update_fsns:
                minfsn, maxfsn = session.query(
                    sqlalchemy.func.min(exposuredb.Exposure.fsn),
                    sqlalchemy.func.max(exposuredb.Exposure.fsn)
                ).where(exposuredb.Exposure.year == self.yearSpinBox.value(),
                        exposuredb.Exposure.prefix == self.prefixComboBox.currentText()).one()
                self.fsnSpinBox.blockSignals(True)
                try:
                    self.fsnSpinBox.setRange(minfsn, maxfsn)
                    self.fsnSpinBox.setValue(maxfsn)
                finally:
                    self.fsnSpinBox.blockSignals(False)
        self.debug(
            f'Updating from SQL database took {time.monotonic()-t0:.3f} seconds to complete')

    @Slot(int)
    def on_yearSpinBox_valueChanged(self, value: int):
        self.updateFromSQL(False, True, True)

    @Slot(bool)
    def on_reloadToolButton_clicked(self, checked: bool):
        self.on_fsnSpinBox_valueChanged(self.fsnSpinBox.value())

    @Slot()
    def on_firstToolButton_clicked(self):
        self.updateFromSQL(False, False, True)
        self.fsnSpinBox.setValue(self.fsnSpinBox.minimum())
        self.on_fsnSpinBox_valueChanged(self.fsnSpinBox.value())

    @Slot()
    def on_lastToolButton_clicked(self):
        self.updateFromSQL(False, False, True)
        self.fsnSpinBox.setValue(self.fsnSpinBox.maximum())
        self.on_fsnSpinBox_valueChanged(self.fsnSpinBox.value())

    def setInvalid(self, invalid: bool):
        self.yearSpinBox.setEnabled(not invalid)
        if invalid:
            try:
                self.yearSpinBox.blockSignals(True)
                self.yearSpinBox.setRange(0, 0)
            finally:
                self.yearSpinBox.blockSignals(True)
        self.fsnSpinBox.setEnabled(not invalid)
        if invalid:
            self.fsnSpinBox.blockSignals(True)
            self.fsnSpinBox.setRange(0, 0)
            self.fsnSpinBox.blockSignals(False)
        self.firstToolButton.setEnabled(not invalid)
        self.lastToolButton.setEnabled(not invalid)
        self.reloadToolButton.setEnabled(not invalid)

    @Slot(int)
    def on_prefixComboBox_currentIndexChanged(self, index: int):
        self.updateFromSQL(False, False, True)

    @Slot(int)
    def on_fsnSpinBox_valueChanged(self, value: Optional[int] = None):
        if self.yearSpinBox.isEnabled() and self.prefixComboBox.isEnabled() and self.fsnSpinBox.isEnabled():
            self.fsnSelected.emit(
                self.yearSpinBox.value(),
                self.prefixComboBox.currentText(),
                value if value is not None else self.fsnSpinBox.value())

    def loadExposure(self, raw: bool = True) -> Exposure:
        dataroot = tango.Database().get_property(
            'CREDO', 'dataroot')['dataroot'][0]
        yearlydataroot = os.path.join(dataroot, str(self.yearSpinBox.value()))
        headerfilename = f'{self.prefixComboBox.currentText()}_{self.fsnSpinBox.value():05d}'
        header = None
        for paramsubdir in ['param_override', 'param'] if raw else ['eval2d']:
            try:
                headerfile = fastfindfile(
                    os.path.join(yearlydataroot, paramsubdir),
                    headerfilename,
                    ['.pickle.gz', '.pickle', '.param.gz', '.param'])
                header = Header(headerfile)
                break
            except FileNotFoundError:
                pass
        else:
            raise FileNotFoundError(
                f'Cannot find header for year {self.yearSpinBox.value()}, '
                f'prefix {self.prefixComboBox.currentText()}, fsn {self.fsnSpinBox.value()}')
        imgfilename = None
        extn = '.cbf' if raw else '.npz'
        try:
            imgfilename = fastfindfile(
                os.path.join(yearlydataroot, 'images' if raw else 'eval2d'),
                headerfilename, [extn])
        except FileNotFoundError:
            raise FileNotFoundError(
                f'Cannot find image file for year {self.yearSpinBox.value()}, '
                f'prefix {self.prefixComboBox.currentText()}, fsn {self.fsnSpinBox.value()}')

        maskname = os.path.split(header.maskname)[1].rsplit('.', 1)[0]
        maskfilename = None
        try:
            maskfilename = fastfindfile(os.path.join(
                yearlydataroot, 'mask'), maskname, ['.mat', '.npy'])
        except FileNotFoundError:
            raise FileNotFoundError(
                f'Cannot find mask file {maskname} in year {self.yearSpinBox.value()}')
        if maskfilename.endswith('.npy'):
            mask = np.load(maskfilename)
        else:
            m = scipy.io.loadmat(maskfilename, appendmat=False)
            mask = m[[k for k in m.keys() if not (
                k.startswith('__') and k.endswith('__'))][0]]
        if imgfilename.endswith('.npz'):
            img = np.load(imgfilename)
            intensity = img['Intensity']
            unc = img['Error']
        elif imgfilename.endswith('.cbf'):
            intensity = fabio.open(imgfilename).data.astype(float)
            unc = np.ones_like(intensity)
            unc[intensity > 0] = intensity[intensity > 0]**0.5
        else:
            raise ValueError(f'Invalid image extension: {imgfilename}')

        return Exposure(intensity, header, unc, mask)

    def setPrefix(self, prefix: str):
        self.updateFromSQL(False, True, True)
        if (i := self.prefixComboBox.findText(prefix)) >= 0:
            self.prefixComboBox.setCurrentIndex(i)
        else:
            raise ValueError(f'Prefix {prefix} unknown.')

    def prefix(self) -> str:
        return self.prefixComboBox.currentText()

    def fsn(self) -> int:
        return self.fsnSpinBox.value()

    def setYear(self, year: int):
        self.updateFromSQL(True, True, True)
        self.yearSpinBox.setValue(year)

    def year(self) -> int:
        return self.yearSpinBox.value()

    def setYearPrefixFSN(self, year: int, prefix: str, fsn: int, try_update: bool = True):
        if year < self.yearSpinBox.minimum() or year > self.yearSpinBox.maximum():
            if try_update:
                self.updateFromSQL(True, True, True)
                return self.setYearPrefixFSN(year, prefix, fsn, try_update=False)
            raise ValueError('Invalid year')
        else:
            self.yearSpinBox.setValue(year)
        if (m := self.prefixComboBox.findText(prefix)) < 0:
            if try_update:
                self.updateFromSQL(False, True, True)
                return self.setYearPrefixFSN(year, prefix, fsn, try_update=False)
            raise ValueError('Invalid prefix')
        else:
            self.prefixComboBox.setCurrentIndex(m)
        if fsn < self.fsnSpinBox.minimum() or fsn > self.fsnSpinBox.maximum():
            if try_update:
                self.updateFromSQL(False, False, True)
                return self.setYearPrefixFSN(year, prefix, fsn, try_update=False)
            raise ValueError('Invalid FSN')
        else:
            self.fsnSpinBox.setValue(fsn)
        self.on_fsnSpinBox_valueChanged(fsn)

    def on_environment_changed(self, envname: str, envdata: Any):
        if envname == 'LastReadyFSNs':
            self.debug(f'LastReadyFSNs changed: {envdata=}')
            try:
                self.fsnSpinBox.setMaximum(envdata[self.year(), self.prefix()])
            except KeyError:
                pass
