from taurus.external.qt import QtWidgets
from taurus.external.qt.QtCore import Slot
import datetime

from .sardanaguibase import UILoadable
from .models.projectmanager import ProjectManager


@UILoadable()
class AddProjectDialog(QtWidgets.QDialog):
    _projectmanager = ProjectManager

    def __init__(self, parent: QtWidgets.QWidget, prjman: ProjectManager) -> None:
        super().__init__(parent)
        self._projectmanager = prjman
        self.loadUi()
        self.yearSpinBox.setRange(2000, 2100)
        self.yearSpinBox.setValue(datetime.date.today().year)
        self.categoryComboBox.addItems(self._projectmanager.knownCategories())
        self.categoryComboBox.setCurrentIndex(0)
        self.idSpinBox.setRange(0, 1000)

    @Slot(int)
    def on_yearSpinBox_valueChanged(self, value: int):
        self.idSpinBox.setValue(self._projectmanager.minFreeID(
            value, self.categoryComboBox.currentText()))

    @Slot(str)
    def on_categoryComboBox_currentTextChanged(self, value: str):
        self.idSpinBox.setValue(self._projectmanager.minFreeID(
            self.yearSpinBox.value(), value))
