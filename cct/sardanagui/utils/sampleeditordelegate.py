from taurus.external.qt import QtWidgets, QtCore

from ...core2.orm_schema import Categories, Situations
from ..models.samplestore import SampleStore


class SampleEditorDelegate(QtWidgets.QStyledItemDelegate):
    def _get_attribute(self, model: SampleStore, index: QtCore.QModelIndex) -> str:
        while isinstance(model, QtCore.QSortFilterProxyModel):
            model = model.sourceModel()
        assert isinstance(model, SampleStore)
        return model._columns[index.column()][0]

    def createEditor(self, parent: QtWidgets.QWidget, option: QtWidgets.QStyleOptionViewItem,
                     index: QtCore.QModelIndex) -> QtWidgets.QWidget:
        model = index.model()
        attribute = self._get_attribute(model, index)
        if attribute in ['title', 'description', 'preparedby', 'maskoverride']:
            w = super().createEditor(parent, option, index)
        elif attribute in ['positionx', 'positiony', 'distminus']:
            w = QtWidgets.QDoubleSpinBox(parent)
            w.setRange(-1000, 1000)
            w.setDecimals(4)
            w.setSingleStep(0.1)
        elif attribute == 'preparetime':
            w = QtWidgets.QDateEdit(parent)
            w.setCalendarPopup(True)
        elif attribute == 'transmission':
            w = QtWidgets.QDoubleSpinBox(parent)
            w.setRange(0, 1)
            w.setSingleStep(0.01)
            w.setDecimals(4)
        elif attribute == 'thickness':
            w = QtWidgets.QDoubleSpinBox(parent)
            w.setRange(0, 1000)
            w.setSingleStep(0.01)
            w.setDecimals(4)
        elif attribute == 'category':
            w = QtWidgets.QComboBox(parent)
            w.addItems([x.value for x in Categories])
        elif attribute == 'situation':
            w = QtWidgets.QComboBox(parent)
            w.addItems([x.value for x in Situations])
        else:
            w = super().createEditor(parent, option, index)
        w.setFrame(False)
        return w

    def updateEditorGeometry(self, editor: QtWidgets.QWidget, option: QtWidgets.QStyleOptionViewItem,
                             index: QtCore.QModelIndex) -> None:
        editor.setGeometry(option.rect)

    def setEditorData(self, editor: QtWidgets.QWidget, index: QtCore.QModelIndex) -> None:
        attribute = self._get_attribute(index.model(), index)
        sample = index.data(QtCore.Qt.ItemDataRole.UserRole)
        if attribute in ['title', 'description', 'preparedby', 'maskoverride', 'preparetime']:
            super().setEditorData(editor, index)
        elif attribute in ['positionx', 'positiony', 'thickness', 'transmission', 'distminus']:
            assert isinstance(editor, QtWidgets.QDoubleSpinBox)
            editor.setValue(sample[attribute])
        elif attribute in ['category', 'situation']:
            assert isinstance(editor, QtWidgets.QComboBox)
            editor.setCurrentIndex(editor.findText(
                sample[attribute].value))
        elif attribute == 'preparetime':
            assert isinstance(editor, QtWidgets.QDateEdit)
            editor.setDate(QtCore.QDate(sample['preparetime'].year,
                           sample['preparetime'].month, sample['preparetime'].day))
        else:
            return super().setEditorData(editor, index)

    def setModelData(self, editor: QtWidgets.QWidget, model: QtCore.QAbstractItemModel,
                     index: QtCore.QModelIndex) -> None:
        if isinstance(editor, QtWidgets.QDoubleSpinBox):
            model.setData(index, editor.value(),
                          QtCore.Qt.ItemDataRole.EditRole)
        elif isinstance(editor, QtWidgets.QComboBox):
            model.setData(index, editor.currentText(),
                          QtCore.Qt.ItemDataRole.EditRole)
        else:
            super().setModelData(editor, model, index)
