import taurus


def getDoor():
    auth = taurus.Authority()
    try:
        return list(sorted([x.name() for x in auth.getDomainDevices('Door')]))[0]
    except IndexError:
        raise RuntimeError('Cannot find any Door device')
