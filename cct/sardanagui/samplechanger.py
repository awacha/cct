from .sardanaguibase import UILoadable, SardanaGUIBase
from .models.samplestore import SampleStore

from taurus.external.qt.QtCore import Slot


@UILoadable()
class SampleChanger(SardanaGUIBase):
    samplestore: SampleStore

    def setupUi(self):
        self.samplestore = SampleStore(self, door=self._door.name)
        self.sampleNameComboBox.setModel(self.samplestore.sortedmodel)
        self.on_reloadSamplesToolButton_clicked(False)

    @Slot(bool)
    def on_reloadSamplesToolButton_clicked(self, checked: bool):
        self.samplestore.loadFromDB()
        self.sampleNameComboBox.setCurrentIndex(self.sampleNameComboBox.findText(self.samplestore.currentSampleName()))

    @Slot(bool)
    def on_movetoSampleToolButton_clicked(self, checked: bool):
        pass

    @Slot(bool)
    def on_movetoSampleXToolButton_clicked(self, checked: bool):
        pass

    @Slot(bool)
    def on_movetoSampleYToolButton_clicked(self, checked: bool):
        pass        