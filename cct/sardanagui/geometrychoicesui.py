from taurus.external.qt import QtCore
from taurus.external.qt.QtCore import Slot
from .models.geometry import ComponentType, GeometryChoices

from .sardanaguibase import SardanaGUIBase, UILoadable


@UILoadable()
class GeometryChoicesUI(SardanaGUIBase):
    def __init__(self, parent=None, **kwargs):
        self._geometry = kwargs.pop('geometry')
        super().__init__(parent, **kwargs)

    def setupUi(self):
        # a treeview to show and edit component (pinhole, beamstop, spacers, flight pipes) choices
        self.choicesTreeView.setModel(self._geometry.choices)
        self.choicesTreeView.selectionModel().currentChanged.connect(self.onChoicesTreeViewCurrentChanged)

    @Slot()
    def on_addChoiceToolButton_clicked(self):
        current = self.choicesTreeView.selectionModel().currentIndex()
        if not current.isValid():
            return
        ip = current.internalPointer()
        self.debug(ip)
        assert isinstance(ip, GeometryChoices.IndexObject)
        if ip.componenttype == ComponentType.Beamstop:
            self._geometry.choices.addBeamstop(0.0)
        elif ip.componenttype == ComponentType.FlightPipe:
            self._geometry.choices.addFlightPipe(0.0)
        elif ip.componenttype == ComponentType.PinholeSpacer:
            self._geometry.choices.addSpacer(0.0)
        elif (ip.componenttype == ComponentType.Pinhole) and (ip.level == 2):
            self._geometry.choices.addPinhole(ip.index1, 0.0)
        else:
            return

    @Slot()
    def on_removeChoiceToolButton_clicked(self):
        current = self.choicesTreeView.selectionModel().currentIndex()
        if not current.isValid():
            return
        ip = current.internalPointer()
        assert isinstance(ip, GeometryChoices.IndexObject)
        if (((ip.componenttype == ComponentType.Beamstop) and (ip.level == 2)) or
                ((ip.componenttype == ComponentType.FlightPipe) and (ip.level == 2)) or
                ((ip.componenttype == ComponentType.PinholeSpacer) and (ip.level == 2)) or
                ((ip.componenttype == ComponentType.Pinhole) and (ip.level == 3))):
            self._geometry.choices.removeRow(current.row(), current.parent())

    @Slot(QtCore.QModelIndex, QtCore.QModelIndex)
    def onChoicesTreeViewCurrentChanged(self, current: QtCore.QModelIndex, previous: QtCore.QModelIndex):
        if not current.isValid():
            self.debug('Current index is not valid')
        else:
            self.debug(f'Current index data: {current.internalPointer()}')
