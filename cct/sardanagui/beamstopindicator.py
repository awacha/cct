from typing import Any
import tango
from taurus.external.qt import QtWidgets, QtGui
from taurus.external.qt.QtCore import Slot
from .sardanaguibase import SardanaGUIBase, UILoadable
from taurus.core.tango import TangoAttribute, TangoDevice
import taurus
import math


@UILoadable()
class BeamstopIndicator(SardanaGUIBase):
    xmotor: TangoDevice = None
    ymotor: TangoDevice = None
    xpos: TangoAttribute = None
    ypos: TangoAttribute = None

    def setupUi(self):
        self.on_environment_changed(
            'BeamstopInfo', self._door.getEnvironment('BeamstopInfo'))
        self.inToolButton.clicked.connect(self.moveBeamstopIn)
        self.outToolButton.clicked.connect(self.moveBeamstopOut)
        self.onBeamstopStateChanged()

    def on_environment_changed(self, envname: str, envdata: Any):
        if envname != 'BeamstopInfo':
            return
        if (self.xmotor is not None) and (self.xmotor.name != envdata['xmotor']):
            self.xmotor.stateObj.removeListener(self)
            self.xpos.removeListener(self)
            self.xpos = None
            self.xmotor = None
        if (self.ymotor is not None) and (self.ymotor.name != envdata['ymotor']):
            self.ymotor.stateObj.removeListener(self)
            self.ypos.removeListener(self)
            self.ypos = None
            self.ymotor = None
        if self.xmotor is None:
            self.xmotor = taurus.Device(envdata["xmotor"])
            self.xpos = self.xmotor.getAttribute("Position")
            self.xmotor.stateObj.addListener(self)
            self.xpos.addListener(self)
        if self.ymotor is None:
            self.ymotor = taurus.Device(envdata["ymotor"])
            self.ypos = self.ymotor.getAttribute("Position")
            self.ymotor.stateObj.addListener(self)
            self.ypos.addListener(self)
        self.onBeamstopStateChanged()

    def handleEvent(self, evt_src, evt_type, evt_value):
        if self.xpos is None or self.ypos is None or self.xmotor is None or self.ymotor is None:
            return
        if evt_src in [self.xpos, self.ypos, self.xmotor, self.ymotor] \
                and (evt_type == taurus.core.taurusbasetypes.TaurusEventType.Change):
            self.onBeamstopStateChanged()

    @Slot()
    def onBeamstopStateChanged(self):
        bsinfo = self._door.getEnvironment('BeamstopInfo')
        xpos = float(self.xpos.rvalue) if self.xpos.rvalue is not None else math.nan
        ypos = float(self.ypos.rvalue) if self.ypos.rvalue is not None else math.nan
        xstate = self.xmotor.getDeviceProxy().state()
        ystate = self.ymotor.getDeviceProxy().state()
        if ((xstate not in [tango.DevState.MOVING, tango.DevState.ON])
                or (ystate not in [tango.DevState.MOVING, tango.DevState.ON])):
            # the motors are neither in the ON, nor in the MOVING state: something bad happened
            self.statusLabel.setPixmap(QtGui.QPixmap(
                ':/icons/beamstop-inconsistent.svg'))
            self.inToolButton.setEnabled(True)
            self.outToolButton.setEnabled(True)

        elif (xstate == tango.DevState.MOVING) or (ystate == tango.DevState.MOVING):
            self.statusLabel.setPixmap(
                QtGui.QPixmap(':/icons/beamstop-moving.svg'))
            self.inToolButton.setEnabled(False)
            self.outToolButton.setEnabled(False)
        else:
            assert xstate == tango.DevState.ON and ystate == tango.DevState.ON
            inpos = bsinfo['in']
            outpos = bsinfo['out']
            tolerance = bsinfo.setdefault('tolerance', 0.1)
            if abs(inpos[0]-xpos) < tolerance and abs(inpos[1]-ypos) < tolerance:
                self.statusLabel.setPixmap(
                    QtGui.QPixmap(':/icons/beamstop-in.svg'))
                self.inToolButton.setEnabled(False)
                self.outToolButton.setEnabled(True)
            elif abs(outpos[0]-xpos) < tolerance and abs(outpos[1] - ypos) < tolerance:
                self.statusLabel.setPixmap(
                    QtGui.QPixmap(':/icons/beamstop-out.svg'))
                self.inToolButton.setEnabled(True)
                self.outToolButton.setEnabled(False)
            else:
                self.statusLabel.setPixmap(QtGui.QPixmap(
                    ':/icons/beamstop-inconsistent.svg'))
                self.inToolButton.setEnabled(True)
                self.outToolButton.setEnabled(True)

    @Slot()
    def moveBeamstopIn(self):
        env = self._door.getEnvironment('BeamstopInfo')
        try:
            self.xpos.write(env['in'][0])
            self.ypos.write(env['in'][1])
        except Exception as exc:
            QtWidgets.QMessageBox.critical(
                self.window(), 'Cannot move beam-stop', exc.args[0])

    @Slot()
    def moveBeamstopOut(self):
        env = self._door.getEnvironment('BeamstopInfo')
        try:
            self.xpos.write(env['out'][0])
            self.ypos.write(env['out'][1])
        except Exception as exc:
            QtWidgets.QMessageBox.critical(
                self.window(), 'Cannot move beam-stop', exc.args[0])
