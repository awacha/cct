import logging
from typing import Optional

from taurus.external.qt import QtWidgets, QtCore, compat
from taurus.external.qt.QtCore import Slot

from .sardanaguibase import SardanaGUIBase, UILoadable
from .models.projectmanager import ProjectManager, Project

logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)


@UILoadable()
class AccountingIndicator(SardanaGUIBase):
    _projectmanager: ProjectManager

    def setupUi(self):
        self._projectmanager = ProjectManager(
            parent=self, door=self._door.name)
        self.projectIDComboBox.clear()
        self.projectIDComboBox.setModel(self._projectmanager)
        self.projectIDComboBox.setModelColumn(0)
        view = QtWidgets.QTreeView()
        view.setModel(self._projectmanager)
        view.setSelectionBehavior(view.SelectionBehavior.SelectRows)
        view.setHeaderHidden(True)
        view.setRootIsDecorated(False)
        view.setMinimumWidth(600)
        view.setAlternatingRowColors(True)
        view.setWordWrap(True)
        for column in range(self._projectmanager.columnCount(QtCore.QModelIndex())):
            view.resizeColumnToContents(column)
        self.projectIDComboBox.setSizeAdjustPolicy(
            self.projectIDComboBox.SizeAdjustPolicy.AdjustToContents)
        self.projectIDComboBox.setView(view)
        self.projectIDComboBox.setCurrentIndex(
            self.projectIDComboBox.findText(self._projectmanager.projectID()))
        self.projectIDComboBox.activated.connect(self.onProjectChosen)
        self._projectmanager.projectChanged.connect(self.onProjectChanged)
        self.onProjectChanged(self._projectmanager.project())

    @Slot(compat.PY_OBJECT)
    def onProjectChanged(self, currentproject: Optional[Project]):
        if currentproject is None:
            title = '-- NO PROJECT --'
            proposer = '-- NO PROJECT --'
        else:
            title = currentproject.title
            proposer = currentproject.proposer
        self.titleLineEdit.setText(title)
        self.titleLineEdit.setToolTip(self.titleLineEdit.text())
        self.titleLineEdit.home(False)
        self.proposerLineEdit.setText(proposer)
        self.proposerLineEdit.setToolTip(self.proposerLineEdit.text())
        self.proposerLineEdit.home(False)
        self.projectIDComboBox.blockSignals(True)
        try:
            self.projectIDComboBox.setCurrentIndex(
                self.projectIDComboBox.findText(currentproject.projectid)
            )
        finally:
            self.projectIDComboBox.blockSignals(False)

    @Slot()
    def onProjectChosen(self):
        projectname = self.projectIDComboBox.currentText()
        if not projectname.strip():
            projectname = None
        self._projectmanager.setProjectID(projectname)
        self.onProjectChanged(self._projectmanager.project())

    @Slot(bool)
    def on_reloadToolButton_clicked(self, checked: bool):
        currentprojectname = self.projectIDComboBox.currentText()
        self.projectIDComboBox.blockSignals(True)
        self._projectmanager.syncfromdb()
        self.projectIDComboBox.setCurrentIndex(self.projectIDComboBox.findText(currentprojectname))
        self.projectIDComboBox.blockSignals(False)