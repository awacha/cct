from typing import Any, Optional, Set, Tuple
import tango

import sqlalchemy.orm

from ..resource import icons_rc
from sardana.taurus.qt.qtcore.tango.sardana.macroserver import QDoor, QMacroServer
import sardana.taurus.qt.qtcore.tango.sardana
import sardana.taurus.core.tango.sardana
import taurus
from taurus.qt.qtgui.container import TaurusWidget
import taurus.core.util.codecs
from taurus.qt.qtgui.util import UILoadable
from .utils import getDoor

sardana.taurus.core.tango.sardana.registerExtensions()
sardana.taurus.qt.qtcore.tango.sardana.registerExtensions()


icons_rc.qInitResources()

__all__ = ['SardanaGUIBase', 'UILoadable']


class SardanaGUIBase(TaurusWidget):
    _door: Optional[QDoor]
    _mainwindow: Optional[TaurusWidget]
    _sqlengine_expdb: sqlalchemy.Engine = None
    _sqlengine_credo: sqlalchemy.Engine = None

    def __init__(self, parent=None, **kwargs):
        try:
            doorname = kwargs.pop('door')
        except KeyError:
            doorname = getDoor()
        try:
            self.mainwindow = kwargs.pop('mainwindow')
        except KeyError:
            self.mainwindow = None
        super().__init__(parent, **kwargs)
        self.loadUi()

        self._door = taurus.Factory().getDevice(doorname)
        assert isinstance(self._door, QDoor)
        assert isinstance(self._door.macro_server, QMacroServer)

        self.setupUi()
        self._door.macro_server.environmentChanged.connect(
            self._on_environment_changed)

    def setupUi(self):
        pass

    @property
    def dburl(self) -> str:
        return tango.Database().get_property('CREDO', 'dburl')['dburl'][0]

    @property
    def dburl_expdb(self) -> str:
        return tango.Database().get_property('CREDO', 'dburl.exposures')['dburl.exposures'][0]

    def getSQLEngine(self, exposuredb: bool = False):
        if exposuredb:
            if self._sqlengine_expdb is None:
                self._sqlengine_expdb = sqlalchemy.create_engine(
                    self.dburl_expdb, pool_pre_ping=True)
            return self._sqlengine_expdb
        else:
            if self._sqlengine_credo is None:
                self._sqlengine_credo = sqlalchemy.create_engine(self.dburl, pool_pre_ping=True)
            return self._sqlengine_credo

    def getSQLSession(self, exposuredb: bool = False) -> sqlalchemy.orm.Session:
        return sqlalchemy.orm.Session(self.getSQLEngine(exposuredb))

    def _on_environment_changed(self, envchanges: Tuple[Set[str], Set[str], Set[str]]):
        added, removed, modified = envchanges
        for envname in (added | modified):
            self.on_environment_changed(
                envname, self._door.getEnvironment(envname))

    def on_environment_changed(self, envname: str, envdata: Any):
        pass
