import taurus

from .sardanaguibase import SardanaGUIBase, UILoadable


@UILoadable()
class VacuumIndicator(SardanaGUIBase):
    def setupUi(self):
        attrname = self._door.getEnvironment('SampleEnvironmentInfo')[
            'vacuum_pressure']
        devname = taurus.Attribute(attrname).getParentObj().name
        self.vacuumTaurusLCD.setModel(attrname)
        self.unitTaurusLabel.setModel(attrname)
        self.stateTaurusLed.setModel(devname+'/State')
        self.stateTaurusLabel.setModel(devname+'/State')
