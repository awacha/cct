from typing import List, Tuple
from .models.sardanaitemmodel import SardanaItemModel
from .sardanaguibase import SardanaGUIBase, UILoadable
from taurus.external.qt import QtCore, QtWidgets
from taurus.core.tango import TangoAttribute, TangoDevice, TangoAuthority
from taurus.core.taurusbasetypes import TaurusEventType
import taurus


@UILoadable()
class FlagUI(SardanaGUIBase):
    flagdevice: TangoDevice
    _flags: List[Tuple[TangoAttribute, QtWidgets.QPushButton]]
    n_columns: int = 4

    def __init__(self, parent=None, **kwargs):
        self._flags = []
        super().__init__(parent, **kwargs)

    def setupUi(self):
        self.debug('Finding CredoFlags device server')
        instance = [sni for sni in taurus.Authority().getServerNameInstances(
            'CredoFlags') if sni.exported()][0]
        self.debug(f'Instance is {instance}')
        devname = [name for name, info in instance.devices(
        ).items() if info.klass().name() == 'CredoFlags'][0]
        self.debug(f'Devname is {devname}')
        self.flagdevice = taurus.Device(devname)
        gridlayout = QtWidgets.QGridLayout()
        self.frame.setLayout(gridlayout)
        self.createButtons()
        return super().setupUi()

    def createButtons(self):
        self.debug('creating buttons')
        for attr, button in self._flags:
            self.debug(f'Destroying button for attribute {attr.name}')
            button.destroy()
            button.deleteLater()
            attr.removeListener(self)
        self._flags = []
        iattr = 0
        self.debug('Now adding buttons for attributes')
        for attr in sorted(self.flagdevice.getDeviceProxy().get_attribute_list()):
            if (attr.lower() in ['state', 'status']) or attr.startswith('_'):
                self.debug(f'Not adding button for special attribute {attr}')
                continue
            self.debug(f'Creating button for attribute {attr}   ')
            devattr = self.flagdevice.getAttribute(attr)
            button = QtWidgets.QPushButton(self.frame)
            button.setText(attr)
            self.frame.layout().addWidget(
                button, iattr // self.n_columns, iattr % self.n_columns)
            button.setCheckable(True)
            button.setObjectName(f'attrButton_{attr}')
            button.setChecked(bool(devattr.rvalue))
            button.toggled.connect(self.onButtonToggled)
            button.setSizePolicy(QtWidgets.QSizePolicy.MinimumExpanding, QtWidgets.QSizePolicy.Fixed)
            iattr += 1
            self._flags.append((devattr, button))
            devattr.addListener(self)

    def onButtonToggled(self, checked: bool):
        attr = [a for a, b in self._flags if b is self.sender()][0]
        attr.write(checked)

    def handleEvent(self, evt_src, evt_type, evt_value):
        if evt_type == TaurusEventType.Change:
            try:
                button = [b for a, b in self._flags if a is evt_src][0]
            except IndexError:
                self.debug(f'Button does not exist for attribute {evt_src.name}')
                self.createButtons()
                return
            button.blockSignals(True)
            try:
                button.setChecked(bool(evt_value.rvalue))
            finally:
                button.blockSignals(False)

    def on_refreshToolButton_clicked(self):
        self.createButtons()

    def on_addFlagToolButton_clicked(self):
        if name := self.newFlagNameLineEdit.text().strip():
            try:
                self.flagdevice.CreateFlag(name)
                self.newFlagNameLineEdit.setText('')
            except Exception as exc:
                QtWidgets.QMessageBox.critical(
                    self, 'Error', f'Cannot create flag: {exc}')

    def cleanUp(self):
        self.info('Cleaning up')