from typing import Optional

import taurus
from taurus.core.tango import DevState, TangoAttribute, TangoDevice
from taurus.core.taurusbasetypes import TaurusEventType
from taurus.external.qt import QtGui
from taurus.external.qt.QtCore import Slot

from .sardanaguibase import SardanaGUIBase, UILoadable


@UILoadable()
class MotorMover(SardanaGUIBase):
    connectedmotor: Optional[TangoDevice] = None
    connectedpositionattribute: Optional[TangoAttribute] = None

    def setupUi(self):
        motors = sorted(self._door.macro_server.getElementNamesOfType('Motor'))
        self.motorNameComboBox.addItems(motors)

    @Slot(int)
    def on_motorNameComboBox_currentIndexChanged(self, currentIndex: int):
        self.connectMotor(self.motorNameComboBox.currentText())

    @Slot(bool)
    def on_relativeMotionCheckBox_toggled(self, checked: bool):
        self.updateGUI()

    @Slot(float)
    def on_motorTargetDoubleSpinBox_valueChanged(self, value: float):
        pass

    @Slot(bool)
    def on_moveMotorPushButton_clicked(self, checked: bool):
        if self.moveMotorPushButton.text() == 'Move':
            newpos = self.motorTargetDoubleSpinBox.value()
            if self.relativeMotionCheckBox.isChecked():
                newpos += self.connectedpositionattribute.rvalue.magnitude
            if self.connectedmotor.stateObj.rvalue != DevState.MOVING:
                self.connectedpositionattribute.write(newpos)
        else:
            self.connectedmotor.stop(False)

    def connectMotor(self, motorname: str):
        if self.connectedmotor:
            self.connectedmotor.stateObj.removeListener(self)
            self.connectedpositionattribute.removeListener(self)
            self.connectedmotor = None
            self.connectedpositionattribute = None
        if motorname is not None:
            self.connectedmotor = taurus.Device(motorname)
            self.connectedpositionattribute = self.connectedmotor.getAttribute(
                'Position')
            self.connectedmotor.stateObj.addListener(self)
            self.connectedpositionattribute.addListener(self)

    def handleEvent(self, evt_src, evt_type: TaurusEventType, evt_value):
        if (evt_src == self.connectedmotor.stateObj) and (evt_type == TaurusEventType.Change) and \
                (evt_value.rvalue == DevState.MOVING):
            # motor started moving
            self.updateGUI()
        elif (evt_src == self.connectedmotor.stateObj) and (evt_type == TaurusEventType.Change) and \
                (evt_value.rvalue != DevState.MOVING):
            # motor stopped
            self.updateGUI()
        elif (evt_src == self.connectedpositionattribute) and (evt_type == TaurusEventType.Change):
            # motor moved
            self.updateGUI()
        elif (evt_src == self.connectedpositionattribute) and (evt_type == TaurusEventType.Config):
            # motor limits changed
            self.updateGUI()

    def updateGUI(self):
        if self.connectedmotor is None:
            return
        left, right = self.connectedpositionattribute.getRange()
        where = self.connectedpositionattribute.rvalue.magnitude
        if self.relativeMotionCheckBox.isChecked():
            self.motorTargetDoubleSpinBox.setRange(left-where, right-where)
            self.motorTargetDoubleSpinBox.setValue(0)
        else:
            self.motorTargetDoubleSpinBox.setRange(left, right)
            self.motorTargetDoubleSpinBox.setValue(where)
        self.motorTargetDoubleSpinBox.setEnabled(
            self.connectedmotor.stateObj.rvalue != DevState.MOVING)
        self.relativeMotionCheckBox.setEnabled(
            self.connectedmotor.stateObj.rvalue != DevState.MOVING)
        self.moveMotorPushButton.setText(
            'Move' if self.connectedmotor.stateObj.rvalue != DevState.MOVING else 'Stop')
        self.moveMotorPushButton.setIcon(
            QtGui.QIcon(QtGui.QPixmap(
                ":/icons/start.svg"
                if self.connectedmotor.stateObj.rvalue != DevState.MOVING
                else ":/icons/stop.svg")))
