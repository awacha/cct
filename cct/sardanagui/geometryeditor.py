from typing import Dict, Optional

from taurus.external.qt import QtWidgets, QtGui
from taurus.external.qt.QtCore import Slot

from .sardanaguibase import SardanaGUIBase, UILoadable

from .models.geometry import Geometry, GeometrySettings

from .spacerselectordialog import SpacerSelectorDialog
from .utils.filebrowsers import browseMask, getOpenFile, getSaveFile
from .geometrychoicesui import GeometryChoicesUI
from .geometryoptimizerui import GeometryOptimizerUI


@UILoadable()
class GeometryEditor(SardanaGUIBase):
    _spacerselectordialog: Optional[SpacerSelectorDialog] = None
    choicesUi: GeometryChoicesUI
    optimizerUi: GeometryOptimizerUI
    geometry: Geometry

    _widgetname2settingsparam: Dict[str, str] = {
        'l0DoubleSpinBox': 'sourcetoph1',
        'lsDoubleSpinBox': 'ph3tosample',
        'lbsDoubleSpinBox': 'beamstoptodetector',
        'lfpDoubleSpinBox': 'ph3toflightpipes',
        'lfp2detDoubleSpinBox': 'lastflightpipetodetector',
        'pixelSizeValDoubleSpinBox': 'pixelsize',
        'pixelSizeErrDoubleSpinBox': 'pixelsize_err',
        'wavelengthValDoubleSpinBox': 'wavelength',
        'wavelengthErrDoubleSpinBox': 'wavelength_err',
        'sealingRingWidthDoubleSpinBox': 'isoKFspacer',
        'L1WithoutSpacersDoubleSpinBox': 'l1base',
        'L2WithoutSpacersDoubleSpinBox': 'l2base',
        'D1DoubleSpinBox': 'pinhole_1',
        'D2DoubleSpinBox': 'pinhole_2',
        'D3DoubleSpinBox': 'pinhole_3',
        'DBSDoubleSpinBox': 'beamstop',
        'descriptionPlainTextEdit': 'description',
        'sdValDoubleSpinBox': 'dist_sample_det',
        'sdErrDoubleSpinBox': 'dist_sample_det_err',
        'beamPosXValDoubleSpinBox': 'beamposy',    # in the geometry, beamposx is the row coordinate, beamposy is the column coordinate :-(.
        'beamPosXErrDoubleSpinBox': 'beamposy_err',
        'beamPosYValDoubleSpinBox': 'beamposx',
        'beamPosYErrDoubleSpinBox': 'beamposx_err',
        'maskFileNameLineEdit': 'mask',
    }

    def __init__(self, **kwargs):
        self.geometry = Geometry(door=kwargs['door'])
        super().__init__(**kwargs)

    def setupUi(self):
        self.choicesUi = GeometryChoicesUI(self.choicesGroupBox, geometry=self.geometry, door=self._door.name)
        self.choicesGroupBox.setLayout(QtWidgets.QVBoxLayout())
        self.choicesGroupBox.layout().addWidget(self.choicesUi)
        self.optimizerUi = GeometryOptimizerUI(self.optimizationTab, geometry=self.geometry, door=self._door.name)
        self.optimizationTab.setLayout(QtWidgets.QVBoxLayout())
        self.optimizationTab.layout().addWidget(self.optimizerUi)

        # widgets to change geometry parameters
        geosetting: GeometrySettings = self.geometry.settings()
        self.debug(f'Geometry settings: {geosetting.__getstate__()}')
        for name, param in self._widgetname2settingsparam.items():
            obj = getattr(self, name)
            assert isinstance(obj, QtWidgets.QWidget)
            try:
                value = getattr(geosetting, param)
            except KeyError:
                continue
            if isinstance(obj, QtWidgets.QDoubleSpinBox):
                obj.setValue(float(value))
                obj.valueChanged.connect(self.onDoubleSpinBoxValueChanged)
            elif isinstance(obj, QtWidgets.QLineEdit):
                obj.setText(value)
                obj.editingFinished.connect(self.onLineEditChanged)
            elif isinstance(obj, QtWidgets.QPlainTextEdit):
                obj.setPlainText(value)
                obj.textChanged.connect(self.onPlainTextEditChanged)
            else:
                assert False

        self.recalculateCollimationProperties()
        self.updateL1L2Labels()
        self.l1EditToolButton.clicked.connect(self.editSpacers)
        self.l2EditToolButton.clicked.connect(self.editSpacers)
        self.flightpipesToolButton.clicked.connect(self.editSpacers)
        self.geometry.geometryChanged.connect(
            self.onGeometryChanged)

    @Slot()
    def on_loadPresetPushButton_clicked(self):
        filename = getOpenFile(self, 'Load geometry from a file',
                               filters='Geometry files (*.geop *.geoj);;'
                                       'Geometry pickle files (*.geop);;'
                                       'Geometry JSON files (*.geoj);;'
                                       'All files (*)')
        if not filename:
            return
        self.geometry.loadGeometry(filename)

    @Slot()
    def on_savePresetPushButton_clicked(self):
        filename = getSaveFile(self, 'Save geometry to a file', defaultsuffix='.geo',
                               filters='Geometry JSON files (*.geoj);;'
                                       'All files (*)')
        if not filename:
            return
        self.geometry.saveGeometry(filename)

    @Slot()
    def on_browseMaskPushButton_clicked(self):
        filename = browseMask(self)
        if not filename:
            return
        self.maskFileNameLineEdit.setText(filename)
        self.maskFileNameLineEdit.editingFinished.emit()

    @staticmethod
    def setLabelBackground(label: QtWidgets.QLabel, good: bool):
        pal = label.palette()
        pal.setColor(QtGui.QPalette.ColorRole.Window,
                     QtGui.QColor('red' if not good else 'lightgreen'))
        label.setPalette(pal)
        label.setAutoFillBackground(True)

    def recalculateCollimationProperties(self):
        """Show current characteristics such as relative intensity, estimated qmin, etc.
        """
        geosettings: GeometrySettings = self.geometry.settings()
        self.relativeintensityLabel.setText(f'{geosettings.intensity:.4f}')
        self.qMinLabel.setText(f'{geosettings.qmin:.4f}')
        self.maxRgLabel.setText(f'{1 / geosettings.qmin:.4f}')
        parasiticscattering_around_bs = geosettings.dparasitic_at_bs > geosettings.beamstop
        self.parasiticScatteringLabel.setText(
            'Expected' if parasiticscattering_around_bs else 'Not Expected')
        self.setLabelBackground(
            self.parasiticScatteringLabel, not parasiticscattering_around_bs)
        directbeam_around_bs = geosettings.dbeam_at_bs > geosettings.beamstop
        self.directBeamHitsDetectorLabel.setText(
            'YES!!!' if directbeam_around_bs else 'No')
        self.setLabelBackground(
            self.directBeamHitsDetectorLabel, not directbeam_around_bs)
        self.sampleSizeLabel.setText(f'{geosettings.dbeam_at_sample:.2f}')
        self.beamStopSizeLabel.setText(f'{geosettings.dbeam_at_bs:.2f}')
        ph3_cuts_beam = geosettings.dbeam_at_ph3 > geosettings.pinhole_3 / 1000
        self.pinhole3LargeEnoughLabel.setText(
            'NO!!!' if ph3_cuts_beam else 'Yes')
        self.setLabelBackground(
            self.pinhole3LargeEnoughLabel, not ph3_cuts_beam)

    def updateL1L2Labels(self):
        geosettings: GeometrySettings = self.geometry.settings()
        self.l1Label.setText(f'{geosettings.l1:.2f}')
        self.l2Label.setText(f'{geosettings.l2:.2f}')
        self.flightpipesLabel.setText(
            ' + '.join([f'{x:.0f} mm' for x in geosettings.flightpipes]))
        self.l1Label.setToolTip(
            ' + '.join([f'{x:.0f} mm' for x in geosettings.l1_elements]))
        self.l2Label.setToolTip(
            ' + '.join([f'{x:.0f} mm' for x in geosettings.l2_elements]))

    @Slot()
    def editSpacers(self):
        if self._spacerselectordialog is not None:
            raise RuntimeError(
                'Spacer / flight pipe selector dialog is already running.')
        if self.sender() is self.flightpipesToolButton:
            allspacers = list(
                self.geometry.componentchoices["flightpipes"])
        elif self.sender() in [self.l1EditToolButton, self.l2EditToolButton]:
            allspacers = list(self.geometry.choices.spacers)
        else:
            assert False
        geosettings: GeometrySettings = self.geometry.settings()
        if self.sender() is self.flightpipesToolButton:
            this = geosettings.flightpipes
            target = SpacerSelectorDialog.TargetTypes.FlightPipes
        else:
            if self.sender() is self.l1EditToolButton:
                this = geosettings.l1_elements
                other = geosettings.l2_elements
                target = SpacerSelectorDialog.TargetTypes.L1
            else:
                assert self.sender() is self.l2EditToolButton
                this = geosettings.l2_elements
                other = geosettings.l1_elements
                target = SpacerSelectorDialog.TargetTypes.L2
            for spacer in other:
                allspacers.remove(spacer)
        self._spacerselectordialog = SpacerSelectorDialog(
            self, allspacers, this, target)
        self._spacerselectordialog.finished.connect(
            self.spacerSelectorFinished)
        self._spacerselectordialog.open()

    @Slot(int)
    def spacerSelectorFinished(self, result: int):
        if result == QtWidgets.QDialog.DialogCode.Accepted:
            if self._spacerselectordialog.target == SpacerSelectorDialog.TargetTypes.L2:
                self.geometry.update.setParameter(
                    'l2_elements', list(self._spacerselectordialog.selectedSpacers()))
            elif self._spacerselectordialog.target == SpacerSelectorDialog.TargetTypes.L1:
                self.geometry.update.setParameter(
                    'l1_elements', list(self._spacerselectordialog.selectedSpacers()))
            elif self._spacerselectordialog.target == SpacerSelectorDialog.TargetTypes.FlightPipes:
                self.geometry.update.setParameter(
                    'flightpipes', list(self._spacerselectordialog.selectedSpacers()))
            else:
                assert False
        self._spacerselectordialog.close()
        self._spacerselectordialog.deleteLater()
        del self._spacerselectordialog
        self.updateL1L2Labels()

    @Slot(float)
    def onDoubleSpinBoxValueChanged(self, newvalue: float):
        w = self.sender()
        param = [p for wn, p in self._widgetname2settingsparam.items() if wn ==
                 w.objectName()][0]
        self.geometry.setParameter(param, newvalue)

    @Slot()
    def onLineEditChanged(self):
        w = self.sender()
        param = [p for wn, p in self._widgetname2settingsparam.items() if wn ==
                 w.objectName()][0]
        self.geometry.setParameter(param, w.text())

    @Slot()
    def onPlainTextEditChanged(self):
        w = self.sender()
        param = [p for wn, p in self._widgetname2settingsparam.items() if wn ==
                 w.objectName()][0]
        self.geometry.setParameter(param, w.toPlainText())

    @Slot()
    def onGeometryChanged(self):
        self.updateL1L2Labels()
        geosettings: GeometrySettings = self.geometry.settings()
        for wn, p in self._widgetname2settingsparam.items():
            widget = getattr(self, wn)
            newvalue = getattr(geosettings, p)
            widget.blockSignals(True)
            try:
                if isinstance(widget, QtWidgets.QDoubleSpinBox):
                    if widget.value() != newvalue:
                        widget.setValue(newvalue)
                elif isinstance(widget, QtWidgets.QPlainTextEdit):
                    if widget.toPlainText() != newvalue:
                        widget.setPlainText(newvalue)
                elif isinstance(widget, QtWidgets.QLineEdit):
                    if widget.text() != newvalue:
                        widget.setText(newvalue)
                else:
                    assert False
            finally:
                widget.blockSignals(False)
        self.recalculateCollimationProperties()
