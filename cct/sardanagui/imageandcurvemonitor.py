from typing import Any, Final, Union
import time
from taurus.external.qt import QtGui
from taurus.external.qt.QtCore import Slot

from .fsnselector import FSNSelector
from .plotcurve import PlotCurve
from .plotimage import PlotImage
from .sardanaguibase import SardanaGUIBase, UILoadable


@UILoadable()
class ImageAndCurveMonitor(SardanaGUIBase):
    fsnselector: FSNSelector
    plotwidget: Union[PlotImage, PlotCurve]
    mode_image: bool
    min_time_between_two_subsequent_images: float = 0.1
    last_image_read: float = 0

    def __init__(self, **kwargs):
        super().__init__(**kwargs)

    def setupUi(self):
        self.fsnselector = FSNSelector(self)
        self.fsnSelectorHorizontalLayout.insertWidget(0, self.fsnselector, 1)
        self.plotwidget = PlotImage(self) if self.mode_image else PlotCurve(self)
        self.plotImageVerticalLayout.addWidget(self.plotwidget, 1)
        self.setWindowTitle("Image monitor" if self.mode_image else "Curve monitor")
        icon = QtGui.QIcon()
        icon.addPixmap(
            QtGui.QPixmap(
                ":/icons/imagemonitor.svg"
                if self.mode_image
                else ":/icons/curvemonitor.svg"
            ),
            QtGui.QIcon.Mode.Normal,
            QtGui.QIcon.State.Off,
        )
        self.setWindowIcon(icon)
        self.fsnselector.fsnSelected.connect(self.onFSNSelected)
        self.autoUpdatePushButton.setChecked(True)

    def on_environment_changed(self, envname: str, envdata: Any):
        if envname == "LastReadyFSNs":
            if self.autoUpdatePushButton.isChecked():
                try:
                    year, prefix, fsn = envdata[None]
                except KeyError:
                    return
                self.fsnselector.setYearPrefixFSN(year, prefix, fsn)

    @Slot(int, str, int)
    def onFSNSelected(self, year: int, prefix: str, fsn: int):
        self.debug(f"onFSNSelected({year}, {prefix}, {fsn})")
        t0 = time.monotonic()
        if (
            time.monotonic() - self.last_image_read
            < self.min_time_between_two_subsequent_images
        ):
            self.warning("Not loading new exposure: they come too frequently.")
            return
        ex = self.fsnselector.loadExposure()
        self.debug(f"Loaded image in {time.monotonic()-t0:.2f} sec")
        self.debug(
            f"Loaded exposure {ex.header.prefix}/{ex.header.fsn} from year {ex.header.date.year}"
        )
        t1 = time.monotonic()
        if isinstance(self.plotwidget, PlotCurve):
            self.plotwidget.clear()
            self.plotwidget.addCurve(
                ex.radial_average(),
                label=f"{prefix}/{ex.header.fsn}: {ex.header.title} @ {ex.header.distance[0]:.2f} mm",
            )
            self.plotwidget.replot()
        elif isinstance(self.plotwidget, PlotImage):
            self.plotwidget.setExposure(
                ex,
                None,
                f"{prefix}/{ex.header.fsn}: {ex.header.title} @ {ex.header.distance[0]:.2f} mm",
            )
        self.debug(f"Time required for plotting: {time.monotonic()-t1:.2f} sec")
        self.last_image_read = time.monotonic()

    @Slot(int, str, int)
    def onLastFSNChanged(self, year: int, prefix: str, fsn: int):
        if self.autoUpdatePushButton.isChecked():
            self.fsnselector.setPrefix(prefix)
            self.fsnselector.gotoLast()


class ImageMonitor(ImageAndCurveMonitor):
    mode_image: Final[bool] = True


class CurveMonitor(ImageAndCurveMonitor):
    mode_image: Final[bool] = False


@UILoadable()
class TwinImageAndCurveMonitor(SardanaGUIBase):
    immonitor: ImageMonitor
    curmonitor: CurveMonitor

    def setupUi(self):
        self.immonitor = ImageMonitor(parent=self.imageMonitorFrame)
        self.curmonitor = CurveMonitor(parent=self.curveMonitorFrame)
        self.imageMonitorFrame.setLayout(QtGui.QVBoxLayout())
        self.curveMonitorFrame.setLayout(QtGui.QVBoxLayout())
        self.imageMonitorFrame.layout().addWidget(self.immonitor)
        self.imageMonitorFrame.layout().setContentsMargins(0, 0, 0, 0)
        self.curveMonitorFrame.layout().addWidget(self.curmonitor)
        self.curveMonitorFrame.layout().setContentsMargins(0, 0, 0, 0)
