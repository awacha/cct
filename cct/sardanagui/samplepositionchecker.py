from typing import List, Optional
import math

import taurus
from taurus.external.qt import QtCore
from taurus.external.qt.QtCore import Slot
from adjustText import adjust_text
from matplotlib.axes import Axes
from matplotlib.lines import Line2D
from matplotlib.text import Text
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg, NavigationToolbar2QT
from matplotlib.figure import Figure
from matplotlib.patches import Rectangle
from taurus.core.tango import TangoAttribute, TangoDevice

from .sardanaguibase import SardanaGUIBase, UILoadable
from .models.samplestore import SampleStore


@UILoadable()
class SamplePositionChecker(SardanaGUIBase):
    figure: Figure
    canvas: FigureCanvasQTAgg
    figtoolbar: NavigationToolbar2QT
    axes: Axes
    texts: List[Text]
    xdata: List[float]
    ydata: List[float]
    samplestore: SampleStore
    xcursor: Line2D | None = None
    ycursor: Line2D | None = None
    xmotordevice: Optional[TangoDevice] = None
    xpositionattr: Optional[TangoAttribute] = None
    ymotordevice: Optional[TangoDevice] = None
    ypositionattr: Optional[TangoAttribute] = None

    def __init__(self, **kwargs):
        self.texts = []
        self.xdata = []
        self.ydata = []
        super().__init__(**kwargs)

    def setupUi(self):
        self.samplestore = SampleStore(parent=self, door=self._door.name)
        self.samplestore.sampleListChanged.connect(self.repopulateListWidget)
        self.figure = Figure(constrained_layout=True)
        self.canvas = FigureCanvasQTAgg(self.figure)
        self.figtoolbar = NavigationToolbar2QT(self.canvas, self)
        self.verticalLayout.addWidget(self.figtoolbar)
        self.verticalLayout.addWidget(self.canvas)
        self.axes = self.figure.add_subplot(self.figure.add_gridspec(1, 1)[:, :])

        if self._door is not None:
            self.on_environment_changed(
                "SampleInfo", self._door.getEnvironment("SampleInfo")
            )

        self.replotPushButton.clicked.connect(self.replot)
        self.upsideDownToolButton.toggled.connect(self.flipPlot)
        self.rightToLeftToolButton.toggled.connect(self.flipPlot)
        # ToDo: support dragging
        self.snapXToolButton.setVisible(False)
        self.snapYToolButton.setVisible(False)
        self.enableDragSamplesToolButton.setVisible(False)
        self.repopulateListWidget()

    def draw_cursors(self):
        if self.xcursor is None:
            limits = self.axes.axis()
            try:
                self.xcursor = self.axes.axvline(
                    self.xpositionattr.read().rvalue,
                    lw=1,
                    ls="dashed",
                    color="g",
                )
                self.axes.axis(limits)
                self.canvas.draw_idle()
            except Exception as exc:
                self.warning(f"Cannot plot motor lines: {exc}")
                self.xcursor = None
        if self.ycursor is None:
            self.debug("Creating ycursor")
            limits = self.axes.axis()
            try:
                self.ycursor = self.axes.axhline(
                    self.ypositionattr.read().rvalue,
                    lw=1,
                    ls="dashed",
                    color="g",
                )
                self.axes.axis(limits)
                self.canvas.draw_idle()
            except Exception as exc:
                self.warning(f"Cannot plot motor lines: {exc}")
                self.ycursor = None
            self.debug("Created ycursor")

    def on_environment_changed(self, envname, envdata):
        if envname == "SampleInfo":
            if self.xmotordevice is not None:
                self.xpositionattr.removeListener(self)
                self.xpositionattr = None
                self.xmotordevice = None
            if self.ymotordevice is not None:
                self.ypositionattr.removeListener(self)
                self.ypositionattr = None
                self.ymotordevice = None
            self.debug(f"envdata: {envdata}")
            self.xmotordevice = taurus.Device(envdata["motx"])
            self.ymotordevice = taurus.Device(envdata["moty"])
            self.xpositionattr = self.xmotordevice.getAttribute("Position")
            self.ypositionattr = self.ymotordevice.getAttribute("Position")
            self.xpositionattr.addListener(self)
            self.ypositionattr.addListener(self)

            self.draw_cursors()
        return super().on_environment_changed(envname, envdata)

    def handleEvent(
        self, evt_src, evt_type, evt_value: taurus.core.tango.TangoAttrValue
    ):
        if (evt_src == self.xpositionattr) and (
            evt_type == taurus.core.taurusbasetypes.TaurusEventType.Change
        ):
            if self.xcursor is not None:
                self.debug("Updating xcursor")
                self.xcursor.set_xdata([float(evt_value.rvalue)])
                self.canvas.draw_idle()
        elif (evt_src == self.ypositionattr) and (
            evt_type == taurus.core.taurusbasetypes.TaurusEventType.Change
        ):
            if self.ycursor is not None:
                self.debug("Updating xcursor")
                self.ycursor.set_ydata([float(evt_value.rvalue)])
                self.canvas.draw_idle()
        else:
            self.debug(f"{evt_src=}")
            self.debug(f"{evt_type=}")
            self.debug(f"{evt_value=}")

    @Slot()
    def repopulateListWidget(self):
        items = [self.listWidget.item(row) for row in range(self.listWidget.count())]
        selected = [
            item.text()
            for item in items
            if item.checkState() == QtCore.Qt.CheckState.Checked
        ]
        self.listWidget.clear()
        self.listWidget.addItems(sorted(self.samplestore.samplenames()))
        for item in [
            self.listWidget.item(row) for row in range(self.listWidget.count())
        ]:
            item.setFlags(
                QtCore.Qt.ItemFlag.ItemNeverHasChildren
                | QtCore.Qt.ItemFlag.ItemIsSelectable
                | QtCore.Qt.ItemFlag.ItemIsEnabled
                | QtCore.Qt.ItemFlag.ItemIsUserCheckable
            )
            item.setCheckState(
                QtCore.Qt.CheckState.Checked
                if (item.text() in selected)
                else QtCore.Qt.CheckState.Unchecked
            )
        self.listWidget.setMinimumWidth(self.listWidget.sizeHintForColumn(0))

    @Slot()
    def replot(self):
        items = [self.listWidget.item(row) for row in range(self.listWidget.count())]
        selected = sorted(
            [
                item.text()
                for item in items
                if item.checkState() == QtCore.Qt.CheckState.Checked
            ]
        )
        self.axes.clear()
        self.xcursor = self.ycursor = None
        samples = [self.samplestore.get(samplename) for samplename in selected]
        self.xdata = [s["positionx"] for s in samples]
        self.ydata = [s["positiony"] for s in samples]
        xerr = [s["positionx_error"] for s in samples]
        yerr = [s["positiony_error"] for s in samples]
        self.axes.errorbar(self.xdata, self.ydata, yerr, xerr, "bo")
        self.texts = []
        for x, y, title in zip(self.xdata, self.ydata, selected):
            self.texts.append(
                self.axes.text(
                    x, y, title, fontsize=self.labelSizeHorizontalSlider.value()
                )
            )
        if self.samplestore.hasMotors():
            self.axes.set_xlabel(self.samplestore.xmotorname())
            self.axes.set_ylabel(self.samplestore.ymotorname())
            xrange = [
                float(x)
                for x in self.samplestore.xmotor.getAttribute("Position").getRange()
            ]
            yrange = [
                float(y)
                for y in self.samplestore.ymotor.getAttribute("Position").getRange()
            ]
            if all([math.isfinite(c) for c in xrange + yrange]):
                self.axes.add_patch(
                    Rectangle(
                        [xrange[0], yrange[0]],
                        xrange[1] - xrange[0],
                        yrange[1] - yrange[0],
                        color="gray",
                        alpha=0.3,
                        zorder=-10,
                    )
                )
        else:
            self.axes.set_xlabel("Sample X motor of unknown name")
            self.axes.set_ylabel("Sample Y motor of unknown name")
        self.axes.grid(True, which="both")
        self.axes.axis("equal")
        self.draw_cursors()
        self.flipPlot()

    @Slot()
    def flipPlot(self):
        x1, x2, y1, y2 = self.axes.axis()
        self.axes.axis(
            xmin=max(x1, x2) if self.rightToLeftToolButton.isChecked() else min(x1, x2),
            xmax=min(x1, x2) if self.rightToLeftToolButton.isChecked() else max(x1, x2),
            ymin=max(y1, y2) if self.upsideDownToolButton.isChecked() else min(y1, y2),
            ymax=min(y1, y2) if self.upsideDownToolButton.isChecked() else max(y1, y2),
        )
        self.canvas.draw()
        # , arrowprops={'arrowstyle': "-", "color": 'k', 'lw': 0.5})
        adjust_text(self.texts, self.xdata, self.ydata, ax=self.axes)
        self.canvas.draw_idle()
