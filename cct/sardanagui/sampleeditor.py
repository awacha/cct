import datetime
import enum
from typing import Any, Dict, Final, List, Optional, Tuple

from taurus.external.qt import QtCore, QtWidgets
from taurus.external.qt.QtCore import Slot

from .utils.filebrowsers import browseMask
from .utils import SampleEditorDelegate
from .models.samplestore import SampleStore
from ..core2.orm_schema import Categories, Situations
from .models.projectmanager import ProjectManager

from .sardanaguibase import SardanaGUIBase, UILoadable


class ProxyModel(QtCore.QSortFilterProxyModel):
    """A simplified model for the sample list"""

    simplemode: bool = False

    def filterAcceptsColumn(
        self, source_column: int, source_parent: QtCore.QModelIndex
    ) -> bool:
        return (not self.simplemode) or (source_column == 0)

    def setSimpleMode(self, simplemode: bool):
        self.simplemode = simplemode
        self.invalidateFilter()


@UILoadable()
class SampleEditor(SardanaGUIBase):
    _param2widgets: Final[Dict[str, Tuple[str, ...]]] = {
        "title": "sampleNameLineEdit",
        "description": "descriptionPlainTextEdit",
        "category": "categoryComboBox",
        "situation": "situationComboBox",
        "preparedby": "preparedByLineEdit",
        "thickness": "thicknessValDoubleSpinBox",
        "thickness_error": "thicknessErrDoubleSpinBox",
        "positionx": "xPositionValDoubleSpinBox",
        "positionx_error": "xPositionErrDoubleSpinBox",
        "positiony": "yPositionValDoubleSpinBox",
        "positiony_error": "yPositionErrDoubleSpinBox",
        "distminus": "distminusValDoubleSpinBox",
        "distminus_error": "distminusErrDoubleSpinBox",
        "transmission": "transmissionValDoubleSpinBox",
        "transmission_error": "transmissionErrDoubleSpinBox",
        "project": "projectComboBox",
        "maskoverride": "maskOverrideLineEdit",
        "preparetime": "preparationDateDateEdit",
    }
    sampleeditordelegate: SampleEditorDelegate
    proxymodel: ProxyModel
    _description_update_timeout: float = 1  # seconds
    _description_update_timer: Optional[int] = None
    _samplestore: SampleStore
    _projectstore: ProjectManager
    _title_being_modified: Optional[str] = None

    def setupUi(self):
        self._samplestore = SampleStore(parent=self, door=self._door.name)
        self._projectstore = ProjectManager(parent=self, door=self._door.name)
        # first create the proxy model and the treeview
        self.proxymodel = ProxyModel()
        self.proxymodel.setSourceModel(self._samplestore)
        self.proxymodel.rowsInserted.connect(self.on_rows_inserted)
        self.proxymodel.rowsRemoved.connect(self.on_rows_removed)
        self.treeView.setModel(self.proxymodel)
        self.treeView.setSortingEnabled(True)
        self.treeView.sortByColumn(0, QtCore.Qt.SortOrder.AscendingOrder)
        self.treeView.selectionModel().selectionChanged.connect(self.onSelectionChanged)
        self.treeView.selectionModel().currentRowChanged.connect(
            self.onCurrentSelectionChanged
        )
        self._samplestore.sampleEdited.connect(self.onSampleChangedInStore)
        self.sampleeditordelegate = SampleEditorDelegate(self.treeView)
        self.treeView.setItemDelegate(self.sampleeditordelegate)
        self.situationComboBox.clear()
        self.situationComboBox.addItems([e.value for e in Situations])
        self.categoryComboBox.clear()
        self.categoryComboBox.addItems([e.value for e in Categories])
        self.projectComboBox.setModel(self._projectstore)
        self.projectComboBox.setModelColumn(0)
        view = QtWidgets.QTreeView()
        view.setModel(self._projectstore)
        view.setSelectionBehavior(view.SelectionBehavior.SelectRows)
        view.setHeaderHidden(True)
        view.setRootIsDecorated(False)
        view.setMinimumWidth(600)
        view.setAlternatingRowColors(True)
        view.setWordWrap(True)
        for column in range(self._projectstore.columnCount(QtCore.QModelIndex())):
            view.resizeColumnToContents(column)
        self.projectComboBox.setSizeAdjustPolicy(
            self.projectComboBox.SizeAdjustPolicy.AdjustToContents
        )
        self.projectComboBox.setView(view)

        # now connect "editing finished" / "value changed"-like signals to slots
        for attr, widgetname in self._param2widgets.items():
            widget = getattr(self, widgetname)
            if isinstance(widget, QtWidgets.QLineEdit):
                widget.editingFinished.connect(self.onLineEditEditingFinished)
            elif isinstance(widget, QtWidgets.QComboBox):
                widget.setCurrentIndex(-1)
                widget.currentIndexChanged.connect(
                    self.onComboBoxCurrentIndexChanged
                )
            elif isinstance(widget, QtWidgets.QPlainTextEdit):
                widget.textChanged.connect(self.onPlainTextEdited)
            elif isinstance(widget, QtWidgets.QDoubleSpinBox):
                widget.valueChanged.connect(self.onDoubleSpinBoxValueChanged)
            elif isinstance(widget, QtWidgets.QDateEdit):
                widget.dateChanged.connect(self.onDateEditDateChanged)
            elif isinstance(widget, QtWidgets.QPushButton):
                # the "Today" push button for the date
                continue
            else:
                raise ValueError(
                    f"Unknown type for widget {widget} ({type(widget)})"
                )
            widget.setDisabled(True)
        self.todayPushButton.setDisabled(True)
        self.multiColumnPushButton.setChecked(False)
        self._title_being_modified = None

    def on_rows_inserted(self, *args):
        if self._title_being_modified:
            # new title is in the title editor box
            index1 = self._samplestore.indexForSample(self._title_being_modified)
            index = self.proxymodel.mapFromSource(
                self._samplestore.indexForSample(self._title_being_modified)
            )
            self.debug(
                f"{index=}, {index.row()=}, {index1.row()=}, {self._title_being_modified=}"
            )
            self._title_being_modified = None
            self.treeView.selectionModel().select(
                index,
                QtCore.QItemSelectionModel.SelectionFlag.Rows
                | QtCore.QItemSelectionModel.SelectionFlag.ClearAndSelect
                | QtCore.QItemSelectionModel.SelectionFlag.SelectCurrent,
            )
            self.treeView.setCurrentIndex(index)

    def on_rows_removed(self, *args):
        self.debug(f"on_rows_removed({args})")

    @Slot(bool)
    def on_multiColumnPushButton_toggled(self, checked: bool):
        self.multiColumnPushButton.setText("Detailed" if checked else "Simple")
        self.proxymodel.setSimpleMode(not checked)
        # for column in range(self.proxymodel.columnCount(QtCore.QModelIndex())):
        #    self.treeView.resizeColumnToContents(column)
        # self.treeView.resize(self.treeView.minimumSizeHint().width(), self.treeView.height())
        # self.resize(1, self.height())

    @Slot()
    def on_addSamplePushButton_clicked(self):
        samplename = self._samplestore.addSample()
        self.setCurrentSample(samplename)

    @Slot()
    def on_removeSamplePushButton_clicked(self):
        while lis := self.treeView.selectionModel().selectedRows(0):
            delendum = self.treeView.model().mapToSource(lis[0])
            self._samplestore.removeRow(delendum.row(), delendum.parent())

    @Slot()
    def on_duplicateSamplePushButton_clicked(self):
        title = self.currentSampleName()
        if title is None:
            return
        newname = self._samplestore.addSample(
            self._samplestore.getFreeSampleName(title), self.currentSample()
        )
        self.setCurrentSample(newname)

    # Housekeeping of current sample ###############################x

    @Slot(QtCore.QItemSelection, QtCore.QItemSelection)
    def onSelectionChanged(
        self, selected: QtCore.QItemSelection, deselected: QtCore.QItemSelection
    ):
        nonemptyselection = len(self.treeView.selectionModel().selectedRows())
        self.removeSamplePushButton.setEnabled(nonemptyselection)

    @Slot(QtCore.QModelIndex, QtCore.QModelIndex)
    def onCurrentSelectionChanged(
        self, current: QtCore.QModelIndex, previous: QtCore.QModelIndex
    ):
        if not self._title_being_modified:
            self.updateEditWidgets(self.currentSample())

    def setCurrentSample(self, name: str):
        self.treeView.selectionModel().setCurrentIndex(
            self.treeView.model().mapFromSource(self._samplestore.indexForSample(name)),
            QtCore.QItemSelectionModel.SelectionFlag.Select
            | QtCore.QItemSelectionModel.SelectionFlag.Clear
            | QtCore.QItemSelectionModel.SelectionFlag.Current
            | QtCore.QItemSelectionModel.SelectionFlag.Rows,
        )

    def currentSample(self) -> Optional[Dict[str, Any]]:
        if not self.treeView.selectionModel().currentIndex().isValid():
            return None
        try:
            sample = (
                self.treeView.selectionModel()
                .currentIndex()
                .data(QtCore.Qt.ItemDataRole.UserRole)
            )
        except KeyError:
            return None
        try:
            return self._samplestore.get(sample["title"])
        except KeyError:
            return None

    def currentSampleName(self) -> Optional[str]:
        try:
            sam = self.currentSample()
        except KeyError:
            return None
        return None if sam is None else sam["title"]

    # Sample attribute editing finished signal handlers   #########################xx

    @Slot(float)
    def onDoubleSpinBoxValueChanged(self, value: float):
        attribute = self.attributeForWidget(self.sender())
        sample = self.currentSample()
        if sample is None:
            return
        self.updateSampleInStore(attribute, value)

    @Slot(QtCore.QDate)
    def onDateEditDateChanged(self, value: QtCore.QDate):
        sample = self.currentSample()
        if sample is None:
            return
        attribute = self.attributeForWidget(self.sender())
        date = self.sender().date()
        self.updateSampleInStore(
            attribute, datetime.date(date.year(), date.month(), date.day())
        )

    @Slot(int)
    def onComboBoxCurrentIndexChanged(self, currentIndex: int):
        sample = self.currentSample()
        if sample is None:
            return
        attribute = self.attributeForWidget(self.sender())
        if self.sender() is self.situationComboBox:
            self.updateSampleInStore(
                attribute, Situations(self.sender().currentText())
            )
        elif self.sender() is self.categoryComboBox:
            self.updateSampleInStore(
                attribute, Categories(self.sender().currentText())
            )
        elif self.sender() is self.projectComboBox:
            self.updateSampleInStore(attribute, self.sender().currentText())
        else:
            assert False

    @Slot()
    def onPlainTextEdited(self):
        sample = self.currentSample()
        if sample is None:
            return
        attribute = self.attributeForWidget(self.sender())
        if attribute == "description":
            if self._description_update_timer is not None:
                self.killTimer(self._description_update_timer)
            self._description_update_timer = self.startTimer(
                int(self._description_update_timeout * 1000),
                QtCore.Qt.TimerType.PreciseTimer,
            )
        else:
            self.updateSampleInStore(attribute, self.sender().toPlainText())

    def timerEvent(self, event: QtCore.QTimerEvent) -> None:
        if self._description_update_timer is not None:
            self.updateSampleInStore(
                "description", self.descriptionPlainTextEdit.toPlainText()
            )
            self.killTimer(self._description_update_timer)
            self._description_update_timer = None

    @Slot()
    def onLineEditEditingFinished(self):
        sample = self.currentSample()
        if sample is None:
            return
        attribute = self.attributeForWidget(self.sender())
        self._title_being_modified = self.sender().text()
        self.updateSampleInStore(attribute, self.sender().text())
        self._title_being_modified = None

    # Sample change handlers to and fro #############################xx

    def updateSampleInStore(self, attribute: str, newvalue: Any):
        try:
            self._samplestore.updateSample(
                self.currentSampleName(), attribute, newvalue
            )
        except Exception as exc:
            QtWidgets.QMessageBox.critical(
                self, "Error", f"Error while updating the sample: {exc}"
            )

    @Slot(str, str, object)
    def onSampleChangedInStore(self, samplename: str, attribute: str, newvalue: Any):
        if samplename != self.currentSampleName():
            return
        widgetname = self._param2widgets[attribute]
        widget = getattr(self, widgetname)
        widget.blockSignals(True)
        try:
            if isinstance(widget, QtWidgets.QPlainTextEdit) and (
                newvalue != widget.toPlainText()
            ):
                widget.setPlainText(newvalue)
            elif isinstance(widget, QtWidgets.QLineEdit) and (
                newvalue != widget.text()
            ):
                widget.setText(newvalue)
            elif isinstance(widget, QtWidgets.QComboBox):
                if isinstance(newvalue, str) and (widget.currentText() != newvalue):
                    widget.setCurrentIndex(widget.findText(newvalue))
                elif (newvalue is None) and (widget.currentIndex() >= 0):
                    widget.setCurrentIndex(-1)
                elif (newvalue is None) and (widget.currentIndex() < 0):
                    pass
                elif isinstance(newvalue, enum.Enum) and (
                    widget.currentText() != newvalue.value
                ):
                    widget.setCurrentIndex(widget.findText(newvalue.value))
                else:
                    # must not fail: we can reach this line if no change is needed in the widget value
                    pass
            elif isinstance(widget, QtWidgets.QDoubleSpinBox) and (newvalue != widget.value()):
                widget.setValue(newvalue)
            else:
                # must not fail: we can reach this line if no change is needed in the widget value
                pass
        finally:
            widget.blockSignals(False)

    def updateEditWidgets(self, sample: Optional[Dict[str, Any]]):
        self.todayPushButton.setEnabled(sample is not None)
        for attribute, widgetname in self._param2widgets.items():
            value = sample[attribute] if sample is not None else None
            widget = getattr(self, widgetname)
            widget.setEnabled(sample is not None)
            widget.blockSignals(True)
            try:
                if isinstance(widget, QtWidgets.QLineEdit):
                    widget.setText(value if value is not None else "")
                elif isinstance(widget, QtWidgets.QPlainTextEdit):
                    widget.setPlainText(value if value is not None else "")
                elif isinstance(widget, QtWidgets.QComboBox):
                    if value is None:
                        widget.setCurrentIndex(-1)
                        continue
                    elif not isinstance(value, str):
                        # should be an Enum element
                        value = value.value
                    widget.setCurrentIndex(widget.findText(value))
                elif isinstance(widget, QtWidgets.QDoubleSpinBox):
                    widget.setValue(value if value is not None else 0.0)
                elif isinstance(widget, QtWidgets.QDateEdit):
                    if value is None:
                        widget.setDate(QtCore.QDate.currentDate())
                    else:
                        assert isinstance(value, datetime.date)
                        widget.setDate(
                            QtCore.QDate(value.year, value.month, value.day)
                        )
                elif isinstance(widget, QtWidgets.QPushButton):
                    pass
                else:
                    assert False
            finally:
                widget.blockSignals(False)


    def sampleAttributes(self) -> List[str]:
        return list(self._param2widgets.keys())

    def attributeForWidget(self, widget: QtWidgets.QWidget) -> str:
        return [
            attr
            for attr, wname in self._param2widgets.items()
            if widget.objectName() == wname
        ][0]

    #### Auxiliary sample editing tricks ####

    @Slot()
    def on_todayPushButton_clicked(self):
        self.preparationDateDateEdit.setDate(QtCore.QDate.currentDate())

    @Slot()
    def on_maskOverridePushButton_clicked(self):
        if (filename := browseMask(self)) is not None:
            self.maskOverrideLineEdit.setText(filename)
            self.maskOverrideLineEdit.editingFinished.emit()

    @Slot(bool)
    def on_fetchXPositionToolButton_clicked(self, checked: bool):
        assert isinstance(self._samplestore, SampleStore)
        try:
            xpos = float(self._samplestore.xmotor.Position)
            self.xPositionValDoubleSpinBox.setValue(xpos)
            self.xPositionErrDoubleSpinBox.setValue(0)
        except Exception as exc:
            self.Warning(f"Cannot fetch current motor position: {exc}")

    @Slot(bool)
    def on_fetchYPositionToolButton_clicked(self, checked: bool):
        assert isinstance(self._samplestore, SampleStore)
        try:
            ypos = float(self._samplestore.ymotor.Position)
            self.yPositionValDoubleSpinBox.setValue(ypos)
            self.yPositionErrDoubleSpinBox.setValue(0)
        except Exception as exc:
            self.Warning(f"Cannot fetch current motor position: {exc}")
