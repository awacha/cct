import copy

import re
import time
from typing import Dict, Any, List, Optional, Tuple

from taurus.external.qt import QtCore, QtWidgets, compat
from taurus.external.qt.QtCore import Signal

import sqlalchemy.orm

from ...core2 import orm_schema
from .sardanaitemmodel import SardanaItemModel
from ...core2.dataclasses.project import Project


class ProjectManager(SardanaItemModel):
    _projects: Dict[Tuple[int, str, int], Tuple[Project, float]]
    _currentproject: Optional[Project] = None
    projectChanged = Signal(compat.PY_OBJECT)
    _sqlcachetimeout: float = 60

    def __init__(self, parent: QtCore.QObject | None, **kwargs):
        self._projects = {}
        self._currentproject = None
        super().__init__(parent, **kwargs)
        self.syncfromdb()
        try:
            envvar = self.getEnvironment('ProjectInfo')[
                'currentproject']
            self.on_environment_changed('ProjectInfo', {'currentproject': envvar})
        except KeyError:
            if self._projects:
                self.setProject(*sorted(self._projects)[0])
            else:
                self.setProject(None, None, None)

    def on_environment_changed(self, name: str, value: Any):
        if name == 'ProjectInfo':
            self.debug(
                f'ProjectInfo environment variable changed to {value}')
            try:
                year, category, id = value['currentproject']
            except KeyError:
                self.warning(
                    'Current project not defined in "ProjectInfo" Sardana environment variable')
                self._currentproject = None
                self.projectChanged.emit(None)
                return
            except ValueError:
                self.warning(
                    'Malformed "currentproject" member of the "ProjectInfo" Sardana environment variable')
                self._currentproject = None
                self.projectChanged.emit(None)
            try:
                cproj = self.get(year, category, id)
            except KeyError:
                self.warning('Could not get current project from the DB')
            if self._currentproject != cproj:
                self._currentproject = cproj
                self.projectChanged.emit(cproj)
                self.debug(f'Current project is now {cproj}')
            else:
                self.debug(
                    f'Not changing current project: {self._currentproject} == {cproj} ')

    def get(self, year: int, category: str, id: int, force_refresh: bool = False) -> Project:
        if force_refresh or ((year % 100, category, id) not in self._projects) or \
                (time.monotonic() - self._projects[year % 100, category, id][1] > self._sqlcachetimeout):
            with self.getSQLSession(False) as session:
                ormprj = list(session.scalars(
                    sqlalchemy.select(orm_schema.Project).where(
                        sqlalchemy.or_(orm_schema.Project.year == year % 100,
                                       orm_schema.Project.year == 2000 + year % 100),
                        orm_schema.Project.id == id,
                        orm_schema.Project.category == category)))
                if (not ormprj) and ((year % 100, category, id) in self._projects):
                    # this project has been deleted from the database
                    row = list(self._projects).index(
                        (year % 100, category, id))
                    self.removeRow(row, QtCore.QModelIndex())
                    raise KeyError('Deleted project')
                elif not ormprj:
                    raise KeyError('Nonexistent project')
                elif len(ormprj) > 1:
                    self.warning(
                        'Inconsistence in the SQL database: multiple projects exist for '
                        '({year=}, {category=}, {id=}). '
                        'Taking the one with two-digit year.')
                    ormprj = [op for op in ormprj if op.year < 100]
                assert len(ormprj) == 1
                self._projects[(year % 100, category, id)] = Project.fromORM(
                    ormprj[0]), time.monotonic()
        return copy.copy(self._projects[(year % 100, category, id)][0])

    def _rowForProjectId(self, year: int, category: str, id: int) -> int:
        return sorted(self._projects.keys()).index((year % 100, category, id))

    def _projectIdForRow(self, row: int) -> Tuple[int, str, int]:
        return sorted(self._projects.keys())[row]

    def rowCount(self, parent: QtCore.QModelIndex = ...) -> int:
        return len(self._projects)

    def columnCount(self, parent: QtCore.QModelIndex = ...) -> int:
        return 6

    def data(self, index: QtCore.QModelIndex, role: int = ...) -> Any:
        prj = self.get(*self._projectIdForRow(index.row()))
        if (index.column() == 0) and (role in [QtCore.Qt.ItemDataRole.DisplayRole, QtCore.Qt.ItemDataRole.EditRole]):
            return prj.projectid
        elif (index.column() == 1) and (role in [QtCore.Qt.ItemDataRole.DisplayRole, QtCore.Qt.ItemDataRole.EditRole]):
            return prj.year
        elif (index.column() == 2) and (role in [QtCore.Qt.ItemDataRole.DisplayRole, QtCore.Qt.ItemDataRole.EditRole]):
            return prj.category
        elif (index.column() == 3) and (role in [QtCore.Qt.ItemDataRole.DisplayRole, QtCore.Qt.ItemDataRole.EditRole]):
            return prj.id
        elif (index.column() == 4) and (role in [QtCore.Qt.ItemDataRole.DisplayRole, QtCore.Qt.ItemDataRole.EditRole]):
            return prj.proposer
        elif (index.column() == 5) and (role in [QtCore.Qt.ItemDataRole.DisplayRole, QtCore.Qt.ItemDataRole.EditRole]):
            return prj.title
        elif role == QtCore.Qt.ItemDataRole.UserRole:
            return prj
        elif (role == QtCore.Qt.ItemDataRole.DecorationRole) and (index.column() == 0):
            return QtWidgets.QApplication.instance().style().standardIcon(
                QtWidgets.QStyle.StandardPixmap.SP_DialogOkButton) if (self._currentproject == prj) else None

    def setData(self, index: QtCore.QModelIndex, value: Any, role: int = ...) -> bool:
        if role != QtCore.Qt.ItemDataRole.EditRole:
            return False
        year, category, id = sorted(self._projects.keys())[index.row()]
        with self.getSQLSession() as session, session.begin():
            ormprj = list(session.scalars(sqlalchemy.select(orm_schema.Project).where(
                sqlalchemy.or_(orm_schema.Project.year == year %
                               100, orm_schema.Project.year == 2000 + year % 100),
                orm_schema.Project.category == category,
                orm_schema.Project.id == id
            )))
            if not ormprj:
                self.error(
                    f'Project {category} {year}/{id} has been deleted')
                self.syncfromdb()
                return False
            elif len(ormprj) > 1:
                self.error(
                    msg=f'Project {category} {year}/{id} defined multiple times in the SQL database')
                for prj in ormprj:
                    self.error(
                        f'{prj.year}, {prj.category}, {prj.id}, {prj.proposer}, {prj.title}')
                return False
            if index.column() == 1:
                ormprj[0].year = int(value) % 100
            elif index.column() == 2:
                ormprj[0].category = value
            elif index.column() == 3:
                ormprj[0].id = int(value)
            elif index.column() == 4:
                ormprj[0].proposer = value
            elif index.column() == 5:
                ormprj[0].title = value
            else:
                return False
            session.add(ormprj[0])

            if (year, category, id) != (ormprj[0].year, ormprj[0].category, ormprj[0].id):
                oldrow = self._rowForProjectId(year, category, id)
                self.beginRemoveRows(QtCore.QModelIndex(), oldrow, oldrow)
                del self._projects[(year, category, id)]
                self.endRemoveRows()
                self._projects[ormprj[0].year, ormprj[0].category, ormprj[0].id] = Project.fromORM(
                    ormprj[0]), time.monotonic()
                newrow = self._rowForProjectId(
                    ormprj[0].year, ormprj[0].category, ormprj[0].id)
                self.beginInsertRows(QtCore.QModelIndex(), newrow, newrow)
                self.endInsertRows()
            else:
                newrow = self._rowForProjectId(
                    ormprj[0].year, ormprj[0].category, ormprj[0].id)
        self.get(year, category, id, force_refresh=True)
        self.dataChanged.emit(self.index(newrow, index.column(), QtCore.QModelIndex()),
                              self.index(newrow, index.column(), QtCore.QModelIndex()))
        try:
            if self._currentproject is not None:
                self.get(self._currentproject.year, self._currentproject.category,
                         self._currentproject.id, force_refresh=True)
        except KeyError:
            self._currentproject = None
            self.projectChanged.emit(self._currentproject)
        return True

    def flags(self, index: QtCore.QModelIndex) -> QtCore.Qt.ItemFlag:
        if index.column() > 2:
            return QtCore.Qt.ItemFlag.ItemNeverHasChildren | QtCore.Qt.ItemFlag.ItemIsEnabled | \
                QtCore.Qt.ItemFlag.ItemIsSelectable | QtCore.Qt.ItemFlag.ItemIsEditable
        else:
            return QtCore.Qt.ItemFlag.ItemNeverHasChildren | QtCore.Qt.ItemFlag.ItemIsEnabled | \
                QtCore.Qt.ItemFlag.ItemIsSelectable

    def headerData(self, section: int, orientation: QtCore.Qt.Orientation, role: int = ...) -> Any:
        if (orientation == QtCore.Qt.Orientation.Horizontal) and (role == QtCore.Qt.ItemDataRole.DisplayRole):
            return ['ProjectID', 'Year', 'Category', 'ID', 'Proposer', 'Title'][section]

    def parent(self, child: QtCore.QModelIndex) -> QtCore.QModelIndex:
        return QtCore.QModelIndex()

    def index(self, row: int, column: int, parent: QtCore.QModelIndex = ...) -> QtCore.QModelIndex:
        return self.createIndex(row, column, None)

    def addProject(self, year: int, category: str, id: int,
                   title: Optional[str] = None, proposer: Optional[str] = None):
        self.syncfromdb()
        if (year % 100, category, id) in self._projects:
            raise ValueError(
                f'Project {category} {year%100}/{id} already exists')
        with self.getSQLSession() as session, session.begin():
            ormprj = orm_schema.Project(
                id=id, year=year % 100, category=category, title=title, proposer=proposer, samples=[])
            session.add(ormprj)
            prj = Project.fromORM(ormprj)
            session.commit()
        self._projects[(year % 100, category, id)] = prj, time.monotonic()
        self.debug(list(self._projects.keys()))
        row = self._rowForProjectId(year % 100, category, id)
        self.beginInsertRows(QtCore.QModelIndex(), row, row)
        self.endInsertRows()

    def removeProject(self, year: int, category: str, id: int):
        row = self._rowForProjectId(year, category, id)
        self.beginRemoveRows(QtCore.QModelIndex(), row, row)
        try:
            del self._projects[year, category, id]
            with self.getSQLSession() as session, session.begin():
                ormprjs = list(session.scalars(sqlalchemy.select(
                    orm_schema.Project).where(
                        sqlalchemy.or_(orm_schema.Project.year == year % 100,
                                       orm_schema.Project.year == 2000 + year % 100),
                        orm_schema.Project.id == id,
                        orm_schema.Project.category == category
                )))
                for ormprj in ormprjs:
                    session.delete(ormprj)
                session.commit()
        finally:
            self.endRemoveRows()

    def setProject(self, year: int = None, category: str = None, id: int = None):
        if isinstance(year, str):
            if (m := re.match(r'^(?P<category>.+)\s+(?P<year>\d{2,4})/(?P<id>\d+)$', year)) is None:
                raise ValueError(f'Invalid project id format: {year}')
            id = int(m['id'])
            year = int(m['year']) % 100
            category = m['category']
        elif (year is None) and (category is None) and (id is None):
            self._currentproject = None
            return

        self.debug(f'Setting project to {category} {year}/{id}')
        try:
            self._currentproject = self.get(year, category, id)
        except KeyError:
            self.error(f'Project {category} {year}/{id} does not exist.')
            return
        try:
            prjinfo = self.getEnvironment('ProjectInfo')
        except KeyError:
            prjinfo = {}
        prjinfo['currentproject'] = (year, category, id)
        self.setEnvironment('ProjectInfo', prjinfo)
        self.info(f'Current project changed to {self._currentproject}')

    def setProjectID(self, projectid: str):
        return self.setProject(projectid, None, None)        

    def projectID(self) -> Optional[str]:
        return self._currentproject.projectid if self._currentproject is not None else None

    def project(self) -> Optional[Project]:
        return self._currentproject

    def synctodb(self, delete_notfound: bool = False) -> bool:
        """Synchronize local list to the SQL database"""
        engine = self.getSQLEngine()
        orm_schema.Base.metadata.create_all(engine)
        with self.getSQLSession() as session, session.begin():
            for prj, ts in self._projects.values():
                self.debug(f'ToORM: {prj.projectid}')
                prj.toORM(session)
            if delete_notfound:
                self.debug('Deleting not found projects')
                self.debug(
                    ', '.join([p.projectid for p, ts in self._projects.values()]))
                for ormprj in session.scalars(sqlalchemy.select(orm_schema.Project)):
                    prj = Project.fromORM(ormprj)
                    if not [p for p, ts in self._projects.values() if p == prj]:
                        self.debug(
                            f'Deleting project {prj.projectid}, as it is not in self.')
                        session.delete(ormprj)
            session.commit()
        self.debug('Synchronized projects to SQL DB.')
        return True

    def syncfromdb(self, delete_notfound: bool = True):
        with self.getSQLSession() as session:
            prjs_in_sql = {
                (p.year, p.category, p.id): p
                for p in [Project.fromORM(op) for op in session.scalars(sqlalchemy.select(orm_schema.Project))]
            }
            prjs_in_self = {key: p for key, (p, ts) in self._projects.items()}
            for prjkey, prj_self in prjs_in_self.items():
                row = self._rowForProjectId(*prjkey)
                if prjkey not in prjs_in_sql:
                    # remove projects from the cache which don't exist anymore in the SQL database
                    self.beginRemoveRows(QtCore.QModelIndex(), row, row)
                    del self._projects[prjkey]
                    self.endRemoveRows()
                else:
                    # update projects in the cache
                    prj_sql = prjs_in_sql[prjkey]
                    for column, attr in [(3, 'proposer'), (4, 'title')]:
                        if getattr(prj_self, attr) != getattr(prj_sql, attr):
                            setattr(prj_self, attr, getattr(prj_sql, attr))
                            self.dataChanged.emit(
                                self.index(row, column, QtCore.QModelIndex()),
                                self.index(row, column, QtCore.QModelIndex()))
            for prjkey, prj_sql in prjs_in_sql.items():
                if prjkey not in prjs_in_self:
                    # add new projects
                    self._projects[prjkey] = prj_sql, time.monotonic()
                    row = self._rowForProjectId(*prjkey)
                    self.beginInsertRows(QtCore.QModelIndex(), row, row)
                    self.endInsertRows()

    def knownCategories(self) -> List[str]:
        return sorted({c for y, c, i in self._projects})

    def minFreeID(self, year: int, category: str) -> int:
        return max({i for y, c, i in self._projects if y % 100 == year % 100 and c == category}, default=0)+1
