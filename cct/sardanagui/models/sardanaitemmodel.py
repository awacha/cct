from typing import Tuple, Dict, Any, Set
import sqlalchemy
from taurus.external.qt import QtCore
from taurus.external.qt.QtCore import QObject
from taurus.qt.qtcore.taurusqlistener import QTaurusBaseListener
import sardana.taurus.core.tango.sardana
import sardana.taurus.qt.qtcore.tango.sardana
from sardana.taurus.qt.qtcore.tango.sardana.macroserver import QDoor, QMacroServer
from sardana.taurus.core.tango.sardana.macro import Macro
import taurus
import tango

from ...core2 import orm_schema

sardana.taurus.core.tango.sardana.registerExtensions()
sardana.taurus.qt.qtcore.tango.sardana.registerExtensions()


class SardanaItemModel(QtCore.QAbstractItemModel, QTaurusBaseListener):
    door: QDoor
    _sqlengine_expdb: sqlalchemy.Engine = None
    _sqlengine_credo: sqlalchemy.Engine = None

    def __init__(self, parent: QObject | None = ..., **kwargs) -> None:
        doorname = kwargs.pop('door')
        super().__init__(parent)
        self.door = taurus.Device(doorname)
        assert isinstance(self.door, QDoor)
        assert isinstance(self.door.macro_server, QMacroServer)
        engine = self.getSQLEngine(False)
        orm_schema.Base.metadata.create_all(engine)
        self.door.macroStatusUpdated.connect(self._on_macro_status_updated)
        self.door.macro_server.environmentChanged.connect(
            self._on_environment_changed)
        self.door.resultUpdated.connect(self._on_macro_result_updated)
        self.door.recordDataUpdated.connect(self._on_recorddata_updated)

    @property
    def dburl(self) -> str:
        return tango.Database().get_property('CREDO', 'dburl')['dburl'][0]

    @property
    def dburl_expdb(self) -> str:
        return tango.Database().get_property('CREDO', 'dburl.exposures')['dburl.exposures'][0]

    def getSQLSession(self, exposuredb: bool = False) -> sqlalchemy.orm.Session:
        return sqlalchemy.orm.Session(self.getSQLEngine(exposuredb))

    def getSQLEngine(self, exposuredb: bool = False):
        if exposuredb:
            if self._sqlengine_expdb is None:
                self._sqlengine_expdb = sqlalchemy.create_engine(
                    self.dburl_expdb, pool_pre_ping=True)
            return self._sqlengine_expdb
        else:
            if self._sqlengine_credo is None:
                self._sqlengine_credo = sqlalchemy.create_engine(self.dburl, pool_pre_ping=True)
            return self._sqlengine_credo

    def _on_macro_status_updated(self, macroinfotuple: Tuple[Macro, Dict[str, Any]]):
        macro, infodict = macroinfotuple
        self.on_macro_status_updated(macro, infodict)
        if infodict['state'] == 'start':
            self.on_macro_stated(macro)
        elif infodict['state'] in ['finish', 'stop', 'abort']:
            self.on_macro_finished(macro, infodict['state'])
        elif infodict['state'] == 'exception':
            self.error(f'Exception in macro: {infodict["exc_type"]}: {infodict["exc_value"]}\n' + ''.join(
                infodict["exc_stack"]))
            self.on_macro_finished(macro, infodict['state'])
        elif infodict['state'] == 'step':
            self.debug(f'Macro step, {infodict=}')
            self.on_macro_step(
                macro, infodict['range'][0], infodict['range'][1], infodict['step'])
        else:
            raise ValueError(f'Unknown macro state: {infodict["state"]}')

    def on_macro_status_updated(self, macro: Macro, macroinfo: Dict[str, Any]):
        pass

    def on_macro_started(self, macro: Macro):
        pass

    def on_macro_finished(self, macro: Macro, status: str):
        pass

    def on_macro_step(self, macro: Macro, minimum: int, maximum: int, step: int) -> None:
        pass

    def _on_macro_result_updated(self, result):
        self.on_macro_result_updated(result)

    def on_macro_result_updated(self, result):
        pass

    def _on_environment_changed(self, envchanges: Tuple[Set[str], Set[str], Set[str]]):
        added, removed, modified = envchanges
        for envname in (added | modified):
            self.on_environment_changed(
                envname, self.door.getEnvironment(envname))

    def on_environment_changed(self, envname: str, envdata: Any):
        pass

    def _on_recorddata_updated(self, recorddata: Tuple[str, Dict[str, Any]]):
        assert recorddata[0] == ''
        self.on_recorddata_updated(recorddata[1])

    def on_recorddata_updated(self, recorddata: Dict[str, Any]):
        pass

    def isDoorIdle(self) -> bool:
        return self.door.getDeviceProxy().State() in [tango.DevState.ON, tango.DevState.ALARM]

    def runMacro(self, macroname, parameters, synch=False):
        if not self.isDoorIdle():
            raise RuntimeError(
                'Cannot run macro: another macro is already running '
                f'(door state: {self._door.getDeviceProxy().State()})')
        self.debug(f'Running macro {macroname}, {parameters=}')
        macroinfo = self.door.macro_server.getMacroInfoObj(macroname)
        if macroinfo is None:
            raise KeyError(f'Unknown macro {macroname}')
        parser = sardana.util.parser.ParamParser(macroinfo.parameters)
        self.door.runMacro(macroname, parser.parse(parameters), synch=synch)

    def stopMacro(self):
        self.door.stop(synch=True)

    def getEnvironment(self, name: str) -> Any:
        return self.door.getEnvironment(name)

    def setEnvironment(self, name: str, value: Any):
        return self.door.setEnvironment(name, value)
