
import json
import math
import pickle
from typing import Any, Dict, Final, List


class GeometrySettings:

    _core_parameters: Final[List[str]] = [
        'l1_elements', 'l2_elements', 'pinhole_1', 'pinhole_2', 'pinhole_3', 'flightpipes', 'beamstop',
        'l1base', 'l2base', 'isoKFspacer', 'sourcetoph1', 'ph3tosample', 'ph3toflightpipes',
        'beamstoptodetector', 'lastflightpipetodetector', 'dist_sample_det', 'dist_sample_det_err',
        'wavelength', 'wavelength_err', 'beamposx', 'beamposx_err', 'beamposy', 'beamposy_err',
        'pixelsize', 'pixelsize_err', 'mask', 'description',
    ]

    _derived_parameters: Final[List[str]] = [
        'l1', 'l2', 'ph3todetector', 'ph3tobeamstop', 'intensity', 'dbeam_at_ph3', 'dbeam_at_sample',
        'dbeam_at_bs', 'dparasitic_at_sample', 'dparasitic_at_bs', 'dparasitic_at_det',
        'dbeamstopshadow', 'qmin',
    ]
    l1_elements: List[float]
    l2_elements: List[float]
    pinhole_1: float
    pinhole_2: float
    pinhole_3: float
    flightpipes: List[float]
    beamstop: float
    l1base: float = 104.0
    l2base: float = 104.0
    isoKFspacer: float = 4.0
    sourcetoph1: float = 200.0
    ph3tosample: float
    ph3toflightpipes: float
    beamstoptodetector: float
    lastflightpipetodetector: float
    dist_sample_det: float
    dist_sample_det_err: float

    wavelength: float
    wavelength_err: float
    beamposx: float
    beamposy: float
    beamposx_err: float
    beamposy_err: float
    pixelsize: float
    pixelsize_err: float
    mask: str
    description: str

    def __init__(self, **kwargs) -> None:
        params = {
            'l1_elements': [],
            'l2_elements': [],
            'pinhole_1': 0.0,
            'pinhole_2': 0.0,
            'pinhole_3': 0.0,
            'flightpipes': [],
            'beamstop': 4.0,
            'l1base': 104.0,
            'l2base': 104.0,
            'isoKFspacer': 4.0,
            'ph3toflightpipes': 304.0,
            'lastflightpipetodetector': 89.0,
            'ph3tosample': 126.0,
            'beamstoptodetector': 78.0,
            'wavelength': 0.1542,
            'wavelength.err': 0.0,
            'sourcetoph1': 200.0,
            'dist_sample_det': 100.0,
            'dist_sample_det.err': 0.1,
            'beamposx': 300.0,
            'beamposx.err': 1.0,
            'beamposy': 240.0,
            'beamposy.err': 1.0,
            'mask': 'mask.mat',
            'description': 'Unspecified geometry',
            'pixelsize': 0.172,
            'pixelsize.err': 0.0002,
        }
        params.update(kwargs)
        self.__setstate__(params)

    def save(self, filename: str):
        if filename.endswith('.geoj'):
            with open(filename, 'wt') as f:
                json.dump(self.__getstate__(), f, indent=4)
        elif filename.endswith('.geop'):
            with open(filename, 'wb') as f:
                pickle.dump(self.__getstate__(), f)
        else:
            raise ValueError(f'Unknown file extension: {filename}')

    @classmethod
    def load(cls, filename: str) -> "GeometrySettings":
        if filename.endswith('.geoj'):
            with open(filename, 'rt') as f:
                return GeometrySettings(**json.load(f))
        elif filename.endswith('.geop'):
            with open(filename, 'rb') as f:
                return GeometrySettings(**pickle.load(f))
        else:
            raise ValueError(f'Unknown file extension: {filename}')

    def __getstate__(self) -> Dict[str, Any]:
        dic = {}
        for parameter in self._core_parameters + self._derived_parameters:
            val = getattr(self, parameter)
            if parameter.endswith('_err'):
                parameter = parameter.rsplit('_', 1)[0]+'.err'
            if isinstance(val, list):
                dic[parameter] = val[:]
            elif isinstance(val, (float, int, str)):
                dic[parameter] = val
            else:
                raise TypeError(
                    f"Invalid type for parameter {parameter}: {type(val)}")
        return dic

    def __setstate__(self, state: Dict[str, Any]):
        for parameter in self._core_parameters:
            if (parameter not in state) and \
                    (parameter.endswith('_err') and ((parameter.rsplit('_', 1)[0]+'.err') not in state)):
                raise KeyError(
                    f'Required core parameter "{parameter}" missing!')
        for parameter in self._core_parameters:
            value = state[
                parameter.rsplit('_', 1)[0]+'.err'
                if parameter.endswith('_err')
                else parameter]
            setattr(self, parameter, value)

    def __copy__(self) -> "GeometrySettings":
        return GeometrySettings(**self.__getstate__())

    def setsddist(self):
        self.dist_sample_det = self.ph3toflightpipes - self.ph3tosample + sum(self.flightpipes)\
            + self.lastflightpipetodetector
        self.dist_sample_det_err = 0.0

    @property
    def l1(self) -> float:
        return len(self.l1_elements) * self.isoKFspacer + sum(self.l1_elements) + self.l1base

    @property
    def l2(self) -> float:
        return len(self.l2_elements) * self.isoKFspacer + sum(self.l2_elements) + self.l2base

    @property
    def ph3todetector(self) -> float:
        return self.ph3tosample + self.dist_sample_det

    @property
    def ph3tobeamstop(self) -> float:
        return self.ph3todetector - self.beamstoptodetector

    @property
    def intensity(self) -> float:
        return self.pinhole_1 ** 2 * self.pinhole_2**2 / self.l1**2

    @property
    def dbeam_at_ph3(self) -> float:
        return self._dbeam(-self.ph3tosample)

    @property
    def dbeam_at_sample(self) -> float:
        return self._dbeam(0)

    @property
    def dbeam_at_bs(self) -> float:
        return self._dbeam(self.dist_sample_det - self.beamstoptodetector)

    @property
    def dparasitic_at_sample(self) -> float:
        return self._dparasitic(0)

    @property
    def dparasitic_at_bs(self) -> float:
        return self._dparasitic(self.dist_sample_det - self.beamstoptodetector)

    @property
    def dparasitic_at_det(self) -> float:
        return self._dparasitic(self.dist_sample_det)

    @property
    def dbeamstopshadow(self) -> float:
        return self._dbeam_two_pinholes(
            self.dbeam_at_sample*1000, self.beamstop*1000,
            self.dist_sample_det - self.beamstoptodetector, self.beamstoptodetector)

    @property
    def qmin(self) -> float:
        rshadow = self.dbeamstopshadow / 2
        return 4*math.pi * math.sin(0.5*math.atan(rshadow / self.dist_sample_det)) / self.wavelength

    @staticmethod
    def _dbeam_two_pinholes(d1, d2, dbetween, dafter2) -> float:
        """Calculate the beam size (maximum divergence) at defined by two pinholes of apertures
        of diameter `d1` and `d2`, placed `dbetween` distance apart from each other. The beam
        diameter is measured `dafter2` distance downstream from the 2nd pinhole.

        Pinhole diameters are in microns, pinhole-pinhole distances and beam sizes are in mm.
        """
        return (d2 + (d1+d2) / dbetween * dafter2) / 1000

    def _dbeam(self, dist) -> float:
        """Calculate the size of the primary beam `dist` mm downstream the sample."""
        return self._dbeam_two_pinholes(
            self.pinhole_1, self.pinhole_2, self.l1, self.l2 + self.ph3tosample + dist)

    def _dparasitic(self, dist) -> float:
        """Calculate the size of the parasitic scattering `dist` mm after the sample."""
        return self._dbeam_two_pinholes(self.pinhole_2, self.pinhole_3, self.l2, self.ph3tosample + dist)

    def compare(self, other: "GeometrySettings") -> List[str]:
        different = []
        for par in self._core_parameters:
            if par in ['l1_elements', 'l2_elements', 'flightpipes']:
                lis1 = list(sorted(getattr(self, par)))
                lis2 = list(sorted(getattr(other, par)))
                if (len(lis1) != len(lis2)) or any([x != y for x, y in zip(lis1, lis2)]):
                    different.append(par)
            elif getattr(self, par) != getattr(other, par):
                different.append(par)
        return different

    def __eq__(self, __value: "GeometrySettings") -> bool:
        return not self.compare(__value)

    def __ne__(self, __value: "GeometrySettings") -> bool:
        return bool(self.compare(__value))
