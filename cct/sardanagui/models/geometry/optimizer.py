import time
from typing import Dict, List, Optional, Tuple

import concurrent.futures
from taurus import Logger
from taurus.external.qt import QtCore
from taurus.external.qt.QtCore import Signal

from .settings import GeometrySettings
from . import optimizeworker


class GeometryOptimizer(QtCore.QObject, Logger):
    coresettings: GeometrySettings
    choices: Dict[str, List[float]]
    timerid: Optional[int] = None
    starttime: Optional[float] = None
    finished = Signal(float)  # elapsed time
    _executor: Optional[concurrent.futures.ProcessPoolExecutor] = None
    _future: Optional[concurrent.futures.Future] = None
    compatiblegeometries: List[GeometrySettings]

    def __init__(self, settings: GeometrySettings, choices: Dict[str, List[float]]):
        super().__init__()
        self.call__init__(Logger)
        self.coresettings = settings
        self.compatiblegeometries = []
        self.choices = choices
        self.timerid = None

    def start(
        self,
        beamdiameteratsample: Tuple[float, float],
        qmin_interval: Tuple[float, float],
        l1min: float,
        l2min: float,
        lmax: float,
    ):
        if self._executor is not None:
            raise RuntimeError("Cannot start processing: already running")
        self._executor = concurrent.futures.ProcessPoolExecutor()
        self._future = self._executor.submit(
            optimizeworker.worker,
            self.coresettings,
            self.choices,
            beamdiameteratsample,
            qmin_interval,
            l1min,
            l2min,
            lmax,
        )
        self.timerid = self.startTimer(100, QtCore.Qt.TimerType.PreciseTimer)
        self.starttime = time.monotonic()

    def cancel(self):
        if self._future is not None:
            self._future.cancel()

    def timerEvent(self, timerevent: QtCore.QTimerEvent) -> None:
        if timerevent.timerId() != self.timerid:
            return
        if not self._future.done():
            return
        try:
            self.compatiblegeometries = self._future.result()
            self.debug(f"Compatible geometries found: {len(self.compatiblegeometries)}")
            if self.compatiblegeometries:
                self.debug(f"First one: {self.compatiblegeometries[0].__getstate__()}")
        except Exception:
            self.error(f"Error in geometry optimizer task: {self._future.exception()}")
        self._future = None
        self._executor.shutdown(True)  # this also closes the cluster
        self.killTimer(self.timerid)
        self.finished.emit(time.monotonic() - self.starttime)
        self.starttime = None
        self.timerid = None
