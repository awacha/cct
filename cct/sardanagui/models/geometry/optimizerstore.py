from typing import Any, List, Iterator, Dict

import functools
import math
from taurus.external.qt import QtCore
from .settings import GeometrySettings


class OptimizerStore(QtCore.QAbstractItemModel):
    _optimizationresults: List[GeometrySettings]
    _columns = {
        'Intensity': 'intensity',
        'Qmin': 'qmin',
        'Sample': 'dbeam_at_sample',
        'Beamstop': 'beamstop',
        'PH#1-PH#2': 'l1',
        'PH#2-PH#3': 'l2',
        'S-D': 'dist_sample_det',
        'PH#1': 'pinhole_1',
        'PH#2': 'pinhole_2',
        'PH#3': 'pinhole_3',
    }

    def __init__(self):
        self._optimizationresults = []
        super().__init__()

    def headerData(self, section: int, orientation: QtCore.Qt.Orientation, role: int = ...) -> Any:
        if (orientation == QtCore.Qt.Orientation.Horizontal) and (role == QtCore.Qt.ItemDataRole.DisplayRole):
            return ['Intensity', 'Qmin', 'Sample', 'Beamstop', 'PH#1-PH#2', 'PH#2-PH#3',
                    'S-D', 'PH#1', 'PH#2', 'PH#3'][section]

    def flags(self, index: QtCore.QModelIndex) -> QtCore.Qt.ItemFlag:
        return QtCore.Qt.ItemFlag.ItemNeverHasChildren | QtCore.Qt.ItemFlag.ItemIsEnabled | \
            QtCore.Qt.ItemFlag.ItemIsSelectable

    def data(self, index: QtCore.QModelIndex, role: int = ...) -> Any:
        columnlabel, param = list(self._columns.items())[index.column()]
        optimizationresult = self._optimizationresults[index.row()]
        if role == QtCore.Qt.ItemDataRole.UserRole:
            return optimizationresult
        elif role == QtCore.Qt.ItemDataRole.EditRole:
            return getattr(optimizationresult, param)
        elif role == QtCore.Qt.ItemDataRole.DisplayRole:
            if columnlabel == 'Intensity':
                return f'{optimizationresult.intensity:.0f}'
            elif columnlabel == 'Qmin':
                return f'{optimizationresult.qmin:.4f}'
            elif columnlabel == 'Sample':
                return f'{optimizationresult.dbeam_at_sample:.2f}'
            elif columnlabel == 'Beamstop':
                return f'{optimizationresult.beamstop:.2f}'
            elif columnlabel == 'PH#1-PH#2':
                return f'{optimizationresult.l1:.0f}'
            elif columnlabel == 'PH#2-PH#3':
                return f'{optimizationresult.l2:.0f}'
            elif columnlabel == 'S-D':
                return f'{optimizationresult.dist_sample_det:.2f}'
            elif columnlabel == 'PH#1':
                return f'{optimizationresult.pinhole_1:.0f}'
            elif columnlabel == 'PH#2':
                return f'{optimizationresult.pinhole_2:.0f}'
            elif columnlabel == 'PH#3':
                return f'{optimizationresult.pinhole_3:.0f}'
            else:
                return None
        elif role == QtCore.Qt.ItemDataRole.ToolTipRole:
            if columnlabel == 'Qmin':
                return f'Qmin: {optimizationresult.qmin:.4f} 1/nm, ' +\
                    f'corresponding to Dmax: {2 * math.pi / optimizationresult.qmin:.0f} ' +\
                    f'nm periodic distance and Rgmax {1 / optimizationresult.qmin:.0f} ' +\
                    'nm radius of gyration.'
            elif columnlabel == 'Sample':
                return f'Beam diameter at sample: {optimizationresult.dbeam_at_sample:.2f} mm'
            elif columnlabel == 'PH#1-PH#2':
                return 'Spacers needed: ' + ' + '.join(
                    [f'{x:.0f} mm' for x in sorted(optimizationresult.l1_elements)])
            elif columnlabel == 'PH#2-PH#3':
                return 'Spacers needed: ' + ' + '.join(
                    [f'{x:.0f} mm' for x in sorted(optimizationresult.l2_elements)])
            elif columnlabel == 'S-D':
                return 'Flight pipes needed: ' + ' + '.join(
                    [f'{x:.0f} mm' for x in sorted(optimizationresult.flightpipes)])
            elif columnlabel == 'PH#1':
                return f'{optimizationresult.pinhole_1:.0f}'
            elif columnlabel == 'PH#2':
                return f'{optimizationresult.pinhole_2:.0f}'
            elif columnlabel == 'PH#3':
                return f'{optimizationresult.pinhole_3:.0f}'
            else:
                return None
        else:
            return None

    def columnCount(self, parent: QtCore.QModelIndex = ...) -> int:
        return 10

    def parent(self, child: QtCore.QModelIndex) -> QtCore.QModelIndex:
        return QtCore.QModelIndex()

    def rowCount(self, parent: QtCore.QModelIndex = ...) -> int:
        return len(self._optimizationresults)

    def index(self, row: int, column: int, parent: QtCore.QModelIndex = ...) -> QtCore.QModelIndex:
        return self.createIndex(row, column, None)

    def sort(self, column: int, order: QtCore.Qt.SortOrder = ...) -> None:
        columnlabel, param = list(self._columns.items())[column]
        self.beginResetModel()
        self._optimizationresults.sort(
            key=functools.partial(getattr, __name=param))
        self._optimizationresults.sort(key=lambda optresult: optresult.intensity,
                                       reverse=(order == QtCore.Qt.SortOrder.DescendingOrder))
        self.endResetModel()

    def addOptResult(self, optresult: GeometrySettings):
        self.beginInsertRows(QtCore.QModelIndex(), len(
            self._optimizationresults), len(self._optimizationresults))
        self._optimizationresults.append(optresult)
        self.endInsertRows()

    def setOptResultList(self, optresultlist: List[GeometrySettings]):
        self.beginResetModel()
        self._optimizationresults = optresultlist[:]
        self.endResetModel()

    def clear(self):
        self.beginResetModel()
        self._optimizationresults = []
        self.endResetModel()

    def iter_results(self) -> Iterator[Dict[str, Any]]:
        for result in self._optimizationresults:
            yield result

    def get(self, item: int) -> Dict[str, Any]:
        return self._optimizationresults[item]

    def __len__(self) -> int:
        return len(self._optimizationresults)
