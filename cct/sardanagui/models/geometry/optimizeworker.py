from typing import Sequence, Dict, Any, Tuple, List
import math
import itertools
from .settings import GeometrySettings
import copy


def yieldspacers(spacers: Sequence[float]):
    uniquespacers = sorted(set(spacers))
    spacercounts = [spacers.count(s) for s in uniquespacers]
    # for L1:
    for countsforl1 in itertools.product(*[range(c + 1) for c in spacercounts]):
        assert len(countsforl1) == len(spacercounts)
        for countsforl2 in itertools.product(*[range(c + 1 - cl1) for c, cl1 in zip(spacercounts, countsforl1)]):
            assert all([c1 + c2 <= tot for c1, c2,
                       tot in zip(countsforl1, countsforl2, spacercounts)])
            yield countsforl1, countsforl2, uniquespacers


def yieldflightpipes(flightpipes: Sequence[float]):
    uniquepipes = sorted(set(flightpipes))
    pipecounts = [flightpipes.count(s) for s in uniquepipes]
    # for L1:
    for counts in itertools.product(*[range(0, c + 1) for c in pipecounts]):
        if sum(counts) > 0:  # do not allow 0 flight pipes
            yield counts, uniquepipes


def worker(geosetting: GeometrySettings, choices: Dict[str, Any], beamdiameteratsample: Tuple[float, float],
           qmin_interval: Tuple[float, float], l1min: float = 0.0, l2min: float = 0.0,
           lmax: float = math.inf):
    compatible: List[GeometrySettings] = []
    drop_reason = {'l1_too_short': 0,
                   'l2_too_short': 0,
                   'total_length_too_long': 0,
                   'dsample_too_small': 0,
                   'dsample_too_large': 0,
                   'no_ph3_large_enough': 0,
                   'no_bs_large_enough': 0,
                   'qmin_too_small': 0,
                   'qmin_too_large': 0,
                   }
    for spacers in yieldspacers(choices['spacers']):
        geosetting.l1_elements = []
        geosetting.l2_elements = []
        for countinl1, countinl2, spacer in zip(*spacers):
            geosetting.l1_elements += [spacer] * countinl1
            geosetting.l2_elements += [spacer] * countinl2
        if geosetting.l1 < l1min:
            drop_reason['l1_too_short'] += 1
            continue
        elif geosetting.l2 < l2min:
            drop_reason['l2_too_short'] += 1
            continue
        for flightpipes in yieldflightpipes(choices['flightpipes']):
            geosetting.flightpipes = []
            for count, length in zip(*flightpipes):
                geosetting.flightpipes += [length] * count
            geosetting.setsddist()
            if geosetting.l1 + geosetting.l2 + geosetting.ph3todetector > lmax:
                drop_reason['total_length_too_long'] += 1
                continue
            for ph1, ph2 in itertools.product(choices['pinholes1'], choices['pinholes2']):
                geosetting.pinhole_1 = ph1
                geosetting.pinhole_2 = ph2
                dbeamsample = geosetting.dbeam_at_sample
                if (dbeamsample < beamdiameteratsample[0]):
                    drop_reason['dsample_too_small'] += 1
                    continue
                if (dbeamsample > beamdiameteratsample[1]):
                    drop_reason['dsample_too_large'] += 1
                    continue
                dbeamph3 = geosetting.dbeam_at_ph3
                try:
                    geosetting.pinhole_3 = min(
                        [aperture for aperture in choices['pinholes3'] if aperture > dbeamph3*1000])
                except ValueError:
                    # happens when the beam is too large at pinhole #3, larger than the largest available pinhole
                    drop_reason['no_ph3_large_enough'] += 1
                    continue
                dparasiticbeamstop = geosetting.dparasitic_at_bs
                try:
                    geosetting.beamstop = min(
                        [bsdiameter for bsdiameter in choices['beamstops'] if bsdiameter > dparasiticbeamstop])
                except ValueError:
                    # happens when the parasitic scattering has a larger diameter at the beamstop position
                    # than the largest beamstop available
                    drop_reason['no_bs_large_enough'] += 1
                    continue
                assert geosetting.beamstop >= geosetting.dbeam_at_bs
                qmin = geosetting.qmin
                if (qmin > qmin_interval[1]):
                    drop_reason['qmin_too_large'] += 1
                    continue
                if (qmin < qmin_interval[0]):
                    drop_reason['qmin_too_small'] += 1
                    continue
                compatible.append(copy.copy(geosetting))
#    print(drop_reason)
    return compatible
