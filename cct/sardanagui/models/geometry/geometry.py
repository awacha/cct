import copy
from typing import Any, List, Optional, Dict

import h5py
import numpy as np
from taurus.external.qt.QtCore import Signal


from .choices import GeometryChoices
from .settings import GeometrySettings
from ..sardanaitemmodel import SardanaItemModel


class Geometry(SardanaItemModel):
    """Describes the current geometry
    """
    choices: Optional[GeometryChoices] = None
    geometryChanged = Signal()
    _currentsettings: GeometrySettings

    def __init__(self, parent=None, **kwargs):
        super().__init__(parent, **kwargs)
        self._currentsettings = GeometrySettings()
        # load the settings from the Sardana environment variable, and listen for changes
        geoconf = self.getEnvironment('SAXSGeometry')
        self._currentsettings = GeometrySettings(**geoconf)
        self.choices = GeometryChoices(door=self.door.name)

    def cleanUp(self):
        self.choices.cleanUp()
        del self.choices
        del self._currentsettings

    def on_environment_changed(self, envname: str, envdata: Any):
        if envname == 'SAXSGeometry':
            newsettings = GeometrySettings(**envdata)
            if self._currentsettings.compare(newsettings):
                self._currentsettings = newsettings
                self.geometryChanged.emit()
        elif envname == 'SAXSGeometryChoices':
            if self.choices is not None:
                self.choices.loadFromConfig()

    def getParameter(self, paramname: str) -> Any:
        """Get a parameter of the current geometry settings. See the class `GeometrySettings`
        for the available parameters"""
        return getattr(self._currentsettings, paramname)

    def setParameter(self, paramname: str, value: Any):
        """Set a parameter of the current geometry settings. See the class `GeometrySettings`
        for the available parameters."""
        setattr(self._currentsettings, paramname, value)
        self.setEnvironment(
            'SAXSGeometry', self._currentsettings.__getstate__())
        self.geometryChanged.emit()

    def saveGeometry(self, filename: str, configdict: Optional[Dict[str, Any]] = None):
        """Save the current settings under a preset name"""
        if configdict is not None:
            settings = GeometrySettings(**configdict)
        else:
            settings = self._currentsettings
        retval = settings.save(filename)
        if configdict is None:
            self.info(f'Saved current geometry to file {filename}.')
        return retval

    def loadGeometry(self, filename: str):
        """Load the geometry from a file"""
        settings = GeometrySettings.load(filename)
        self.updateSettings(settings)
        self.info(f'Loaded geometry from file {filename}.')

    def updateSettings(self, settings: GeometrySettings):
        if settings != self._currentsettings:
            self._currentsettings = settings
            self.setEnvironment(
                'SAXSGeometry', self._currentsettings.__getstate__())
            self.geometryChanged.emit()

    def settings(self) -> GeometrySettings:
        """Get the current geometry settings

        :return: A copy of the current geometry settings
        :rtype: GeometrySettings
        """
        return copy.copy(self._currentsettings)

    def componentchoices(self) -> Dict[str, List[float]]:
        return self.choices.__getstate__()

    def getHeaderEntry(self) -> Dict[str, Any]:
        dic = {}
        geoconf = self.geoconf
        for key in ['dist_sample_det', 'dist_sample_det.err', 'pinhole_1', 'pinhole_2', 'pinhole_3', 'description',
                    'beamstop', 'wavelength', 'wavelength.err', 'beamposx', 'beamposy', 'beamposx.err', 'beamposy.err',
                    'mask', 'pixelsize', 'pixelsize.err']:
            if key.endswith('.err'):
                value = getattr(geoconf, key.rsplit('_', 1)[0]+'_err')
            else:
                value = getattr(geoconf, key)
            dic[key] = value
        for key, alias in [('dist_source_ph1', 'sourcetoph1'),
                           ('dist_ph1_ph2', 'l1'),
                           ('dist_ph2_ph3', 'l2'),
                           ('dist_ph3_sample', 'ph3tosample'),
                           ('dist_det_beamstop', 'beamstoptodetector'),
                           ('dist_sample_det.val', 'dist_sample_det'),
                           ('truedistance', 'dist_sample_det'),
                           ('truedistance.err', 'dist_sample_det.err')]:
            if alias.endswith('.err'):
                value = getattr(geoconf, alias.rsplit('_', 1)[0]+'_err')
            else:
                value = getattr(geoconf, alias)
            dic[key] = value
        for key, value in dic.items():
            if isinstance(value, np.number):
                dic[key] = float(value)
        return dic

    def toNeXus(self, instrumentgroup: h5py.Group, sampleshift: float = 0.0) -> h5py.Group:
        """Write NeXus information

        :param instrumentgroup: NXinstrument HDF5 group
        :type instrumentgroup: h5py.Group instance
        :param sampleshift: how much the sample is nearer to the detector than the canonical position
        :type sampleshift: float (mm)
        :return: the updated NXinstrument HDF5 group
        :rtype: h5py.Group
        """
        geoconf = self.geoconf
        # Add information on the beamstop
        # should be created by the beamstop component
        bsgroup = instrumentgroup.require_group('beam_stop')
        bsgroup.create_dataset(
            'size', data=geoconf.beamstop).attrs.update({'units': 'mm'})
        bsgroup.create_dataset(
            'distance_to_detector', data=geoconf.beamstoptodetector).attrs.update({'units': 'mm'})
        trans = bsgroup.create_group('transformations')
        trans.attrs['NX_class'] = 'NXtransformations'
        trans.create_dataset(
            'z',
            data=geoconf.dist_sample_det - sampleshift - geoconf.beamstoptodetector).attrs.update({
                'transformation_type': 'translation',
                'vector': [0, 0, 1],
                'units': 'mm',
                'depends_on': '.'
            })
        trans.create_dataset('y', data=float(np.asanyarray(bsgroup["y"]))).attrs.update({
            'transformation_type': 'translation',
            'vector': [0, 1, 0],
            'units': 'mm',
            'depends_on': 'z'
        })
        trans.create_dataset('x', data=float(np.asanyarray(bsgroup["x"]))).attrs.update({
            'transformation_type': 'translation',
            'vector': [1, 0, 0],
            'units': 'mm',
            'depends_on': 'y'
        })
        # Create a legacy geometry group for the beamstop
        geogrp = bsgroup.create_group('geometry')
        geogrp.attrs['NX_class'] = 'NXgeometry'
        geogrp.create_dataset('description', data='Beam-stop')
        geogrp.create_dataset('component_index', 1)
        shapegrp = geogrp.create_group('shape')
        shapegrp.attrs['NX_class'] = 'NXshape'
        shapegrp.create_dataset('shape', data='nxcylinder')
        shapegrp.create_dataset(
            'size', data=[[geoconf.beamstop, 0, 0, 0, 1]]).attrs.update({'units': 'mm'})
        translationgrp = geogrp.create_group('translation')
        translationgrp.attrs['NX_class'] = 'NXtranslation'
        translationgrp.create_dataset(
            'distances',
            data=[[
                0, 0, geoconf.dist_sample_det -
                sampleshift - geoconf.beamstoptodetector]]).attrs.update({'units': 'mm'})

        # Pinholes
        for ipinhole, (dist, aperture) in enumerate([
            (geoconf.ph3tosample + geoconf.l2 +
             geoconf.l1 + sampleshift, geoconf.pinhole_1),
            (geoconf.ph3tosample + geoconf.l2 +
             sampleshift, geoconf.pinhole_2),
                (geoconf.ph3tosample + sampleshift, geoconf.pinhole_3)], start=1):
            phgrp = instrumentgroup.create_group(f'pinhole_{ipinhole}')
            phgrp.attrs['NX_class'] = 'NXaperture'
            phgrp.create_dataset('material', data='Pt-Ir alloy')
            phgrp.create_dataset('description', data=f'Pinhole #{ipinhole}')
            transgrp = phgrp.create_group('transformations')
            transgrp.attrs['NX_class'] = 'NXtransformations'
            transgrp.create_dataset('x', data=0).attrs.update(
                {'transformation_type': 'translation', 'vector': [1, 0, 0],
                 'units': 'mm', 'depends_on': '.'})
            transgrp.create_dataset('y', data=0).attrs.update(
                {'transformation_type': 'translation', 'vector': [0, 1, 0],
                 'units': 'mm', 'depends_on': 'x'})
            transgrp.create_dataset('z', data=-dist).attrs.update(
                {'transformation_type': 'translation', 'vector': [0, 0, 1],
                 'units': 'mm', 'depends_on': 'y'})
            shapegrp = phgrp.create_group('shape')
            shapegrp.attrs['NX_class'] = 'NXcylindrical_geometry'
            shapegrp.create_dataset(
                'vertices',
                data=[[0, 0, -dist], [0, aperture / 1000, -dist], [0, 0, -dist]]).attrs.update({
                    "units": 'mm'
                })
            shapegrp.create_dataset('cylinders', data=[0, 1, 2])
            # create legacy geometry class
            geogrp = phgrp.create_group('geometry')
            geogrp.attrs['NX_class'] = 'NXgeometry'
            geogrp.create_dataset('description', data=f'Pinhole #{ipinhole}')
            geogrp.create_dataset('component_index', data=-(3 - ipinhole) - 1)
            shapegrp = geogrp.create_group('shape')
            shapegrp.attrs['NX_class'] = 'NXshape'
            shapegrp.create_dataset('shape', data='nxcylinder')
            shapegrp.create_dataset(
                'size', data=[[aperture / 1000, 0.1, 0, 0, 1]]).attrs.update({'units': 'mm'})
            translationgrp = geogrp.create_group('translation')
            translationgrp.attrs['NX_class'] = 'NXtranslation'
            translationgrp.create_dataset(
                'distances', data=[[0, 0, -dist]]).attrs.update({'units': 'mm'})

        # Crystal and Monochromator: only to set the wavelength
        crystgrp = instrumentgroup.create_group('crystal')
        crystgrp.attrs['NX_class'] = 'NXcrystal'
        crystgrp.create_dataset(
            'wavelength', data=geoconf.wavelength).attrs.update({'units': 'nm'})
        crystgrp.create_dataset(
            'wavelength_errors', data=geoconf.wavelength_err).attrs.update({'units': 'nm'})
        mcgrp = instrumentgroup.create_group('monochromator')
        mcgrp.attrs['NX_class'] = 'NXmonochromator'
        mcgrp.create_dataset(
            'wavelength', data=geoconf.wavelength).attrs.update({'units': 'nm'})
        mcgrp.create_dataset(
            'wavelength_errors', data=geoconf.wavelength_err).attrs.update({'units': 'nm'})
        hcdive = (299792458 * 6.6260705e-34 / 1.60217663e-19) * 1e9  # eV * nm
        mcgrp.create_dataset('energy', data=hcdive /
                             geoconf.wavelength).attrs.update({'units': 'eV'})
        mcgrp.create_dataset(
            'energy_errors',
            data=hcdive / geoconf.wavelength ** 2 * geoconf.wavelength_err).attrs.update({
                'units': 'eV'})
        mcgrp.create_dataset(
            'wavelength_spread', data=geoconf.wavelength_err / geoconf.wavelength)

        # update the source
        sourcegrp: h5py.Group = instrumentgroup[[grp for grp in instrumentgroup if
                                                 ('NX_class' in instrumentgroup[grp].attrs) and (
                                                     instrumentgroup[grp].attrs['NX_class'] == 'NXsource')][0]]
        transformgrp = sourcegrp.create_group('transformations')
        transformgrp.attrs['NX_class'] = 'NXtransformations'
        transformgrp.create_dataset('x', data=0).attrs.update(
            {'transformation_type': 'translation', 'vector': [1, 0, 0],
             'units': 'mm', 'depends_on': '.'})
        transformgrp.create_dataset('y', data=0).attrs.update(
            {'transformation_type': 'translation', 'vector': [1, 0, 0],
             'units': 'mm', 'depends_on': 'x'})
        transformgrp.create_dataset(
            'z',
            data=-geoconf.ph3tosample - geoconf.l2 -
            geoconf.l1 - geoconf.sourcetoph1 - sampleshift).attrs.update({
                'transformation_type': 'translation', 'vector': [1, 0, 0], 'units': 'mm', 'depends_on': 'y'})
        geogrp = sourcegrp.create_group('geometry')
        geogrp.attrs['NX_class'] = 'NXgeometry'
        geogrp.create_dataset('component_index', data=-4)
        translationgrp = geogrp.create_group('translation')
        translationgrp.attrs['NX_class'] = 'NXtranslation'
        translationgrp.create_dataset(
            'distances', data=[
                [0, 0, -geoconf.ph3tosample - geoconf.l2 -
                 geoconf.l1 - geoconf.sourcetoph1 - sampleshift]]).attrs.update({'units': 'mm'})

        # update the detector
        detgroup: h5py.Group = instrumentgroup[[grp for grp in instrumentgroup if
                                                ('NX_class' in instrumentgroup[grp].attrs) and (
                                                    instrumentgroup[grp].attrs['NX_class'] == 'NXdetector')][0]]
        detgroup.create_dataset(
            'distance', data=geoconf.dist_sample_det - sampleshift).attrs['units'] = 'mm'
        detgroup.create_dataset(
            'distance_errors', data=geoconf.dist_sample_det_err).attrs['units'] = 'mm'
        detgroup.create_dataset(
            'x_pixel_size', data=geoconf.pixelsize).attrs['units'] = 'mm'
        detgroup.create_dataset(
            'x_pixel_size_errors', data=geoconf.pixelsize_err).attrs['units'] = 'mm'
        detgroup.create_dataset(
            'y_pixel_size', data=geoconf.pixelsize).attrs['units'] = 'mm'
        detgroup.create_dataset(
            'y_pixel_size_errors', data=geoconf.pixelsize_err).attrs['units'] = 'mm'
        detgroup.create_dataset(
            'beam_center_x', data=geoconf.beamposy * geoconf.pixelsize).attrs['units'] = 'mm'
        detgroup.create_dataset(
            'beam_center_y', data=geoconf.beamposx * geoconf.pixelsize).attrs['units'] = 'mm'
        detgroup.create_dataset(
            'beam_center_x_errors', data=(geoconf.beamposx_err ** 2 * geoconf.pixelsize ** 2 +
                                          geoconf.pixelsize_err ** 2 * geoconf.beamposx ** 2) ** 0.5
        ).attrs['units'] = 'mm'
        detgroup.create_dataset(
            'beam_center_y_errors', data=(geoconf.beamposy_err ** 2 * geoconf.pixelsize ** 2 +
                                          geoconf.pixelsize_err ** 2 * geoconf.beamposy ** 2) ** 0.5
        ).attrs['units'] = 'mm'
        detgroup.create_dataset('polar_angle', data=0.0).attrs['units'] = 'rad'
        detgroup.create_dataset(
            'azimuthal_angle', data=0.0).attrs['units'] = 'rad'
        detgroup.create_dataset(
            'rotation_angle', data=0.0).attrs['units'] = 'rad'
        detgroup.create_dataset('aequatorial_angle',
                                data=0.0).attrs['units'] = 'rad'
        transformgrp = detgroup.create_group('transformations')
        transformgrp.attrs['NX_class'] = 'NXtransformations'
        transformgrp.create_dataset('x', data=0).attrs.update(
            {'transformation_type': 'translation', 'vector': [1, 0, 0],
             'units': 'mm', 'depends_on': '.'})
        transformgrp.create_dataset('y', data=0).attrs.update(
            {'transformation_type': 'translation', 'vector': [1, 0, 0],
             'units': 'mm', 'depends_on': 'x'})
        transformgrp.create_dataset('z', data=geoconf.dist_sample_det - sampleshift).attrs.update({
            'transformation_type': 'translation', 'vector': [1, 0, 0], 'units': 'mm', 'depends_on': 'y'})
        geogrp = detgroup.create_group('geometry')
        geogrp.attrs['NX_class'] = 'NXgeometry'
        geogrp.create_dataset('component_index', data=2)
        translationgrp = geogrp.create_group('translation')
        translationgrp.attrs['NX_class'] = 'NXtranslation'
        translationgrp.create_dataset('distances', data=[
                                      [0, 0, geoconf.dist_sample_det - sampleshift]]).attrs.update({'units': 'mm'})

        return instrumentgroup
