import logging
from typing import Any, Dict, List

import sqlalchemy.orm
import tango
import taurus
from taurus.core.tango.tangoattribute import TangoAttribute
from taurus.core.tango.tangodevice import TangoDevice
from taurus.core.taurusbasetypes import TaurusEventType
from taurus.external.qt import QtCore, QtGui
from taurus.external.qt.QtCore import Qt

from ...core2 import orm_schema
from .sardanaitemmodel import SardanaItemModel

logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)


class Motor:
    name: str
    role: orm_schema.MotorRole
    direction: orm_schema.MotorDirection
    dev: TangoDevice
    _position: TangoAttribute
    _state: TangoAttribute
    _watched_attributes = ['Position', 'Limit_switches', 'CurrentVelocity',
                           'CurrentLoad', 'State', 'DriverError', 'TargetPosition']
    _attrs: Dict[str, TangoAttribute]

    def __init__(self, name: str, role: orm_schema.MotorRole, direction: orm_schema.MotorDirection) -> None:
        self.dev = taurus.Device(name)
        self._attrs = {}
        assert isinstance(self.dev, TangoDevice)
        self.role = role
        self.name = name
        self.direction = direction
        for attr in self._watched_attributes:
            self._attrs[attr] = self.dev.getAttribute(attr)

    def addListener(self, listener):
        self.dev.addListener(listener)
        for attr in self._watched_attributes:

            self.dev.getAttribute(attr).addListener(listener)

    def removeListener(self, listener):
        self.dev.removeListener(listener)
        for attr in self._watched_attributes:
            self.dev.getAttribute(attr).removeListener(listener)

    def getPosition(self) -> float:
        return float(self.dev.getAttribute('Position').rvalue)

    def getVelocity(self) -> float:
        return float(self.dev.getAttribute('CurrentVelocity').rvalue)

    def getLeftLimitSwitchStatus(self) -> float:
        return self.dev.getAttribute('Limit_switches').rvalue[2]

    def getRightLimitSwitchStatus(self) -> float:
        return self.dev.getAttribute('Limit_switches').rvalue[1]

    def getRange(self) -> float:
        return [float(x) for x in self.dev.getAttribute('Position').getRange()]

    def isMoving(self) -> float:
        return self.dev.stateObj.read().rvalue == tango.DevState.MOVING

    def getLoad(self):
        return int(self.dev.getAttribute('CurrentLoad').rvalue)

    def getDriverError(self) -> int:
        return int(self.dev.getAttribute('DriverError').rvalue)


class Motors(SardanaItemModel):
    motors: List[Motor]

    def __init__(self, **kwargs):
        self.motors = []
        super().__init__(**kwargs)
        self.beginResetModel()
        with self.getSQLSession() as session:
            sardanamotors = session.scalars(
                sqlalchemy.select(orm_schema.SardanaMotor))
            for sm in sardanamotors:
                assert isinstance(sm, orm_schema.SardanaMotor)
                self.motors.append(Motor(sm.motor, sm.role, sm.direction))
        self.motors = sorted(self.motors, key=lambda m: m.name)
        self.endResetModel()
        for m in self.motors:
            m.addListener(self)

    def cleanUp(self):
        for m in self.motors:
            m.removeListener(self)
        self.motors = []

    def columnCount(self, parent: QtCore.QModelIndex = ...) -> int:
        return 9

    def rowCount(self, parent: QtCore.QModelIndex = ...) -> int:
        return len(self.motors)

    def index(self, row: int, column: int, parent: QtCore.QModelIndex = ...) -> QtCore.QModelIndex:
        return self.createIndex(row, column, None)

    def headerData(self, section: int, orientation: QtCore.Qt.Orientation, role: int = ...) -> Any:
        if orientation == QtCore.Qt.Orientation.Horizontal and role == QtCore.Qt.ItemDataRole.DisplayRole:
            return \
                ['Motor name', 'Left limit', 'Right limit', 'Position', 'Speed',
                 'Left switch', 'Right switch', 'Load', 'Status flags'][section]

    def data(self, index: QtCore.QModelIndex, role: int = ...) -> Any:
        if role not in [
                QtCore.Qt.ItemDataRole.DisplayRole,
                QtCore.Qt.ItemDataRole.CheckStateRole,
                QtCore.Qt.ItemDataRole.BackgroundRole,
                QtCore.Qt.ItemDataRole.FontRole,
                QtCore.Qt.ItemDataRole.UserRole,
                QtCore.Qt.ItemDataRole.DecorationRole]:
            return None
#        logger.debug(f'data({index.row()=}, {index.column()=}, {role=})')
        if role == QtCore.Qt.ItemDataRole.DisplayRole:
            try:
                if index.column() == 0:  # motor name
                    return self.motors[index.row()].name
                elif index.column() == 1:  # left limit
                    return f"{self.motors[index.row()].getRange()[0]:.4f}"
                elif index.column() == 2:  # right limit
                    return f"{self.motors[index.row()].getRange()[1]:.4f}"
                elif index.column() == 3:  # position
                    return f"{self.motors[index.row()].getPosition():.4f}"
                elif index.column() == 4:  # speed
                    return f"{self.motors[index.row()].getVelocity():.4f}"
                elif index.column() in [5, 6]:  # left and right switches
                    return None  # CheckStateRole will show the switch status
                elif index.column() == 7:
                    return self.motors[index.row()].getLoad()
                elif index.column() == 8:
                    return self.motors[index.row()].getDriverError()
            except (KeyError):
                # happens when a controller is missing
                return None
        elif role == QtCore.Qt.ItemDataRole.CheckStateRole:
            try:
                if index.column() == 5:
                    return QtCore.Qt.CheckState.Checked if self.motors[index.row()].getLeftLimitSwitchStatus() \
                        else QtCore.Qt.CheckState.Unchecked
                elif index.column() == 6:
                    return QtCore.Qt.CheckState.Checked if self.motors[index.row()].getRightLimitSwitchStatus() \
                        else QtCore.Qt.CheckState.Unchecked
            except (KeyError):
                # happens when a controller is missing
                return None
        elif role == QtCore.Qt.ItemDataRole.BackgroundRole:
            try:
                return QtGui.QColor('lightgreen') if self.motors[index.row()].isMoving() else None
            except KeyError:
                # happens when a controller is missing
                return None
        elif role == QtCore.Qt.ItemDataRole.FontRole:
            if index.column() == 3:
                font = QtGui.QFont()
                font.setBold(True)
                return font
        elif role == QtCore.Qt.ItemDataRole.UserRole:
            return self.motors[index.row()]
        elif (role == QtCore.Qt.ItemDataRole.DecorationRole) and (index.column() == 0):
            if self.motors[index.row()].role == orm_schema.MotorRole.Sample:
                return QtGui.QIcon(QtGui.QPixmap(":/icons/sample.svg"))
            elif self.motors[index.row()].role == orm_schema.MotorRole.BeamStop:
                return QtGui.QIcon(QtGui.QPixmap(":/icons/beamstop-in.svg"))
            elif self.motors[index.row()].role == orm_schema.MotorRole.Pinhole:
                return QtGui.QIcon(QtGui.QPixmap(":/icons/pinhole.svg"))
            else:
                return None
        return None

    def flags(self, index: QtCore.QModelIndex) -> QtCore.Qt.ItemFlag:
        return QtCore.Qt.ItemFlag.ItemNeverHasChildren | QtCore.Qt.ItemFlag.ItemIsEnabled | \
            QtCore.Qt.ItemFlag.ItemIsSelectable

    def parent(self, child: QtCore.QModelIndex) -> QtCore.QModelIndex:
        return QtCore.QModelIndex()

    def handleEvent(self, evt_src, evt_type, evt_value):
        if isinstance(evt_src, TangoDevice):
            motorname = evt_src.name
            attrname = None
        elif isinstance(evt_src, TangoAttribute):
            motorname = evt_src.parentObj.name
            attrname = evt_src.name
        else:
            motorname = attrname = None
        row = [i for i, m in enumerate(
            self.motors) if m.name == motorname][0]
        if evt_type == TaurusEventType.Change:
            if attrname is None:
                self.dataChanged.emit(
                    self.index(row, 0),
                    self.index(row, self.columnCount())
                )
            else:
                match attrname:
                    case 'Position':
                        self.dataChanged.emit(self.index(row, 3), self.index(
                            row, 3), [Qt.ItemDataRole.DisplayRole])
                    case 'Limit_switches':
                        self.dataChanged.emit(self.index(row, 5), self.index(
                            row, 6), [Qt.ItemDataRole.CheckStateRole])
                    case 'CurrentVelocity':
                        self.dataChanged.emit(self.index(row, 4), self.index(
                            row, 4), [Qt.ItemDataRole.DisplayRole])
                    case 'CurrentLoad':
                        self.dataChanged.emit(self.index(row, 7), self.index(
                            row, 7), [Qt.ItemDataRole.DisplayRole])
                    case 'state':
                        self.dataChanged.emit(self.index(row, 0), self.index(
                            row, self.columnCount()), [Qt.ItemDataRole.BackgroundRole])
                    case 'DeviceError':
                        self.dataChanged.emit(self.index(row, 8), self.index(
                            row, 8), [Qt.ItemDataRole.DisplayRole])
                    case _:
                        self.warning(f'Unknown attribute: {attrname}')
        elif evt_type == TaurusEventType.Config:
            if isinstance(evt_src, TangoAttribute) and evt_src.name == 'Position':
                self.dataChanged.emit(self.index(row, 1), self.index(
                    row, 2), [Qt.ItemDataRole.DisplayRole])
