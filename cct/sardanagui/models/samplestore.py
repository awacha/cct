import copy
import pickle
import time
import json
import traceback
from typing import Any, Dict, Final, Iterable, List, Optional, Tuple


from sardana.taurus.core.tango.sardana.macro import Macro
import taurus.core.tango
from taurus.external.qt import QtCore
from taurus.external.qt.QtCore import Signal, Slot
import tango
import dateutil.parser

from ...core2 import orm_schema
from .sardanaitemmodel import SardanaItemModel


class SampleStore(SardanaItemModel):
    _columns: Final[List[Tuple[str, str]]] = [
        ('title', 'Title'),
        ('positionx', 'X position'),
        ('positiony', 'Y position'),
        ('distminus', 'Dist. decr.'),
        ('thickness', 'Thickness (cm)'),
        ('transmission', 'Transmission'),
        ('preparedby', 'Prepared by'),
        ('preparetime', 'Preparation date'),
        ('category', 'Category'),
        ('situation', 'Situation')]

    _currentsample: str
    _cache_lifetime: int = 5.0  # seconds
    _sample_cache: Dict[str, Tuple[Dict[str, Any], float]]
    sampleListChanged = Signal()
    currentSampleChanged = Signal(str)  # sample name or None
    sampleEdited = Signal(str, str, object)
    movingStarted = Signal(str)
    movingToSample = Signal(str, str, float, float,
                            float)  # sample, motor name, motor position, start position, end position
    movingFinished = Signal(bool, str)  # success, sample
    sortedmodel: QtCore.QSortFilterProxyModel
    xmotor: Optional[taurus.core.tango.TangoDevice] = None
    ymotor: Optional[taurus.core.tango.TangoDevice] = None
    _xmotorstartpos: Optional[float] = None
    _ymotorstartpos: Optional[float] = None
    _movetargetsample: Optional[str] = None

    def __init__(self, parent: QtCore.QObject | None = ..., **kwargs):
        super().__init__(parent, **kwargs)
        try:
            tangodevicename = kwargs['tangodevice']
        except KeyError:
            tangodevicename = sorted(tango.Database().get_device_exported_for_class('SampleStore'))[0]
        self._tangodevice = tango.DeviceProxy(tangodevicename)
        self._sample_cache = {}
        self.sortedmodel = QtCore.QSortFilterProxyModel()
        self.sortedmodel.setSourceModel(self)
        self.sortedmodel.sort(0, QtCore.Qt.SortOrder.AscendingOrder)
        self._connectSampleMotors()
        self.loadFromDB()
        try:
            self._currentsample = self.sinfo['currentsample']
            self.currentSampleChanged.emit(self._currentsample)
        except KeyError:
            self._currentsample = None
            self.warning(
                'SampleInfo environment variable is not found in Sardana or current sample is not defined.')

    @staticmethod
    def _find_category(value):
        if isinstance(value, orm_schema.Categories):
            return value
        for n in orm_schema.Categories:
            options = [n.name.lower(), n.value.lower()]
            for o in options[:]:
                options.append(o.replace("_", " "))
            for o in options[:]:
                options.append(o.replace("_and_", "+"))
            options = set(options)
            if value in options:
                return n
        else:
            raise ValueError(f"Invalid sample category {value}")

    @staticmethod
    def _find_situation(value):
        if isinstance(value, orm_schema.Situations):
            return value
        for n in orm_schema.Situations:
            options = [n.name.lower(), n.value.lower()]
            for o in options[:]:
                options.append(o.replace("_", " "))
            for o in options[:]:
                options.append(o.replace("_and_", "+"))
            options = set(options)
            if value in options:
                return n
        else:
            raise ValueError(f"Invalid sample situation {value}")


    def _loadsamplefromjson(self, encoded: Tuple[str, bytes]):
        if encoded[0] == 'json':
            data = json.loads(encoded[1])
            data['category'] = self._find_category(data['category'])
            data['situation'] = self._find_situation(data['situation'])
            data['preparetime'] = dateutil.parser.parse(data['preparetime'])
            return data
        else:
            raise ValueError(f'Unexpected encoding: {encoded[0]}')

    def _dumpsampletojson(self, dic: Dict[str, Any]) -> Tuple[str, bytes]:
        dic = dic.copy()
        dic['category'] = dic['category'].value
        dic['situation'] = dic['situation'].value
        dic['preparetime'] = str(dic['preparetime'])
        for key in dic:
            dic[key] = str(dic[key])
        return ('json', json.dumps(dic))

    def cleanUp(self) -> None:
        self._disconnectSampleMotors()
        super().cleanUp()

    def loadFromDB(self):
        self.beginResetModel()
        self._sample_cache = {}
        try:
            self._sample_cache = {sn: (self._loadsamplefromjson(self._tangodevice.GetSampleJSON(sn)), time.monotonic()) for sn in self._tangodevice.GetSampleList()}
        except Exception as exc:
            self.error(f'Cannot load samples from database: {exc}')
        finally:
            self.endResetModel()
            self.sampleListChanged.emit()

    def on_environment_changed(self, name: str, value: Any):
        if name == 'SampleInfo':
            self.debug(
                f'SampleInfo environment variable changed in Sardana: {value=}')
            try:
                if self._currentsample != value['currentsample']:
                    self._currentsample = value['currentsample']
                    self.currentSampleChanged.emit(self._currentsample)
            except KeyError:
                self.warning(
                    'Current sample information is missing from the Sardana environment variable "SampleInfo"')
            if ((self.xmotor is None) or (self.xmotor.name != value['motx'])) or \
                    ((self.ymotor is None) or (self.ymotor.name != value['moty'])):
                self._connectSampleMotors()

    @property
    def sinfo(self) -> Dict[str, Any]:
        return self.door.getEnvironment('SampleInfo')

    @sinfo.setter
    def sinfo(self, value: Dict[str, Any]):
        self.debug(f'Setting SampleInfo environment to {value}')
        self.door.setEnvironment('SampleInfo', value)

# ------------  QAbstractItemModel overloaded functions ------------------

    def rowCount(self, parent: QtCore.QModelIndex = ...) -> int:
        return len(self._sample_cache)

    def columnCount(self, parent: QtCore.QModelIndex = ...) -> int:
        return len(self._columns)

    def parent(self, child: QtCore.QModelIndex) -> QtCore.QModelIndex:
        return QtCore.QModelIndex()

    def index(self, row: int, column: int, parent: QtCore.QModelIndex = ...) -> QtCore.QModelIndex:
        return self.createIndex(row, column, None)

    def data(self, index: QtCore.QModelIndex, role: int = ...) -> Any:
        attribute = self._columns[index.column()][0]
        samplename = self._titleForRowIndex(index.row())
        sample = self.get(samplename)
#        self.info(f'Sample at row {index.row()}, column {index.column()} is {sample}')
        if ((role == QtCore.Qt.ItemDataRole.DisplayRole) or (role == QtCore.Qt.ItemDataRole.EditRole)) \
                and (attribute == 'title'):
            return sample['title']
        elif ((role == QtCore.Qt.ItemDataRole.DisplayRole) or (role == QtCore.Qt.ItemDataRole.EditRole)) \
                and (attribute == 'preparedby'):
            return sample['preparedby']
        elif ((role == QtCore.Qt.ItemDataRole.DisplayRole) or (role == QtCore.Qt.ItemDataRole.EditRole)) \
                and (attribute == 'category'):
            return sample["category"].value
        elif ((role == QtCore.Qt.ItemDataRole.DisplayRole) or (role == QtCore.Qt.ItemDataRole.EditRole)) \
                and (attribute == 'situation'):
            return sample['situation'].value
        elif (role == QtCore.Qt.ItemDataRole.DisplayRole) and (attribute == 'preparetime'):
            return str(sample['preparetime'])
        elif (role == QtCore.Qt.ItemDataRole.EditRole) and (attribute == 'preparetime'):
            return dateutil.parser.parse(sample['preparetime'])
        elif (role == QtCore.Qt.ItemDataRole.DisplayRole) and (attribute == 'positionx'):
            return f'{sample["positionx"]:.4f}'
        elif (role == QtCore.Qt.ItemDataRole.DisplayRole) and (attribute == 'positiony'):
            return f'{sample["positiony"]:.4f}'
        elif (role == QtCore.Qt.ItemDataRole.DisplayRole) and (attribute == 'thickness'):
            return f'{sample["thickness"]:.4f}'
        elif (role == QtCore.Qt.ItemDataRole.DisplayRole) and (attribute == 'distminus'):
            return f'{sample["distminus"]:.4f}'
        elif (role == QtCore.Qt.ItemDataRole.DisplayRole) and (attribute == 'transmission'):
            return f'{sample["transmission"]:.4f}'
        elif (role == QtCore.Qt.ItemDataRole.EditRole) and (
                attribute in ['positionx', 'positiony', 'thickness', 'distminus', 'transmission']):
            return sample[attribute]
        elif (role == QtCore.Qt.ItemDataRole.ToolTipRole) and (attribute == 'title'):
            return sample["description"]
        elif (role == QtCore.Qt.ItemDataRole.ToolTipRole) and (
                attribute in ['positionx', 'positiony', 'thickness', 'distminus', 'transmission']):
            attr = sample[attribute], sample[attribute+'_error']
            return f'{attr[0]:.4f} \xb1 {attr[1]:.4f}'
        elif role == QtCore.Qt.ItemDataRole.UserRole:
            return sample
        else:
            return None

    def setData(self, index: QtCore.QModelIndex, value: Any, role: int = ...) -> bool:
        sample = self.get(self._titleForRowIndex(index.row()))
        attribute = self._columns[index.column()][0]
        if role == QtCore.Qt.ItemDataRole.EditRole:
            try:
                return self.updateSample(sample["title"], attribute, value)
            except Exception as exc:
                self.warning(
                    f'Could not update attribute {attribute} of sample {sample.title} to {value}: {exc}')
                return False

    def flags(self, index: QtCore.QModelIndex) -> QtCore.Qt.ItemFlag:
        return QtCore.Qt.ItemFlag.ItemNeverHasChildren | QtCore.Qt.ItemFlag.ItemIsSelectable |\
            QtCore.Qt.ItemFlag.ItemIsDragEnabled | QtCore.Qt.ItemFlag.ItemIsEditable | \
            QtCore.Qt.ItemFlag.ItemIsEnabled

    def removeRow(self, row: int, parent: QtCore.QModelIndex = ...) -> bool:
        return self.removeRows(row, 1, parent)

    def removeRows(self, row: int, count: int, parent: QtCore.QModelIndex = ...) -> bool:
        if parent is ...:
            parent = QtCore.QModelIndex()
        assert not parent.isValid()
        self.beginRemoveRows(parent, row, row + count - 1)
        try:
            self.debug(f'Beginning removal of rows {row} (inclusive) to {row+count} (not inclusive)')
            titles = [self._titleForRowIndex(r) for r in range(row, row+count)]
            for sn in titles:
                try:
                    self._tangodevice.DeleteSample(sn)
                except tango.DevFailed:
                    self.warning(f'Cannot delete sample {sn}: it has probably already been deleted.')
                    pass
                del self._sample_cache[sn]
            return True
        except ValueError:
            self.error(f'Error while removing rows {row}..{row+count-1}: {traceback.format_exc()}')
            return False
        finally:
            self.endRemoveRows()
            self.sampleListChanged.emit()

    def headerData(self, section: int, orientation: QtCore.Qt.Orientation, role: int = ...) -> Any:
        if (orientation == QtCore.Qt.Orientation.Horizontal) and (role == QtCore.Qt.ItemDataRole.DisplayRole):
            return self._columns[section][1]

    def mimeData(self, indexes: Iterable[QtCore.QModelIndex]) -> QtCore.QMimeData:
        md = QtCore.QMimeData()
        samples = [self.get(self._titleForRowIndex(i.row())) for i in indexes]
        md.setData('application/x-cctsampledictlist', pickle.dumps(samples))
        return md

    def mimeTypes(self) -> List[str]:
        return ['application/x-cctsampledictlist']

    def supportedDragActions(self) -> QtCore.Qt.DropAction:
        return QtCore.Qt.DropAction.CopyAction

    # --------------   Various helper methods  -----------------------

    def __contains__(self, item: str) -> bool:
        return item in self._sample_cache

    def samplenames(self) -> List[str]:
        """Get the list of sample names

        :return: the list of sample names
        :rtype: List[str]
        """
        return list(self._sample_cache.keys())

    def samples(self):
        for sn in self._sample_cache:
            yield self.get(sn)

    def get(self, title: str, force_refresh: bool = False) -> Dict[str, Any]:
        """Get the sample instance with the given title.

        :param title: sample title
        :type title: str
        :param force_refresh: if True, fetch this sample from the database, defaults to False
        :type force_refresh: bool, optional
        :raises KeyError: if the sample does not exist in the database
        :return: the sample instance
        :rtype: Sample
        """
        if force_refresh or (title not in self._sample_cache) or \
                (time.monotonic() - self._sample_cache[title][1]) > self._cache_lifetime:
            try:
                s = self._tangodevice.GetSampleJSON(title)
            except tango.DevFailed as devf:
                if devf.args[0].desc.startswith('ValueError: No such sample'):
                    if title in self._sample_cache:
                        # the sample has been removed from the database: remove the corresponding row from the model
                        row = self._rowIndexForTitle(title)
                        self.beginRemoveRows(QtCore.QModelIndex(), row, row)
                        del self._sample_cache[title]
                        self.endRemoveRows()
                        raise KeyError(f'Sample {title} removed by someone else')
                    else:
                        raise KeyError(f'Sample {title} does not exist')
            if title not in self._sample_cache:
                # a new sample, not yet cached. Create a cache entry and add a new row to the model, too.
                assert s is not None
                self._sample_cache[title] = (self._loadsamplefromjson(s), time.monotonic())
                row = self._rowIndexForTitle(title)
                self.beginInsertRows(QtCore.QModelIndex(), row, row)
                self.endInsertRows()
            else:
                # old sample, refreshing
                self._sample_cache[title] = (self._loadsamplefromjson(s), time.monotonic())
                # we don't issue dataChanged signals
        try:
            return copy.deepcopy(self._sample_cache[title][0])
        except KeyError:
            raise KeyError(f'Sample {title} does not exist in the database.')

    def __len__(self) -> int:
        return len(self._sample_cache)

    def _rowIndexForTitle(self, title: str):
        return sorted(self._sample_cache.keys()).index(title)

    def _titleForRowIndex(self, row: int) -> str:
        return sorted(self._sample_cache.keys())[row]

    def updateSample(self, samplename: str, attribute: str, value: Any) -> bool:
        if self.get(samplename, force_refresh=True)[attribute] == value:
            # Do not update if no change
            return
        if attribute in ['category', 'situation'] and isinstance(value, (orm_schema.Categories, orm_schema.Situations)):
            value = value.value
        elif attribute in ['preparetime']:
            value = str(value)
        else:
            value = str(value)
        self._tangodevice.ModifySample([samplename, attribute, value])
        # sample title is the primary key in the SQL database, so the SQL backend takes care of the unicity of sample
        # title.
        # Now that the sample has been refreshed in the database, we'll emit dataChanged events when required
        if attribute == 'title':
            try:
                self.get(samplename, force_refresh=True)
                raise ValueError(f'Sample {samplename} should have been renamed to {value}, but still exists!')
            except KeyError:
                # this is what we expect
                pass
            samplename = value
        # The get() command will refresh the cache, as well as taking care of issuing rowInserted and rowRemoved
        # signals.
        sample = self.get(samplename, force_refresh=True)
        # if this line is reached, the sample is still in the database
        row = self._rowIndexForTitle(sample["title"])
        self.dataChanged.emit(
            self.index(row, 0, None), 
            self.index(row, self.columnCount(QtCore.QModelIndex()), None))
        self.sampleEdited.emit(sample["title"], attribute,
                               sample[attribute])
        return True

    def getFreeSampleName(self, prefix: str) -> str:
        return self._tangodevice.NextFreeSampleName(prefix)

    def addSample(self, samplename: Optional[str] = None, sample: Optional[Dict[str, Any]] = None) -> str:
        """Create a new sample

        :param samplename: the name of the new sample. If None (default), an empty sample with an Untitled name is added
        :type samplename: Optional[str], optional
        :param sample: A sample dictionary if known. Otherwise (if None, default) an empty sample dictionary is made
        :type sample: Optional[Dict[str, Any]], optional
        :return: The name of the newly added sample
        :rtype: str
        """
        self.debug(f'Inserting a new sample with name {samplename}, data: {sample}')
        if (samplename is None) and (sample is None):
            # create a new empty sample
            samplename = self.getFreeSampleName('Untitled')
            self._tangodevice.CreateSample([samplename])
        elif (sample is None) and (samplename is not None):
            self._tangodevice.CreateSample([samplename])
        elif (samplename is not None) and (sample is not None):
            sample['title'] = samplename
            self._tangodevice.CreateSample([samplename])
            self._tangodevice.SetSampleJSON(self._dumpsampletojson(sample))
        else:
            assert (samplename is None) and (sample is not None)
            samplename = sample["title"]
            self._tangodevice.CreateSample([sample["title"]])
            self._tangodevice.SetSampleJSON(self._dumpsampletojson(sample))
        self.debug(f'Inserted a new sample with name {samplename}, data: {sample}')
        self.get(samplename)
        samplenames = self.samplenames()
        row = samplenames.index(sample["title"])
        self.sampleListChanged.emit()
        return samplename

    def indexForSample(self, samplename: str) -> QtCore.QModelIndex:
        try:
            row = self._rowIndexForTitle(samplename)
        except ValueError:
            # if the sample does not exist, return an invalid model index.
            return QtCore.QModelIndex()
        return self.index(row, 0, QtCore.QModelIndex())

    def setCurrentSample(self, title: Optional[str]):
        if (title is not None) and not title:
            raise ValueError('Invalid title')
        self.debug(f'Setting current sample to {title}')
        sinfo = self.sinfo
        sinfo['currentsample'] = title
        self.sinfo = sinfo

    def currentSample(self) -> Optional[Dict[str, Any]]:
        return self.get(self.sinfo['currentsample'])

    def currentSampleName(self) -> Optional[str]:
        return self.sinfo['currentsample']

    def hasMotors(self) -> bool:
        return (self.xmotor is not None) and (self.ymotor is not None)

    def xmotorname(self) -> Optional[str]:
        if self.xmotor is not None:
            return self.xmotor.name
        else:
            return None

    def ymotorname(self) -> Optional[str]:
        if self.ymotor is not None:
            return self.ymotor.name
        else:
            return None

    def on_macro_started(self, macro: Macro):
        if self._movetargetsample is None:
            if macro.name == 'mv_sample':
                self.movingStarted.emit(self._movetargetsample)

    def on_macro_finished(self, macro: Macro, state: str):
        if self._movetargetsample is not None:
            self.door.macroStarted.disconnect(self.onMacroStarted)
            self.door.macroFinished.disconnect(self.onMacroFinished)
            self.door.macroStep.disconnect(self.onMacroStep)
            self._xmotorstartpos = None
            self._ymotorstartpos = None
            if macro.name == 'mv_sample':
                self.movingFinished.emit(
                    state == 'finish', self._movetargetsample)
            self._movetargetsample = None

    def moveToSample(self, samplename: str, direction: str = 'both',
                     setcurrent: bool = True):
        if (self._movetargetsample is not None) or (self._xmotorstartpos is not None) \
                or (self._ymotorstartpos is not None):
            raise RuntimeError(
                'Refusing to start moving the sample holder: already in motion')
        self._xmotorstartpos = self.xmotor.Position
        self._ymotorstartpos = self.ymotor.Position
        self._movetargetsample = samplename
        try:
            self.runMacro(
                'setsample' if setcurrent else 'mv_sample', f'{samplename} ' +
                ('X' if direction.lower() in ['x', 'both'] else '') +
                ('Y' if direction.lower() in ['y', 'both'] else ''))
        except Exception as exc:
            raise RuntimeError(f'Cannot move to sample ({exc})')
        else:
            self._movetargetsample = samplename
        if setcurrent:
            self.setCurrentSample(samplename)

    def _connectSampleMotors(self):
        sinfo = self.sinfo
        self._disconnectSampleMotors()
        for motattr, motname in [('xmotor', 'motx'), ('ymotor', 'moty')]:
            motdev = taurus.Device(sinfo[motname])
            for attr in ['State', 'Position']:
                motdev.getAttribute(attr).addListener(self)
            setattr(self, motattr, motdev)

    def _disconnectSampleMotors(self):
        for motattr in ['xmotor', 'ymotor']:
            motdev = getattr(self, motattr)
            if motdev is None:
                continue
            for attr in ['State', 'Position']:
                motdev.getAttribute(attr).removeListener(self)
            setattr(self, motattr, None)

    def handleEvent(
            self, evt_src: taurus.core.tango.TangoAttribute,
            evt_type: taurus.core.taurusbasetypes.TaurusEventType,
            evt_value: taurus.core.tango.TangoAttrValue):
        if evt_type != taurus.core.taurusbasetypes.TaurusEventType.Change:
            return
        if ((self.xmotor is not None) and evt_src == self.xmotor.getAttribute('Position')) or \
                ((self.ymotor is not None) and evt_src == self.ymotor.getAttribute('Position')):
            if self._movetargetsample is None:
                return
            where = self.xmotor.Position, self.ymotor.Position
            try:
                targetsample = self.get(self._movetargetsample)
            except KeyError:
                return
            target = targetsample.positionx[0], targetsample.positiony[0]
            start = self._xmotorstartpos, self._ymotorstartpos
            delta = [(w-s)/(e-s) if e != s else 1.0 for w,
                     s, e in zip(where, start, target)]
            slowest = delta.index(min(delta))
            self.movingToSample.emit(targetsample.title, [self.xmotorname(), self.ymotorname()][slowest],
                                     where[slowest], start[slowest], target[slowest])

    @Slot(float)
    def onMotorStarted(self, start: float):
        pass

    @Slot(bool, float)
    def onMotorStopped(self, success: bool, end: float):
        return False

    def stopMotors(self):
        if self._movetargetsample is not None:
            self.door.stopMacro()

    def sortedSamplesOfCategory(self, category: orm_schema.Categories) -> QtCore.QSortFilterProxyModel:
        model = QtCore.QSortFilterProxyModel()
        model.setSourceModel(self)
        model.sort(0, QtCore.Qt.SortOrder.AscendingOrder)
        model.setFilterRegularExpression(
            QtCore.QRegularExpression(
                f"^{category.value}$",
                QtCore.QRegularExpression.PatternOption.NoPatternOption))
        model.setFilterKeyColumn(
            [i for i in range(len(self._columns)) if self._columns[i][0] == 'category'][0])
        model.setFilterRole(QtCore.Qt.ItemDataRole.DisplayRole)
        return model
