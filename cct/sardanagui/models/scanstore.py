import datetime
import os
import re
import traceback
from typing import Any, Optional

import h5py
from sardana.macroserver.msexception import UnknownEnv
from taurus.external.qt import QtCore
from taurus.external.qt.QtCore import QModelIndex, Qt
import dateutil.parser

from ...core2.dataclasses import Scan
from .sardanaitemmodel import SardanaItemModel


class ScanStore(SardanaItemModel):
    scanfile: Optional[str] = None

    def __init__(self, parent: QtCore.QObject | None = ..., **kwargs):
        self.scanfile = kwargs.pop('scanfile', None)
        self._data = []
        super().__init__(parent, **kwargs)
        self.reload()

    def rowCount(self, parent: QModelIndex = ...) -> int:
        return len(self._data)

    def columnCount(self, parent: QModelIndex = ...) -> int:
        return 7

    def flags(self, index: QModelIndex) -> Qt.ItemFlags:
        return Qt.ItemFlag.ItemIsEnabled | Qt.ItemFlag.ItemIsSelectable | Qt.ItemFlag.ItemNeverHasChildren

    def data(self, index: QModelIndex, role: int = ...) -> Any:
        try:
            if (index.column() == 0) and (role == Qt.ItemDataRole.DisplayRole):
                return str(self._data[index.row()]['serialno'])
            elif (index.column() == 1) and (role == Qt.ItemDataRole.DisplayRole):
                return str(datetime.datetime.fromtimestamp(self._data[index.row()]['startts']))
            elif (index.column() == 2) and (role == Qt.ItemDataRole.DisplayRole):
                return str(datetime.datetime.fromtimestamp(self._data[index.row()]['endts'])) \
                    if self._data[index.row()]['endts'] is not None else '--'
            elif (index.column() == 3) and (role == Qt.ItemDataRole.DisplayRole):
                return self._data[index.row()]['endstatus']
            elif (index.column() == 4) and (role == Qt.ItemDataRole.DisplayRole):
                return self._data[index.row()]['title']
            elif (index.column() == 5) and (role == Qt.ItemDataRole.DisplayRole):
                return self._data[index.row()]['ScanDir']
            elif (index.column() == 6) and (role == Qt.ItemDataRole.DisplayRole):
                return self._data[index.row()]['ScanFile'][0]
        except Exception as exc:
            self.error(f'{exc}, {traceback.format_exc()}')
        return None

    def headerData(self, section: int, orientation: Qt.Orientation, role: int = ...) -> Any:
        if (orientation == Qt.Orientation.Horizontal) and (role == Qt.ItemDataRole.DisplayRole):
            return ['Serial no', 'Start', 'End', 'End state', 'Title', 'ScanDir', 'ScanFile', ][section]
        return super().headerData(section, orientation, role)

    def parent(self, index: QtCore.QModelIndex):
        return QtCore.QModelIndex()

    def index(self, row: int, column: int, parent: QModelIndex = ...) -> QModelIndex:
        return self.createIndex(row, column)

    def on_environment_changed(self, envname: str, envdata: Any):
        if (envname == 'ScanHistory') and (self.scanfile is None):
            self.reload()

    def reload(self):
        self.beginResetModel()
        if self.scanfile is None:
            # use ScanHistory from Sardana
            try:
                self._data = self.door.getEnvironment('ScanHistory')
            except UnknownEnv:
                self._data = []
        elif self.scanfile.endswith('.spec'):
            self._data = self.reloadSPEC(self.scanfile)
        elif self.scanfile.endswith('.h5'):
            self._data = self.reloadH5(self.scanfile)
        self.endResetModel()

    def loadscan(self, index: int | QtCore.QModelIndex) -> Scan:
        if isinstance(index, QtCore.QModelIndex):
            index = index.row()
        filename = os.path.join(
            self._data[index]['ScanDir'], self._data[index]['ScanFile'][0])
        serialno = self._data[index]['serialno']
        if filename.endswith('.h5'):
            return Scan.fromh5file(filename, serialno)
        elif filename.endswith('.spec'):
            return Scan.fromspecfile(filename, serialno)
        else:
            raise Exception(f'Unknown file extension: {filename}')

    def reloadH5(self, h5filename: str):
        data = []
        scandir, scanfile = os.path.split(h5filename)
        with h5py.File(h5filename) as h5:
            entries = [int(m[1]) for m in [re.match(r'^entry(\d+)$', e)
                                           for e in h5] if m is not None]
            for iscan in sorted(entries):
                group: h5py.Group = h5[f'entry{iscan}']
                cmd = group["title"][()].decode('utf-8')
                macroparams = re.match(
                    r'^(?P<macro>.+scan.*?)\s+'
                    r'(?P<start>[+-]?\d+(\.(\d+)?)?([eE][+-]?\d+)?)\s+'
                    r'(?P<end>[+-]?\d+(\.(\d+)?)?([eE][+-]?\d+)?)\s+'
                    r'(?P<intervals>\d+)\s+'
                    r'(?P<countingtime>[+]?\d+(\.(\d+)?)?([eE][+-]?\d+)?)$', cmd)
                channels = [name
                            for name in group['measurement']
                            if isinstance(group[f'measurement/{name}'], h5py.Dataset)
                            and len(group[f'measurement/{name}'].shape) == 1]
                npoints = group[f'measurement/{channels[0]}'].shape[0]
                if macroparams is not None:
                    nintervals = int(macroparams['intervals'])
                    endstatus = 'Normal' if npoints >= nintervals + 1 else 'Stop'
                else:
                    nintervals = None
                    endstatus = None
                data.append(
                    {
                        'ScanDir': scandir,
                        'ScanFile': [scanfile],
                        'channels': channels,
                        'deadtime': None,
                        'endstatus': endstatus,
                        'endts': dateutil.parser.parse(group['end_time'][()]).timestamp()
                        if 'end_time' in group else None,
                        'estimatedtime': None,
                        'serialno': iscan,
                        'startts': dateutil.parser.parse(group['macro_start_time'][()]).timestamp(),
                        'title': group['title'][()].decode('utf-8'),
                        'user': group['user/name'][()].decode('utf-8'),
                    }
                )
        return data

    def reloadSPEC(self, specfilename: str):
        data = []
        scandir, scanfile = os.path.split(specfilename)
        with open(specfilename, 'rt') as f:
            for line in f:
                if not line.strip():
                    continue
                elif line.startswith('#S '):
                    macroparams_sardana = re.match(
                        r'^#S\s+(?P<scanid>\d+)\s+'
                        r'(?P<macro>.+scan.*?)\s+'
                        r'(?P<start>[+-]?\d+(\.(\d+)?)?([eE][+-]?\d+)?)\s+'
                        r'(?P<end>[+-]?\d+(\.(\d+)?)?([eE][+-]?\d+)?)\s+'
                        r'(?P<intervals>\d+)\s+'
                        r'(?P<countingtime>[+]?\d+(\.(\d+)?)?([eE][+-]?\d+)?)\s*$', line)
                    macroparams_cct = re.match(
                        r'^#S\s+(?P<scanid>\d+)\s+'
                        r'(?P<macro>scan(rel)?)\('
                        r'"(?P<motorname>[a-zA-Z0-9_]+)",\s*'
                        r'(?P<start>[+-]?\d+(\.(\d+)?)?([eE][+-]?\d+)?),\s*'
                        r'(?P<end>[+-]?\d+(\.(\d+)?)?([eE][+-]?\d+)?),\s*'
                        r'(?P<intervals>\d+),\s*'
                        r'(?P<countingtime>[+]?\d+(\.(\d+)?)?([eE][+-]?\d+)?),\s*'
                        r'"(?P<comment>[^"]+)"\)$',
                        line)
                    if macroparams_sardana:
                        macroparams = macroparams_sardana
                    elif macroparams_cct:
                        macroparams = macroparams_cct
                    else:
                        raise Exception(f'Invalid #S line: {line}')
                    
                    data.append(
                        {'ScanDir': scandir, 'ScanFile': [scanfile], 'title': line.split(None, 2)[2].strip(),
                         'serialno': int(macroparams['scanid']), 'countingtime': float(macroparams['countingtime']),
                         'nintervals': int(macroparams['intervals']), 'deadtime': None, 'estimatedtime': None,
                         'endstatus': 'Running',
                         'endts': 0, }
                    )
                elif line.startswith('#U '):
                    data[-1]['user'] = line.split(None, 1)[1]
                elif line.startswith('#D ') and data:
                    data[-1]['startts'] = dateutil.parser.parse(
                        line.split(None, 1)[1]).timestamp()
                elif (m := re.match(r'#C Acquisition started at (?P<time>.+)', line)):
                    data[-1]['startts'] = dateutil.parser.parse(
                        m['time']).timestamp()
                elif (m := re.match(r'#C Acquisition ended at (?P<time>.+)', line)):
                    data[-1]['endts'] = dateutil.parser.parse(
                        m['time']).timestamp()
                    if data[-1]['lastscanpoint'] == data[-1]['nintervals']:
                        data[-1]['endstatus'] = 'Normal'
                    else:
                        data[-1]['endstatus'] = 'Stop'
                elif line.startswith('#O') or line.startswith('#P'):
                    continue
                elif line.startswith('#N'):
                    continue
                elif line.startswith('#L '):
                    data[-1]['channels'] = line.split()[1:]
                    data[-1]['lastscanpoint'] = -1
                elif line.startswith('#'):
                    continue
                else:
                    try:
                        scanpt = int(line.split()[0])
                    except ValueError:
                        scanpt = data[-1]['lastscanpoint'] + 1
                    if data[-1]['lastscanpoint'] == scanpt - 1:
                        data[-1]['lastscanpoint'] = scanpt
                    else:
                        raise Exception(
                            'Scan points in the file are not contiguous, error in scan'
                            f' {data[-1]["scanid"]}, scan point {scanpt}.')
        return data
