from typing import Dict, List, Optional, Tuple

import pint
import sqlalchemy.orm
import tango
import taurus
import taurus.core.tango
from taurus.external.qt import QtCore, QtGui

from ...core2 import orm_schema
from .sardanaitemmodel import SardanaItemModel


class WatchedDevice:
    name: str
    attribute: taurus.core.tango.TangoAttribute

    def __init__(self, name: str, attribute: str = 'status') -> None:
        self.name = name
        self.attribute = attribute
        self.device = taurus.Device(name)
        self.attribute = self.device.getAttribute(attribute)
        self.state = self.device.stateObj
        self.status = self.device.getAttribute('status')

    def addListener(self, listener):
        self.state.addListener(listener)
        self.attribute.addListener(listener)
        self.status.addListener(listener)

    def removeListener(self, listener):
        self.state.removeListener(listener)
        self.attribute.removeListener(listener)
        self.status.removeListener(listener)


class TangoDeviceManager(SardanaItemModel):
    _devices: List[WatchedDevice]
    _timer: Optional[int] = None
    devicesChanged = QtCore.Signal()

    LedMap = {
        tango.DevState.ON: 'led_green_on',
        tango.DevState.OFF: 'led_black_off',
        tango.DevState.CLOSE: 'led_white_on',
        tango.DevState.OPEN: 'led_green_on',
        tango.DevState.INSERT: 'led_white_on',
        tango.DevState.EXTRACT: 'led_green_on',
        tango.DevState.MOVING: 'led_blue_on',
        tango.DevState.STANDBY: 'led_yellow_on',
        tango.DevState.FAULT: 'led_red_on',
        tango.DevState.INIT: 'led_yellow_on',
        tango.DevState.RUNNING: 'led_blue_on',
        tango.DevState.ALARM: 'led_orange_on',
        tango.DevState.DISABLE: 'led_magenta_on',
        tango.DevState.UNKNOWN: 'led_black_off',
        None: 'led_black_off',
    }

    def __init__(self, **kwargs):
        self._devices = []
        super().__init__(**kwargs)
#        self.setLogLevel(self.Debug)
        self.debug('Loading tango device manager data from the SQL database')
        self.loadFromConfig()
        self.debug('Config loaded')

    def rowCount(self, parent=None, *args, **kwargs):
        return len(self._devices)

    def columnCount(self, parent=None, *args, **kwargs):
        return 5

    def data(self, index: QtCore.QModelIndex, role=None):
        wd = self._devices[index.row()]
        if (index.column() == 0) and (role == QtCore.Qt.ItemDataRole.DisplayRole):
            return wd.name
        elif (index.column() == 0) and (role == QtCore.Qt.ItemDataRole.DecorationRole):
            return QtGui.QIcon.fromTheme(f'leds_images256:{self.LedMap[wd.state.rvalue]}.png')
        elif (index.column() == 1) and (role == QtCore.Qt.ItemDataRole.DisplayRole):
            try:
                return wd.state.rvalue.name
            except Exception as exc:
                return f'Tango/Taurus error: {exc}'
        elif (index.column() == 2) and (role == QtCore.Qt.ItemDataRole.DisplayRole):
            try:
                return wd.status.rvalue
            except Exception as exc:
                return f'Tango/Taurus error: {exc}'
        elif (index.column() == 3) and (role == QtCore.Qt.ItemDataRole.DisplayRole):
            return wd.attribute.name
        elif (index.column() == 4) and (role == QtCore.Qt.ItemDataRole.DisplayRole):
            try:
                val = wd.attribute.rvalue
                if isinstance(val, pint.Quantity):
                    return val.magnitude
                else:
                    return str(val)
            except Exception as exc:
                return f'Tango/Taurus error: {exc}'

    def index(self, row, column, parent=None, *args, **kwargs) -> QtCore.QModelIndex:
        return self.createIndex(row, column, None)

    def flags(self, index: QtCore.QModelIndex) -> QtCore.Qt.ItemFlags:
        return QtCore.Qt.ItemFlag.ItemIsEnabled | QtCore.Qt.ItemFlag.ItemIsSelectable | \
            QtCore.Qt.ItemFlag.ItemNeverHasChildren

    def parent(self, index: Optional[QtCore.QModelIndex] = None):
        if index is None:
            return QtCore.QObject.parent(self)
        else:
            return QtCore.QModelIndex()

    def headerData(self, section, orientation, role=None):
        if (orientation == QtCore.Qt.Horizontal) and (role == QtCore.Qt.ItemDataRole.DisplayRole):
            return ['Device', 'State', 'Status', 'Attribute', 'Value'][section]

    def handleEvent(self, evt_src, evt_type, evt_value):
        if evt_type == taurus.core.taurusbasetypes.TaurusEventType.Change:
            if isinstance(evt_src, taurus.core.tango.TangoDevice):
                devname = evt_src.name
                attrname = None
            elif isinstance(evt_src, taurus.core.tango.TangoAttribute):
                devname = evt_src.getParentObj().name
                attrname = evt_src.name
            else:
                self.warning(f'Unknown type for evt_src: {type(evt_src)}')
                return
            wd = [d for d in self._devices if d.name == devname][0]
            idev = self._devices.index(wd)
            if attrname is None:
                self.debug(f'{evt_value}')
            elif attrname == wd.attribute.name:
                self.dataChanged.emit(
                    self.index(idev, 4, QtCore.QModelIndex()),
                    self.index(idev, 4, QtCore.QModelIndex()))
            elif attrname.lower() == 'state':
                self.dataChanged.emit(
                    self.index(idev, 0, QtCore.QModelIndex()),
                    self.index(idev, 1, QtCore.QModelIndex()),
                    [QtCore.Qt.ItemDataRole.DecorationRole, QtCore.Qt.ItemDataRole.DisplayRole])
            elif attrname.lower() == 'status':
                self.dataChanged.emit(
                    self.index(idev, 2, QtCore.QModelIndex()),
                    self.index(idev, 2, QtCore.QModelIndex()))
            else:
                self.warning(f'Attribute not handled: {evt_src.name}')
                pass

    def cleanUp(self):
        for wd in self._devices:
            wd.removeListener(self)
            del wd
        self.beginResetModel()
        self._devices = []
        self.endResetModel()

    def loadFromConfig(self):
        engine = self.getSQLEngine()
        orm_schema.Base.metadata.create_all(engine)
        with self.getSQLSession() as session:
            tangodevices = list(session.scalars(
                sqlalchemy.select(orm_schema.WatchedTangoDevice)))
            for tangodevice in tangodevices:
                self.debug(f'Loading tango device {tangodevice.device}/{tangodevice.attribute}')
                assert isinstance(
                    tangodevice, orm_schema.WatchedTangoDevice)
                if [d for d in self._devices if d.name == tangodevice.device]:
                    self.debug(
                        f'Tango device {tangodevice.device} already present')
                    continue
                self.debug('Creating WatchedDevice instance')
                wd = WatchedDevice(tangodevice.device, tangodevice.attribute)
                self._devices.append(wd)
                self.debug('Adding row')
                self._devices = sorted(self._devices, key=lambda d: d.name)
                newrow = self._devices.index(wd)
                self.beginInsertRows(QtCore.QModelIndex(), newrow, newrow)
                self.endInsertRows()
                self.debug('Adding listener')
                wd.addListener(self)
                self.debug(f'Done loading {tangodevice.device}/{tangodevice.attribute}')
            for idevice, wd in reversed(list(enumerate(self._devices))):
                if wd.name not in [td.device for td in tangodevices]:
                    wd.removeListener(self)
                    self.beginRemoveRows(
                        QtCore.QModelIndex(), idevice, idevice)
                    del self._devices[idevice]
                    self.endRemoveRows()
        self.devicesChanged.emit()

    def __contains__(self, item: str):
        return item in [d.name for d in self._devices]

    def devices(self) -> Dict[str, taurus.core.tango.TangoDevice]:
        return {wd.name: wd for wd in self._devices}

    def devicesAndAttributes(self) -> Dict[str, Tuple[taurus.core.tango.TangoDevice, str]]:
        return {wd.name: (wd, wd.attribute) for wd in self._devices}
