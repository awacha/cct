from typing import Any, List, Iterator

from taurus.external.qt import QtCore, QtGui
from taurus.core.tango import TangoDevice, TangoAttribute
from .sardanaitemmodel import SardanaItemModel
import taurus
import pint
from taurus.core.taurusbasetypes import AttrQuality, TaurusEventType

from ...core2 import orm_schema

import sqlalchemy.orm


class TangoSensor:
    name: str
    device: TangoDevice
    attribute: TangoAttribute
    sensortype: str
    quantityname: str

    def __init__(self, name: str, device: str, attribute: str, sensortype: str, quantityname: str) -> None:
        self.name = name
        self.device = taurus.Device(device)
        self.attribute = self.device.getAttribute(attribute)
        self.sensortype = sensortype
        self.quantityname = quantityname

    def addListener(self, listener):
        self.attribute.addListener(listener)

    def removeListener(self, listener):
        self.attribute.removeListener(listener)

    def value(self):
        if isinstance(self.attribute.rvalue, pint.Quantity):
            return self.attribute.rvalue.magnitude
        else:
            return float(self.attribute.rvalue)

    def units(self):
        if isinstance(self.attribute.rvalue, pint.Quantity):
            return str(self.attribute.rvalue.units)
        else:
            return ''

    def isWarning(self) -> bool:
        return (self.attribute.quality == AttrQuality.ATTR_WARNING)

    def isError(self) -> bool:
        return (self.attribute.quality == AttrQuality.ATTR_ALARM)

    def isOK(self) -> bool:
        return (
            self.attribute.quality in [
                AttrQuality.ATTR_VALID,
                AttrQuality.ATTR_CHANGING])

    def isUnknown(self) -> bool:
        return self.attribute.quality == AttrQuality.ATTR_INVALID


class Sensors(SardanaItemModel):
    _data: List[orm_schema.TangoSensor]

    def __init__(self, parent: QtCore.QObject | None = None, **kwargs):
        self._data = []
        super().__init__(parent, **kwargs)
        self.loadFromConfig()

    def rowCount(self, parent: QtCore.QModelIndex = ...) -> int:
        return len(self._data)

    def columnCount(self, parent: QtCore.QModelIndex = ...) -> int:
        return 3

    def parent(self, child: QtCore.QModelIndex) -> QtCore.QModelIndex:
        return QtCore.QModelIndex()

    def flags(self, index: QtCore.QModelIndex) -> QtCore.Qt.ItemFlag:
        return QtCore.Qt.ItemFlag.ItemIsEnabled | QtCore.Qt.ItemFlag.ItemIsSelectable \
            | QtCore.Qt.ItemFlag.ItemNeverHasChildren

    def headerData(self, section: int, orientation: QtCore.Qt.Orientation, role: int = ...) -> Any:
        if (orientation == QtCore.Qt.Orientation.Horizontal) and (role == QtCore.Qt.ItemDataRole.DisplayRole):
            return ['Sensor', 'Device', 'Reading'][section]

    def data(self, index: QtCore.QModelIndex, role: int = ...) -> Any:
        s: TangoSensor = self._data[index.row()]
        if (index.column() == 0) and (role == QtCore.Qt.ItemDataRole.DisplayRole):
            return s.name
        elif (index.column() == 1) and (role == QtCore.Qt.ItemDataRole.DisplayRole):
            return s.device.name
        elif (index.column() == 2) and (role == QtCore.Qt.ItemDataRole.DisplayRole):
            if s.isUnknown():
                return '-- Unknown --'
            return f'{s.value():.4f} {s.units()}'
        elif (index.column() == 0) and (role == QtCore.Qt.ItemDataRole.DecorationRole):
            if s.isOK():
                return self.getIcon('answer-correct', 'answer', 'emblem-ok')
            elif s.isWarning():
                return self.getIcon('data-warning', 'dialog-warning')
            elif s.isError():
                return self.getIcon('data-error', 'dialog-error')
            elif s.isUnknown():
                return self.getIcon('question', 'dialog-question')

    def index(self, row: int, column: int, parent: QtCore.QModelIndex = ...) -> QtCore.QModelIndex:
        return self.createIndex(row, column, None)

    @staticmethod
    def getIcon(*iconnames):
        for name in iconnames:
            if QtGui.QIcon.hasThemeIcon(name):
                return QtGui.QIcon.fromTheme(name)
        return QtGui.QIcon(iconnames[0])  # rely on the fallback mechanism

    def handleEvent(self, evt_src, evt_type, evt_value):
        if isinstance(evt_src, TangoAttribute):
            try:
                s = [s for s in self._data if s.attribute is evt_src][0]
            except IndexError:
                self.warning(f'{evt_src.name=}')
                return
            isensor = self._data.index(s)
            if evt_type == TaurusEventType.Change:
                self.dataChanged.emit(
                    self.index(isensor, 0, QtCore.QModelIndex()),
                    self.index(isensor, self.columnCount(QtCore.QModelIndex()), QtCore.QModelIndex()))

    def __iter__(self) -> Iterator[TangoSensor]:
        yield from self._data

    def loadFromConfig(self):
        try:
            self.beginResetModel()
            for s in self._data:
                s.removeListener(self)
            self._data = []
            with self.getSQLSession(False) as session:
                for tangosensor in session.scalars(sqlalchemy.select(orm_schema.TangoSensor)):
                    assert isinstance(tangosensor, orm_schema.TangoSensor)
                    try:
                        sensor = TangoSensor(
                            name=tangosensor.name,
                            device=tangosensor.device,
                            attribute=tangosensor.attribute,
                            sensortype=tangosensor.sensortype,
                            quantityname=tangosensor.quantityname)
                    except Exception as exc:
                        self.error(
                            f'Cannot add tango sensor {tangosensor.name}: {exc}')
                        continue
                    self._data.append(sensor)
        finally:
            self.endResetModel()
        for s in self._data:
            s.addListener(self)
