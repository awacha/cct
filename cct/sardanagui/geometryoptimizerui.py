from typing import Optional


from .models.geometry import GeometrySettings, OptimizerStore, GeometryOptimizer, Geometry
from taurus.external.qt import QtCore, QtWidgets, QtGui
from taurus.external.qt.QtCore import Slot
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg, NavigationToolbar2QT
from matplotlib.figure import Figure

from .sardanaguibase import SardanaGUIBase, UILoadable


@UILoadable()
class GeometryOptimizerUI(SardanaGUIBase):
    _optimizerstore: OptimizerStore
    _optimizer: Optional[GeometryOptimizer] = None
    _geometry: Geometry
    figure: Figure
    canvas: FigureCanvasQTAgg
    figtoolbar: NavigationToolbar2QT

    def __init__(self, parent: QtWidgets.QWidget | None = ..., **kwargs) -> None:
        self._geometry = kwargs.pop('geometry')
        self._optimizerstore = OptimizerStore()
        super().__init__(parent)

    def setupUi(self):
        self.figure = Figure()
        self.canvas = FigureCanvasQTAgg(self.figure)
        self.figtoolbar = NavigationToolbar2QT(self.canvas, self.graphTab)
        self.graphTab.setLayout(QtWidgets.QVBoxLayout())
        self.graphTab.layout().addWidget(self.canvas)
        self.graphTab.layout().addWidget(self.figtoolbar)

        self.progressBar.setVisible(False)
        sortmodel = QtCore.QSortFilterProxyModel()
        sortmodel.setSourceModel(self._optimizerstore)
        sortmodel.setSortRole(QtCore.Qt.ItemDataRole.EditRole)
        self.optimizationTreeView.setModel(sortmodel)
        self.optimizationTreeView.selectionModel().selectionChanged.connect(
            self.onOptimizationSelectionChanged)

    @Slot()
    def on_updateSetupParametersPushButton_clicked(self):
        settings: GeometrySettings = self.optimizationTreeView.model().data(
            self.optimizationTreeView.currentIndex(), role=QtCore.Qt.ItemDataRole.UserRole)
        self._geometry.updateSettings(settings)

    @Slot(QtCore.QItemSelection, QtCore.QItemSelection)
    def onOptimizationSelectionChanged(self, selected: QtCore.QItemSelection,
                                       deselected: QtCore.QItemSelection):
        self.updateSetupParametersPushButton.setEnabled(
            bool(len(self.optimizationTreeView.selectionModel().selectedRows(0))))
        self.copyToClipboardPushButton.setEnabled(
            bool(len(self.optimizationTreeView.selectionModel().selectedRows(0))))

    @Slot()
    def on_optimizePushButton_clicked(self):
        if self._optimizer is not None:
            self._optimizer.cancel()
            return
        self._optimizer = GeometryOptimizer(
            self._geometry.settings(), self._geometry.componentchoices())
        self._optimizer.finished.connect(self.onOptimizationFinished)
        self.info('Starting geometry search')
        self._optimizerstore.clear()
        self._optimizer.start(
            (self.minMaxSampleSizeDoubleSpinBox.value(),
             self.maxMaxSampleSizeDoubleSpinBox.value()),
            (self.optQMinMinDoubleSpinBox.value(),
             self.optQMinMaxDoubleSpinBox.value()),
            self.minPh1Ph2DistanceDoubleSpinBox.value(
            ), self.minPh2Ph3DistanceDoubleSpinBox.value(),
            self.maxCameraLengthDoubleSpinBox.value())
        self.progressBar.setVisible(True)
        self.optimizePushButton.setText('Stop')
        self.optimizePushButton.setIcon(
            QtGui.QIcon(QtGui.QPixmap(":/icons/stop.svg")))

    @Slot(float)
    def onOptimizationFinished(self, elapsedtime: float):
        self._optimizerstore.setOptResultList(
            self._optimizer.compatiblegeometries)
        self._optimizer.deleteLater()
        del self._optimizer
        self._optimizer = None
        self.info('Geometry search finished.')
        self.progressBar.setVisible(False)
        QtWidgets.QMessageBox.information(
            self, 'Geometry search finished',
            f'Searching for geometries finished in {elapsedtime // 60:.0f} minutes {elapsedtime % 60:.2f} seconds, '
            f'found {self._optimizerstore.rowCount()} compatible geometries.'
        )
        self.debug('Plotting...')
        self.figure.clear()
        axes = self.figure.add_subplot(1, 1, 1)
        qmin = [p.qmin for p in self._optimizerstore.iter_results()]
        intensity = [p.intensity
                     for p in self._optimizerstore.iter_results()]
        axes.plot(qmin, intensity, '.')
        axes.set_xlabel('Lowest q (nm$^{-1}$)')
        axes.set_ylabel('Intensity (\u03bcm$^4$ mm$^{-2}$)')
        axes.grid(True, which='both')
        self.debug('Redrawing...')
        self.canvas.draw_idle()
        self.optimizePushButton.setText('Find optimum')
        self.optimizePushButton.setIcon(
            QtGui.QIcon(QtGui.QPixmap(":/icons/start.svg")))
