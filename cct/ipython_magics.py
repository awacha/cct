from IPython.core.magic import Magics, magics_class, line_magic
from sardana.util.graphics import display_available
from sardana.util.whichpython import which_python_executable
from . import __main__ as cct__main__


@magics_class
class SardanaGuiMagics(Magics):
    # Various GUIs

    @line_magic
    def capillarysizer(self, line):
        """Capillary size determination utility"""
        self.open_window('CapillarySizer', line)

    @line_magic
    def scangui(self, line) -> None:
        """Scan measurement GUI"""
        self.open_window('ScanMeasurement', line)

    @line_magic
    def plotscan(self, line) -> None:
        """Plot a scan. """
        if not display_available():
            print('Graphical interface support is not available.')
            return
        verbose = 'verbose' in line.split()
        import subprocess
        doorname = self.shell.user_ns['_DOOR'].name
        fname = cct__main__.__file__
        python_executable = which_python_executable()
        args = [python_executable, fname,
                'plotscan', '--door', doorname, '--last']
        subprocess.Popen(
            args,
            stdout=subprocess.DEVNULL if not verbose else subprocess.PIPE,
            stderr=subprocess.DEVNULL if not verbose else subprocess.PIPE,
            stdin=subprocess.DEVNULL if not verbose else subprocess.PIPE)

    @line_magic
    def scanviewer(self, line) -> None:
        self.open_window('ScanViewer', line)

    @line_magic
    def masker(self, line):
        self.open_window('MaskEditor', line)

    @line_magic
    def motorstatusgui(self, line) -> None:
        self.open_window('MotorView', line)

    @line_magic
    def distcal(self, line):
        self.open_window('Calibration', line)

    @line_magic
    def beamcentering(self, line):
        self.open_window('Centering', line)

    @line_magic
    def edsample(self, line) -> None:
        """Open the sample editor GUI"""
        self.open_window('SampleEditor', line)

    @line_magic
    def flagui(self, line) -> None:
        self.open_window('FlagUI', line)

    @line_magic
    def chksamplepos(self, line) -> None:
        """Open the sample position checker GUI"""
        self.open_window('SamplePositionChecker', line)
    
    @line_magic
    def edgeo(self, line) -> None:
        """Edit the geometry parameters in a graphical window"""
        self.open_window('GeometryEditor', line)

    @line_magic
    def curmon(self, line) -> None:
        """Open a curve monitor window"""
        self.open_window('CurveMonitor', line)

    @line_magic
    def immon(self, line) -> None:
        """Open an image monitor window"""
        self.open_window('ImageMonitor', line)


    # GUIs for Tango devices

    @line_magic
    def genixgui(self, line) -> None:
        self.open_tangogui_window(self.get_devices_for_class('Genix'), line)

    @line_magic
    def upsgui(self, line) -> None:
        self.open_tangogui_window(
            self.get_devices_for_class('VoltronicWinnerUPS'), line)

    @line_magic
    def se521gui(self, line) -> None:
        self.open_tangogui_window(
            self.get_devices_for_class('SE521Thermometer'), line)

    @line_magic
    def pilatusgui(self, line) -> None:
        self.open_tangogui_window(self.get_devices_for_class('Pilatus'), line)

    @line_magic
    def haakephoenixgui(self, line) -> None:
        self.open_tangogui_window(
            self.get_devices_for_class('HaakePhoenixCirculator'), line)

    def get_devices_for_class(self, classname):
        import tango
        db = tango.Database()
        drserver = db.get_server_list(f'{classname}/*')[0]
        devclasslist = db.get_device_class_list(drserver)
        for i in range(len(devclasslist) // 2):
            devname = devclasslist[i*2]
            devclass = devclasslist[i*2+1]
            if devclass == classname:
                return devname
        else:
            raise RuntimeError(
                f'Could not find any instance of "{classname}" device server.')

    def open_tangogui_window(self, device, line) -> None:
        if not display_available():
            print('Graphical interface support is not available.')
            return
        verbose = 'verbose' in line.split()
        import subprocess
        doorname = self.shell.user_ns['_DOOR'].name
        fname = cct__main__.__file__
        python_executable = which_python_executable()
        args = [python_executable, fname, 'tangogui',
                device]
        print('Opening window, please wait.')
        subprocess.Popen(
            args,
            stdout=subprocess.DEVNULL if not verbose else subprocess.PIPE,
            stderr=subprocess.DEVNULL if not verbose else subprocess.PIPE,
            stdin=subprocess.DEVNULL if not verbose else subprocess.PIPE)

    def open_window(self, windowclass, line):
        if not display_available():
            print('Graphical interface support is not available.')
            return
        verbose = 'verbose' in line.split()
        import subprocess
        doorname = self.shell.user_ns['_DOOR'].name
        fname = cct__main__.__file__
        python_executable = which_python_executable()
        args = [python_executable, fname, 'sardanagui',
                windowclass, '--door', doorname, '--verbose' if verbose else '--quiet']
        print('Opening window, please wait.')
        subprocess.Popen(
            args,
            stdout=subprocess.DEVNULL if not verbose else subprocess.PIPE,
            stderr=subprocess.DEVNULL if not verbose else subprocess.PIPE,
            stdin=subprocess.DEVNULL if not verbose else subprocess.PIPE)


def load_ipython_extension(ipython):
    ipython.register_magics(SardanaGuiMagics)
